<?php

$CI=&get_instance();

$CI->load->model('site/site_model');

$danhmuctintuc=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,alias,tintuc,thutu,status',1,'tintuc',1,'');

$danhmuctraimb=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,thumb,alias,uid,trai,status','','trai',1,0);

if($danhmuctraimb->num_rows()>0)

{

?>

<ul class="list_cathome">

    <?php 

        foreach($danhmuctraimb->result() as $itemdanhmuctraimb)

        {

            $this->db->where('uid',$itemdanhmuctraimb->id);

            $sqldequicount=$this->db->get('tblchuyenmuc');

            if($sqldequicount->num_rows()>0)

            {

            ?>

            <li>

                <a href="<?php echo site_url($itemdanhmuctraimb->alias.'-sub'.$itemdanhmuctraimb->id.'.html'); ?>" title="<?php echo $itemdanhmuctraimb->name; ?>">

                    <img src="<?php echo linkanh().$itemdanhmuctraimb->thumb; ?>" title="<?php echo $itemdanhmuctraimb->name; ?>" alt="<?php echo $itemdanhmuctraimb->name; ?>">

                    <?php echo $itemdanhmuctraimb->name; ?><span class="i i_arrow"></span>

                </a>

            </li>

            <?php

            }

            else

            {

            ?>

            <li>

                <a href="<?php echo site_url($itemdanhmuctraimb->alias.'-c'.$itemdanhmuctraimb->id.'.html'); ?>" title="<?php echo $itemdanhmuctraimb->name; ?>">

                    <?php 

                        if($itemdanhmuctraimb->thumb!='')

                        {

                    ?>

                    <img src="<?php echo linkanh().$itemdanhmuctraimb->thumb; ?>" title="<?php echo $itemdanhmuctraimb->name; ?>" alt="<?php echo $itemdanhmuctraimb->name; ?>">

                    <?php 

                    }

                    ?>

                    <?php echo $itemdanhmuctraimb->name; ?><span class="i i_arrow"></span>

                </a>

            </li>

            <?php    

            } 

        }

        $danhmuctraimb->free_result();

    ?>   

</ul>

<?php 

}

if($danhmuctintuc->num_rows()>0)

{

    $danhmuctintuc=$danhmuctintuc->row();

?>

<div class="tinhome_mb">

    <div class="tintuchome_mb_top"><a href="<?php echo site_url($danhmuctintuc->alias.'-c'.$danhmuctintuc->id.'.html'); ?>" title="<?php echo $danhmuctintuc->name; ?>"><?php echo $danhmuctintuc->name; ?></a></div>

    <div class="tintuchome_mb_main">

        <?php 

            $this->db->where('status',1);

            //$this->db->where('catid',$danhmuctintuc->id);

            $this->db->order_by('sxtt','desc');

            $this->db->order_by('id','desc');

            $this->db->limit(3);

            $sqltintthome=$this->db->get('tblbaiviet');

            if($sqltintthome->num_rows()>0)

            {

                foreach($sqltintthome->result() as $itemtintthome)

                {

                ?>

                <div class="tintuchome_item">

                    <a href="<?php echo site_url($itemtintthome->alias.'-'.$itemtintthome->id.'.html'); ?>" class="tintuchome_item_img" title="<?php echo $itemtintthome->title; ?>"><img src="<?php echo linkanh().$itemtintthome->thumb; ?>" title="<?php echo $itemtintthome->title; ?>" alt="<?php echo $itemtintthome->title; ?>" /></a>

                    <a href="<?php echo site_url($itemtintthome->alias.'-'.$itemtintthome->id.'.html'); ?>" class="tintuchome_item_name" title="<?php echo $itemtintthome->title; ?>"><?php echo $itemtintthome->title; ?></a>

                    <div class="clear"></div>   

                </div>

                <?php

                }

                $sqltintthome->free_result();

            }

        ?>

        <div class="clear"></div>

    </div>

    <div class="clear"></div>

</div>

<?php 

}

?>