<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
$headermb_ft=$this->db->get('tblfooter')->row();
$danhmucleft=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,alias,uid,menu,status','','menu',1,0);
?>
<div class="menu">
    <div class="menu_fixed">
        <div class="menu_relative">            
            <a class="menu-trigger but_mn"></a>
            <div class="mobile_mn" id="menu">
                <ul class="menu_mobile">
                    <li><a href="<?php echo base_url(); ?>" title="<?php echo $headermb_ft->tencongty; ?>">Trang chủ</a></li>
                    <?php 
                        if($danhmucleft->num_rows()>0)
                        {
                            foreach($danhmucleft->result() as $itemdanhmucleft)
                            {
                            ?>
                            <li><a href="<?php echo site_url($itemdanhmucleft->alias.'-c'.$itemdanhmucleft->id.'.html'); ?>" title="<?php echo $itemdanhmucleft->name; ?>"><?php echo $itemdanhmucleft->name; ?></a></li>
                            <?php    
                            }
                        }
                    ?>
                    <li><a href="<?php echo site_url('lien-he.html'); ?>" rel="nofollow" title="Liên hệ">Liên hệ</a></li>                                 
                </ul>                
            </div>      
            <a class="logo" href="<?php echo base_url(); ?>"><img src="<?php echo linkanh().$headermb_ft->logo; ?>" title="<?php echo $headermb_ft->tencongty; ?>" alt="<?php echo $headermb_ft->tencongty; ?>" height="40" /></a>
        </div>
    </div>
</div>