<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K3QWQ7D');</script>
<!-- End Google Tag Manager -->

<base href="<?php echo base_url(); ?>">
<title>
<?php
        $CI = &get_instance();
        $CI->load->model('site/site_model');
        $meta = $CI->site_model->gettbl('tblmeta')->row();        
        if(isset($header_title))
        {
            echo $header_title;
        }
        else
        {
	       echo $meta->title;
        }
    ?>
</title>
<?php
        function catchuoi($chuoi,$gioihan){
        if(strlen($chuoi)<=$gioihan)
        {
            return $chuoi;
        }
        else{
        if(strpos($chuoi," ",$gioihan) > $gioihan){
            $new_gioihan=strpos($chuoi," ",$gioihan);
            $new_chuoi = substr($chuoi,0,$new_gioihan)."...";
            return $new_chuoi;
        }
        $new_chuoi = substr($chuoi,0,$gioihan)."...";
            return $new_chuoi;
        }
    }
    ?>           
<?php 
if(isset($home))
{
?>
<meta content="<?php echo base_url(); ?>" property="og:url">
<meta content="<?php echo $meta->title; ?>" property="og:title">
<meta content="<?php echo $meta->description; ?>" property="og:description">	
<meta content="<?php echo base_url(); ?>images/logo.png" property="og:image">
<?php 
}
?>	
<meta name="description" content="<?php
    if(isset($description))
    {
        echo $description;
    }
    else
    {
        echo $meta->description;
    }?>">
<meta name="keywords" content="<?php 
    if(isset($keyword))
    {
        echo $keyword;    
    }
    else
    {
        echo $meta->keywords;    
    }     
    ?>">
<meta name="author" content="daotaovieclam">
<meta name="copyright" content="daotaovieclam">
<meta name="robots" content="index,follow">
<link rel="stylesheet" href="css/style.min.css" type="text/css">
<link href="daotao.ico" rel="shortcut icon" type="image/x-icon"/>
<?php 
if(isset($home))
{
?>
<link rel="canonical" href="https://daotaovieclam.edu.vn">
<?php    
}
if(isset($contact))
{
?>
<link rel="canonical" href="https://daotaovieclam.edu.vn/lien-he.html">
<?php    
}
if(isset($dm))
{
    $this->db->where('id',$dm);
    $this->db->select('id,alias');
    $sqldmcano=$this->db->get('tblchuyenmuc')->row();
    $linkdmcano=site_url($sqldmcano->alias.'-c'.$sqldmcano->id.'.html');
    $linkdmcano1=str_replace('m.daotaovieclam.edu.vn','daotaovieclam.edu.vn',$linkdmcano);
?>
<link rel="canonical" href="<?php echo $linkdmcano1; ?>">
<?php
}
if(isset($chitiet))
{
    $this->db->where('id',$chitiet);
    $this->db->select('id,alias');
    $sqlbvctcano=$this->db->get('tblbaiviet')->row();
    $linkbvctcano=site_url($sqlbvctcano->alias.'-'.$sqlbvctcano->id.'.html');
    $linkbvctcano1=str_replace('m.daotaovieclam.edu.vn','daotaovieclam.edu.vn',$linkbvctcano);
?>
<link rel="canonical" href="<?php echo $linkbvctcano1; ?>">
<?php   
}
if(isset($dangky))
{
?>
<link rel="canonical" href="https://daotaovieclam.edu.vn/dang-ky-ngay.html">
<?php    
}
?>
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/mobile.js"></script>
<script type="text/javascript" src="js/jquery.jpanelmenu.min.js"></script>
<script type="text/javascript" src="js/jRespond.js"></script>
<meta property="fb:app_id" content="1628978170653064" />
<meta property="fb:admins" content="100003946699319">
<?php
if(isset($chitiet_code_head)){
    $this->db->where('id',$chitiet_code_head);
    $code_header_artice = $this->db->get('tblbaiviet');
    if($code_header_artice->num_rows()>0){
        echo $code_header_artice->row()->code_head;   
    }
}
?>
<meta name="google-site-verification" content="7ExHMS_BFaWMmrw0QT0rNqwmNT_lwZGXU0JcO-c2yPI" />
</head>

<body id="ipboard_body">
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
FB.init({
appId : '980470102018825',
status : true, // check login status
cookie : true, // enable cookies to allow the server to access the session
xfbml : true // parse XFBML
});
};
(function() {
var e = document.createElement('script');
e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
e.async = true;
document.getElementById('fb-root').appendChild(e);
}());
</script>


    <div id="rbPage" class="box_content">
    <div class="header">
        <?php $this->load->view('includes/header') ?>
    </div><!--End #header-->
    <div id="mainDiv">
        <div id="content">
            <div class="tools">
                <?php $this->load->view('includes/search') ?>
            </div>
            <div class="clear"></div>            
            <div class="container">
                <?php $this->load->view($content) ?>
            </div>
            </div>
            <div id="m-footer">
                <?php $this->load->view('includes/footer') ?>  
            </div><!--End #footer-->
        </div>
    </div>

</body>
</html>