<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'site';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['lien-he.html']='site/lienhe';
$route['dang-ky-ngay.html']='site/dangkyngay';
$route['scaffolding_trigger'] = "";
$route['deal-moi.html']='site/deal';
$route['deal-moi/([0-9\-]+)']='site/deal/$1';
$route['nhap-email-giam-gia.html']='site/emailgiamgia';
$route['trang-ca-nhan.html']='site/panel';
$route['danh-sach-tin-dang.html']='site/listtindang';
$route['doi-mat-khau.html']='site/doimatkhau';
$route['xu-ly-doi-mat-khau.html']='site/dodoimatkhau';
$route['quen-mat-khau.html']='site/quenmatkhau';
$route['xu-ly-quen-mat-khau.html']='site/doquenmatkhau';
$route['dia-chi-nhan-hang.html']='site/diachinhanhang';     
$route['xu-ly-dia-chi-nhan-hang.html']='site/dodiachich';
$route['dang-tin.html']='site/dangtin';
$route['xu-ly-dang-tin.html']='site/dodangtin';
$route['sua-tin-dang.html']='site/edittindang';
$route['sua-tai-khoan.html']='site/dotaikhoan';
$route['dang-ky.html']='site/dangky';
$route['dang-nhap.html']='site/dangnhap';
$route['thoat.html']='site/thoat';
$route['theo-doi-don-hang.html']='site/theodoidonhang';
//Duong dan Site	
$route['(:any)-t(:num).html'] = 'site/tinhmore/$2';

$route['(:any)-(:num).html'] = 'site/sanphambyid/$2';
$route['(:any)-c(:num)(.html)?(/:num)?'] = 'site/show_chuyenmuc/$2/$1/$3';
$route['(:any)-sub(:num)(.html)?(/:num)?'] = 'site/show_chuyenmucsub/$2/$1/$3';
$route['tags/(:any)'] = 'site/show_tags/$1';
$route['^ket-qua-tim-kiem.html'] = 'site/search';
$route['^ket-qua-tim-kiem/(:any)(/:num)?'] = 'site/search/$1/$2';

$route['event/(:any)-(:num)'] = 'site/listsukien/$2/$1';
$route['result-event.html'] = 'site/listsukien/';
$route['list-event.html'] = 'site/listevent/';
