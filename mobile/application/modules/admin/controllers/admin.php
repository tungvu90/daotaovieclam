<?php 
class Admin extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();  
		$this->load->helper('locdau');
		//$this->load->helper('status');
		$this->load->helper('images');// Xu ly anh
		$this->load->library('pagination');
		$this->load->helper('simple_html_dom');
		$this->load->model('admin/admin_model');			
	}   
	public function index()
    {  
		$this->checklogin();
        $data['content']='includes/content';
        $this->load->view('template',$data);
		unset($_SESSION['txt_search']);
		unset($_SESSION['search_catid']);	
		unset($_SESSION['search_user']);	
		unset($_SESSION['search_status']);	
    } 
    public function information()
    {
        $this->checklogin();
        $data['content']='information';
        $this->load->view('template',$data);
    }
    public function reset_password($id)
    {
        $this->checklogin();
        $data['content']='reset_password';
        $data['id']=$id;
        $this->load->view('template',$data);
    }
    public function change_password()
    {
        $this->checklogin();
        $data['content']='change_password';
        $this->load->view('template',$data);
    }
    public function update_change_password()
    {
        $this->checklogin();
        $this->db->where('status',1);
        $this->db->where('id',$_SESSION['author']);
        $admin=$this->db->get('tbladmin1');	
        $id=$admin->row()->id;
        $password = $_POST['password'];
        $password_new = $_POST['password_new'];
        $this->db->where('matkhau',md5($password));
        $check_pass = $this->db->get('tbladmin1');
        if($check_pass->num_rows() > 0){
            $data = [
                'matkhau'=>md5($password_new)
            ];
            $this->db->where('id',$id);
            $this->db->update('tbladmin1',$data);
            echo json_encode([
                'status' => 200,
                'success' => 'Đổi mật khẩu thành công'
            ]);
        }else{
            echo json_encode([
                'status' => 400,
                'error' => 'Mật khẩu cũ không đúng'
            ]);
        }
    }
    public function update_password()
    {
        $this->checklogin();
        $id = $_POST['id'];
        $password = $_POST['password'];
        $data = [
            'matkhau'=>md5($password)
        ];
        $this->db->where('id',$id);
        $this->db->update('tbladmin1',$data);
        echo json_encode([
            'success' => 'Reset mật khẩu thành công'
        ]);
    }
    public function update_profile()
    {
        $this->checklogin();
        $this->db->where('status',1);
        $this->db->where('id',$_SESSION['author']);
        $admin=$this->db->get('tbladmin1');	
        $id=$admin->row()->id;
        $hoten = $_POST['hoten'];
        $diachi = $_POST['diachi'];
        $dienthoai = $_POST['dienthoai'];
        $email = $_POST['email'];
        $data = [
            'hoten' => $hoten,
            'diachi' => $diachi,
            'dienthoai' => $dienthoai,
            'email' => $email
        ];
        $this->db->where('id',$id);
        $this->db->update('tbladmin1',$data);
        echo json_encode([
            'success' => 'Cập nhật thành công'
        ]);
    }
    public function do_edit_image_title()
    {
        $id = $_POST['id'];
        $data['id'] = $id;
        $title = $_POST['title'];
        $data_update = [
            'title' => $title
        ];
        $this->db->where('id',$id);
        $this->db->update('tblalbum_images',$data_update);
        $html = $this->load->view('load_image_title',$data);
        echo $html;
    }
	public function tblget_data()
    {		
		$data['query'] = $this->admin_model->gettbl('site','');
        $data['content']='tbldata'; 
        $this->load->view('template',$data);  
    }
	public function frmget_data()
    {			
        $data['content']='frmdata'; 
        $this->load->view('template',$data);  
    }
	public function editfrm_data($id)
    {		
		if($id){
			$data['id']=$id;
		}		
        $data['content']='frmdata'; 
        $this->load->view('template',$data);  
    }
	public function delfrm_data($id)
    {		
		if($id){			
			$this->admin_model->del_tbl('site',$id); 
		}		
		$data['query'] = $this->admin_model->gettbl('site','');
        $data['content']='tbldata'; 
        $this->load->view('template',$data);  
    }
	public function copyfrm_data($id)
    {		
		if($id){			
			$data_base=$this->admin_model->gettbl('site',$id)->row();			
			$data=array(			
				'name'  				=>  $data_base->name,					
				'host'  				=>  $data_base->host,					
				'url'  					=>  $data_base->url,					
				'extra'  				=>  $data_base->extra,					
				'table_name'  			=>  $data_base->table_name,					
				'image_size'  			=>  $data_base->image_size,					
				'image_size_small'  	=>  $data_base->image_size_small,					
				'image_size_small_01'  	=>  $data_base->image_size_small_01,					
				'tags'  				=>  $data_base->tags,					
				'image_dir_base'  		=>  $data_base->image_dir_base,					
				'image_pattern_base'  	=>  $data_base->image_pattern_base,					
				'pattern_bound'  		=>  $data_base->pattern_bound,									
				'category_id'  			=>  $data_base->category_id,					
				'page_num'    			=>  $data_base->page_num,
				'value_page'    		=>  $data_base->value_page,
				'auto'    				=>  $data_base->auto,
				'page_num'    			=>  $data_base->page_num,
				'image_dir'  			=>  $data_base->image_dir
			); 			
			$this->admin_model->add_tbl('site',$data,'');    
			$sql_dataid="select id from site order by id desc limit 1";
			$data_baseid=$this->db->query($sql_dataid)->row()->id;
			
			$sql_data="select * from site_structure where site_id=$id";
				$data_base1=$this->db->query($sql_data);
				if($data_base1->num_rows()>0){
					foreach($data_base1->result() as $data_bas){
						$data1=array(			
							'site_id'  			=>  $data_baseid,					
							'field_name'  		=>  $data_bas->field_name,					
							'extra'  			=>  $data_bas->extra,	
							'element_delete'  	=>  $data_bas->element_delete
						);
						$this->admin_model->add_tbl('site_structure',$data1,'');
					}
				}
		}		
		$data['query'] = $this->admin_model->gettbl('site','');
        $data['content']='tbldata'; 
        $this->load->view('template',$data);  
    }
	public function frmget_data_item()
    {		
        $data['content']='frmdata_item'; 
        $this->load->view('template',$data);  
    }
	public function tblcache()
    {		
		$this->load->helper('status');
		$data['query']= $this->admin_model->gettbl('news',''); 
        $data['content']='tblcache'; 
        $this->load->view('template',$data);  
    }
	public function edit_cache($id)
    {
		$this->checklogin();
        $data['id']=$id;
        $data['content']='frmcache';
        $this->load->view('template',$data);    
    }
	public function edit_frmget_data_item($id)
    {		
		if($id){
			$data['id']=$id;
		}
        $data['content']='frmdata_item'; 
        $this->load->view('template',$data);  
    }
	public function do_edit_cache()
    {		
		$id=$this->input->post('id');
		$data=array(			
			'title'  				=>  $this->input->post('title'),					
			'sapo'  				=>  $this->input->post('sapo'),					
			'fulltext'  			=>  $this->input->post('fulltext'),					
			'catid'  				=>  $this->input->post('catid')									
		); 
		$this->admin_model->add_tbl('news',$data,$id);	
		redirect('admin/tblcache');		   
    }	
	public function add_data()
    {
		$id = $this->input->post('id');					  		
		
		/*-----------------------------*/
		$data=array(			
				'name'  				=>  $this->input->post('name'),					
				'host'  				=>  $this->input->post('host'),					
				'url'  					=>  $this->input->post('url'),					
				'extra'  				=>  htmlspecialchars($this->input->post('extra')),					
				'table_name'  			=>  $this->input->post('table_name'),					
				'image_size'  			=>  $this->input->post('image_size'),					
				'image_size_small'  	=>  $this->input->post('image_size_small'),					
				'image_size_small_01'  	=>  $this->input->post('image_size_small_01'),					
				'image_dir_base'  		=>  $this->input->post('image_dir_base'),					
				'image_pattern_base'  	=>  $this->input->post('image_pattern_base'),					
				'pattern_bound'  		=>  $this->input->post('pattern_bound'),									
				'tags'  				=>  $this->input->post('tags'),									
				'category_id'  			=>  $this->input->post('category_id'),					
				'page_num'    			=>  $this->input->post('page_num'),
				'value_page'    		=>  $this->input->post('value_page'),
				'auto'    				=>  $this->input->post('auto'),
				'page_num'    			=>  $this->input->post('page_num'),
				'limit'    				=>  $this->input->post('limit'),
				'image_dir'  			=>  $this->input->post('image_dir')
			); 
		$this->admin_model->add_tbl('site',$data,$id);    
		redirect('admin/tblget_data');	
    }
	public function add_data_item()
    {
		$id = $this->input->post('id');		
		$catid = $this->input->post('site_id');
		$field=(isset($_REQUEST['field']) and $_REQUEST['field'])?$_REQUEST['field']:false;
		if($field){
			foreach($field as $key=>$value){
				$data=array(
					'field_name'=>$key,
					'extra'=>$value['extra'],
					'element_delete'=>$value['element_delete'],
					'site_id'=>$catid
				);
				$this->admin_model->add_tbl1('site_structure',$data,$catid);
			}
		}				
		redirect('admin/tblget_data');	
    }
	public function del_data()
	{     
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];			
				/*$sql="select * from news where id=".$del_id;				
				$query=$this->db->query($sql)->row();					
				$this->db->like('title',$query->title);
				$check=$this->db->get('tblbaiviet');								
				if($check->num_rows==0){
					$regex = "/\<img.+src\s*=\s*\"([^\"]*)\"[^\>]*\>/Us";
					preg_match_all($regex, $query->fulltext, $matches);						
					for($j=0;$j<count($matches);$j++){
						$unimg=explode('http',$matches[1][$j]);					
						if(count($unimg)==1 and file_exists($matches[1][$j])){								
							unlink($matches[1][$j]);
						}
					}
					if($query->thumb!='' and file_exists($query->thumb)){
						unlink($query->thumb);//xoa base img					
					}				
				}*/
				$result = $this->admin_model->del_tbl('news',$del_id);    
			}
            if($result)
            {
                redirect('admin/tblcache');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/tblcache');
        }
	}
	public function loading_data()
	{     
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];				
				$result = $this->get_data($del_id);    
			}
            if($result)
            {
                //redirect('admin/tblget_data');        				
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/tblget_data');
        }
		$this->load->helper('status');		
		$data['query']= $this->admin_model->gettbl('news',''); 
		$data['content']='tblcache'; 
		$this->load->view('template',$data); 
	}
	public function insert_data()
    {			
		$this->admin_model->insert_data_query();
		$data['query']= $this->admin_model->gettbl('news',''); 
        $data['content']='tblcache'; 
        $this->load->view('template',$data);  
    }
	/////////////////////
    
	/*Sukien*/
	public function sukien()
    {
		$this->checklogin();	
		$this->checkrole();	
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
            $per_page=15;
    		if(is_numeric($start_row))
    		{
    			$start_row=$start_row;
    		}
    		else
    		{
    			$start_row=0;
    		}
            $query=$this->admin_model->gettbl('tblsukien','');			
    		$total_rows = $query->num_rows();
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/sukien/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'sau';
    		$config['prev_link'] = 'trước';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'đầu';
    		$config['last_link'] = 'cuối';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_limited('tblsukien',$start_row,$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='sukien';			
    		$this->load->view('template',$data);    	   
    }
	public function frmsukien()
    {		
        $data['content']='frmsukien'; 
        $this->load->view('template',$data);  
    }
	public function add_sukien()
    {
		$this->checklogin();
		$this->checkrole();
		$id = $this->input->post('id');	
		$alias=$this->input->post('alias');
		if($alias=='')
        {
            $alias=vn_str_filter($this->input->post('name'));
        } 
		$data=array(								
				'name'  	=>  $this->input->post('name'),					
				'alias'  	=>  $alias,					
				'showed'  	=>  $this->input->post('showed'),					
				'checked'  	=>  $this->input->post('checked'),					
				'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tblsukien',$data,$id);    
		redirect('admin/sukien');	
    }
	
	public function edit_sukien($id)
    {
		$this->checklogin();
		$this->checkrole();
        $data['id']=$id;
        $data['content']='frmsukien';
        $this->load->view('template',$data);    
    }
	public function del_sukien()
	{     
		$this->checklogin();
		$this->checkrole();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$sql="select image from tblsukien where id=".$del_id;			
				$result = $this->admin_model->del_tbl('tblsukien',$del_id);    
			}
            if($result)
            {
                redirect('admin/sukien');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/sukien');
        }
	}
	public function checksukien()
	{
		$this->checklogin();
		$this->checkrole();
		$action=$_POST['active'];
        $id=$_POST['id'];
        $this->admin_model->checkstatus('tblsukien',$action,$id);		
	}
	/////////////////
    public function lienhe()
    {
		$this->checklogin();
		$this->checkrole();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
            $per_page=15;
    		if(is_numeric($start_row))
    		{
    			$start_row=$start_row;
    		}
    		else
    		{
    			$start_row=0;
    		}
            $query=$this->admin_model->gettbl('tbllienhe','');			
    		$total_rows = $query->num_rows();
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/lienhe/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Next';
    		$config['prev_link'] = 'Prev';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'First';
    		$config['last_link'] = 'Last';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_limited('tbllienhe',$start_row,$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='lienhe';			
    		$this->load->view('template',$data);    	   
    }
    public function support()
    {
		$this->checklogin();
		$this->checkrole();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
            $per_page=15;
    		if(is_numeric($start_row))
    		{
    			$start_row=$start_row;
    		}
    		else
    		{
    			$start_row=0;
    		}
            $query=$this->admin_model->gettbl('tblsupport','');			
    		$total_rows = $query->num_rows();
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/support/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Next';
    		$config['prev_link'] = 'Prev';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'First';
    		$config['last_link'] = 'Last';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_limited('tblsupport',$start_row,$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='support';			
    		$this->load->view('template',$data);    	   
    }
    public function frmsupport()
    {		
		$this->checklogin();
		$this->checkrole();
        $data['content']='frmsupport'; 
        $this->load->view('template',$data);  
    }
    public function add_support()
    {
        $this->checklogin();
		$this->checkrole();
		$id = $this->input->post('id');
		if(!empty($this->input->post('thutu'))){
		    $thutu = $this->input->post('thutu');;
		}else{
		    $thutu = 0;
		}
        $data=array(
                'title'     =>$this->input->post('title'),	              		
				'dienthoai'  	=>  $this->input->post('dienthoai'),					
				'yahoo'  	=>  $this->input->post('yahoo'),	
                'thutu'  =>  $thutu,				
				'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tblsupport',$data,$id);    
		redirect('admin/support');	    
    }
    public function edit_support($id)
    {
        $this->checklogin();
		$this->checkrole();
        $data['id']=$id;
        $data['content']='frmsupport';
        $this->load->view('template',$data);    
    }
	public function slider()
    {
		$this->checklogin();
		$this->checkrole();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
            $per_page=15;
    		if(is_numeric($start_row))
    		{
    			$start_row=$start_row;
    		}
    		else
    		{
    			$start_row=0;
    		}
            $query=$this->admin_model->gettbl('tblslider','');			
    		$total_rows = $query->num_rows();
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/slider/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Next';
    		$config['prev_link'] = 'Prev';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'First';
    		$config['last_link'] = 'Last';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_limited('tblslider',$start_row,$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='slider';			
    		$this->load->view('template',$data);    	   
    }
	public function frmslider()
    {		
		$this->checklogin();
		$this->checkrole();
        $data['content']='frmslider'; 
        $this->load->view('template',$data);  
    }
	public function add_slider()
    {
		$this->checklogin();
		$this->checkrole();
		$id = $this->input->post('id');					  		
		//Xử lý ảnh	
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);
		
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0755, TRUE);
		}
		
		if ( ! $this->upload->do_upload('image'))
		{
			if($id==''){
				$image='';
			}
			else{
				$image=$this->input->post('image');
			}
		}
		else
		{
			$image_data = $this->upload->data();
			$image='upload/'.$image_data['file_name'];                
			$duongdan=$image_data['file_name'];	
		}		
		/*-----------------------------*/
		$data=array(
                'title'     =>$this->input->post('title'),	              		
				'image'  	=>  $image,					
				'link'  	=>  $this->input->post('link'),	
                'thutu'  =>  $this->input->post('ordernum'),				
				'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tblslider',$data,$id);    
		redirect('admin/slider');	
    }
	
	public function edit_slider($id)
    {
		$this->checklogin();
		$this->checkrole();
        $data['id']=$id;
        $data['content']='frmslider';
        $this->load->view('template',$data);    
    }
	public function del_slider()
	{     
		$this->checklogin();
		$this->checkrole();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];				
				$result = $this->admin_model->del_tbl('tblslider',$del_id);    
			}
            if($result)
            {
                redirect('admin/slider');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/slider');
        }
	}
    public function del_support()
	{     
		$this->checklogin();
		$this->checkrole();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];				
				$result = $this->admin_model->del_tbl('tblsupport',$del_id);    
			}
            if($result)
            {
                redirect('admin/support');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/support');
        }
	}
    public function edit_lienhe($id)
    {
        $this->checklogin();
		$this->checkrole();
        $data['id']=$id;
        $data['content']='frmlienhe';
        $this->load->view('template',$data);    
    }
    public function add_lienhe()
    {
		$this->checklogin();
		$this->checkrole();
        $date_ngayct=explode('/',$this->input->post('ngaylienhe'));        
        $ngaylh=$date_ngayct[2].'-'.$date_ngayct[1].'-'.$date_ngayct[0];
        //var_dump();
		$id = $this->input->post('id');					  				
		/*-----------------------------*/
		$data=array(
                'hoten'     =>$this->input->post('hoten'),	              		
				'diachi'  	=>$this->input->post('diachi'),					
				'dienthoai'  	=>  $this->input->post('dienthoai'),	
                'email'  =>  $this->input->post('email'),
                'noidung'   =>$this->input->post('noidung'),
                'ngaylienhe' =>$ngaylh,				
				'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tbllienhe',$data,$id);    
		redirect('admin/lienhe');	
    }
    public function del_lienhe()
	{     
		$this->checklogin();
		$this->checkrole();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];				
				$result = $this->admin_model->del_tbl('tbllienhe',$del_id);    
			}
            if($result)
            {
                redirect('admin/lienhe');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/lienhe');
        }
	}
	public function checkslider()
	{
		$this->checklogin();
		$this->checkrole();
		$action=$_POST['active'];
        $id=$_POST['id'];
        $this->admin_model->checkstatus('tblslider',$action,$id);		
	}
    public function album()
    {
        $this->checklogin();
		$this->checkrole();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
        $per_page=15;
		if(is_numeric($start_row))
		{
			$start_row=$start_row;
		}
		else
		{
			$start_row=0;
		}
        $query=$this->admin_model->gettbl('tblalbum','');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/album/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_limited('tblalbum',$start_row,$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='album';			
		$this->load->view('template',$data);     
    }
    public function dangkyhoc()
    {
        $this->checklogin();
		$this->checkrole();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
        $per_page=15;
		if(is_numeric($start_row))
        {
			$_SESSION['start_row']=$start_row;					
		}				    		
		else
		{
			$_SESSION['start_row']=0;				
		}	
        $query=$this->admin_model->gettbl('tbldangkyhoc','');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/dangkyhoc/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_limited('tbldangkyhoc',$_SESSION['start_row'],$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='dangkyhoc';			
		$this->load->view('template',$data);      
    }
    public function quangcao()
    {
        $this->checklogin();
		$this->checkrole();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
        $per_page=15;
		if(is_numeric($start_row))
        {
			$_SESSION['start_row']=$start_row;					
		}				    		
		else
		{
			$_SESSION['start_row']=0;				
		}	
        $query=$this->admin_model->gettbl('tblquangcao','');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/quangcao/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_limited('tblquangcao',$_SESSION['start_row'],$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='quangcao';			
		$this->load->view('template',$data);        
    }
    public function link()
    {
        $this->checklogin();
		$this->checkrole();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
        $per_page=15;
		if(is_numeric($start_row))
        {
			$_SESSION['start_row']=$start_row;					
		}				    		
		else
		{
			$_SESSION['start_row']=0;				
		}	
        $query=$this->admin_model->gettbl('tbllink','');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/link/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_limited('tbllink',$_SESSION['start_row'],$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='link';			
		$this->load->view('template',$data);    
    }
    public function frmquangcao()
    {
        $this->checklogin();
		$this->checkrole();
        $data['content']='frmquangcao'; 
        $this->load->view('template',$data);    
    }
    public function add_quangcao()
    {
		$this->checklogin();
		$this->checkrole();
		$id = $this->input->post('id');					  		
		//Xử lý ảnh	
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);
		
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0755, TRUE);
		}
		
		if ( ! $this->upload->do_upload('image'))
		{
			if($id==''){
				$image='';
			}
			else{
				$image=$this->input->post('image');
			}
		}
		else
		{
			$image_data = $this->upload->data();
			$image='upload/'.$image_data['file_name'];                
			$duongdan=$image_data['file_name'];	
		}		
		/*-----------------------------*/
		if(!empty($this->input->post('thutu'))){
		    $thutu = $this->input->post('thutu');
		}else{
		    $thutu = 0;
		}
		$data=array(
                'title'     =>$this->input->post('title'),	              		
				'anh'  	=>  $image,					
				'link'  	=>  $this->input->post('link'),	
                'thutu'  =>  $thutu,				
				'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tblquangcao',$data,$id); 
        if(isset($_SESSION['start_row'])==0)
        {
            redirect('admin/quangcao/');
        }
		else
        {
			redirect('admin/quangcao/'.$_SESSION['start_row']);
		}  		
    }
    public function edit_quangcao($id)
    {
		$this->checklogin();
		$this->checkrole();
        $data['id']=$id;
        $data['content']='frmquangcao';
        $this->load->view('template',$data);    
    }
    public function del_quangcao()
	{     
		$this->checklogin();
		$this->checkrole();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$sql="select anh from tblquangcao where id=".$del_id;
				$query=$this->db->query($sql);
                if($query->num_rows()>0)
                {
                    $query=$query->row();
				    unlink($query->anh);//xoa file
                }
				$result = $this->admin_model->del_tbl('tblquangcao',$del_id);    
			}
            if($result)
            {
                if($_SESSION['start_row']==0)
                {
                    redirect('admin/quangcao/');
                }
				else{
					redirect('admin/quangcao/'.$_SESSION['start_row']);
				}                       
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/quangcao');
        }
	}
    public function doitac()
    {
		$this->checklogin();
		$this->checkrole();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
            $per_page=15;
    		if(is_numeric($start_row))
    		{
    			$start_row=$start_row;
    		}
    		else
    		{
    			$start_row=0;
    		}
            $query=$this->admin_model->gettbl('tbldoitac','');			
    		$total_rows = $query->num_rows();
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/doitac/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Next';
    		$config['prev_link'] = 'Prev';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'First';
    		$config['last_link'] = 'Last';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_limited('tbldoitac',$start_row,$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='doitac';			
    		$this->load->view('template',$data);    	   
    }
    public function frmdoitac()
    {		
		$this->checklogin();
		$this->checkrole();
        $data['content']='frmdoitac'; 
        $this->load->view('template',$data);  
    }
    public function add_dangkyhoc()
    {
        $this->checklogin();
		$this->checkrole();
		$id = $this->input->post('id');
        $data=array(
                'hoten'     =>$this->input->post('hoten'),	              		
				'diachi'  	=>  $this->input->post('diachi'),					
				'dienthoai'  	=>  $this->input->post('dienthoai'),
                'email'  	=>  $this->input->post('email'),
                'soluong'  	=>  $this->input->post('soluong'),
                'monhoc'  	=>  $this->input->post('monhoc'),
                'thoigian'  	=>  $this->input->post('thoigian'),
                'noidung'   =>  $this->input->post('noidungkhac'),	
                'thutu'  =>  $this->input->post('ordernum'),				
				'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tbldangkyhoc',$data,$id);
        if(isset($_SESSION['start_row'])==0)
        {
            redirect('admin/dangkyhoc/');
        }
		else
        {
			redirect('admin/dangkyhoc/'.$_SESSION['start_row']);
		}  	    		  
    }
    public function add_doitac()
    {
		$this->checklogin();
		$this->checkrole();
		$id = $this->input->post('id');					  		
		//Xử lý ảnh	
		$config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);
		
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0755, TRUE);
		}
		
		if ( ! $this->upload->do_upload('image'))
		{
			if($id==''){
				$image='';
			}
			else{
				$image=$this->input->post('image');
			}
		}
		else
		{
			$image_data = $this->upload->data();
			$image='upload/'.$image_data['file_name'];                
			$duongdan=$image_data['file_name'];	
		}	
		if(!empty($this->input->post('ordernum'))){
		    $thutu = $this->input->post('ordernum');;
		}else{
		    $thutu = 0;
		}
		/*-----------------------------*/
		$data=array(
                'title'     =>$this->input->post('title'),	              		
				'image'  	=>  $image,					
				'link'  	=>  $this->input->post('link'),	
                'thutu'  =>  $thutu,				
				'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tbldoitac',$data,$id);    
		redirect('admin/doitac');	
    }
    public function edit_doitac($id)
    {
		$this->checklogin();
		$this->checkrole();
        $data['id']=$id;
        $data['content']='frmdoitac';
        $this->load->view('template',$data);    
    }
    public function edit_dangkyhoc($id)
    {
        $this->checklogin();
		$this->checkrole();
        $data['id']=$id;
        $data['content']='frmdangkyhoc';
        $this->load->view('template',$data);    
    }
	public function del_doitac()
	{     
		$this->checklogin();
		$this->checkrole();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$sql="select image from tbldoitac where id=".$del_id;
				$query=$this->db->query($sql)->row();
				unlink($query->image);//xoa file
				$result = $this->admin_model->del_tbl('tbldoitac',$del_id);    
			}
            if($result)
            {
                redirect('admin/doitac');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/doitac');
        }
	}
	public function del_album()
	{     
		$this->checklogin();
		$this->checkrole();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$result = $this->admin_model->del_tbl('tblalbum',$del_id);    
			}
            if($result)
            {
                redirect('admin/album');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/album');
        }
	}
    public function del_link()
    {
           
		$this->checklogin();
		$this->checkrole();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$sql="select image from tbllink where id=".$del_id;
				$query=$this->db->query($sql);
                if($query->num_rows()>0)
                {
                    $query=$query->row();
				    unlink($query->image);//xoa file
                }
				$result = $this->admin_model->del_tbl('tbllink',$del_id);    
			}
            if($result)
            {
                if($_SESSION['start_row']==0)
                {
                    redirect('admin/link/');
                }
				else{
					redirect('admin/link/'.$_SESSION['start_row']);
				}                       
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/quangcao');
        }    
    }
    public function del_dangkyhoc()
    {
        $this->checklogin();
		$this->checkrole();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];				
				$result = $this->admin_model->del_tbl('tbldangkyhoc',$del_id);    
			}
            if($result)
            {
                redirect('admin/dangkyhoc');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/dangkyhoc');
        }     
    }
    public function frmalbum()
    {
        $this->checklogin();
		$this->checkrole();
        $data['content']='frmalbum'; 
        $this->load->view('template',$data);    
    }
    public function add_album()
    {
        $uploadDir = "upload/"; 
        $tmpFile = $_FILES['file']['tmp_name'];
        $filename = $uploadDir.'/'.time().'-'. $_FILES['file']['name'];
        $album=$this->input->post('title'); 
        if(!empty($this->input->post('thutu'))){
    	    $thutu = $this->input->post('thutu');;
    	}else{
    	    $thutu = 0;
    	}
    	$id =0;
        $this->db->limit(1);
        $this->db->where('album',$album);
    	$query = $this->db->get('tblalbum');
    	if($query->num_rows()>0){
    	    $id = $query->row()->id;
    	}else{
    	    $data=array(
                'album' =>$album,
                'thutu' =>$thutu,
                'status'    =>$this->input->post('status'),
            ); 
    	    $this->admin_model->add_tbl_image('tblalbum',$data,0);
    	    $id = $this->db->insert_id();
    	}
        if(move_uploaded_file($tmpFile,$filename)){ 
            $this->db->insert('tblalbum_images',['album_id'=>$id,'images'=>$filename]);
        }
        redirect('admin/album');						     
    }
    public function do_edit_album()
    {
        $id = $_POST['id'];
        $title = $_POST['title'];
        $ordernum = $_POST['ordernum'];
        $status = $_POST['status'];
        $data = [
            'album' => $title,
            'thutu' => $ordernum,
            'status' => $status
        ];
        $this->db->where('id',$id);
        $this->db->update('tblalbum',$data);
    }
    public function delete_image()
    {
        $id = $_POST['id'];
        $sql="select images from tblalbum_images where id=".$id;
		$query=$this->db->query($sql);
		if($query->num_rows() > 0){
    		if(file_exists($query->row()->images)){
    			unlink($query->row()->images);				
    		}
		}
        $this->db->where('id',$id);
        $this->db->delete('tblalbum_images');
    }
    public function edit_album($id)
    {
        $this->checklogin();
		$this->checkrole();
        $data['id']=$id;
        $data['content']='frmalbum';
        $this->load->view('template',$data);    
    }
	//Banner
	public function banner()
    {
		$this->checklogin();
		$this->checkrole1();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
            $per_page=15;
    		if(is_numeric($start_row))
    		{
    			$start_row=$start_row;
    		}
    		else
    		{
    			$start_row=0;
    		}
            $query=$this->admin_model->gettbl('tblbanner','');			
    		$total_rows = $query->num_rows();
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/banner/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Trước';
    		$config['prev_link'] = 'Sau';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'Đầu';
    		$config['last_link'] = 'Cuối';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_limited('tblbanner',$start_row,$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='banner';			
    		$this->load->view('template',$data);    	   
    }
	public function frmbanner()
    {		
		$this->checklogin();
		$this->checkrole1();
        $data['content']='frmbanner'; 
        $this->load->view('template',$data);  
    }
	public function add_banner()
    {
		$this->checklogin();
		$this->checkrole1();
		$id = $this->input->post('id');					  		
		//Xử lý ảnh	
		$config['upload_path'] = './upload/banner/';
		$config['allowed_types'] = 'gif|jpg|png|swf|jpeg';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);
		
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0755, TRUE);
		}
		
		if ( ! $this->upload->do_upload('file'))
		{
			if($id==''){
				$file='';
			}
			else{
				$file=$this->input->post('file');
			}
		}
		else
		{
			$image_data = $this->upload->data();
			$file='upload/banner/'.$image_data['file_name'];                			
		}		
		/*-----------------------------*/
		$data=array(			
				'name'  	=>  $this->input->post('name'),	
                'tinh'      =>  $this->input->post('tinhthanh'),				
				'link'  	=>  $this->input->post('link'),					
				'file'  	=>  $file,						
				'vitri'  	=>  $this->input->post('vitri'),										
				'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tblbanner',$data,$id);    
		redirect('admin/banner');	
    }
	
	public function edit_banner($id)
    {
		$this->checklogin();
		$this->checkrole1();
        $data['id']=$id;
        $data['content']='frmbanner';
        $this->load->view('template',$data);    
    }
	public function del_banner()
	{     
		$this->checklogin();
		$this->checkrole1();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$sql="select file from tblbanner where id=".$del_id;
				$query=$this->db->query($sql);
                if($query->num_rows()>0)
                {
                    $query=$query->row();
                    unlink($query->file);//xoa file
                }				
				$result = $this->admin_model->del_tbl('tblbanner',$del_id);    
			}
            if($result)
            {
                redirect('admin/banner');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/banner');
        }
	}
	public function checkbanner()
	{
		$this->checklogin();
		$this->checkrole1();
		$action=$_POST['active'];
        $id=$_POST['id'];
        $this->admin_model->checkstatus('tblbanner',$action,$id);		
	}
    public function diachi()
    {
        $this->checklogin();
        $this->checkrole1();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
		$per_page=15;
		if(is_numeric($start_row))
		{
			$_SESSION['start_row']=$start_row;
		}
		else
		{
			$_SESSION['start_row']=0;
		}
		$query=$this->admin_model->gettbl('tbldiachi','');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/diachi/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_limited('tbldiachi',$_SESSION['start_row'],$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='diachi';			
		$this->load->view('template',$data);    
    }
	/*Video*/	
	public function city()
    {
		$this->checklogin();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
		$per_page=15;
		if(is_numeric($start_row))
		{
			$_SESSION['start_row']=$start_row;
		}
		else
		{
			$_SESSION['start_row']=0;
		}
		$query=$this->admin_model->gettbl('tbltinh','');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/city/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_limited('tbltinh',$_SESSION['start_row'],$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='tinh';			
		$this->load->view('template',$data);    	   
    }
    public function quan()
    {
        $this->checklogin();
		$this->load->helper('status');
        $start_row=$this->uri->segment(3);
		$per_page=15;
		if(is_numeric($start_row))
		{
			$_SESSION['start_row']=$start_row;
		}
		else
		{
			$_SESSION['start_row']=0;
		}
		$query=$this->admin_model->gettbl('tblquan','');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/quanhuyen/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_limited('tblquan',$_SESSION['start_row'],$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='quan';			
		$this->load->view('template',$data);    
    }
    public function frmdiachi()
    {
        $this->checklogin();
        $data['content']='frmdiachi'; 
        $this->load->view('template',$data);    
    }
    public function add_diachi()
    {
        $this->checklogin();
        $this->checkrole1();
		$id = $this->input->post('id');		
		$data=array(	
			'title'  	=>  $this->input->post('title'),	
			'diachi'  	=>  $this->input->post('diachi'),						
			'thutu'  =>  $this->input->post('thutu'),					
			'status'    =>  $this->input->post('status')
		); 
		$this->admin_model->add_tbl('tbldiachi',$data,$id);
        if(isset($_SESSION['start_row'])==0)
        {
            redirect('admin/diachi/');
        }
		else
        {
			redirect('admin/diachi/'.$_SESSION['start_row']);
		}    
    }
    public function edit_diachi($id)
    {
        $this->checklogin();
        $this->checkrole1();
        $data['id']=$id;
        $data['content']='frmdiachi';
        $this->load->view('template',$data);    
    }
    public function del_diachi()
	{     
		$this->checklogin();
        $this->checkrole1();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$result = $this->admin_model->del_tbl('tbldiachi',$del_id);    
			}
            if($result)
            {
                if(isset($_SESSION['start_row'])==0)
                {
                    redirect('admin/diachi/');
                }
    			else
                {
    				redirect('admin/diachi/'.$_SESSION['start_row']);
    			}       
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            if(isset($_SESSION['start_row'])==0)
            {
                redirect('admin/diachi/');
            }
			else
            {
				redirect('admin/city/'.$_SESSION['start_row']);
			}       
        }
	}
    public function frmquan()
    {
        $this->checklogin();
        $data['content']='frmquan'; 
        $this->load->view('template',$data);    
    }
    public function add_quan()
    {
        $this->checklogin();
		$id = $this->input->post('id');		
		$quan=$this->input->post('quan');
        $tinh=$this->input->post('tinhthanh');		   
		/*-----------------------------*/
		$data=array(
            'quan'      =>$quan,	
			'tinh'  	=> $tinh,										
			'thutu'  =>  $this->input->post('thutu'),					
			'status'    =>  $this->input->post('status')
		); 
		$this->admin_model->add_tbl('tblquan',$data,$id);
        if(isset($_SESSION['start_row'])==0)
        {
            redirect('admin/quan/');
        }
		else
        {
			redirect('admin/quan/'.$_SESSION['start_row']);
		}    
    }
    public function edit_quan($id)
    {
        $this->checklogin();
        $data['id']=$id;
        $data['content']='frmquan';
        $this->load->view('template',$data);    
    }
	public function frmcity()
    {		
		$this->checklogin();
        $data['content']='frmcity'; 
        $this->load->view('template',$data);  
    }
	public function add_city()
    {
		$this->checklogin();
		$id = $this->input->post('id');		
		$alias=$this->input->post('alias');
		if($alias=='')
        {
            $alias=vn_str_filter($this->input->post('tinh'));
        }   
		/*-----------------------------*/
		$data=array(	
			'tinh'  	=>  $this->input->post('tinh'),	
			'alias'  	=>  $alias,						
			'thutu'  =>  $this->input->post('thutu'),					
			'status'    =>  $this->input->post('status')
		); 
		$this->admin_model->add_tbl('tbltinh',$data,$id);
        if(isset($_SESSION['start_row'])==0)
        {
            redirect('admin/city/');
        }
		else
        {
			redirect('admin/city/'.$_SESSION['start_row']);
		}    	
    }
	
	public function edit_city($id)
    {
		$this->checklogin();
        $data['id']=$id;
        $data['content']='frmcity';
        $this->load->view('template',$data);    
    }
    public function del_quan()
    {
        $this->checklogin();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$result = $this->admin_model->del_tbl('tblquan',$del_id);    
			}
            if($result)
            {
                if(isset($_SESSION['start_row'])==0)
                {
                    redirect('admin/quan/');
                }
    			else
                {
    				redirect('admin/quan/'.$_SESSION['start_row']);
    			}       
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            if(isset($_SESSION['start_row'])==0)
            {
                redirect('admin/quan/');
            }
			else
            {
				redirect('admin/quan/'.$_SESSION['start_row']);
			}       
        }    
    }
	public function del_city()
	{     
		$this->checklogin();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$result = $this->admin_model->del_tbl('tbltinh',$del_id);    
			}
            if($result)
            {
                if(isset($_SESSION['start_row'])==0)
                {
                    redirect('admin/city/');
                }
    			else
                {
    				redirect('admin/city/'.$_SESSION['start_row']);
    			}       
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            if(isset($_SESSION['start_row'])==0)
            {
                redirect('admin/city/');
            }
			else
            {
				redirect('admin/city/'.$_SESSION['start_row']);
			}       
        }
	}
	public function checkvideo()
	{
		$this->checklogin();
		$action=$_POST['active'];
        $id=$_POST['id'];
        $this->admin_model->checkstatus('tblvideo',$action,$id);		
	}
	///////////////////
	//Thông tin footer	
	public function frmfooter()
    {		
		$this->checklogin();
		$this->checkrole();
        $data['content']='frmfooter'; 
        $this->load->view('template',$data);  
    }
	public function add_footer()
    {
		$this->checklogin();
		$this->checkrole();
		$id = $this->input->post('id');	
        $config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);
		
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0755, TRUE);
		}
		if ( ! $this->upload->do_upload('banner'))
		{
			if($id==''){
				$imageh='';                
			}
			else{
				$imageh=$this->input->post('banner_he');               
			}
		}
		else
		{
			$image_datah = $this->upload->data();
			$imageh='upload/'.$image_datah['file_name'];                
			$duongdanh=$image_datah['file_name'];	
		}
        if ( ! $this->upload->do_upload('logo'))
		{
			if($id==''){
				$imagelo='';                
			}
			else{
				$imagelo=$this->input->post('logo_he');               
			}
		}
		else
		{
			$image_datalo = $this->upload->data();
			$imagelo='upload/'.$image_datalo['file_name'];                
			$duongdanlo=$image_datalo['file_name'];	
		}
		if ( ! $this->upload->do_upload('trai'))
		{
			if($id==''){
				$image='';                
			}
			else{
				$image=$this->input->post('trai_thumb');               
			}
		}
		else
		{
			$image_data = $this->upload->data();
			$image='upload/'.$image_data['file_name'];                
			$duongdan=$image_data['file_name'];	
		}
        if ( ! $this->upload->do_upload('phai'))
		{
			if($id==''){
				$image1='';                
			}
			else{
				$image1=$this->input->post('phai_thumb');               
			}
		}
		else
		{
			$image_data1 = $this->upload->data();
			$image1='upload/'.$image_data1['file_name'];                
			$duongdan1=$image_data1['file_name'];	
		}				
		$data=array(										
				'giaychungnhan'  	=>  $this->input->post('giaychungnhan'),												
				'tencongty'    =>  $this->input->post('tencongty'),
                'diachi'   =>  $this->input->post('diachi'),
                'dienthoai' =>  $this->input->post('dienthoai'),
                'hotline' =>  $this->input->post('hotline'),
                'email'     =>  $this->input->post('email'),
                'toado'     =>  $this->input->post('toado'),
                'googleantic'   =>  $this->input->post('googleantic'),
                'fanpage'   =>  $this->input->post('fanpage'),
                'banner'    =>$imageh,
                'logo'      =>$imagelo,
                'theh'          =>  $this->input->post('theh'),
                'anhtrai'       =>  $image,
                'vitri1'        =>  $this->input->post('vitri1'),
                'linktrai'      =>  $this->input->post('linktrai'),
                'anhphai'       =>$image1,
                'vitri2'        =>  $this->input->post('vitri2'),
                'linkphai'      =>  $this->input->post('linkphai'),
                'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tblfooter',$data,$id);    
		redirect('admin');	
    }
    public function edit_footer($id)
    {
        if(isset($_SESSION['quyen'])){
            if($_SESSION['quyen'] == 3 || $_SESSION['quyen'] == 4){
				$this->redirectPreviousPage(); 
            }
        }
		$this->checklogin();
		$this->checkrole();
        $data['id']=$id;
        $data['content']='frmfooter';
        $this->load->view('template',$data);    
    }
    //Thong tin seo
	public function frmmeta()
    {
        $this->checklogin();
		$this->checkrole1();
        $data['content']='frmmeta'; 
        $this->load->view('template',$data);     
    }
    public function add_meta()
    {
        $this->checklogin();   
        $this->checkrole1(); 
        $id = $this->input->post('id');					  									  			
		$data=array(										
				'title'  	=>  $this->input->post('title'),												
				'description'    =>  $this->input->post('meta_description'),
                'keywords'  =>  $this->input->post('keywords'),
                'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tblmeta',$data,$id);    
		redirect('admin'); 
    }
    public function edit_seo($id)
    {
        $this->checklogin();
		$this->checkrole1();
        $data['id']=$id;
        $data['content']='frmmeta';
        $this->load->view('template',$data);     
    }
    
	//Bài viết
	public function baiviet()
    {
		$this->checklogin();
		//$this->checkrole1();
		$this->load->helper('status');	
		if(isset($_POST['txt_search']) OR isset($_POST['catid']) OR isset($_POST['search_status']) OR isset($_POST['search_user'])){	
			unset($_SESSION['txt_search']);
			unset($_SESSION['search_catid']);			
			unset($_SESSION['search_user']);			
			unset($_SESSION['search_status']);			
			$_SESSION['txt_search'] = $_POST['txt_search'];
			$_SESSION['search_catid'] = $_POST['catid'];
			$_SESSION['search_user'] = $_POST['search_user'];
			$_SESSION['search_status'] = $_POST['search_status'];			
		}
		else{
			if(isset($_SESSION['txt_search']) AND isset($_SESSION['search_catid']) AND isset($_SESSION['search_status']) AND isset($_SESSION['search_user']) AND $_SESSION['txt_search'] == 'Nhập từ khóa tìm kiếm' AND $_SESSION['search_catid'] == 0 AND $_SESSION['search_user'] == 0 AND $_SESSION['search_status'] == -1){
				$_SESSION['txt_search'] = 'Nhập từ khóa tìm kiếm';
				$_SESSION['search_catid'] = 0;
				$_SESSION['search_user'] = 0;
				$_SESSION['search_status'] = -1;
			}			
		}			
		
        $start_row=$this->uri->segment(3);		
            $per_page=20;    		
			if(is_numeric($start_row)){
				$_SESSION['start_row']=$start_row;					
			}				    		
    		else
    		{
    			$_SESSION['start_row']=0;				
    		}			
            //$query=$this->admin_model->gettbl('tblbaiviet','');	
			$query=$this->admin_model->gettbl_search_limited('tblbaiviet','','');			
    		$total_rows = $query->num_rows();				
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/baiviet/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Tiếp';
    		$config['prev_link'] = 'Trước';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'Đầu';
    		$config['last_link'] = 'Cuối';    		
    		$this->pagination->initialize($config);
    		//$data['query']=$this->admin_model->gettbl_limited('tblbaiviet',$start_row,$per_page);
			$data['query']=$this->admin_model->gettbl_search_limited('tblbaiviet',$_SESSION['start_row'],$per_page);			
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='baiviet';			
    		$this->load->view('template',$data);    	   
    }
	public function user_baiviet()
    {
		$this->checklogin();
		$this->load->helper('status');
		$this->db->where('status',1);
		$this->db->where('taikhoan',$_SESSION['name']);
		$admin=$this->db->get('tbladmin1')->row();
		
		if(isset($_POST['txt_search']) and isset($_POST['catid'])){	
			unset($_SESSION['txt_search']);
			unset($_SESSION['search_catid']);			
			$_SESSION['txt_search'] = $_POST['txt_search'];
			$_SESSION['search_catid'] = $_POST['catid'];
		}
		else{
			if(isset($_SESSION['txt_search']) AND isset($_SESSION['search_catid']) AND $_SESSION['txt_search'] == 'Nhập từ khóa tìm kiếm' AND $_SESSION['search_catid'] == 0){
				$_SESSION['txt_search'] = 'Nhập từ khóa tìm kiếm';
				$_SESSION['search_catid'] = 0;
			}			
		}			
		
        $start_row=$this->uri->segment(3);		
            $per_page=20;    		
			if(is_numeric($start_row)){
				$_SESSION['start_row']=$start_row;					
			}				    		
    		else
    		{
    			$_SESSION['start_row']=0;				
    		}			            
		
            $query=$this->admin_model->gettbl_search_limited_user('tblbaiviet','','');				
    		$total_rows = $query->num_rows();		
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/user_baiviet/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Tiếp';
    		$config['prev_link'] = 'Trước';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'Đầu';
    		$config['last_link'] = 'Cuối';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_search_limited_user('tblbaiviet',$_SESSION['start_row'],$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='baiviet';			
    		$this->load->view('template',$data);    	   
    }
	public function frmbaiviet()
    {		
		$this->checklogin();
        $this->checkrole2();
        $data['content']='frmbaiviet'; 
        $this->load->view('template',$data);  
    }
    public function frmlink()
    {
        $this->checklogin();
		$this->checkrole();
        $data['content']='frmlink'; 
        $this->load->view('template',$data);     
    }
    public function add_link()
    {
        $this->checklogin();
		$this->checkrole();   
        $id=$this->input->post('id');
        $config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);
		
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0755, TRUE);
		}
		if(!is_dir($config['upload_path'].'resized/')){
			mkdir($config['upload_path'].'resized/', 0755, TRUE);
		}
		
		if ( ! $this->upload->do_upload('image'))
		{
			if($id==''){
				$image='';
				$thumb='';
			}
			else{
				$image=$this->input->post('image');
				$thumb=$this->input->post('thumb');
			}
		}
		else
		{			
			$image_data = $this->upload->data();
			$name_img='upload/'.$image_data['file_name']; 			
			
			//$duongdan=$image_data['file_name'];
			//Xu ly crop anh
			$temp=explode('.',$image_data['file_name']);
			if($temp[1]=='jpeg' or $temp[1]=='JPEG'){
				$temp[1]='jpg';
			}
			$temp[1]=strtolower($temp[1]);
			$thumb='upload/resized/'.$temp[0].'_thumb'.'.'.$temp[1];
			$image='upload/'.$image_data['file_name'];			
			$imageThumb = new Image($name_img);
			$imageThumb->resize(312,137,'crop');
			$imageThumb->save($temp[0].'_thumb', './upload/resized');
			
			$thumb_path = $temp[0].'_thumb_290x185';
			$imageThumb->resize(290,185,'crop');
			$imageThumb->save($thumb_path, './upload/resized');		
			
			$thumb_path = $temp[0].'_thumb_160x150';
			$imageThumb->resize(160,150,'crop');
			$imageThumb->save($thumb_path, './upload/resized');																	
		}
		if(!empty($this->input->post('thutu'))){
		    $thutu = $this->input->post('thutu');
		}else{
		    $thutu = 0;
		}
        $data=array(
            'title' =>  $this->input->post('title'),
            'image' =>  $image,
            'thumb' =>  $thumb,
            'link'  =>  $this->input->post('link'),
            'vitri1'    =>  $this->input->post('vitri1'),
            'vitri2'    =>  $this->input->post('vitri2'),
            'vitri3'    =>  $this->input->post('vitri3'),
            'thutu'     =>  $thutu,
            'status'    =>  $this->input->post('status')
        );
        $this->admin_model->add_tbl('tbllink',$data,$id);
        if(isset($_SESSION['start_row'])==0)
        {
            redirect('admin/link/');
        }
		else
        {
			redirect('admin/link/'.$_SESSION['start_row']);
		} 	
    }
    public function edit_link($id)
    {
        $this->checklogin();
        $this->checkrole();
        $data['id']=$id;
        $data['content']='frmlink';
        $this->load->view('template',$data);    
    }
	public function add_baiviet()
    {        
		$this->checklogin();
        $this->checkrole2();
        $id=$this->input->post('id');
        $title=trim($this->input->post('title'));		
        $alias=$this->input->post('alias');
        $ncatid1=$this->input->post('ncatid1');
        $ncatid2=$this->input->post('ncatid2');
		//$ncatid=$ncatid1.','.$ncatid2;
        $listbaiviet=trim($this->input->post('listbaiviet'));
		if($listbaiviet!=''){			
			$listid1 =$listbaiviet;			
		}
		else{
			$listid1 ='';
		}			
        if($alias=='')
        {
            $alias=LocDau($title);
        }    						
		$uid=$this->input->post('uid');
		$nguoidang=$this->input->post('nguoidang');
		$ngaydang=$this->input->post('ngaydang');
		if($this->input->post('status')==1){	
			$this->db->where('status',1);
			$this->db->where('taikhoan',$_SESSION['name']);
			$admin=$this->db->get('tbladmin1')->row();	
			$nguoidang=$admin->id;	
			date_default_timezone_set('Asia/Ho_Chi_Minh');
			$ngaydang = date('Y-m-d H:i:s');					
		}        
		else{
			$this->db->where('status',1);
			$this->db->where('taikhoan',$_SESSION['name']);
			$admin=$this->db->get('tbladmin1')->row();	
			$nguoidang=$admin->id;	
		}
		$ngay=explode('/',$this->input->post('created_day'));
		if($this->input->post('time')==''){
			$time='00:00:00';
		}
		else{
			$time=$this->input->post('time');
		}
		$created_day=$ngay[2].'-'.$ngay[1].'-'.$ngay[0].' '.$time;
		$feature=$this->input->post('feature');
        $status =$this->input->post('status');									
		//Xử lý ảnh	
		$alias_cat = $this->admin_model->gettbl('tblchuyenmuc',$this->input->post('catid'))->row()->alias;
		$config['upload_path'] = './upload/'.$alias_cat.'/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);
		
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0755, TRUE);
		}
		if(!is_dir($config['upload_path'].'resized/')){
			mkdir($config['upload_path'].'resized/', 0755, TRUE);
		}
		
		if ( ! $this->upload->do_upload('image'))
		{
			if($id==''){
				$image='';
				$thumb='';
			}
			else{
				$image=$this->input->post('image');
				$thumb=$this->input->post('thumb');
			}
		}
		else
		{			
			$image_data = $this->upload->data();
			$name_img='upload/'.$alias_cat.'/'.$image_data['file_name']; 
			//Xoa file anh cu truoc khi upload
			$check_image = $this->admin_model->gettbl('tblbaiviet',$id)->row()->image;
			if($check_image != $name_img){				
				$check_image_thumb = $this->admin_model->gettbl('tblbaiviet',$id)->row()->thumb;		
				$rbs=explode('.',$check_image_thumb);
				$rb = $rbs[0].'_290x185.'.$rbs[1];				
				$rb1 = $rbs[0].'_160x150.'.$rbs[1];	
				if(file_exists($check_image)){
					unlink($check_image);
					unlink($check_image_thumb);
					unlink($rb);
					unlink($rb1);
				}
			}
			//$duongdan=$image_data['file_name'];
			//Xu ly crop anh
			$temp=explode('.',$image_data['file_name']);
			if($temp[1]=='jpeg' or $temp[1]=='JPEG'){
				$temp[1]='jpg';
			}
			$temp[1]=strtolower($temp[1]);
			$thumb='upload/'.$alias_cat.'/resized/'.$temp[0].'_thumb'.'.'.$temp[1];
			$image='upload/'.$alias_cat.'/'.$image_data['file_name'];
			$imageThumb = new Image($name_img);
			$imageThumb->resize(475,310,'crop');
			$imageThumb->save($temp[0].'_thumb', './upload/'.$alias_cat.'/resized');
			
			$thumb_path = $temp[0].'_thumb_290x185';
			$imageThumb->resize(290,185,'crop');
			$imageThumb->save($thumb_path, './upload/'.$alias_cat.'/resized');		
			
			$thumb_path = $temp[0].'_thumb_160x150';
			$imageThumb->resize(160,150,'crop');
			$imageThumb->save($thumb_path, './upload/'.$alias_cat.'/resized');					
		}	
		if(!empty($this->input->post('thutu'))){
		    $thutu = $this->input->post('thutu');
		}else{
		    $thutu = 0;
		}
		if(!empty($this->input->post('sxtt'))){
		    $sxtt = $this->input->post('sxtt');
		}else{
		    $sxtt = 0;
		}
		$data=array(
				'title' 	=>	$title,
				'alias'		=>	$alias,				
				'catid'		=>	$this->input->post('catid'),
                'ncatid'    =>  $this->input->post('ncatid'),						
				'image'  	=>  $image,
				'thumb'		=>	$thumb,
                'created_day'	=>	$created_day,
                'mota'  =>  $this->input->post('mota'),
                'fulltext'  =>  $this->input->post('fulltext'),
                'home'      =>  $this->input->post('home'),
                'thutu'     =>  $thutu,
                'sxtt'      =>  $sxtt,
                'status'    =>  $this->input->post('status'),
                'uid' 		=>	$uid,				
				'tacgia' 	=>	$this->input->post('tacgia'),
				'nguon' 	=>	$this->input->post('nguon'),
                'tags'    	=>  $this->input->post('tags'),				
                'tinlienquan' 	=>	$listid1,					
				'nguoidang' =>  $nguoidang,
                'meta_title'    =>  $this->input->post('meta_title'),
                'meta_des'      =>  $this->input->post('meta_des'),
                'keyword'   => $this->input->post('keyword'),	
                'code_head' => $this->input->post('code_head'),
				'ngaydang'  =>  $ngaydang,																	
			); 		
        $this->admin_model->add_tbl('tblbaiviet',$data,$id); 
		$this->sitemap();
		//Kiem tra quyen
		$this->db->where('status',1);
		$this->db->where('name',$_SESSION['name']);
		$admin=$this->db->get('tbladmin')->row();
		//if($admin->role==2){
			//redirect('admin/user_baiviet');
        //}
		//else{
			if(isset($_SESSION['start_row'])==0)
            {
                redirect('admin/baiviet/');
            }
			else
            {
				redirect('admin/baiviet/'.$_SESSION['start_row']);
			}
		//}        
    }
    public function redirectPreviousPage()
    {
        if (isset($_SERVER['HTTP_REFERER']))
        {
            header('Location: '.$_SERVER['HTTP_REFERER']);
        }
        else
        {
            header('Location: http://'.$_SERVER['SERVER_NAME']);
        }
        
        exit;
    }
	public function edit_baiviet($id)
    {
        if(isset($_SESSION['quyen'])){
            if($_SESSION['quyen'] == 3 || $_SESSION['quyen'] == 4){
                $this->db->where('id',$id);
				$check_redirect =$this->db->get('tblbaiviet')->row();
				if($check_redirect->uid!=$_SESSION['author']){
				    $this->redirectPreviousPage(); 
				}
            }
        }
		$this->checklogin();
        $this->checkrole2();
        $data['id']=$id;
        $data['content']='frmbaiviet';
        $this->load->view('template',$data);    
    }
	public function link_baiviet($id)
    {
		$this->checklogin();
        $this->checkrole1();
        $data['id']=$id;
        $data['content']='frmbaiviet';
        $this->load->view('template',$data);    
    }	
	public function del_baiviet()
	{     
		$this->checklogin();
        $this->checkrole1();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$sql="select image,thumb from tblbaiviet where id=".$del_id;
				$query=$this->db->query($sql)->row();
				if(file_exists($query->image)){
					unlink($query->image);				
				}
				if(file_exists($query->thumb)){
					unlink($query->thumb);
				}
				$result = $this->admin_model->del_tbl('tblbaiviet',$del_id);    
			}
			//Kiem tra quyen
			$this->db->where('status',1);
			$this->db->where('name',$_SESSION['name']);
			$admin=$this->db->get('tbladmin')->row();
            if($result)
            {                
				if($admin->role==2){
					redirect('admin/user_baiviet');
				}
				else{
					if($_SESSION['start_row']==0){redirect('admin/baiviet/');}
					else{
						redirect('admin/baiviet/'.$_SESSION['start_row']);
					}
				}        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            if($admin->role==2){
				redirect('admin/user_baiviet');
			}
			else{
				redirect('admin/baiviet');
			}  
        }
        
	}	
    public function sanpham()
    {
        $this->checklogin();
        $this->checkrole2();
        $this->load->helper('status');	
        if(isset($_POST['txt_searchsp']) OR isset($_POST['catidsp']) OR isset($_POST['search_statussp']) OR isset($_POST['search_usersp'])){	
			unset($_SESSION['txt_searchsp']);
			unset($_SESSION['search_catidsp']);			
			unset($_SESSION['search_usersp']);			
			unset($_SESSION['search_statussp']);			
			$_SESSION['txt_searchsp'] = $_POST['txt_searchsp'];
			$_SESSION['search_catidsp'] = $_POST['catidsp'];
			$_SESSION['search_usersp'] = $_POST['search_usersp'];
			$_SESSION['search_statussp'] = $_POST['search_statussp'];			
		}
		else{
			if(isset($_SESSION['txt_searchsp']) AND isset($_SESSION['search_catidsp']) AND isset($_SESSION['search_statussp']) AND isset($_SESSION['search_usersp']) AND $_SESSION['txt_searchsp'] == 'Nhập từ khóa tìm kiếm' AND $_SESSION['search_catidsp'] == 0 AND $_SESSION['search_usersp'] == 0 AND $_SESSION['search_statussp'] == -1){
				$_SESSION['txt_searchsp'] = 'Nhập từ khóa tìm kiếm';
				$_SESSION['search_catidsp'] = 0;
				$_SESSION['search_usersp'] = 0;
				$_SESSION['search_statussp'] = -1;
			}			
		}			
		
        $start_row=$this->uri->segment(3);		
        $per_page=20;    		
		if(is_numeric($start_row)){
			$_SESSION['start_row']=$start_row;					
		}				    		
		else
		{
			$_SESSION['start_row']=0;				
		}			
		$query=$this->admin_model->gettbl_search_limited('tblsanpham','','');			
		$total_rows = $query->num_rows();				
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/sanpham/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Tiếp';
		$config['prev_link'] = 'Trước';
		$config['num_links'] = 4;
		$config['first_link'] = 'Đầu';
		$config['last_link'] = 'Cuối';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_search_limited('tblsanpham',$_SESSION['start_row'],$per_page);			
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='sanpham';			
		$this->load->view('template',$data);    	    
    }
    public function ordercongty()
    {
        $this->checklogin();
        if(isset($_POST['txt_dienthoaisp']))
        {
            unset($_SESSION['txt_dienthoaisp']);  
            $_SESSION['txt_dienthoaisp'] = $_POST['txt_dienthoaisp'];  
        }
        else
        {
            if(isset($_SESSION['txt_dienthoaisp']))
            {
                $_SESSION['txt_dienthoaisp'] = 'Nhập số điện thoại tìm kiếm';     
            }    
        }
        $start_row=$this->uri->segment(3);		
        $per_page=20;    		
		if(is_numeric($start_row)){
			$_SESSION['start_row']=$start_row;					
		}				    		
		else
		{
			$_SESSION['start_row']=0;				
		}			
		$query=$this->admin_model->gettbl_donhang_limited('tbldonhangcongty','','');			
		$total_rows = $query->num_rows();				
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/ordercongty/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Tiếp';
		$config['prev_link'] = 'Trước';
		$config['num_links'] = 4;
		$config['first_link'] = 'Đầu';
		$config['last_link'] = 'Cuối';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_donhang_limited('tbldonhangcongty',$_SESSION['start_row'],$per_page);			
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='donhangcongty';			
		$this->load->view('template',$data);    
    }
    public function orderweb()
    {
        $this->checklogin();        	
		$data['content']='donhangweb';			
		$this->load->view('template',$data);
    }
    public function orderweb1()
    {
        $this->checklogin();
        if(isset($_POST['txt_dienthoaisp']))
        {
            unset($_SESSION['txt_dienthoaisp']);  
            $_SESSION['txt_dienthoaisp'] = $_POST['txt_dienthoaisp'];  
        }
        else
        {
            if(isset($_SESSION['txt_dienthoaisp']))
            {
                $_SESSION['txt_dienthoaisp'] = 'Nhập số điện thoại tìm kiếm';     
            }    
        }
        $start_row=$this->uri->segment(3);		
        $per_page=20;    		
		if(is_numeric($start_row)){
			$_SESSION['start_row']=$start_row;					
		}				    		
		else
		{
			$_SESSION['start_row']=0;				
		}			
		$query=$this->admin_model->gettbl_donhang_limited('tbldonhangweb','','');			
		$total_rows = $query->num_rows();				
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/orderweb/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Tiếp';
		$config['prev_link'] = 'Trước';
		$config['num_links'] = 4;
		$config['first_link'] = 'Đầu';
		$config['last_link'] = 'Cuối';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_donhang_limited('tbldonhangweb',$_SESSION['start_row'],$per_page);			
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='donhangweb1';			
		$this->load->view('template',$data);
    }
    public function edit_donhangweb($id)
    {
        $this->checklogin();
        $data['id']=$id;
        $data['content']='frmdonhangweb';
        $this->load->view('template',$data);    
    }
    public function edit_donhangcty($id)
    {
        $this->checklogin();
        $data['id']=$id;
        $data['content']='frmdonhangcty';
        $this->load->view('template',$data);     
    }
    public function do_editdonhangweb()
    {
        $this->checklogin();
        $datadhweb=array(
            'hinhthuc'  =>$this->input->post('hinhthuc'),
            'hoten'     =>$this->input->post('hoten'),
            'dienthoai' =>$this->input->post('dienthoai'),
            'email'     =>$this->input->post('email'),
            'diachi'    =>$this->input->post('cuahang'),    
            'diachinhanhang'    =>  $this->input->post('diachinhanhang'),  
            'yeucauthem'    =>$this->input->post('yeucauthem'),
            'soluong'   =>$this->input->post('soluong'),
            'status'    =>$this->input->post('status')
        ); 
        $this->db->where('id',$this->input->post('id'));
        $this->db->update('tbldonhangweb',$datadhweb);
        //cap nhat so luon va luot mua cho san pham
        $this->db->where('id',$this->input->post('idsp'));          
        $sqlsanphamcheckweb=$this->db->get('tblsanpham');
        if($sqlsanphamcheckweb->num_rows()>0)
        {
            $sanphamcheckweb=$sqlsanphamcheckweb->row();
            $data_slspw=array(
                'luotmua'   =>$sanphamcheckweb->luotmua + 1,  
                'soluong'=>($sanphamcheckweb->soluong - $this->input->post('soluong'))
            );
            $this->db->where('id',$this->input->post('idsp'));
            $this->db->update('tblsanpham',$data_slspw);
        } 
        if($_SESSION['start_row']==0){redirect('admin/orderweb/');}
		else{
	       redirect('admin/orderweb/'.$_SESSION['start_row']);
	   }
    }
    public function do_editdonhangcty()
    {
        $this->checklogin();
        $datadhcty=array(
            'cuahang'=>$this->input->post('cuahang'),
            'soluong'=>$this->input->post('soluong'),
            'status'    =>$this->input->post('status')
        ); 
        $this->db->where('id',$this->input->post('id')); 
        $this->db->update('tbldonhangcongty',$datadhcty);  
        //cap nhat so luong san pham
        $this->db->where('id',$this->input->post('idsp'));
        $sqlsanphamchecksl=$this->db->get('tblsanpham');
        
        if($sqlsanphamchecksl->num_rows()>0)
        {
            $sanphamchecksl=$sqlsanphamchecksl->row();
            $data_slsp=array(
                'luotmua'   =>$sanphamchecksl->luotmua + 1,  
                'soluong'=>($sanphamchecksl->soluong - $this->input->post('soluong'))
            );
            $this->db->where('id',$this->input->post('idsp'));
            $this->db->update('tblsanpham',$data_slsp);
        }
        if($_SESSION['start_row']==0){redirect('admin/ordercongty/');}
		else{
	       redirect('admin/ordercongty/'.$_SESSION['start_row']);
	   }
        
    }
    public function frmsanpham()
    {
        $this->checklogin();
        $this->checkrole2();
        $data['content']='frmsanpham'; 
        $this->load->view('template',$data);      
    }
    public function add_sanpham()
    {
        $this->checklogin();
        $this->checkrole2();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title','Tên sản phẩm','required');        
        $this->form_validation->set_message('required','%s không để trống');
		if($this->form_validation->run()==FALSE)
        {
            $data['error']=validation_errors();
            $data['query']=$this->admin_model->gettbl('tblsanpham','');
            $data['content']='frmsanpham';
            $this->load->view('template',$data);
        }
        else
        {
            $id=$this->input->post('id');
            $title=trim($this->input->post('title'));		
            $alias=$this->input->post('alias');
            $ncatid1=$this->input->post('ncatid1');
            $ncatid2=$this->input->post('ncatid2');
            $ngaydang1=explode('-',$this->input->post('created_day'));    
            $ngaydang=$ngaydang1[2].'-'.$ngaydang1[1].'-'.$ngaydang1[0];
            if($this->input->post('created_day1')!='')
            {
                $ngayhh1=explode('-',$this->input->post('created_day1'));
                $ngayhethan=$ngayhh1[2].'-'.$ngayhh1[1].'-'.$ngayhh1[0];
            }
            else
            {
                $ngayhethan='';    
            }
            if($id=='')
            {
                $this->db->where('status',1);
			     $this->db->where('taikhoan',$_SESSION['name']);
  	             $admin=$this->db->get('tbladmin1')->row();	
     	          $nguoidang=$admin->id;
            }
            else
            {
                $nguoidang=$this->input->post('nguoidang');
            }	
            if($alias=='')
            {
                $alias=vn_str_filter($title);
            }
           	//Xử lý ảnh	
    		$alias_cat = $this->admin_model->gettbl('tblchuyenmucsp',$this->input->post('catid'))->row()->alias;
    		$config['upload_path'] = './upload/'.$alias_cat.'/';
    		$config['allowed_types'] = 'gif|jpg|png|jpeg';
    		$config['max_size']	= '1000000';
    		$config['max_width']  = '0';
    		$config['max_height']  = '0';
    		$this->load->library('upload', $config);
    		
    		if(!is_dir($config['upload_path'])){
    			mkdir($config['upload_path'], 0755, TRUE);
    		}
    		if(!is_dir($config['upload_path'].'resized/')){
    			mkdir($config['upload_path'].'resized/', 0755, TRUE);
    		}
    		
    		if ( ! $this->upload->do_upload('image'))
    		{
    			if($id==''){
    				$image='';
    				$thumb='';
    			}
    			else{
    				$image=$this->input->post('image');
    				$thumb=$this->input->post('thumb');
    			}
    		}
    		else
    		{			
    			$image_data = $this->upload->data();
    			$name_img='upload/'.$alias_cat.'/'.$image_data['file_name']; 
    			//Xoa file anh cu truoc khi upload
    			$check_image = $this->admin_model->gettbl('tblsanpham',$id)->row()->image;
    			if(isset($check_image) != $name_img){				
    				$check_image_thumb = $this->admin_model->gettbl('tblsanpham',$id)->row()->thumb;		
    				$rbs=explode('.',$check_image_thumb);
    				$rb = $rbs[0].'_300x300.'.$rbs[1];				    					
    				if(file_exists($check_image)){
    					unlink($check_image);
    					unlink($check_image_thumb);
    					unlink($rb);    					
    				}
    			}
    			//$duongdan=$image_data['file_name'];
    			//Xu ly crop anh
    			$temp=explode('.',$image_data['file_name']);
    			if($temp[1]=='jpeg' or $temp[1]=='JPEG'){
    				$temp[1]='jpg';
    			}
    			$temp[1]=strtolower($temp[1]);
    			$thumb='upload/'.$alias_cat.'/resized/'.$temp[0].'_thumb'.'.'.$temp[1];
    			$image='upload/'.$alias_cat.'/'.$image_data['file_name'];
    			$imageThumb = new Image($name_img);
    			$imageThumb->resize(300,300,'crop');
    			$imageThumb->save($temp[0].'_thumb', './upload/'.$alias_cat.'/resized');					
    		}
                $image1 = json_encode($_POST['image_filename']);	
                $data=array(
                    'title' =>  $this->input->post('title'),
                    'alias' =>  $alias,
                    'catid' =>  $this->input->post('catid'),
                    'ncatid'    =>  $ncatid1,
                    'ncatid1'   =>  $ncatid2,
                    'tinh'  =>  $this->input->post('city'),
                    'image'     =>  $image,
                    'image1'   =>  $image1,
                    'thumb'     =>  $thumb,
                    'soluong'   =>  $this->input->post('soluong'),
                    'gia'       =>  $this->input->post('gia'),
                    'giakm'     =>  $this->input->post('giakm'),
                    'donvitinh' =>  $this->input->post('donvitinh'),
                    'home'  =>  $this->input->post('home'),
                    'top'   =>  $this->input->post('top'),
                    'noidung'   =>  $this->input->post('thongtinct'),
                    'diemnoibat'    =>  $this->input->post('diennb'),
                    'dieukiensudung'    =>  $this->input->post('dieukiensd'),
                    'nguoidang' =>  $nguoidang,
                    'ngaydang'  =>  $ngaydang,
                    'ngayhethan'    =>  $ngayhethan,
                    'tags'  =>  $this->input->post('tags'),
                    'meta_title'    =>  $this->input->post('meta_title'),
                    'meta_des'    =>  $this->input->post('meta_des'),
                    'keyword'    =>  $this->input->post('keyword'),
                    'thutu' =>  $this->input->post('thutu'),
                    'status'    =>  $this->input->post('status')
                ); 
                if($this->input->post('catid')!=0)
                {
                    $this->db->where('catid',$this->input->post('catid'));
                    $sqlsanphamcout=$this->db->get('tblsanpham');
                    $total=$sqlsanphamcout->num_rows(); 
                    $this->db->where('danhmucsp',$this->input->post('catid'));
                    $this->db->where('tinh',$this->input->post('city'));
                    $sqlcounttotal=$this->db->get('tbltotal');
                    if($sqlcounttotal->num_rows()>0){                        
                    }
                    else
                    {
                        $data_count=array(
                            'danhmucsp'   =>$this->input->post('catid'),
                            'tinh'  =>$this->input->post('city'),
                            'total' =>$total
                        );
                        $this->db->insert('tbltotal',$data_count);
                    }
                }   
                if($ncatid1!=0)
                {
                    $this->db->where('ncatid',$ncatid1);
                    $sqlsanphamcout2=$this->db->get('tblsanpham');
                    $total2=$sqlsanphamcout2->num_rows();  
                    $this->db->where('danhmucsp',$ncatid1);
                    $this->db->where('tinh',$this->input->post('city'));
                    $sqlcounttotal2=$this->db->get('tbltotal');
                    if($sqlcounttotal2->num_rows()>0){                        
                    }
                    else
                    {
                        $data_count2=array(
                            'danhmucsp'   =>$ncatid1,
                            'tinh'  =>$this->input->post('city'),
                            'total' =>$total2
                        );
                        $this->db->insert('tbltotal',$data_count2);
                    }  
                }    
                if($ncatid2!=0)
                {
                    $this->db->where('ncatid1',$ncatid2);
                    $sqlsanphamcout3=$this->db->get('tblsanpham');
                    $total3=$sqlsanphamcout3->num_rows();  
                    $this->db->where('danhmucsp',$ncatid2);
                    $this->db->where('tinh',$this->input->post('city'));
                    $sqlcounttotal3=$this->db->get('tbltotal');
                    if($sqlcounttotal3->num_rows()>0){                        
                    }
                    else
                    {
                        $data_count2=array(
                            'danhmucsp'   =>$ncatid2,
                            'tinh'  =>$this->input->post('city'),
                            'total' =>$total3
                        );
                        $this->db->insert('tbltotal',$data_count2);
                    }  
                }        
                $this->admin_model->add_tbl('tblsanpham',$data,$id);                
                $this->sitemap(); 
                if($_SESSION['start_row']==0){redirect('admin/sanpham/');}
    			else{
    				redirect('admin/sanpham/'.$_SESSION['start_row']);
    			}  
	    	}              
    }
    
    public function edit_sanpham($id)
    {
        $this->checklogin();
        $this->checkrole2();
        $data['id']=$id;
        $data['content']='frmsanpham';
        $this->load->view('template',$data);     
    }
    
    public function user_sanpham()
    {
		$this->checklogin();
		$this->load->helper('status');
		$this->db->where('status',1);
		$this->db->where('name',$_SESSION['name']);
		$admin=$this->db->get('tbladmin')->row();
		
		if(isset($_POST['txt_searchsp']) and isset($_POST['catidsp'])){	
			unset($_SESSION['txt_searchsp']);
			unset($_SESSION['search_catidsp']);			
			$_SESSION['txt_searchsp'] = $_POST['txt_searchsp'];
			$_SESSION['search_catidsp'] = $_POST['catidsp'];
		}
		else{
			if(isset($_SESSION['txt_searchsp']) AND isset($_SESSION['search_catidsp']) AND $_SESSION['txt_searchsp'] == 'Nhập từ khóa tìm kiếm' AND $_SESSION['search_catidsp'] == 0){
				$_SESSION['txt_searchsp'] = 'Nhập từ khóa tìm kiếm';
				$_SESSION['search_catidsp'] = 0;
			}			
		}			
		
        $start_row=$this->uri->segment(3);		
            $per_page=20;    		
			if(is_numeric($start_row)){
				$_SESSION['start_row']=$start_row;					
			}				    		
    		else
    		{
    			$_SESSION['start_row']=0;				
    		}			            
		
            $query=$this->admin_model->gettbl_search_limited_usersp('tblsanpham','','');				
    		$total_rows = $query->num_rows();		
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/user_sanpham/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Tiếp';
    		$config['prev_link'] = 'Trước';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'Đầu';
    		$config['last_link'] = 'Cuối';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_search_limited_usersp('tblsanpham',$_SESSION['start_row'],$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='sanpham';			
    		$this->load->view('template',$data);    	   
    }
    public function user_donhangcongty()
    {
        $this->checklogin();
		
		if(isset($_POST['txt_dienthoaisp'])){	
			unset($_SESSION['txt_dienthoaisp']);		
			$_SESSION['txt_dienthoaisp'] = $_POST['txt_dienthoaisp'];
		}
		else{
			if(isset($_SESSION['txt_dienthoaisp']) ){
				$_SESSION['txt_dienthoaisp'] = 'Nhập số điện thoại tìm kiếm';				
			}			
		}			
		
        $start_row=$this->uri->segment(3);		
            $per_page=20;    		
			if(is_numeric($start_row)){
				$_SESSION['start_row']=$start_row;					
			}				    		
    		else
    		{
    			$_SESSION['start_row']=0;				
    		}			            
		
            $query=$this->admin_model->gettbl_search_limited_usersp('tbldonhangweb','','');				
    		$total_rows = $query->num_rows();		
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/user_donhangcongty/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Tiếp';
    		$config['prev_link'] = 'Trước';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'Đầu';
    		$config['last_link'] = 'Cuối';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_search_limited_usersp('tbldonhangweb',$_SESSION['start_row'],$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='donhangcongty';			
    		$this->load->view('template',$data);    
    }
    public function del_sanpham()
	{     
		$this->checklogin();
        $this->checkrole2();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$sql="select image,thumb from tblsanpham where id=".$del_id;
				$query=$this->db->query($sql)->row();
				if(file_exists($query->image)){
					unlink($query->image);				
				}
				if(file_exists($query->thumb)){
					unlink($query->thumb);
				}
				$result = $this->admin_model->del_tbl('tblsanpham',$del_id);    
			}
			if($_SESSION['start_row']==0){redirect('admin/sanpham/');}
			else{
			 redirect('admin/sanpham/'.$_SESSION['start_row']);
			}            
        }
        else
        { 
		  redirect('admin/sanpham'); 
        }
        $this->sitemap();
	}	
    public function del_donhangcongty()
    {
        $this->checklogin();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$sql="select image,thumb from tbldonhangcongty where id=".$del_id;
				$result = $this->admin_model->del_tbl('tbldonhangcongty',$del_id);    
			}
			if($_SESSION['start_row']==0){redirect('admin/ordercongty/');}
			else{
			 redirect('admin/ordercongty/'.$_SESSION['start_row']);
			}            
        }
        else
        { 
		  redirect('admin/ordercongty'); 
        }    
    }
    public function del_donhangweb()
    {
        $this->checklogin();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$result = $this->admin_model->del_tbl('tbldonhangweb',$del_id);    
			}
			if($_SESSION['start_row']==0){redirect('admin/orderweb/');}
			else{
			 redirect('admin/orderweb/'.$_SESSION['start_row']);
			}            
        }
        else
        { 
		  redirect('admin/orderweb'); 
        }    
    }
	public function comment()
    {
		$this->load->helper('status');
		$this->checklogin();
		$this->checkrole();
        $start_row=$this->uri->segment(3);
            $per_page=20;
    		if(is_numeric($start_row))
    		{
    			$start_row=$start_row;
    		}
    		else
    		{
    			$start_row=0;
    		}
            $query=$this->admin_model->gettbl('tblcomment','');			
    		$total_rows = $query->num_rows();
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/comment/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'sau';
    		$config['prev_link'] = 'trước';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'đầu';
    		$config['last_link'] = 'cuối';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_limited('tblcomment',$start_row,$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='comment';			
    		$this->load->view('template',$data);    	   
    }
	public function edit_comment($id)
    {
		$this->checklogin();
		$this->checkrole();
        $data['id']=$id;
        $data['content']='frmcomment';
        $this->load->view('template',$data);    
    }
	public function add_comment()
    {
		$this->checklogin();
		$this->checkrole();
		$id = $this->input->post('id');					  				
		$data=array(									
				'name'  	=>  $this->input->post('name'),					
				'mail'  	=>  $this->input->post('mail'),					
				'comment'  	=>  $this->input->post('comment'),					
				'status'    =>  $this->input->post('status')
			); 
		$this->admin_model->add_tbl('tblcomment',$data,$id);    
		redirect('admin/comment');	
    }
	public function del_comment()
	{     
		$this->checklogin();
		$this->checkrole();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];				
				$result = $this->admin_model->del_tbl('tblcomment',$del_id);    
			}
            if($result)
            {
                redirect('admin/comment');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/comment');
        }
	}	
	
	public function chuyenmuc()
    {
		$this->load->helper('status');
		$this->checklogin();
		$this->checkrole1();
        $start_row=$this->uri->segment(3);
            $per_page=15;
    		if(is_numeric($start_row))
    		{
    			$start_row=$start_row;
    		}
    		else
    		{
    			$start_row=0;
    		}
            $query=$this->admin_model->gettbl('tblchuyenmuc','');			
    		$total_rows = $query->num_rows();
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'/admin/chuyenmuc/';
    		$config['total_rows'] = $total_rows;
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Next';
    		$config['prev_link'] = 'Prev';
    		$config['num_links'] = 4;
    		$config['first_link'] = 'First';
    		$config['last_link'] = 'Last';    		
    		$this->pagination->initialize($config);
    		$data['query']=$this->admin_model->gettbl_limited('tblchuyenmuc',$start_row,$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='chuyenmuc';			
    		$this->load->view('template',$data);    	   
    }
	
	public function frmchuyenmuc()
    {
		$this->checklogin();
		$this->checkrole1();
        $data['content']='frmchuyenmuc'; 
        $this->load->view('template',$data);  
    }
	
	public function add_chuyenmuc()
    {
		$this->checklogin();
		$this->checkrole1();
		$this->load->library('form_validation');
        $this->form_validation->set_rules('name','Tiêu đề','required');        
        $this->form_validation->set_message('required','%s không để trống');
		if($this->form_validation->run()==FALSE)
        {
            $data['error']=validation_errors();
            $data['query']=$this->admin_model->gettbl('tblchuyenmuc','');
            $data['content']='frmchuyenmuc';
            $this->load->view('template',$data);
        }
        else
        {
			$id = $this->input->post('id');		
			$alias=$this->input->post('alias');
			if($alias=='')
			{
				$alias=LocDau($this->input->post('name'));
			}    
			//Xu ly thu tu
            //Xử lý ảnh	
    		//$alias_cat = $this->admin_model->gettbl('tblchuyenmuc',$this->input->post('uid'))->row()->alias;
    		$config['upload_path'] = './upload/';
    		$config['allowed_types'] = 'gif|jpg|png|jpeg';
    		$config['max_size']	= '1000000';
    		$config['max_width']  = '0';
    		$config['max_height']  = '0';
    		$this->load->library('upload', $config);
    		
    		if(!is_dir($config['upload_path'])){
    			mkdir($config['upload_path'], 0755, TRUE);
    		}
    		if(!is_dir($config['upload_path'].'resized/')){
    			mkdir($config['upload_path'].'resized/', 0755, TRUE);
    		}
    		
    		if ( ! $this->upload->do_upload('image'))
    		{
    			if($id==''){
    				$image='';
    				$thumb='';
    			}
    			else{
    				$image=$this->input->post('image');
    				$thumb=$this->input->post('thumb');
    			}
    		}
    		else
    		{			
    			$image_data = $this->upload->data();
    			$name_img='upload/'.$image_data['file_name']; 
    			
    			//$duongdan=$image_data['file_name'];
    			//Xu ly crop anh
    			$temp=explode('.',$image_data['file_name']);
    			if($temp[1]=='jpeg' or $temp[1]=='JPEG'){
    				$temp[1]='jpg';
    			}
    			$temp[1]=strtolower($temp[1]);
    			$thumb='upload/resized/'.$temp[0].'_thumb'.'.'.$temp[1];
    			$image='upload/'.$image_data['file_name'];
    			$imageThumb = new Image($name_img);	
                $imageThumb->resize(25,23,'crop');
			$imageThumb->save($temp[0].'_thumb', './upload/resized');		    			
    			$thumb_path = $temp[0].'_thumb_25x23';
    			$imageThumb->resize(25,23,'crop');
    			$imageThumb->save($thumb_path, './upload/resized');		    			    								
    		}	
    		if(!empty($this->input->post('thutu'))){
    		    $thutu = $this->input->post('thutu');;
    		}else{
    		    $thutu = 0;
    		}
			/*-----------------------------*/
			$data=array(
					'name' 		=>	$this->input->post('name'),
					'alias'		=>	$alias,
                    'image'     =>  $image,
                    'thumb'     =>  $thumb,
                    'thutu'   	=>  $thutu,
					'uid'  		=>  $this->input->post('uid'),					
					'menu'   	=>  $this->input->post('menu'),
                    'trai'   	=>  $this->input->post('trai'),
                    'home'   	=>  $this->input->post('home'),
                    'tintuc'    =>  $this->input->post('tintuc'),
                    'footer'   	=>  $this->input->post('footer'),
                    'theh'      =>  $this->input->post('theh'),
                    'meta_title'=>  $this->input->post('meta_title'),
                    'meta_des'  =>  $this->input->post('meta_des'),
                    'keyword'   =>  $this->input->post('keyword'),					
					'status'    =>  $this->input->post('status'),
				); 
			$this->admin_model->add_tbl('tblchuyenmuc',$data,$id);
            $this->sitemap();    
			redirect('admin/chuyenmuc');
		}        
    }
	
	public function edit_chuyenmuc($id)
    {
		$this->checklogin();
		$this->checkrole1();
        $data['id']=$id;
        $data['content']='frmchuyenmuc';
        $this->load->view('template',$data);    
    }
	
	public function del_chuyenmuc()
	{     
		$this->checklogin();
		$this->checkrole1();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
                $sql="select image,thumb from tblchuyenmuc where id=".$del_id;
				$query=$this->db->query($sql)->row();
				if(file_exists($query->image)){
					unlink($query->image);				
				}
				if(file_exists($query->thumb)){
					unlink($query->thumb);
				}
				$result = $this->admin_model->del_tbl('tblchuyenmuc',$del_id);    
			}
            if($result)
            {
                redirect('admin/chuyenmuc');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/chuyenmuc');
        }
        $this->sitemap();
	}	
    
    public function chuyenmucsp()
    {
		$this->load->helper('status');
		$this->checklogin();
		$this->checkrole1();
        $start_row=$this->uri->segment(3);
        $per_page=15;
		if(is_numeric($start_row))
		{
			$_SESSION['start_row']=$start_row;
		}
		else
		{
			$_SESSION['start_row']=0;
		}
        $query=$this->admin_model->gettbl('tblchuyenmucsp','');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/chuyenmucsp/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_limited('tblchuyenmucsp',$_SESSION['start_row'],$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='chuyenmucsp';			
		$this->load->view('template',$data);    	   
    }
    
    public function frmchuyenmucsp()
    {
        $this->checklogin();
		$this->checkrole1();
        $data['content']='frmchuyenmucsp'; 
        $this->load->view('template',$data);      
    }
    
    public function add_chuyenmucsp()
    {
		$this->checklogin();
		$this->checkrole1();
		$this->load->library('form_validation');
        $this->form_validation->set_rules('name','Tiêu đề','required');        
        $this->form_validation->set_message('required','%s không để trống');
		if($this->form_validation->run()==FALSE)
        {
            $data['error']=validation_errors();
            $data['query']=$this->admin_model->gettbl('tblchuyenmuc','');
            $data['content']='frmchuyenmuc';
            $this->load->view('template',$data);
        }
        else
        {
			$id = $this->input->post('id');		
			$alias=$this->input->post('alias');
			if($alias=='')
			{
				$alias=vn_str_filter($this->input->post('name'));
			}    
			//Xu ly thu tu
			$thutu = $this->input->post('thutu');			
			/*-----------------------------*/
			$data=array(
					'name' 		=>	$this->input->post('name'),
					'alias'		=>	$alias,
                    'menu'   	=>  $this->input->post('menu'),
                    'hover'     =>  $this->input->post('hover'),
                    'home'      =>  $this->input->post('home'),
					'uid'  		=>  $this->input->post('uid'),
                    'meta_title'    =>  $this->input->post('meta_title'),
                    'meta_des'  =>  $this->input->post('meta_des'),
                    'keyword'   =>  $this->input->post('keyword'),					
					'thutu'   	=>  $thutu,
					'status'    =>  $this->input->post('status'),
				); 
			$this->admin_model->add_tbl('tblchuyenmucsp',$data,$id); 
            $this->sitemap();   
            if($_SESSION['start_row']==0)
            {
                redirect('admin/chuyenmucsp');    
            }
            else
            {
                redirect('admin/chuyenmucsp/'.$_SESSION['start_row']);    
            }
		}        
    }
    
    public function edit_chuyenmucsp($id)
    {
		$this->checklogin();
		$this->checkrole1();
        $data['id']=$id;
        $data['content']='frmchuyenmucsp';
        $this->load->view('template',$data);    
    }
    
    public function del_chuyenmucsp()
	{     
		$this->checklogin();
		$this->checkrole1();
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$result = $this->admin_model->del_tbl('tblchuyenmucsp',$del_id);    
			}
            if($result)
            {
                if($_SESSION['start_row']==0)
                {
                    redirect('admin/chuyenmucsp');       
                }
                else
                {
                    redirect('admin/chuyenmucsp/'.$_SESSION['start_row']);      
                }      
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/chuyenmucsp');
        }
        $this->sitemap();
	}	
    	
	/*---------------------*/
	/*Bảng Admin*/
	public function tbladmin()
    {
		$this->checklogin();
        $this->checkrole1();		
		$this->load->helper('status');
		$start_row=$this->uri->segment(3);
		$per_page=15;
		if(is_numeric($start_row))
		{
			$start_row=$start_row;
		}
		else
		{
			$start_row=0;
		}
		$query=$this->admin_model->gettbl('tbladmin','');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/tbladmin/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_limited('tbladmin',$start_row,$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='tbladmin';			
		$this->load->view('template',$data);    	   
    }
    public function tbladmin1()
    {
		$this->checklogin();
        $this->checkrole1();		
		$this->load->helper('status');
		$start_row=$this->uri->segment(3);
		$per_page=15;
		if(is_numeric($start_row))
		{
			$start_row=$start_row;
		}
		else
		{
			$start_row=0;
		}
		$query=$this->admin_model->gettbl('tbladmin1','');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/admin/tbladmin1/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
		$data['query']=$this->admin_model->gettbl_limited('tbladmin1',$start_row,$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='tbladmin1';			
		$this->load->view('template',$data);    	   
    }
    public function frmadmin1()
    {
        $this->checklogin();
        $this->checkrole1();		
        $data['content']='frmadmin1'; 
        $this->load->view('template',$data);    
    }
    public function add_admin1()
    {
        $this->checklogin();
        $this->checkrole1();		
		$id=$this->input->post('id');          
        $pass =md5($this->input->post('matkhau')); 
        if($id > 0){
            $data=array(				
    			'taikhoan'	=>	$this->input->post('taikhoan'),				
                'hoten' =>  $this->input->post('hoten'),
                'diachi'     =>  $this->input->post('diachi'),	
    			'dienthoai' 		=>$this->input->post('dienthoai'),
                'email'     =>  $this->input->post('email'),
                'role'      =>  $this->input->post('quyen'),				
    			'status'    =>  $this->input->post('status'),																
    		); 	
        }else{
    		$data=array(				
    			'taikhoan'	=>	$this->input->post('taikhoan'),				
    			'matkhau'		=>	$pass,	
                'hoten' =>  $this->input->post('hoten'),
                'diachi'     =>  $this->input->post('diachi'),	
    			'dienthoai' 		=>$this->input->post('dienthoai'),
                'email'     =>  $this->input->post('email'),
                'role'      =>  $this->input->post('quyen'),				
    			'status'    =>  $this->input->post('status'),																
    		); 	
        }
        $this->admin_model->add_tbl('tbladmin1',$data,$id);   
        redirect('admin/tbladmin1');    
    }
    public function edit_admin1($id)
    {
        $this->checklogin();	
        $this->checkrole1();	
        $data['id']=$id;
        $data['content']='frmadmin1';
        $this->load->view('template',$data);    
    }
	public function frmthanhvien()
    {
		$this->checklogin();
        $this->checkrole1();		
        $data['content']='frmthanhvien'; 
        $this->load->view('template',$data);  
    }
	public function add_thanhvien()
    {        
		$this->checklogin();	
        $this->checkrole1();	
		$id=$this->input->post('id');       
		//Xử lý ảnh	
		$config['upload_path'] = './upload/avatar';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);
		
		if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0755, TRUE);
		}
		
		if ( ! $this->upload->do_upload('image'))
		{
			if($id==''){
				$image='';
				$thumb='';
			}
			else{
				$image=$this->input->post('image');				
				$thumb=$this->input->post('thumb');				
			}			
		}
		else
		{			
			$image_data = $this->upload->data();
			$name_img='upload/avatar/'.$image_data['file_name']; 			
			//Xoa file anh cu truoc khi upload
			$check_image = $this->admin_model->gettbl('tbladmin',$id)->row()->image;
			if($check_image != $name_img){						
				$check_image_thumb = $this->admin_model->gettbl('tbladmin',$id)->row()->thumb;
				unlink($check_image);
				unlink($check_image_thumb);
			}
			$duongdan=$image_data['file_name'];
			//Xu ly crop anh
			$temp=explode('.',$image_data['file_name']);
			$thumb='upload/avatar/'.$temp[0].'_thumb'.'.'.$temp[1];
			$image='upload/avatar/'.$image_data['file_name'];
			$imageThumb = new Image($name_img);			
			$thumb_path = $temp[0].'_thumb';			
			$imageThumb->resize(155,200,'crop');
			$imageThumb->save($thumb_path, './upload/avatar');							
		}	
		if($id==''){
			$pass = md5($this->input->post('pass'));
		}
		else{
			$pass = $this->input->post('pass');
		}
		$data=array(				
			'fullname'	=>	$this->input->post('fullname'),				
			'name'		=>	$this->input->post('name'),	
            'dienthoai' =>  $this->input->post('phone'),
            'email'     =>  $this->input->post('email'),	
			'pass' 		=>	$pass,				
			'status'    =>  $this->input->post('status'),
			'image'  	=>  $image,
			'thumb'		=>	$thumb													
		); 		
        $this->admin_model->add_tbl('tbladmin',$data,$id);   
        redirect('admin/tbladmin');
    }
	public function edit_thanhvien($id)
    {
		$this->checklogin();
        $this->checkrole1();		
        $data['id']=$id;
        $data['content']='frmthanhvien';
        $this->load->view('template',$data);    
    }	

	public function status()
	{
	   $this->checklogin();
	   $id=$_POST["id"];
	   $tblname=$_POST["tblname"];
	   $test = $this->admin_model->gettbl($tblname,$id)->row();
	   if($test->status==0){
			$this->admin_model->checkstatus($tblname,1,$id);
	   }
	   else{
			$this->admin_model->checkstatus($tblname,0,$id);
	   }	 	  
	}
	public function search_page()
    {
		$this->checklogin();
	    $data['status']=true;		
        $start_row=$this->uri->segment(3);
        $per_page=5;
        if(is_numeric($start_row)=='')
        {
            $start_row=0;
        }
        $query=$this->admin_model->gettbl('tblbaiviet','');
        $total_rows = $query->num_rows();
        $this->load->library('pagination');
        $config['base_url'] = site_url().'/admin/search_page/';
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $per_page;
        $config['uri_segment'] =3;
        $config['next_link'] = 'Tiếp';
        $config['prev_link'] = 'Sau';
        $config['num_links'] = 4;
        $config['first_link'] = 'Đầu';
        $config['last_link'] = 'Cuối';
        $config['full_tag_open']='<div class="pagination1">';
        $config['full_tag_close']='</div>';
        $this->pagination->initialize($config);
        $data['query']=$this->admin_model->gettbl_limited('tblbaiviet',$start_row, $per_page);
        $data['pagination1']= $this->pagination->create_links();
        $data['main_content']='ct-tab';
        $_html=$this->load->view('includes/ct-tab',$data,TRUE);
        echo $_html;
    }
	public function search_ajax()
	{	   
	   $data['ten']=$_POST['ten'];
	   $data['cat']=$_POST['cat'];
	   $html=$this->load->view('search_ajax',$data);
	   echo $html;
	}	
	
	public function del_thanhvien()
	{     
		$this->checklogin();		
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];
				$sql="select image,thumb from tbladmin where id=".$del_id;
				$query=$this->db->query($sql)->row();
				unlink($query->image);//xoa file
				unlink($query->thumb);//xoa file
				$result = $this->admin_model->del_tbl('tbladmin',$del_id);   			
			}
            if($result)
            {
                redirect('admin/tbladmin');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/tbladmin');
        }
	}
    public function del_admin1()
    {
        $this->checklogin();
        $this->checkrole1();		
        $checkbox=$_POST['checkbox'];                             
        $countcheck=count($checkbox);           
        if($countcheck!=0)
        {            
            for($i=0;$i<$countcheck;$i++)
            {
                $del_id = $checkbox[$i];				
				$result = $this->admin_model->del_tbl('tbladmin1',$del_id);   			
			}
            if($result)
            {
                redirect('admin/tbladmin1');        
            }                     
        }
        else
        { 
            echo 'Bạn phải chọn';
            redirect('admin/tbladmin1');
        }    
    }
    public function sitemap()
	{		
	   $doc = new DOMDocument("1.0","utf-8"); 
       $doc->formatOutput = true;
       $r = $doc->createElement("urlset" );
       $r->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
       $doc->appendChild( $r );
       $url = $doc->createElement("url" );
       $name = $doc->createElement("loc" );
       $name->appendChild(
            $doc->createTextNode('http://daotaovieclam.edu.vn/')
	   );
	   $url->appendChild($name);			
	   $changefreq = $doc->createElement( "changefreq" );
	   $changefreq->appendChild(
	       $doc->createTextNode('daily')
	   );
       $url->appendChild($changefreq);			
	   $priority = $doc->createElement( "priority" );
	   $priority->appendChild(
 			$doc->createTextNode('1.00')
	   );
	   $url->appendChild($priority);			
	   $r->appendChild($url);			              
        $this->db->where('status',1);
		$cate=$this->db->get('tblchuyenmuc');
		if($cate->num_rows()>0)
		{
			foreach($cate->result() as $row)
			{
			    $categoryad='';
                $categoryad=$this->admin_model->getcatlink($row->id);
				$url = $doc->createElement( "url" );
				
				$name = $doc->createElement( "loc" );
				$name->appendChild(
					$doc->createTextNode(site_url($categoryad.'-c'.$row->id.'.html'))
				);
				$url->appendChild($name);
				
				$changefreq = $doc->createElement( "changefreq" );
				$changefreq->appendChild(
					$doc->createTextNode('daily')
				);
				$url->appendChild($changefreq);
				
				$priority = $doc->createElement( "priority" );
				$priority->appendChild(
					$doc->createTextNode('1.00')
				);
				$url->appendChild($priority);
				
				$r->appendChild($url);
			}
		} 
        $this->db->where('status',1);
		$cate=$this->db->get('tblbaiviet');
		if($cate->num_rows()>0)
		{
			foreach($cate->result() as $row)
			{			   
				$url = $doc->createElement( "url" );
				
				$name = $doc->createElement( "loc" );
				$name->appendChild(
					$doc->createTextNode(site_url($row->alias.'-'.$row->id.'.html'))
				);
				$url->appendChild($name);
				
				$changefreq = $doc->createElement( "changefreq" );
				$changefreq->appendChild(
					$doc->createTextNode('daily')
				);
				$url->appendChild($changefreq);
				
				$priority = $doc->createElement( "priority" );
				$priority->appendChild(
					$doc->createTextNode('1.00')
				);
				$url->appendChild($priority);
				
				$r->appendChild($url);
			}
		}   		                    		          
		$doc->save("sitemap.xml");	
	}
	/*---------------------*/
	public function login()
    {
        $this->load->view('login_view');    
    }
    public function dologin()
    {       
        if($this->admin_model->getlogin($this->input->post('name'),$this->input->post('pass'))==TRUE)
        {
            $this->db->where('taikhoan',$this->input->post('name'));
            $sqladmin=$this->db->get('tbladmin1')->row();     
            $_SESSION['author'] = $sqladmin->id;
            $_SESSION['name']=$this->input->post('name'); 
            $_SESSION['quyen']=$sqladmin->role;            
            redirect('admin/index');            
        }   
        else
        {            
            redirect('admin/login');
        } 
    }
    public function thoat()
    {
        if(isset($_SESSION['name']))
        {
            unset($_SESSION['name']);
        }
        redirect('admin/login');
    }
    public function checklogin()
    {
        if(isset($_SESSION['name']))
        {            
        }
        else
        {
            redirect('admin/login');
        }
    }
	public function checkrole()
    {
        if(isset($_SESSION['name']))
        {      
			$this->db->where('status',1);
			$this->db->where('taikhoan',$_SESSION['name']);
			$admin=$this->db->get('tbladmin1')->row();	
			if($admin->role==1 or $admin->role==3 or $admin->role==4){				
			}
			else{
				redirect('admin/login');
			}
        }        
    }	
	public function checkrole1()
    {
        if(isset($_SESSION['name']))
        {      
			$this->db->where('status',1);
			$this->db->where('taikhoan',$_SESSION['name']);
			$admin=$this->db->get('tbladmin1')->row();	
			if($admin->role!=1){
				redirect('admin/login');
			}
        }        
    }		
	public function checkrole2()
    {
        if(isset($_SESSION['name']))
        {      
			$this->db->where('status',1);
			$this->db->where('taikhoan',$_SESSION['name']);
			$admin=$this->db->get('tbladmin1');	
			if($admin->row()->role==1 or $admin->row()->role==3 or $admin->row()->role==4){				
			}
			else{
				redirect('admin/login');
			}
        }        
    }	
	public function checkflag($id)
    {
        if(isset($_SESSION['name']))
        {      
			$this->db->where('id',$id);					
			$baiviet=$this->db->get('tblbaiviet')->row();	
				$this->db->where('status',1);
				$this->db->where('taikhoan',$_SESSION['name']);
				$admin=$this->db->get('tbladmin1')->row();
				$this->admin_model->flags($id,$admin->id,1);
        }        
    }	
	///////////////////////////////////
}
?>