﻿<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
?>
<h3 class="header">Nhập dữ liệu trang cần quét</h3>
<?php 
if(isset($error))
{
    echo '<div class="warning">'.$error.'</div>';
}
?>
<div class="content-inner">
	<form name="frmdata" action="<?php echo site_url('xu-ly-data.html'); ?>" method="post" enctype="multipart/form-data">
		<?php 
			if(isset($id))
			{
				$this->db->where('id',$id);
				$item=$this->db->get('site')->row();
				$id=$item->id;            
			}
		?>    
		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />				
		<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border-collapse:collapse" bordercolor="#cccccc">
		<tr>		
			<td width="30%"><strong>Tên mẫu</strong></td>
			<td><input type="text" name="name" value="<?php if(isset($id)) {echo $item->name;} ?>" /></td>
		</tr>
		
		<tr>		
			<td><strong>Website (*)</strong></td>
			<td><input type="text" name="host" value="<?php if(isset($id)) {echo $item->host;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Url (*)</strong></td>
			<td><input type="text" name="url" value="<?php if(isset($id)) {echo $item->url;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Mẫu bao ngoài một đối tượng (*)</strong></td>
			<td><input type="text" name="pattern_bound" value="<?php if(isset($id)) {echo $item->pattern_bound;} ?>" /></td>
		</tr>	
		<tr>		
			<td><strong>Extra</strong></td>
			<td><input type="text" name="extra" value="<?php if(isset($id)) {echo $item->extra;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Chèn vào bảng (*)</strong></td>
			<td><input type="text" name="table_name" value="<?php if(isset($id)) {echo $item->table_name;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Cỡ ảnh crop lớn</strong></td>
			<td><input type="text" name="image_size" value="<?php if(isset($id)) {echo $item->image_size;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Cỡ ảnh crop nhỏ 01</strong></td>
			<td><input type="text" name="image_size_small" value="<?php if(isset($id)) {echo $item->image_size_small;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Cỡ ảnh crop nhỏ 02</strong></td>
			<td><input type="text" name="image_size_small_01" value="<?php if(isset($id)) {echo $item->image_size_small_01;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Thư mục chứa ảnh gốc</strong></td>
			<td><input type="text" name="image_dir_base" value="<?php if(isset($id)) {echo $item->image_dir_base;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Vị trí ảnh trong bài viết</strong></td>
			<td><input type="text" name="image_pattern_base" value="<?php if(isset($id)) {echo $item->image_pattern_base;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Thư mục chứa ảnh đại diện</strong></td>
			<td><input type="text" name="image_dir" value="<?php if(isset($id)) {echo $item->image_dir;} ?>" /></td>
		</tr>		
		<tr>		
			<td><strong>Vị trí từ khóa</strong></td>
			<td><input type="text" name="tags" value="<?php if(isset($id)) {echo $item->tags;} ?>" /></td>
		</tr>	
		<tr>		
			<td><strong>Chuyên mục</strong></td>
			<td><?php 
				if(isset($id)){ $CI->admin_model->selectCtrl($item->category_id,'category_id', 'forFormDim');}
				else{
					$CI->admin_model->selectCtrl('','category_id', 'forFormDim');
				}
			?></td>
		</tr>
		<tr>		
			<td><strong>Page number</strong></td>
			<td><input type="text" name="page_num" value="<?php if(isset($id)) {echo $item->page_num;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Value Page</strong></td>
			<td><input type="text" name="value_page" value="<?php if(isset($id)) {echo $item->value_page;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Giới hạn tin quét</strong></td>
			<td><input type="text" name="limit" value="<?php if(isset($id)) {echo $item->limit;} ?>" /></td>
		</tr>
		<tr>		
			<td><strong>Tự động đăng</strong></td>
			<td>
				<p class="message_head">									
					<?php 
					 if(isset($id))
					 {							 
					?>
						<input class="news_checkbox" type="checkbox" name="auto" value="1" <?php if($item->auto==1): ?>checked="checked"<?php endif; ?> />Tự động				
						<?php         
					 }
						else
						{
						?>
							<input class="news_checkbox" type="checkbox" name="auto" value="1" />Tự động
						<?php             
					}
					?>			
				</p>
			</td>
		</tr>		
		<tr>		
			<td colspan="2">
				<input class="button" type="submit" name="submit" value="Nhập dữ liệu" />					
			</td>
		</tr>			
	</table>
	</form>
	<div class="clr"></div>
</div>
