<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">
          Nội dung SEO
      </h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                Nội dung SEO
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <?php 
                if(isset($error))
                {
                    echo '<div class="warning">'.$error.'</div>';
                }
            ?>
            <form name="frmmeta" action="<?php echo site_url('xu-ly-seo.html'); ?>" method="post" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header" style="display:table;">
                        <p class="text-end" style="display:table-cell;">
                            <a href="<?php echo site_url('admin'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                        </p>
                    </div>
                    <div class="card-body">
                        <?php 
                            if(isset($error))
                            {
                                echo '<div class="warning">'.$error.'</div>';
                            }
                    		if(isset($id))
                    		{
                    			$this->db->where('id',$id);
                    			$item=$this->db->get('tblmeta')->row();
                    			$id=$item->id;            
                    		}
                    		?>    
                		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />	
                    	<div class="row">
                    	    <div class="col-12">
                                <div class="form-group row">
                                    <label for="title" class="col-sm-12 control-label col-form-label"><strong>Tiêu đề</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="title" id="txt_title" value="<?php if(isset($id)){echo $item->title;} ?>" />
                                    </div>
                                </div>
                            </div>
                    	</div>
                    	<div class="row">
                    	    <div class="col-6">
                                <div class="form-group row">
                                    <label for="meta_description" class="col-sm-12 control-label col-form-label"><strong>Meta Description</strong></label>
                                    <div class="col-12">
                                        <textarea rows="5" cols="50" class="form-control" name="meta_description" id="meta_title"><?php if(isset($id)){echo $item->description;} ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row">
                                    <label for="keywords" class="col-sm-12 control-label col-form-label"><strong>Từ khoá</strong></label>
                                    <div class="col-12">
                                        <textarea rows="5" cols="50" class="form-control" name="keywords" id="keywords"><?php if(isset($id)){echo $item->keywords;} ?></textarea>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    	<div class="row">
                    	    <div class="col-12">
                    	        <div class="form-group row">
                    	            <label for="status" class="col-sm-12 control-label col-form-label"><strong>Trạng thái</strong></label>
                    	            <div class="col-12">
                    	                <?php 
                        				 if(isset($id)) {							 
                        				?>
                        					<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />
                        					<?php         
                        				 } else {
                        					?>
                    						<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" checked="checked" />
                        					<?php             
                        				}
                        				?>	
                        				<label class="form-check-label mb-0">Xuất bản</label>
                	                </div>
                	            </div>
                    	    </div>
                    	</div>
                    </div>       
                    <div class="card-footer">
                        <center>
                    		<?php 
                    			if(isset($id))
                    			{
                    			?>			
                				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Lưu thay đổi</button>										
                    			<?php    
                    			}
                    			else
                    			{
                    			?>			
                				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Nhập tin</button>					
                    			<?php 
                    			}
                    		?>
                    		<a href="<?php echo site_url('admin'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                		</center>
                    </div>
                </div>
            </form>        
        </div>
    </div>
</div>