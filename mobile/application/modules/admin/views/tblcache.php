﻿<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_data'); ?>">
<p class="sidebar"><a href="<?php echo site_url('admin/insert_data'); ?>">Chèn vào database</a> <input type="submit" name="submit" value="Xóa" /></p>
<?php 
    if($query->num_rows() >0)
    {
?>
<table width="100%">
    <tr class="title">
        <td width="2%" style="text-align:center;"><input type="checkbox" onclick="checkall('checkbox', this)" name="check"/></td>        
        <td width="20%">Tiêu đề</td>        
        <td width="8%">Chuyên mục</td>		       	        
        <td width="5%">Trạng thái</td>		       	        
    </tr>
    <?php 
        $stt=0;
		foreach($query->result() as $item)
        {
		$stt++;
		$catid=$CI->admin_model->gettbl('tblchuyenmuc',$item->catid)->row();	
    ?>
		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
			<td style="text-align:center;">
			<div id="request-form">
				<input type="checkbox" name="checkbox[]" class="checkbox" value="<?php echo $item->id; ?>" />
			</div>
			</td>								
			<td><a href="<?php echo site_url('admin/edit_cache/'.$item->id); ?>"><?php echo $item->title; ?></a></td>			
			<td><?php echo $catid->name; ?></td>	
			<td style="text-align:center;">
			<?php 
			  if($item->status=='1')
			  {
			  ?>
			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'news','tblcache')"><img src="images/toolbar/tick.png"></a>
			  <?php
			  }
			  else
			  {
			  ?>
			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'news','tblcache')"><img src="images/toolbar/publish_x.png"></a>
			  <?php
			  }
			  ?>		
			</td>							
		</tr>
    <?php 
    }
    ?>
</table>
<?php 
}
else
{
?>
<p>Dữ liệu đang cập nhật</p>
<?php    
}
?>
</form>
<div class="clr"></div>

