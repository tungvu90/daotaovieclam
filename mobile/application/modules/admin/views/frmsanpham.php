<link rel="stylesheet" type="text/css" href="css/uploadify.css" media="all" /> 
<script src="js/jquery.uploadify.min.js" type="text/javascript"></script>
<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
$tinh=$CI->admin_model->gettbl('tbltinh','');
$this->db->where('status',1);
$this->db->where('taikhoan',$_SESSION['name']);
$admin=$this->db->get('tbladmin1')->row();		
?>
<?php 
if(isset($error))
{
    echo '<div class="warning">'.$error.'</div>';
}
?>
<h3 class="header">Thêm mới sản phẩm</h3>
<div class="content-inner1">
	<form name="frmtintuc" action="<?php echo site_url('xu-ly-san-pham.html'); ?>" method="post" enctype="multipart/form-data">
		<?php 
			if(isset($id))
			{
				$this->db->where('id',$id);
				$item=$this->db->get('tblsanpham')->row();
				$id=$item->id;            
			}
		?>    
		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />
        <input type="hidden" name="nguoidang" value="<?php echo $item->nguoidang;?>" />
		<input type="hidden" name="uid" value="<?php if(isset($id)) { echo $item->uid; }
		else{			
			echo $admin->id;
		}
		?>" />
		<div class="gray">
		<table width="100%"><tr><td>		
		<table class="tab1">
		<tr>
			<td width="150"><strong>Tên sản phẩm</strong></td>
			<td><input type="text" name="title" value="<?php if(isset($id)) {echo htmlspecialchars($item->title);} ?>" /></td>
		</tr>
		<tr class="second">
			<td><strong>Đường dẫn thân thiện</strong></td>
			<td><input type="text" name="alias" value="<?php if(isset($id)){ echo $item->alias;} ?>" /></td>
		</tr>
		<tr>
			<td><strong>Chuyên mục 1</strong></td>
			<td><?php 
				if(isset($id)){ $CI->admin_model->selectCtrlsp($item->catid,'catid', 'forFormDim');}
				else{
					$CI->admin_model->selectCtrlsp('','catid', 'forFormDim');
				}
			?></td>	
		</tr>
        <tr class="second">
			<td><strong>Chuyên mục 2</strong></td>
			<td><?php 
				if(isset($id)){ $CI->admin_model->selectCtrlsp($item->ncatid,'ncatid1', 'forFormDim');}
				else{
					$CI->admin_model->selectCtrlsp('','ncatid1', 'forFormDim');
				}
			?></td>	
		</tr>
        <tr>
			<td><strong>Chuyên mục 3</strong></td>
			<td><?php 
				if(isset($id)){ $CI->admin_model->selectCtrlsp($item->ncatid1,'ncatid2', 'forFormDim');}
				else{
					$CI->admin_model->selectCtrlsp('','ncatid2', 'forFormDim');
				}
			?></td>	
		</tr>
        <tr>
            <td><strong>Tỉnh thành</strong></td>
            <td>
                <select name="city">
                    <option value="-1">--Chọn tỉnh thành--</option>
                    <?php 
                        if(isset($id))
                        {
                            if($tinh->num_rows() >0)
                            {
                                foreach($tinh->result() as $itemtinh)
                                {
                                    if($item->tinh==$itemtinh->id)
                                    {
                                    ?>
                                    <option selected="selected" value="<?php echo $itemtinh->id; ?>"><?php echo $itemtinh->tinh; ?></option>
                                    <?php  
                                    } 
                                    else
                                    {
                                    ?>
                                    <option value="<?php echo $itemtinh->id; ?>"><?php echo $itemtinh->tinh; ?></option>
                                    <?php    
                                    } 
                                }
                            }
                        }
                        else
                        {
                            if($tinh->num_rows() >0)
                            {
                                foreach($tinh->result() as $itemtinh)
                                {

                                    ?>
                                    <option value="<?php echo $itemtinh->id; ?>"><?php echo $itemtinh->tinh; ?></option>
                                    <?php   
                                }
                            }    
                        }
                    ?>
                </select>
            </td>
        </tr>				
        <tr>
            <td><strong>Giá</strong></td>
           	<td><input type="text" name="gia" value="<?php if(isset($id)){ echo $item->gia;} ?>" /></td>
        </tr>
        <tr>
            <td><strong>Giá khuyễn mại</strong></td>
           	<td><input type="text" name="giakm" value="<?php if(isset($id)){ echo $item->giakm;} ?>" /></td>
        </tr>
        <tr>
            <td><strong>Đơn vị tính</strong></td>
           	<td><input type="text" name="donvitinh" value="<?php if(isset($id)){ echo $item->donvitinh;} ?>" /></td>
        </tr>	
        <tr>
            <td><strong>Số lượng</strong></td>
            <td><input style="width:60px;" type="text" name="soluong" value="<?php if(isset($id)){ echo $item->soluong;} ?>" /></td>
        </tr>
        <tr class="second">
			<td><strong>Ảnh đại diện</strong></td>
			<td><?php if(isset($id)){?>
			<input type="hidden" name="image" style="width:250px;" value="<?php echo $item->image; ?>"> 
			<input type="hidden" name="thumb" value="<?php echo $item->thumb; ?>"><br /><br />
			<?php if($item->thumb !=''){?>
			<img src="<?php echo $item->thumb; ?>" width="250"><br />
			<?php }} 			
			?>
			<input type="file" name="image" value="" />
			</td>
		</tr>
        <tr>
            <td>ảnh  nhỏ</td>
            <td>
            <div class="row-field">
		<div class="field-content">
			<div id="portfolio" class="block-input-append">                   
                    <div class="title-clear"></div>
                    <div id="queue"></div>
                    <input id="file_upload" name="file_upload" type="file" multiple="true"/>                    
                     <ul id="sortable">  
                        <div class="ci-message"></div>
                        <?php 
                        if(!empty($item->image1)){
                        $images1 =json_decode($item->image1); 
                        if(is_array($images1) && count($images1)>0){
                            foreach ($images1 as $image1) {	
                            ?>
                                    <li>                                       
                                           <img class="img-thumb" width="131px"
                                               src="<?php echo  base_url().'tmp'.'/' . $image1; ?>" />                                   
                                                
                                        <input type="hidden" name="image_filename[]"
                                               value="<?php echo $image1 ?>" /> <br />			
                                       
                                        <a href="javascript:void(0)" class="remove" onclick="removeImage(this)" >Remove</a>
                                    </li>
                            <?php
                            }
                        }
                        }
                        ?>                        
                    </ul>
                </div>			
		</div>
	</div>
            </td>
        </tr>		
		</table>		
		</td>		
		<td valign="top" style="width: 250px">			
			<ul style="padding:4px; margin:0">	
			<?php if($admin->role==1 or $admin->role==3){ ?>
				<li>
					<p class="message_head">									
						<strong>Sản phẩm thuộc nhóm:</strong>
					</p>
				</li>
				<li>
				<p class="message_head">									
					<?php 
					 if(isset($id))
					 {							 
					?>
						<input class="news_checkbox" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />Xuất bản				
						<?php         
					 }
						else
						{
						?>
							<input class="news_checkbox" type="checkbox" name="status" value="1" />Xuất bản
						<?php             
					}
					?>		
                    <?php 
					 if(isset($id))
					 {							 
					?>
						<input class="news_checkbox" type="checkbox" name="home" value="1" <?php if($item->home==1): ?>checked="checked"<?php endif; ?> />Home				
						<?php         
					 }
						else
						{
						?>
							<input class="news_checkbox" type="checkbox" name="home" value="1" />Home
						<?php             
					}
					?>			
				</p>
                <p class="message_head">
                    <?php 
					 if(isset($id))
					 {							 
					?>
						<input class="news_checkbox" type="checkbox" name="top" value="1" <?php if($item->top==1): ?>checked="checked"<?php endif; ?> />Top				
						<?php         
					 }
						else
						{
						?>
							<input class="news_checkbox" type="checkbox" name="top" value="1" />Top
						<?php             
					}
					?>		
                </p>
				</li>				
				<?php } ?>
				<li>
				<p class="message_head">	
					<strong>Từ khóa tìm kiếm</strong>
					<p class="message_head">
						<cite>Từ khóa cách nhau bởi dấu phẩy ','</cite>
					</p>
					<textarea rows="3" cols="26" name="tags" /><?php if(isset($id)) {echo $item->tags;} ?></textarea>
				</p>
				</li>
				<li>
				<p class="message_head">
					<?php 
					if(isset($id)){
						$ngay=explode('-',$item->ngaydang);
						//$ngay=explode('-',$ngays[0]);
						$created_day=$ngay[2].'-'.$ngay[1].'-'.$ngay[0];	
						//$time=$ngays[1];
					}
                    else
                    {
                        $datesp=getdate();
                        //$ngays=explode('-',$datesp);
                        $created_day=$datesp['mday'].'-'.$datesp['mon'].'-'.$datesp['year'];
                    }
					?>
					<strong>Ngày đăng</strong><span class="timestamp"></span><br />
					<input id="created_day" type="text" name="created_day" value="<?php echo $created_day; ?>" />
				</p>
                <p class="message_head">
               	    <?php 
    					if(isset($id)){
    						$ngay1=explode('-',$item->ngayhethan);
    						$created_day1=$ngay1[2].'-'.$ngay1[1].'-'.$ngay1[0];	
    					}
                        else
                        {
                            $created_day1='';
                        }
					?>
                    <strong>Ngày hết hạn</strong><span class="timestamp"></span><br />
                    <input id="created_day1" type="text" name="created_day1" value="<?php echo $created_day1; ?>" />
                </p>
                </li>
			</ul>			
		</td>		
		</tr>        
		<tr>
            <td colspan="2">
                <strong style="display:block;padding-bottom:8px;">Thông tin chi tiết</strong>
                <textarea rows="5" cols="70" name="thongtinct" id="thongtinct"><?php if(isset($id)){ echo $item->noidung;} ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong style="display:block;padding-bottom:8px;">Điểm nổi bật</strong>
                <textarea rows="5" cols="70" name="diennb" id="diennb"><?php if(isset($id)){ echo $item->diemnoibat;} ?></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong style="display:block;padding-bottom:8px;">Điều kiện sử dụng</strong>
                <textarea rows="5" cols="70" name="dieukiensd" id="dieukiensd"><?php if(isset($id)){ echo $item->dieukiensudung;} ?></textarea>
            </td>
        </tr>
		</table>
		</div>
		<div class="gray">
			<table class="tab1">
                <tr>
					<td>
						<strong>Meta Title</strong>
					</td>
					<td>
						<textarea rows="5" cols="70" name="meta_title"><?php if(isset($id)){ echo $item->meta_title;} ?></textarea>
					</td>
				</tr>
                <tr>
					<td>
						<strong>Meta Description</strong>
					</td>
					<td>
						<textarea rows="5" cols="70" name="meta_des"><?php if(isset($id)){ echo $item->meta_des;} ?></textarea>
					</td>
				</tr>
                <tr>
					<td>
						<strong>Keyword</strong>
					</td>
					<td>
						<textarea rows="5" cols="70" name="keyword"><?php if(isset($id)){ echo $item->keyword;} ?></textarea>
					</td>
				</tr>
				<tr>
					<td>
						<strong>Thứ tự</strong>
					</td>
					<td>
						<input type="text" style="width:50px;" name="thutu" value="<?php if(isset($id)){ echo $item->thutu;} ?>" />
					</td>
				</tr>
			</table>
		</div>
		<div class="gray">
		<center>
		<?php 
			if(isset($id))
			{
			?>			
				<input class="button" type="submit" name="submit" value="Lưu thay đổi" />						
			<?php    
			}
			else
			{
			?>			
				<input class="button" type="submit" name="submit" value="Nhập sản phẩm" />
   	            <input class="button" type="reset" name="reset" value="Làm lại" />					
			<?php 
			}
		?>
		</center>
		</div>
	</form>
	<div class="clr"></div>
</div>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script >
<script type="text/javascript">
jQuery(function($){

	$.datepicker.regional['vi'] = {

		closeText: 'Đóng',

		prevText: '&#x3c;Trước',

		nextText: 'Tiếp&#x3e;',

		currentText: 'Hôm nay',

		monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',

		'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],

		monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',

		'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],

		dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],

		dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],

		dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],

		weekHeader: 'Tu',

		dateFormat: 'dd-mm-yy',

		firstDay: 0,

		isRTL: false,

		showMonthAfterYear: false,

		yearSuffix: ''};
		
	$.datepicker.setDefaults($.datepicker.regional['vi']);
});
$(function() {
	$( "#created_day" ).datepicker();
    $( "#created_day1" ).datepicker();
	$.datepicker.setDefaults($.datepicker.regional['vi']);
});
</script>
<!-- Tích hợp jck soạn thảo-->

<script type="text/javascript"> 
$(function() { 
	var editor = CKEDITOR.replace('thongtinct', { 
		filebrowserBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html"; ?>', 
		filebrowserImageBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Images";?>', 
		filebrowserFlashBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Flash" ?>', 
		filebrowserUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>', 
		filebrowserImageUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";?>', 
		filebrowserFlashUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";?>', 
		filebrowserWindowWidth : '800', 
		filebrowserWindowHeight : '480' 
	}); 
	CKFinder.setupCKEditor( editor, "<?php echo base_url().'ckfinder/'?>" ); 
	}) 
</script>
<script type="text/javascript"> 
$(function() { 
	var editor1 = CKEDITOR.replace('diennb', { 
		filebrowserBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html"; ?>', 
		filebrowserImageBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Images";?>', 
		filebrowserFlashBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Flash" ?>', 
		filebrowserUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>', 
		filebrowserImageUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";?>', 
		filebrowserFlashUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";?>', 
		filebrowserWindowWidth : '800', 
		filebrowserWindowHeight : '480' 
	}); 
	CKFinder.setupCKEditor( editor1, "<?php echo base_url().'ckfinder/'?>" ); 
	}) 
</script>
<script type="text/javascript"> 
$(function() { 
	var editor2 = CKEDITOR.replace('dieukiensd', { 
		filebrowserBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html"; ?>', 
		filebrowserImageBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Images";?>', 
		filebrowserFlashBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Flash" ?>', 
		filebrowserUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>', 
		filebrowserImageUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";?>', 
		filebrowserFlashUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";?>', 
		filebrowserWindowWidth : '800', 
		filebrowserWindowHeight : '480' 
	}); 
	CKFinder.setupCKEditor( editor2, "<?php echo base_url().'ckfinder/'?>" ); 
	}) 
</script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script >
<script type="text/javascript" src="js/mootools-core.js"></script >
<script type="text/javascript" src="js/mootools-more.js"></script >
<script type="text/javascript">
	 window.addEvent('domready', function(){
        reNewItem();
       
    });
	<?php $timestamp = time();?>
	$(function() {
		$('#file_upload').uploadify({
			'formData'      : {
				'timestamp' : '<?php echo $timestamp; ?>',
				'token'     : '<?php echo md5('unique_salt' . $timestamp); ?>',
				'sessionid' : '<?php echo session_id(); ?>'
			},
			'buttonText' 	: 'Chèn ảnh ...',
			'width'   		: 180,
            'auto'          : true,
            'multi'         : true,
			'swf'           : 'images/uploadify.swf',
			'uploader'      : '<?php echo base_url() ?>uploadify.php',
            'onProgress'   : function(file, e) {
            
            },
			'onUploadSuccess' : function(file, data, response) {
				// Et ici
               $("#gallery").load("ajax/gallery.php");
                  var data =  $.parseJSON(data);
                  var item = data.files;
                  if (data.success) {
              
                       var html = '<li>';                                   
                                    html += '<img class="img-thumb" width= "140px" src="<?php echo base_url() . 'tmp/'; ?>'+item.filename+'" />';
                                    html += '<input type="hidden" name="image_id[]" value="0" />';
                                    html += '<input type="hidden" name="image_filename[]" value="'+item.filename+'" /><br/>';				
                                    html +='<a href="javascript:void(0)" class="remove" onclick="removeImage(this)" >Remove</a>';
                           html += '</li>';
                              console.log(html); 
                     $('#sortable').append(html);
                     $('#sortable li:last-child ').find('a.edit').data('title', item.title);								 
                        reNewItem();
                               
                   
                   }
			} 
		});
	});
       
    function removeImage(el)
    {
   
    
        jQuery(el).parent().fadeOut(function(){
             jQuery('.ci-message').append('Delete photo').show().fadeOut();
            jQuery(this).remove();
       });
    }	 
     function reNewItem(){      
        new Sortables('#sortable', {
            clone: true,
            revert: true,
            opacity: 0.3
        });
    }
</script>