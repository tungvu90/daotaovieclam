<h3 class="header">Sửa đơn hàng</h3>
<?php 
if(isset($error))
{
    echo '<div class="warning">'.$error.'</div>';
}
?>
<div class="content-inner">
	<form name="frmfooter" action="<?php echo site_url('xu-ly-don-hang-web.html'); ?>" method="post" enctype="multipart/form-data">
		<?php 
			if(isset($id))
			{
				$this->db->where('id',$id);
				$item=$this->db->get('tbldonhangweb')->row();
				$id=$item->id;            
			}
		?>    
		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />
        <input type="hidden" name="idsp" value="<?php echo $item->sanpham; ?>" />	
		<div class="gray">
		<table class="tab1">
        <tr>
            <td><strong>Mã phiếu</strong></td>
            <td><input type="text" name="maphieu" disabled="disabled" value="<?php if(isset($id)){echo $item->maphieu;} ?>" />            
            </td>
        </tr>
        <tr>
            <td><strong>Hình thức thanh toán</strong></td>
            <td><input type="text" name="hinhthuc" value="<?php if(isset($id)){ echo $item->hinhthuc;} ?>" /></td>
        </tr>
        <tr>
            <td><strong>Họ tên</strong></td>
            <td><input type="text" name="hoten" value="<?php if(isset($id)){ echo $item->hoten;} ?>" /></td>    
        </tr>
        <tr>
            <td><strong>Điện thoại</strong></td>
            <td><input type="text" name="dienthoai" value="<?php if(isset($id)){ echo $item->dienthoai;} ?>" /></td>    
        </tr>
        <tr>
            <td><strong>Email</strong></td>
            <td><input type="text" name="email" value="<?php if(isset($id)){ echo $item->email;} ?>" /></td>    
        </tr>
        <tr>
            <td><strong>Cửa hàng</strong></td>
            <td>
                <select name="cuahang">
                    <option value="-1">--Chọn cửa hàng--</option>
                    <?php
                        $this->db->where('status',1);
                        $sqlcuahangor=$this->db->get('tbldiachi');
                        if($sqlcuahangor->num_rows()>0)
                        {
                            foreach($sqlcuahangor->result() as $itemcuahangor)
                            {
                                if(isset($id))
                                {
                                    if($itemcuahangor->id==$item->diachi)
                                    {
                                    ?>
                                    <option value="<?php echo $itemcuahangor->id; ?>" selected="selected"><?php echo $itemcuahangor->diachi; ?></option>
                                    <?php    
                                    }
                                    else
                                    {
                                    ?>
                                    <option value="<?php echo $itemcuahangor->id; ?>"><?php echo $itemcuahangor->diachi; ?></option>
                                    <?php    
                                    }
                                }
                                else
                                {
                                ?>
                                <option value="<?php echo $itemcuahangor->id; ?>"><?php echo $itemcuahangor->diachi; ?></option>
                                <?php
                                }    
                            }
                        } 
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td><strong>Địa chỉ nhận hàng</strong></td>
            <td><input type="text" name="diachinhanhang" value="<?php if(isset($id)){ echo $item->diachinhanhang;} ?>" /></td> 
        </tr>
        <tr>
            <td><strong>Yêu cầu thêm</strong></td>
            <td><textarea name="yeucauthem" cols="50" rows="10"><?php if(isset($id)){ echo $item->yeucauthem;} ?></textarea></td>    
        </tr>
        <tr>
            <td><strong>Sản phẩm</strong></td>
            <?php 
                $this->db->where('id',$item->sanpham);
                $sqlspdh=$this->db->get('tblsanpham')->row();
            ?>
            <td><input type="text" name="sanpham" value="<?php echo $sqlspdh->title; ?>" disabled="disabled" /></td>
        </tr>
        <tr>
            <td><strong>Người đăng</strong></td>
            <td>
            <?php 
            $this->db->where('id',$item->name);
            $sqlngdang=$this->db->get('tbladmin');
            if($sqlngdang->num_rows()==1)
            {
                $sqlngdang=$sqlngdang->row();    
                echo $sqlngdang->name;
            }
            else
            {
                echo 'Không đăng ký';
            }
            ?></td>
        </tr>		
        <tr>
			<td><strong>Số lượng</strong></td>
			<td>
		          <input style="width:30px;text-align: center;padding:3px;" type="text" name="soluong" value="<?php echo $item->soluong; ?>" />
			</td>
		</tr>        	        
        <tr><td width="200">
			<strong>Trạng thái</strong></td>
			<td>									
				<?php 
				 if(isset($id))
				 {							 
				?>
					<input class="news_checkbox" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />Đã duyệt				
					<?php         
				 }
					else
					{
					?>
						<input class="news_checkbox" type="checkbox" name="status" value="1" checked="checked" />Đã duyệt
					<?php             
				}
				?>							
		</td></tr>	
		</table>
		</div>
		<div class="gray">
		<center>
		<?php 
			if(isset($id))
			{
			?>			
				<input class="button" type="submit" name="submit" value="Lưu thay đổi" />						
			<?php    
			}
			else
			{
			?>			
				<input class="button" type="submit" name="submit" value="Nhập" />	
                <input class="button" type="reset" name="reset" value="Làm lại" />				
			<?php 
			}
		?>
		</center>
		</div>
	</form>
	<div class="clr"></div>
</div>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script >
<script type="text/javascript">
jQuery(function($){

	$.datepicker.regional['vi'] = {

		closeText: 'Đóng',

		prevText: '&#x3c;Trước',

		nextText: 'Tiếp&#x3e;',

		currentText: 'Hôm nay',

		monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',

		'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],

		monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',

		'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],

		dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],

		dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],

		dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],

		weekHeader: 'Tu',

		dateFormat: 'dd-mm-yy',

		firstDay: 0,

		isRTL: false,

		showMonthAfterYear: false,

		yearSuffix: ''};
		
	$.datepicker.setDefaults($.datepicker.regional['vi']);
});
$(function() {
	$("#created_day").datepicker();
});
