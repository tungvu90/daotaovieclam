<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
?>
<?php 
$this->db->where('status',1);
$this->db->where('id',$_SESSION['author']);
$admin=$this->db->get('tbladmin1');	
$admin=$admin->row(); 
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">
          Thông tin cá nhân
      </h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                Thông tin cá nhân
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header" style="display:table;">
                    <p class="text-end" style="display:table-cell;">
                        <a href="<?php echo site_url('admin'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                    </p>
                </div>
                <div class="card-body">
            		<input type="hidden" name="id" value="<?php echo $admin->id; ?>" />
            		<div class="row">
                	    <div class="col-6">
                            <div class="form-group row">
                                <label for="taikhoan" class="col-sm-12 control-label col-form-label"><strong>Tài khoản</strong></label>
                                <div class="col-12">
                                    <input type="text" class="form-control" name="taikhoan" value="<?php echo $admin->taikhoan; ?>" disabled/>
                                </div>
                            </div>
                        </div>
                	    <div class="col-6">
                            <div class="form-group row">
                                <label for="hoten" class="col-sm-12 control-label col-form-label"><strong>Họ tên</strong></label>
                                <div class="col-12">
                                    <input type="text" id="hoten" class="form-control" name="hoten" value="<?php echo $admin->hoten; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group row">
                                <label for="diachi" class="col-sm-12 control-label col-form-label"><strong>Địa chỉ</strong></label>
                                <div class="col-12">
                                    <input type="text" id="diachi" class="form-control" name="diachi" value="<?php echo $admin->diachi; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group row">
                                <label for="dienthoai" class="col-sm-12 control-label col-form-label"><strong>Điện thoại</strong></label>
                                <div class="col-12">
                                    <input type="text" id="dienthoai" class="form-control" name="dienthoai" value="<?php echo $admin->dienthoai; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group row">
                                <label for="email" class="col-sm-12 control-label col-form-label"><strong>Email</strong></label>
                                <div class="col-12">
                                    <input type="text" id="email" class="form-control" name="email" value="<?php echo $admin->email; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group row">
                                <label for="status" class="col-sm-12 control-label col-form-label"><strong>Trạng thái</strong></label>
                                <div class="col-12">
                    				<input disabled class="news_checkbox form-check-input" type="checkbox" name="status" value="1" <?php if($admin->status==1): ?>checked="checked"<?php endif; ?> />
                        			<label class="form-check-label mb-0">Kích hoạt</label>
                                </div>
                            </div>
                        </div>
                	</div>
                </div>
                <div class="card-footer">
                    <center>
        				<button class="btn btn-info btn-sm" type="submit" name="submit" id="update_profile"/><i class="mdi mdi-content-save"></i> Cập nhật</button>										
                		<a href="<?php echo site_url('admin'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
            		</center>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
       $('#update_profile').on('click',function(){
           var hoten = $("#hoten").val();
           var diachi = $("#diachi").val();
           var dienthoai = $("#dienthoai").val();
           var email = $("#email").val();
           if(isEmail(email)==false){
               toastr.error('Email không đúng định dạng','Thông báo');
               $("#email").focus();
               return false;
           }else{
               $.ajax({
                    url : "<?php echo site_url('admin/update_profile')?>",
                    type: "POST",
                    dataType: "JSON",
                    data:{hoten: hoten,diachi: diachi,dienthoai: dienthoai,email: email},
                    success: function(data){
                        toastr.success(data.success);
                    }
                }); 
           }
       });
    });
    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      if(!regex.test(email)) {
        return false;
      }else{
        return true;
      }
    }
</script>