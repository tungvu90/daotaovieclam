<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Text link</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Text link
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_link'); ?>">
                <div class="card">
                    <div class="card-header" style="display:table;">
                        <p class="text-end" style="display:table-cell;">
                            <a href="<?php echo site_url('admin/frmlink'); ?>" class="btn-sm btn btn-success text-white"><i class="fas fa-plus"></i> Thêm mới</a> 
                            <input type="submit" name="submit" value="Xóa" class="btn btn-danger btn-sm text-white" />
                        </p>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td><input type="checkbox" onclick="checkall('checkbox', this)" name="check" class="form-check-input"/></td>
                                    <td>Tiêu đề</td>        
                                    <td>Ảnh đại diện (Kích thước:312px x137px)</td>
                                    <td>Link liên kết</td>
                                    <td>Vị trí 1</td>
                                    <td>Vị trí 2</td>
                                    <td>Vị trí 3</td>
                                    <td>Trạng thái</td>
                            		<td>#ID</td>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php 
                                if($query->num_rows() >0)
                                {
                            	?>
                                <?php 
                                    $stt=0;
                            		foreach($query->result() as $item)
                                    {
                            		$stt++;
                                ?>
                            		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
                            			<td style="text-align:center;">
                            			<div id="request-form">
                            				<input type="checkbox" name="checkbox[]" class="checkbox form-check-input" value="<?php echo $item->id; ?>" />
                            			</div>
                            			</td>
                                        <td><a href="<?php echo site_url('admin/edit_link/'.$item->id.'.html'); ?>"><?php echo $item->title; ?></a></td>					
                            			<td><img width="150" src="<?php echo base_url().$item->image; ?>" width="300"></td>            						
                            			<td><?php echo $item->link; ?></td>
                                        <td>
                                        <?php              
                                        if($item->vitri1=='1')
                                        {
                                            echo 'Hiện thị';
                                        }
                                        else
                                        {
                                            echo 'Không hiện thị';    
                                        }
                                        ?>
                                        </td>
                                        <td>
                                        <?php 
                                        if($item->vitri2=='1')
                                        {
                                            echo 'Hiện thị';
                                        }
                                        else
                                        {
                                            echo 'Không hiện thị';    
                                        }
                                        ?></td>
                                        <td>
                                        <?php 
                                        if($item->vitri3=='1')
                                        {
                                            echo 'Hiện thị';
                                        }
                                        else
                                        {
                                            echo 'Không hiện thị';    
                                        }
                                        ?>
                                        </td>
                            			<td style="text-align:center;">						
                            			  <?php 
                            			  if($item->status=='1')
                            			  {
                            			  ?>
                            			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tbllink','link')"><img src="images/toolbar/tick.png"></a>
                            			  <?php
                            			  }
                            			  else
                            			  {
                            			  ?>
                            			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tbllink','link')"><img src="images/toolbar/publish_x.png"></a>
                            			  <?php
                            			  }
                            			  ?>			
                            			</td>       
                            			<td style="text-align:center;"><?php echo $item->id; ?></td>
                            		</tr>		
                                <?php 
                                }
                                ?>
                            	<?php 
                            }
                            else
                            {
                            ?>
                            <tr><td></td><tr>
                            <?php    
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="pagation">
                        	<?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>