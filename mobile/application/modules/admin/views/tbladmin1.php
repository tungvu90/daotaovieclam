<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true){
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}else{
			for(i=0;i<items.length;i++)
				items[i].checked=false;
		}
	}	
</script>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Administrators</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Administrators
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_admin1'); ?>">
                <div class="card">
                    <div class="card-header" style="display:table;">
                        <p class="text-end" style="display:table-cell;">
                            <a href="<?php echo site_url('admin/frmadmin1'); ?>" class="btn-sm btn btn-success text-white"><i class="fas fa-plus"></i> Thêm mới</a> 
                            <input type="submit" name="submit" value="Xóa" class="btn btn-danger btn-sm text-white" />
                        </p>
                    </div>
                    <div class="card-body" style="overflow-x: scroll;width: 99%;">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" class="form-check-input" onclick="checkall('checkbox', this)" name="check"/></th>        
                                        <th>#ID</th>
                                        <th>Tài khoản</th>
                                		<th>Họ tên</th>  
                                        <th>Điện thoại</th>
                                        <th>Email</th>
                                        <th>Quyền</th>
                                        <th>Trạng thái</th>
                                        <th>Thao tác</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        if($query->num_rows() >0)
                                        {
                                    ?>
                                        <?php 
                                            $stt=0;
                                    		foreach($query->result() as $item)
                                            {
                                    		$stt++;			
                                        ?>
                                    		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
                                    			<td>
                                    			<div id="request-form">
                                    				<input type="checkbox" name="checkbox[]" class="checkbox form-check-input" value="<?php echo $item->id; ?>" />
                                    			</div>
                                    			</td>		
                                    			<td><?php echo $item->id; ?></td>
                                    			<td><a href="<?php echo site_url('admin/edit_admin1/'.$item->id); ?>"><?php echo $item->taikhoan; ?></a></td>			
                                                <td><?php echo $item->hoten; ?></td>
                                                <td><?php echo $item->dienthoai; ?></td>
                                                <td><?php echo $item->email; ?></td>
                                                <td>
                                                <?php 
                                                    $this->db->where('id',$item->role);
                                                    $sqlquyen=$this->db->get('tblrole');
                                                    if($sqlquyen->num_rows()>0)
                                                    {
                                                        echo $sqlquyen=$sqlquyen->row()->name;
                                                    }
                                                ?>
                                                </td>						
                                    			<td class="text-center">						
                                    			  <?php 
                                    			  if($item->status=='1'){
                                    			  ?>
                                    			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tbladmin1','tbladmin1')"><img src="images/toolbar/tick.png"></a>
                                    			  <?php
                                    			  }else{
                                    			  ?>
                                    			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tbladmin1','tbladmin1')"><img src="images/toolbar/publish_x.png"></a>
                                    			  <?php
                                    			  }
                                    			  ?>			
                                    			</td>   
                                    			<td width="20%">
                                    			    <a title="Sửa" href="<?php echo site_url('admin/edit_admin1/'.$item->id); ?>" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                    			    <a title="Reset mật khẩu" href="<?php echo site_url('admin/reset_password/'.$item->id); ?>" class="btn btn-info btn-sm"><i class="mdi mdi-account-edit"></i></a>
                                			    </td>
                                    		</tr>
                                        <?php 
                                        }
                                    }
                                    else
                                    {
                                    ?>
                                    <tr>
                                    	<td></td>						
                                    	<td></td>						
                                    	<td></td>						
                                    	<td></td>						
                                    	<td></td>						
                                    	<td></td>						
                                    </tr>
                                    <?php    
                                    }
                                    ?>
                            </tbody>
                            </table>
                    </div>
                    <div class="card-footer">
                        <div class="pagation">
                        	<?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="myAlert" class="modal hide in" aria-hidden="false">

          <div class="modal-header">

            <button data-dismiss="modal" class="close" type="button">×</button>

            <h3>Alert modal</h3>

          </div>

          <div class="modal-body">

            <p>Lorem ipsum dolor sit amet...</p>

          </div>

          <div class="modal-footer"> <a data-dismiss="modal" class="btn btn-primary" href="#">Confirm</a> <a data-dismiss="modal" class="btn" href="#">Cancel</a> </div>

        </div>