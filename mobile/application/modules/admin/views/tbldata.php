﻿<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#frmdata').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<form name="frmdata" id="frmdata" method="post" action="<?php echo site_url('admin/loading_data'); ?>">
<p class="sidebar"><a href="<?php echo site_url('admin/frmget_data'); ?>">Thêm mới</a> <a href="<?php echo site_url('admin/tblcache'); ?>">Dữ liệu quét</a> <input type="submit" name="submit" value="Quét tin" /></p>
<?php 
    if($query->num_rows() >0)
    {
?>
<table width="100%">
    <tr class="title">
        <td width="3%" style="text-align:center;"><input type="checkbox" onclick="checkall('checkbox', this)" name="check"/></td>        
        <td width="18%">Name</td>     
		<td width="15%">Host</td>
        <td>Url</td>		
		<td width="10%">Ảnh crop</td>			
        <td width="10%">Ảnh gốc</td>		   	
        <td width="8%">Chuyên mục</td>		
        <td width="5%">Tự đăng</td>		
        <td width="5%">Số trang</td>		
        <td width="8%"></td>		
       	        
    </tr>
    <?php 
        $stt=0;
		foreach($query->result() as $item)
        {
		$stt++;
		$catid=$CI->admin_model->gettbl('tblchuyenmuc',$item->category_id)->row();	
    ?>
		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
			<td style="text-align:center;">
			<div id="request-form">
				<input type="checkbox" name="checkbox[]" class="checkbox" value="<?php echo $item->id; ?>" />
			</div>
			</td>					
			<td><a href="<?php echo site_url('admin/editfrm_data/'.$item->id); ?>"><?php echo $item->name; ?></a><a href="<?php echo site_url('admin/edit_frmget_data_item/'.$item->id); ?>"> [ chi tiết ]</a></td>					
			<td><?php echo $item->host; ?></td>
			<td><?php echo $item->url; ?></td>		
			<td><?php echo $item->image_dir; ?></td>				
			<td><?php echo $item->image_dir_base; ?></td>
			<td><?php echo $catid->name; ?></td>
			<td style="text-align:center;"><?php echo $item->auto==1?'Có':'Không'; ?></td>				     		
			<td style="text-align:center;"><?php echo $item->page_num ; ?></td>				     		
			<td style="text-align:center;"><a href="<?php echo site_url('admin/copyfrm_data/'.$item->id); ?>">Copy</a> - <a href="<?php echo site_url('admin/delfrm_data/'.$item->id); ?>">Xóa</a></td>				     		
		</tr>
    <?php 
    }
    ?>
</table>
<?php 
}
else
{
?>
<p>Dữ liệu đang cập nhật</p>
<?php    
}
?>
</form>
<div class="clr"></div>

