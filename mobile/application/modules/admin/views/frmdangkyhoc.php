<h3 class="header">Đăng ký học</h3>
<?php 
if(isset($error))
{
    echo '<div class="warning">'.$error.'</div>';
}
?>
<div class="content-inner">
	<form name="frmslider" action="<?php echo site_url('admin/add_dangkyhoc'); ?>" method="post" enctype="multipart/form-data">
		<?php 
			if(isset($id))
			{
				$this->db->where('id',$id);
				$sqlcheck=$this->db->get('tbldangkyhoc')->row();
				$id=$sqlcheck->id;            
			}
		?>    
		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />	
		<div class="gray">
		<table class="tab5">
        <tr><td width="200">
			<strong>Họ tên</strong></td>
			<td>
			<input type="text" name="hoten" value="<?php if(isset($id)) { echo $sqlcheck->hoten; }; ?>" />
		</td></tr>		
		<tr><td width="200">
			<strong>Địa chỉ</strong></td>
			<td>
			<input type="text" name="diachi" value="<?php if(isset($id)) { echo $sqlcheck->diachi; }; ?>" />
		</td></tr>
        <tr><td width="200">
			<strong>Điện thoại</strong></td>
			<td>
			<input type="text" name="dienthoai" value="<?php if(isset($id)) { echo $sqlcheck->dienthoai; }; ?>" />
		</td></tr>
        <tr><td width="200">
			<strong>Email</strong></td>
			<td>
			<input type="text" name="email" value="<?php if(isset($id)) { echo $sqlcheck->email; }; ?>" />
		</td></tr>
        <tr><td width="200">
			<strong>Số lượng</strong></td>
			<td>
			<input type="text" name="soluong" value="<?php if(isset($id)) { echo $sqlcheck->soluong; }; ?>" />
		</td></tr>
        <tr><td width="200">
			<strong>Môn học</strong></td>
			<td>
			<input type="text" name="monhoc" value="<?php if(isset($id)) { echo $sqlcheck->monhoc; }; ?>" />
		</td></tr>
        <tr><td width="200">
			<strong>Thời gian</strong></td>
			<td>
			<input type="text" name="thoigian" value="<?php if(isset($id)) { echo $sqlcheck->thoigian; }; ?>" />
		</td></tr>
        <tr style="background:none !important;">
            <td><strong>Nội dung khác</strong></td>
            <td><textarea name="noidungkhac" id="noidungkhac" cols="92" rows="10"><?php if(isset($id)){echo $sqlcheck->noidung;} ?></textarea></td>
        </tr>
        <tr><td width="200">
			<strong>Thứ tự</strong></td>
			<td>
			<input style="width:70px;" type="text" name="ordernum" value="<?php if(isset($id)) { echo $sqlcheck->thutu; }; ?>" />
		</td></tr>
		<tr><td width="200">
			<strong>Trạng thái</strong></td>
			<td>
			<p class="message_head">									
					<?php 
					 if(isset($id))
					 {							 
					?>
						<input class="news_checkbox" type="checkbox" name="status" value="1" <?php if($sqlcheck->status==1): ?>checked="checked"<?php endif; ?> />Xuất bản				
						<?php         
					 }
						else
						{
						?>
							<input class="news_checkbox" type="checkbox" name="status" value="1" />Xuất bản
						<?php             
					}
					?>			
				</p>
		</td></tr>
		</table>
		</div>
		<div class="gray">
		<center>
		<?php 
			if(isset($id))
			{
			?>			
				<input class="button" type="submit" name="submit" value="Lưu thay đổi" />						
			<?php    
			}
			else
			{
			?>			
				<input class="button" type="submit" name="submit" value="Nhập tin" />					
			<?php 
			}
		?>
		</center>
		</div>
	</form>
	<div class="clr"></div>
</div>
<script type="text/javascript">
function loadEditor()
			{

				var getId=document.getElementById("noidungkhac");

				if(getId!=null)

				{

					CKEDITOR.replace( 'noidungkhac',

					{

						skin : 'kama',

						uiColor: '#44B8C4',

						width: 700,

						filebrowserBrowseUrl : '/ckfinder/ckfinder.html',

						filebrowserImageBrowseUrl : '/ckfinder/ckfinder.html?Type=Images',

						filebrowserFlashBrowseUrl : '/ckfinder/ckfinder.html?Type=Flash',

						filebrowserUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',

						filebrowserImageUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

						filebrowserFlashUploadUrl : '/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',

						toolbar: [

						

						['Source','-','Save','NewPage','Preview','-','Templates'],

    ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],

    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],

    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],

    '/',

    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],

    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],

    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],

    ['BidiLtr', 'BidiRtl'],

    ['Link','Unlink','Anchor'],

    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe','FLVPlayer'],

    '/',

    ['Styles','Format','Font','FontSize'],

    ['TextColor','BGColor'],

    ['Maximize', 'ShowBlocks','-','About']



						]
					} );																									
				}

			}

			

		 	window.onload=loadEditor;	
</script>