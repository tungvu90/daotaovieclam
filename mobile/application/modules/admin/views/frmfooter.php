<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">
          Nội dung chân trang
      </h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                Nội dung chân trang
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form name="frmfooter" action="<?php echo site_url('admin/add_footer'); ?>" method="post" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header" style="display:table;">
                        <p class="text-end" style="display:table-cell;">
                            <a href="<?php echo site_url('admin'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                        </p>
                    </div>
                    <div class="card-body">
                        <?php 
                            if(isset($error)) {
                                echo '<div class="warning">'.$error.'</div>';
                            }
                			if(isset($id)) {
                				$this->db->where('id',$id);
                				$item=$this->db->get('tblfooter')->row();
                				$id=$item->id;            
                			}
                		?>    
                		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />	
                		<div class="row">
                		    <div class="col-6">
                		        <div class="form-group row">
        		                    <label for="giaychungnhan" class="col-sm-12 control-label col-form-label"><strong>Giấy chứng nhận</strong></label>
        		                    <div class="col-sm-12">
        		                        <input class="form-control" type="text" name="giaychungnhan" value="<?php if(isset($id)){ echo $item->giaychungnhan;}?>" />
    		                        </div>
    		                    </div>
            		        </div>
            		        <div class="col-6">
            		            <div class="form-group row">
        		                    <label for="tencongty" class="col-sm-12 control-label col-form-label"><strong>Tên công ty</strong></label>
        		                    <div class="col-sm-12">
        		                        <input class="form-control" type="text" name="tencongty" value="<?php if(isset($id)){ echo $item->tencongty;}?>" />
    		                        </div>
    		                    </div>
            		        </div>
            		    </div>
            		    <div class="row">
                            <div class="col-6">
            		            <div class="form-group row">
        		                    <label for="diachi" class="col-sm-12 control-label col-form-label"><strong>Địa chỉ</strong></label>
        		                    <div class="col-sm-12">
        		                        <input class="form-control" type="text" name="diachi" value="<?php if(isset($id)){ echo $item->diachi;}?>" />
    		                        </div>
    		                    </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row">
        		                    <label for="dienthoai" class="col-sm-12 control-label col-form-label"><strong>Điện thoại</strong></label>
        		                    <div class="col-sm-12">
        		                        <input class="form-control" type="text" name="dienthoai" value="<?php if(isset($id)){ echo $item->dienthoai;}?>" />
    		                        </div>
    		                    </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group row">
        		                    <label for="hotline" class="col-sm-12 control-label col-form-label"><strong>Hotline</strong></label>
        		                    <div class="col-sm-12">
        		                        <input class="form-control" type="text" name="hotline" value="<?php if(isset($id)){ echo $item->hotline;}?>" />
    		                        </div>
    		                    </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row">
        		                    <label for="email" class="col-sm-12 control-label col-form-label"><strong>Email</strong></label>
        		                    <div class="col-sm-12">
        		                        <input class="form-control" type="text" name="email" value="<?php if(isset($id)){ echo $item->email;}?>" />
    		                        </div>
    		                    </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group row">
        		                    <label for="toado" class="col-sm-12 control-label col-form-label"><strong>Tọa độ</strong></label>
        		                    <div class="col-sm-12">
        		                        <input class="form-control" type="text" name="toado" value="<?php if(isset($id)){ echo $item->toado;}?>" />
    		                        </div>
    		                    </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row">
        		                    <label for="fanpage" class="col-sm-12 control-label col-form-label"><strong>Facebook Fanpage</strong></label>
        		                    <div class="col-sm-12">
        		                        <input class="form-control" type="text" name="fanpage" value="<?php if(isset($id)){ echo $item->fanpage;}?>" />
    		                        </div>
    		                    </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
        		                    <label for="googleantic" class="col-sm-12 control-label col-form-label"><strong>Google analytics</strong></label>
        		                    <div class="col-sm-12">
        		                        <textarea class="form-control" name="googleantic" cols="92" rows="10"><?php if(isset($id)){echo $item->googleantic;} ?></textarea>
    		                        </div>
    		                    </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="banner" class="col-sm-12 control-label col-form-label"><strong>Banner</strong></label>
                                    <div class="col-sm-12">
                                        <?php 
                                            if(isset($id)) {
                                            ?>
                                            <input type="hidden" name="banner_he" value="<?php echo $item->banner; ?>" />
                                            <input type="file" name="banner" value="" /><br />
                                            <img src="<?php echo $item->banner; ?>" />
                                            <?php    
                                            }
                                            else {
                                            ?>
                                            <input type="file" name="banner" value="" />
                                            <?php 
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="logo" class="col-sm-12 control-label col-form-label"><strong>Logo mobile</strong></label>
                                    <div class="col-sm-12">
                                        <?php 
                                            if(isset($id)) {
                                            ?>
                                            <input type="hidden" name="logo_he" value="<?php echo $item->logo; ?>" />
                                            <input type="file" name="logo" value="" /><br />
                                            <img src="<?php echo $item->logo; ?>" />
                                            <?php    
                                            }
                                            else {
                                            ?>
                                            <input type="file" name="logo" value="" />
                                            <?php 
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="theh" class="col-sm-12 control-label col-form-label"><strong>Thẻ Heading</strong></label>
                                    <div class="col-sm-12">
                                        <textarea name="theh" id="theh" class="form-control"><?php if(isset($id)){echo $item->theh;} ?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="trai" class="col-sm-12 control-label col-form-label"><strong>Banner trái</strong></label>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <?php 
                                                if(isset($id))
                                                {
                                                ?>
                                                <div class="col-3">
                                                    <div class="form-group">
                                                        <input type="file" name="trai" value="" />
                                                        <input type="hidden" name="trai_thumb" value="<?php echo $item->anhtrai; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-9">
                                                    <div class="form-group">
                                                        <input class="news_checkbox form-check-input" type="checkbox" name="vitri1" value="1" <?php if($item->vitri1==1): ?>checked="checked"<?php endif; ?> />
                                                        <label class="form-check-label mb-0">Hiện thị</label>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <img src="<?php echo $item->anhtrai; ?>" height="100" style="width:unset;" />
                                                    </div>
                                                </div>
                                                <?php 
                                                }
                                                else
                                                {
                                                ?>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <input type="checkbox" class="form-check-input" name="vitri1" value="1" />
                                                        <label class="form-check-label mb-0">Hiện thị</label>
                                                    </div>
                                                </div>
                                                <?php    
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="linktrai" class="col-sm-12 control-label col-form-label"><strong>Link banner trái</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="linktrai" value="<?php if(isset($id)){ echo $item->linktrai;} ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <label for="phai" class="col-sm-12 control-label col-form-label"><strong>Banner phải</strong></label>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <?php 
                                            if(isset($id))
                                            {
                                            ?>
                                            <div class="col-3">
                                                <div class="form-group">
                                                    <input type="file" name="phai" value="" />
                                                    <input type="hidden" name="phai_thumb" value="<?php echo $item->anhphai; ?>" />
                                                </div>
                                            </div>
                                            <div class="col-9">
                                                <div class="form-group">
                                                    <input class="news_checkbox form-check-input" type="checkbox" name="vitri2" value="1" <?php if($item->vitri2==1): ?>checked="checked"<?php endif; ?> />
                                                    <label class="form-check-label mb-0">Hiện thị</label>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <img src="<?php echo $item->anhphai; ?>" height="100" style="width:unset;" />
                                                </div>
                                            </div>
                                            <?php 
                                            }
                                            else
                                            {
                                            ?>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <input type="checkbox" class="form-check-input" name="vitri2" value="1" />
                                                    <label class="form-check-label mb-0">Hiện thị</label>
                                                </div>
                                            </div>
                                            <?php    
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="linkphai" class="col-sm-12 control-label col-form-label"><strong>Link banner phải</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="linkphai" value="<?php if(isset($id)){ echo $item->linkphai;} ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="status" class="col-sm-12 control-label col-form-label"><strong>Trạng thái</strong></label>
                                    <div class="col-12">
                                        <?php 
                        				 if(isset($id))
                        				 {							 
                        				?>
                        					<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />				
                        					<?php         
                        				 }
                        					else
                        					{
                        					?>
                        						<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" checked="checked" />
                        					<?php             
                        				}
                        				?>	
                        				<label class="form-check-label mb-0">Xuất bản</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <center>
                    		<?php 
                    			if(isset($id))
                    			{
                    			?>			
                    				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Lưu thay đổi</button>										
                    			<?php    
                    			}
                    			else
                    			{
                    			?>			
                    				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Nhập tin</button>					
                    			<?php 
                    			}
                    		?>
                    		<a href="<?php echo site_url('admin'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                		</center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
CKEDITOR.replace('theh');
</script>