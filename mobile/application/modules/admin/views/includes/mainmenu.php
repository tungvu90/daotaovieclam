﻿<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
$thongtinseo=$CI->admin_model->gettbl('tblmeta','')->row();
$footer=$CI->admin_model->gettbl('tblfooter','')->row();	
$this->db->where('status',1);
$this->db->where('taikhoan',$_SESSION['name']);
$admin=$this->db->get('tbladmin1')->row();	
?>
<div class="navbar-inner">
	<ul id="menu_ad">
		<li><a href="<?php echo site_url('admin/'); ?>">Quản trị</a></li>  
            <?php 
            if($admin->role==1)
            {
            ?>      
            <li><a href="<?php echo site_url('admin/tbladmin1'); ?>" title="administrators">Administrators</a></li> 
            <?php 
            }
            ?>          
    		<li><a style="cursor: pointer;">Quản lý bài viết</a>
                <ul class="sub_menu">
                    <?php 
                        if($admin->role==1)
                        {
                        ?>
                        <li><a href="<?php echo site_url('admin/chuyenmuc/'); ?>">Chuyên mục</a></li>
                        <?php 
                        }
                                                
                        if($admin->role==1 || $admin->role==3 || $admin->role==4)
                        {
                        ?> 
                    ?>
                    <li><a href="<?php echo site_url('admin/baiviet/'); ?>">Bài viết</a></li>
                    <?php 
                    }
                    ?>
                </ul>
            </li>
            <?php             
            if($admin->role==1)
            {
            ?>
            <li><a href="<?php echo site_url('admin/slider'); ?>" title="Slider">Slider</a></li>
            <li><a href="<?php echo site_url('admin/support'); ?>" title="Hỗ trợ trực tuyến">Hỗ trợ trực tuyến</a></li>
            <li><a href="<?php echo site_url('admin/doitac'); ?>" title="Đối tác">Đối tác</a></li>
            <li><a href="<?php echo site_url('admin/album'); ?>" title="Album ảnh">Album ảnh</a></li>
            <li><a href="<?php echo site_url('admin/quangcao'); ?>" title="Quảng cáo">Quảng cáo</a></li>
            <li><a href="<?php echo site_url('admin/link'); ?>" title="Text Link">Text Link</a></li> 
            <li><a href="<?php echo site_url('admin/dangkyhoc'); ?>" title="Đăng ký học">Đăng ký học</a></li>                        					
    		<li><a style="cursor: pointer;">Other</a>
                <ul class="sub_menu">
                    <li><a href="<?php if (count($thongtinseo)==0){echo site_url('them-thong-tin-seo.html');}
    		         else{
    		             echo site_url('admin/edit_seo/'.$thongtinseo->id);
    	          	}
                    ?>">Thông tin seo</a></li>
                    <li><a href="<?php if (count($footer)==0){echo site_url('them-footer.html');}
    		         else{
    		             echo site_url('admin/edit_footer/'.$footer->id);
    	          	}
                    ?>">Thông tin công ty</a></li>
                    <li><a href="<?php echo site_url('admin/lienhe'); ?>" title="Thông tin liên hệ">Thông tin liên hệ</a></li>
                </ul>
            </li>
            <?php 
            }
            ?>
        	
	</ul>
    <script type="text/javascript">
        $(document).ready(function(){                            
            $('#menu_ad > li').hover(function(){
                $(this).children('.sub_menu').css('display','block');                       
                },function(){
                    $(this).children('.sub_menu').css('display','none');    
                });      
            });
        </script>
</div>
