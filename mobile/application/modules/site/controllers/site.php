<?php 
class Site extends MX_Controller
{
	public function __construct()
	{
		parent::__construct(); 		
		$this->load->model('site_model');	
        $this->load->model('admin/admin_model');
        $this->load->helper('images');	
        $this->load->library('form_validation');        	
	}   
	public function index()
    { 
        $this->sitemap();
        //$this->check_cookie();
		$data['home'] = true;		
		$data['active'] ='';
        $data['content']='includes/main_content';
        $this->load->view('includes/template',$data);
    }
    public function lienhe()
    {
        $this->load->helper('map');
        $data['header_title']='Liên hệ';
		$data['content']='lienhe';
        $data['contact']=true;			
		$this->load->view('includes/template',$data); 
    }
    public function docontact()
    {
        $this->load->helper('map');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txthoten','<span style="color:red;">Họ tên</span>','required');    
        $this->form_validation->set_rules('txtdc','<span style="color:red;">Địa chỉ</span>','required'); 
        $this->form_validation->set_rules('txtdt','<span style="color:red;">Điện thoại</span>','required'); 
        $this->form_validation->set_rules('txtemail','<span style="color:red;">Email</span>','required|valid_email');
        $this->form_validation->set_message('required','<span style="color:red;">%s không để trống</span>'); 
        $this->form_validation->set_message('valid_email','<span style="color:red;">%s không hợp lệ</span>'); 
        if($this->form_validation->run()==FALSE)
        {            
            $data['header_title']='Liên hệ';
            $data['errors_register'] = validation_errors();
		    $data['content']='lienhe';
            $data['contact']=true;			
		    $this->load->view('includes/template',$data);         
        }  
        else
        {
            $date = getdate();
    		$sDate = $date['year'].'-'.$date['mon'].'-'.$date['mday'];
    		$insert_data = array(
				'hoten'	=> $this->input->post('txthoten'),
				'diachi'	=> $this->input->post('txtdc'),
				'dienthoai'	=> $this->input->post('txtdt'),
				'email'	=>	$this->input->post('txtemail'),
				'noidung'	=> $this->input->post('txtnd'),
				'ngaylienhe'	=> $sDate,
				'status'	=>0
            ); 
            $this->db->insert('tbllienhe',$insert_data);
            $data['header_title']='Liên hệ';
            $data['contact']=true;
            $data['kq']=true; 
            $data['content']='lienhe';			
		    $this->load->view('includes/template',$data);   
        }
    } 
    public function emailgiamgia()
    {
        $mailsale=$_POST['mailsale'];    
        require_once('class.phpmailer.php'); 
        require_once('class.pop3.php'); 
        define('GUSER', 'contact@buonchung.com');         
        define('GPWD', 'cogangtopweb@2012');  
        $this->db->where('status',1);
        $this->db->where('gia !=',0);
        $this->db->where('giakm !=',0);
        $this->db->order_by('id','random');
        $this->db->limit(1);
        $sqlsanpham=$this->db->get('tblsanpham')->row();
        $noidung1='<p><b>Thông tin Email giảm giá từ website buonchung.com</b></p>
        <table>
            <tr>                              
               <td> 
                    <p><strong>Tên sản phẩm:&nbsp;'.$sqlsanpham->title.'</strong></p>    
                    <p>Giá gốc:&nbsp;'.number_format($sqlsanpham->gia,0,'.','.').'&nbsp;'.$sqlsanpham->donvitinh.'</p>
                    <p>Giá khuyễn mại:&nbsp;'.number_format($sqlsanpham->giakm,0,'.','.').'&nbsp;'.$sqlsanpham->donvitinh.'</p>
               </td>
            </tr>
        </table>
        <div id="noidung_em">
            <p><b>Thông tin chi tiết</b></p>
            '.$sqlsanpham->noidung.' 
            <div style="clear:both;"></div>   
        </div>
        ';             
        global $message;    
        $this->smtpmailer($mailsale, "contact@buonchung.com", "Buôn chung", "Thông tin Email giảm giá từ website buonchung.com",$noidung1);
        echo $message;
    }
    public function deal()
    {
        $start_row=$this->uri->segment(3);
		$per_page=16;
		if(is_numeric($start_row)){
			$start_row=$start_row;					
		}				    		
		else
		{
			$start_row=0;				
		}	
		$query=$this->site_model->gettable('tblsanpham');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'site/deal/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
        $data['deal']=true;
		$data['query']=$this->site_model->gettable_limited('tblsanpham',$per_page,$start_row);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='deal';			
		$this->load->view('includes/template',$data); 
    }
    public function panel()
    {
        $this->checksession();
        $data['ctp']=true;
        $data['content']='panel';
        $this->load->view('includes/template',$data);
    }
    public function theodoidonhang()
    {
        $this->checksession();
       //$this->load->helper('status');
        $start_row=$this->uri->segment(3);
		$per_page=15;
		if(is_numeric($start_row)){
			$_SESSION['start_row']=$start_row;					
		}				    		
		else
		{
			$_SESSION['start_row']=0;				
		}	
		$query=$this->site_model->gettbl1('tbldonhangweb');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/site/theodoidonhang/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
        $data['theodoi']=true;
		$data['query']=$this->site_model->gettbl_limited1('tbldonhangweb',$per_page,$_SESSION['start_row']);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='theodoidonhang';			
		$this->load->view('includes/template',$data);    
    }
    public function checkdonhangcp()
    {
        $sodt=$_POST['sodt'];
        $radioship=$_POST['radioship'];
        $soluong=$_POST['soluong'];
        $id_cp=$_POST['id_cp'];
        $ngay=getdate();
        $ngaydang=$ngay['year'].'-'.$ngay['mon'].'-'.$ngay['mday'];
        if(isset($_SESSION['username']))
        {
            $this->db->where('name',$_SESSION['username']);
            $sqladmindencuh=$this->db->get('tbladmin')->row();
            $hoten=$sqladmindencuh->fullname;
            $email=$sqladmindencuh->email;
            $name=$sqladmindencuh->id;
        }
        else
        {
            $hoten='';
            $email='';
            $name='';
        }
        $data_dh=array(
            'maphieu'   =>$this->rand_string(6),
            'hinhthuc'  =>  'Đến cửa hàng mua',
            'hoten' =>  $hoten,
            'dienthoai'=>$sodt,
            'email' =>$email,
            'diachi'=>$radioship,
            'sanpham'   =>$id_cp,
            'soluong'   => $soluong,
            'ngaydang'  =>  $ngaydang,
            'name'  =>  $name,
            'status'    =>0
        );  
        $this->db->insert('tbldonhangweb',$data_dh);                                                          
    }
    public function listtindang()
    {
        $this->checksession();
       //$this->load->helper('status');
        $start_row=$this->uri->segment(3);
		$per_page=15;
		if(is_numeric($start_row)){
			$_SESSION['start_row']=$start_row;					
		}				    		
		else
		{
			$_SESSION['start_row']=0;				
		}	
		$query=$this->site_model->gettbl('tblsanpham');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/site/listtindang/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
        $data['listtd']=true;
		$data['query']=$this->site_model->gettbl_limited1('tblsanpham',$per_page,$_SESSION['start_row']);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='sanpham';			
		$this->load->view('includes/template',$data); 
    }
    public function diachinhanhang()
    {
        $this->checksession();
        $data['diachimore']=true;
        $data['content']='diachich';
        $this->load->view('includes/template',$data);
    }
    public function dodiachich()
    {
        $this->checksession();
        $id=$this->input->post('id');        
        $this->db->where('status',1);
        if(isset($_SESSION['username']))
        {
		     $this->db->where('name',$_SESSION['username']);
        }
        if(isset($_COOKIE['user']))
        {
            $this->db->where('name',$_COOKIE['user']);    
        }
		$admin=$this->db->get('tbladmin')->row();	
		$nguoidang=$admin->id;
        $sonha=$this->input->post('sonha');
        
        $data_dc=array(
            'name'  =>  $nguoidang,            
            'sonha'    =>$sonha, 
            'duongpho'  =>  $this->input->post('duongpho'),  
            'phuongxa'  =>  $this->input->post('phuongxa'),
            'tinh'      =>  $this->input->post('tinhhome'),
            'quan'      =>  $this->input->post('quan')         
        );
        $this->admin_model->add_tbl('tbldiachinhanhang',$data_dc,$id);   
        $data['diachimore']=true;
        $data['thanhcong']=true;
        if($id!='')
        {
        $data['id']=$id;
        }
        $data['content']='diachich';
        $this->load->view('includes/template',$data);         
    }
    public function editnhanhang($id)
    {
        $this->checksession();
        $data['id']=$id;
        $data['diachimore']=true;
        $data['content']='diachich';
        $this->load->view('includes/template',$data);    
    }
    public function editdiachi($id)
    {
        $this->checksession();
        $data['diachimore']=true;
        $data['id']=$id;
        $data['content']='diachich';
        $this->load->view('includes/template',$data);    
    }
    public function xoadiachi($id)
    {
        $this->db->delete('tbldiachi',array('id'=>$id));
        redirect(site_url('danh-sach-dia-chi.html'));
    }
    public function loadcate2($id)
    {
        $district=$this->site_model->gettablename_all('tblquan','id,quan,tinh,thutu,status','','tinh',$id,'','');
        echo '<option value="-1">--Chuyên quận huyện--</option>';
	    foreach($district->result() as $item)
	    {
	       echo '<option value="'.$item->id.'"'.set_select('cmbquan',$item->id).'>'.$item->quan.'</option>';
	    }
    }
    public function dangtin()
    {
        $this->checksession();
        $data['listtd']=true;
        $data['content']='dangtin';
        $this->load->view('includes/template',$data);
    }
    public function dodangtin()
    {
        $this->checksession();
        $this->form_validation->set_rules('title','<span style="color:red;">Tên sản phẩm</span>','required');
        $this->form_validation->set_rules('gia','<span style="color:red;">Giá</span>','required');
        $this->form_validation->set_rules('giakm','<span style="color:red;">Giá khuyễn mại</span>','required');
        $this->form_validation->set_rules('donvitinh','<span style="color:red;">Đơn vị tính</span>','required');
        $this->form_validation->set_rules('mabaove', '<span style="color:red;">Mã bảo mật</span>', 'required|check_captcha');
        $this->form_validation->set_message('required','<span style="color:red;">%s không để trống</span>');
        $this->form_validation->set_message('check_captcha','<span style="color:red;">%s không đúng</span>');
        if($this->form_validation->run()==FALSE)
        {
            $data['error_register']=validation_errors(); 
            $data['listtd']=true;
            $data['content']='dangtin';
            $this->load->view('includes/template',$data);
        }
        else
        {
            $id=$this->input->post('id');
            $title=trim($this->input->post('title'));		
            $alias=$this->input->post('alias');
            $ncatid1=$this->input->post('ncatid1');
            $ncatid2=$this->input->post('ncatid2');
            $ngaydang1=getdate();    
            $ngaydang=$ngaydang1['year'].'-'.$ngaydang1['mon'].'-'.$ngaydang1['mday'];
            if($this->input->post('created_day1')!='')
            {
                $ngayhh1=explode('-',$this->input->post('created_day1'));
                $ngayhethan=$ngayhh1[2].'-'.$ngayhh1[1].'-'.$ngayhh1[0];
            }
            else
            {
                $ngayhethan='';    
            }
            $this->db->where('status',1);
            if(isset($_SESSION['username']))
            {
			     $this->db->where('name',$_SESSION['username']);
            }
            if(isset($_COOKIE['user']))
            {
                $this->db->where('name',$_COOKIE['user']);    
            }
			$admin=$this->db->get('tbladmin')->row();	
			$nguoidang=$admin->id;	
            if($alias=='')
            {
                $alias=vn_str_filter($title);
            }
           	//Xử lý ảnh	
            if($this->input->post('catid')==-1)
            {
                $alias_cat=='';    
            }
            else
            {
    		  $alias_cat = $this->admin_model->gettbl('tblchuyenmucsp',$this->input->post('catid'))->row()->alias;
            }
    		$config['upload_path'] = './upload/'.$alias_cat.'/';
    		$config['allowed_types'] = 'gif|jpg|png|jpeg';
    		$config['max_size']	= '1000000';
    		$config['max_width']  = '0';
    		$config['max_height']  = '0';
    		$this->load->library('upload', $config);
    		
    		if(!is_dir($config['upload_path'])){
    			mkdir($config['upload_path'], 0755, TRUE);
    		}
    		if(!is_dir($config['upload_path'].'resized/')){
    			mkdir($config['upload_path'].'resized/', 0755, TRUE);
    		}
    		
    		if ( ! $this->upload->do_upload('image'))
    		{
    			if($id==''){
    				$image='';
    				$thumb='';
    			}
    			else{
    				$image=$this->input->post('image');
    				$thumb=$this->input->post('thumb');
    			}
    		}
    		else
    		{			
    			$image_data = $this->upload->data();
    			$name_img='upload/'.$alias_cat.'/'.$image_data['file_name']; 
    			//Xoa file anh cu truoc khi upload
    			$check_image = $this->admin_model->gettbl('tblsanpham',$id)->row()->image;
    			if(isset($check_image) != $name_img){				
    				$check_image_thumb = $this->admin_model->gettbl('tblsanpham',$id)->row()->thumb;		
    				$rbs=explode('.',$check_image_thumb);
    				$rb = $rbs[0].'_500x500.'.$rbs[1];				
    				if(file_exists($check_image)){
    					unlink($check_image);
    					unlink($check_image_thumb);
    					unlink($rb);
    				}
    			}
    			//$duongdan=$image_data['file_name'];
    			//Xu ly crop anh
    			$temp=explode('.',$image_data['file_name']);
    			if($temp[1]=='jpeg' or $temp[1]=='JPEG'){
    				$temp[1]='jpg';
    			}
    			$temp[1]=strtolower($temp[1]);
    			$thumb='upload/'.$alias_cat.'/resized/'.$temp[0].'_thumb'.'.'.$temp[1];
    			$image='upload/'.$alias_cat.'/'.$image_data['file_name'];
    			$imageThumb = new Image($name_img);
    			$imageThumb->resize(500,500,'crop');
    			$imageThumb->save($temp[0].'_thumb', './upload/'.$alias_cat.'/resized');					
    		}
            $image1 = json_encode($_POST['image_filename']);	
            if($this->input->post('catid')==0)
            {
                $data['error_register']='<p>Bạn chưa chọn chuyên mục</p>'; 
                $data['dangtin']=true;
                $data['content']='dangtin';
                $this->load->view('includes/template',$data);    
            } 
            elseif($this->input->post('city')==-1)
            {
                 $data['error_register']='<p>Bạn chưa chọn tỉnh thành</p>'; 
                $data['dangtin']=true;
                $data['content']='dangtin';
                $this->load->view('includes/template',$data);     
            }
            else
            {
                $data=array(
                    'title' =>  $this->input->post('title'),
                    'alias' =>  $alias,
                    'catid' =>  $this->input->post('catid'),
                    'ncatid'    =>  $ncatid1,
                    'ncatid1'   =>  $ncatid2,
                    'tinh'  =>  $this->input->post('city'),
                    'image'     =>  $image,
                    'image1'   =>  $image1,
                    'thumb'     =>  $thumb,
                    'gia'       =>  $this->input->post('gia'),
                    'giakm'     =>  $this->input->post('giakm'),
                    'donvitinh' =>  $this->input->post('donvitinh'),
                    'noidung'   =>  $this->input->post('thongtinct'),
                    'diemnoibat'    =>  $this->input->post('diennb'),
                    'dieukiensudung'    =>  $this->input->post('dieukiensd'),
                    'nguoidang' =>  $nguoidang,
                    'ngaydang'  =>  $ngaydang,
                    'ngayhethan'    =>  $ngayhethan,
                    'status'    =>  1
                ); 
                $this->admin_model->add_tbl('tblsanpham',$data,$id);
                if($_SESSION['start_row']==0){redirect('site/listtindang/');}
    			else{
    				redirect('site/listtindang/'.$_SESSION['start_row']);
    			}  
            }
        }
    }
    public function edittindang($id)
    {
        $this->checksession();
        $data['dangtin']=true;
        $data['id']=$id;
        $data['content']='dangtin';
        $this->load->view('includes/template',$data);
    }
    public function xoatin($id)
    {
        $this->db->delete('tblsanpham',array('id'=>$id));
        if($_SESSION['start_row']==0)
        {
            redirect('site/listtindang');
        }
	    else
        {
			redirect('site/listtindang/'.$_SESSION['start_row']);
		}  
    }
    public function doimatkhau()
    {
        $this->checksession();
        $data['doimk']=true;
        $data['content']='doimatkhau';
        $this->load->view('includes/template',$data);
    }
    public function dodoimatkhau()
    {
        $this->checksession();
        $this->form_validation->set_rules('matkhaucu','<span style="color:red;">Mật khẩu cũ</span>','required'); 
        $this->form_validation->set_rules('matkhaumoi','<span style="color:red;">Mật khẩu mới</span>','required|matches[matkhaucure]');
        $this->form_validation->set_message('required','<span style="color:red;">%s không để trống</span>');
        $this->form_validation->set_message('matches','<span style="color:red;">%s không trùng nhau</span>');
        if($this->form_validation->run()==FALSE)
        {
            $data['error_register']=validation_errors();   
            $data['content']='doimatkhau';
            $data['doimk']=true;
            $this->load->view('includes/template',$data); 
        }
        else
        {
            $matkhau=md5($_POST['matkhaucu']);
            $this->db->where('pass',$matkhau);
            $d=$this->db->get('tbladmin');
            if($d->num_rows() >0)
            {
                $data_mk=array(
                    'pass'=>md5($this->input->post('matkhaumoi'))
                );  
                $this->db->where('id',$this->input->post('id')); 
                $this->db->update('tbladmin',$data_mk); 
                $data['doimk']=true;
                $data['thanhcong']=true;  
                $data['content']='doimatkhau';                  
                $this->load->view('includes/template',$data);    
            }   
            else
            {
                $data['error_register']='<p>Mật khẩu cũ không đúng</p>';   
                $data['content']='doimatkhau';
                $this->load->view('includes/template',$data);     
            }
        }
    }
    public function quenmatkhau()
    {
        $this->check_cookie();
        $data['quenmk']=true;
        $data['content']='quenmatkhau';
        $this->load->view('includes/template',$data);    
    }
    public function doquenmatkhau()
    {
        $this->check_cookie();
        $this->form_validation->set_rules('email','<span style="color:red;">Email</span>','required|valid_email');
        $this->form_validation->set_message('required','<span style="color:red;">%s không để trống</span>');
        $this->form_validation->set_message('valid_email','<span style="color:red;">%s không hợp lệ</span>');
        if($this->form_validation->run() == FALSE)
        {
            $data['quenmk']=true;
            $data['error_register']=validation_errors();  
            $data['content']='quenmatkhau';
            $this->load->view('includes/template',$data);   
        }
        else
        {
            $email = $_POST['email'];
            $this->db->where('email',$email);
            $u = $this->db->get('tbladmin');
            if($u->num_rows() > 0)
            {
                $u=$u->row();
                $matkhau=$this->rand_string(10);
                $data = array(
                    'pass' =>md5($matkhau),
                );    
                $this->db->where('email',$email);
                $this->db->update('tbladmin',$data);
                require_once('class.phpmailer.php'); 
                require_once('class.pop3.php'); 
                define('GUSER', 'rasu666@gmail.com');         
                define('GPWD', 'cogangtopweb@2012');  
                $noidung='<p><b>Thông tin đăng nhập từ website buonchung.com</b></p>
                <p>Tên đăng nhập:<b>'.$u->name.'</p>
                <p>Mật khẩu:<b>'.$matkhau.'</b></p>
                ';             
                global $message;    
                $this->smtpmailer($email, "rasu666@gmail.com", "Buôn chung", "Quên mật khẩu đăng nhập buonchung.com",$noidung);
                echo $message;
                $data['quenmk']=true;
                $data['thanhcong']=true;                
                $data['content']='quenmatkhau';
                $this->load->view('includes/template',$data);  
            }
            else
            {
                $data['quenmk']=true;
                $data['error_register']='<p>Email không tồn tại trong hệ thống</p>';  
                $data['content']='quenmatkhau';
                $this->load->view('includes/template',$data);       
            }
        } 
    }
    public function rand_string($length)
	  {
	   $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; 
	   $str='';
	   $size=strlen($chars);
	   for($i=0;$i<$length;$i++)
	   {
		$str.=$chars[rand(0,$size-1)];
	   }
	   return $str;
	  }
    public function dotaikhoan()
    {
        $this->checksession();
        $id=$this->input->post('txtid');
        $config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);
        if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0755, TRUE);
		}
		if(!is_dir($config['upload_path'].'resized/')){
			mkdir($config['upload_path'].'resized/', 0755, TRUE);
		}
        if ( ! $this->upload->do_upload('avatar'))
		{
			if($id==''){
				$image='';
				$thumb='';
			}
			else{
				$image=$this->input->post('anh');
				$thumb=$this->input->post('anh_thumb');
			}
		}
        else
        {
            $image_data = $this->upload->data();
		    $name_img='upload/'.$image_data['file_name'];  
        	$temp=explode('.',$image_data['file_name']);
			if($temp[1]=='jpeg' or $temp[1]=='JPEG'){
				$temp[1]='jpg';
			}
			$temp[1]=strtolower($temp[1]);
			$thumb='upload/resized/'.$temp[0].'_thumb'.'.'.$temp[1];
			$image='upload/'.$image_data['file_name'];
			$imageThumb = new Image($name_img);
			$imageThumb->resize(50,50,'crop');
			$imageThumb->save($temp[0].'_thumb', './upload/resized');       
        }
        $data=array(
            'fullname' =>  $this->input->post('hoten'),
            'dienthoai' =>  $this->input->post('dienthoai'),
            'image' =>$image,
            'thumb' =>$thumb
        );   
        $this->site_model->add_tbl('tbladmin',$data,$id); 
        $data['thanhcong']=true;
         $data['ctp']=true;
        $data['content']='panel';
        $this->load->view('includes/template',$data);
    }
    public function checksession()
    {
        if(isset($_SESSION['username']) || isset($_COOKIE['user']))
        {}
        else
        {
            redirect(site_url('dang-nhap.html'));
        }
    }
    public function dangnhap()
    {
        $data['header_title']='Đăng nhập hệ thống';  
        $data['dangnhap_r']=true;
        $data['content']='dangnhap';
        $this->load->view('includes/template',$data);  
    }
    public function checklogin()
    {
        $username=$_POST['username'];
        $pass=$_POST['pass'];
        $this->db->where('name',$username);
        $this->db->where('pass',md5($pass));
        $user=$this->db->get('tbladmin');
        if($user->num_rows() >0)
        {
            echo 'true';
            $_SESSION['username']=$username;
            $_SESSION['id']=$user->row()->id;
        }
        else
        {
            echo 'false';
        }
    }
    public function checkluu()
    {
        $username=$_POST['username'];
        $pass=$_POST['pass'];
        $luu=$_POST['luu'];
        $expire=time()+60*60*24*365; // Tổng số giây có được từ thời có máy chủ unix, tức là thời gian sẽ là ngày mai nếu tính từ thời điểm hiện tại
		setcookie("user",$username, $expire,"/");
		setcookie("pass",$pass,time() +60*60*30*365,"/");
    }
    public function dangky()
    {
        $data['header_title']='Đăng ký thành viên';
        $data['dangky_r']=true;
        $data['content']='dangky';
        $this->load->view('includes/template',$data);
    }
    public function checkdangky()
	{
	   $check = 0;
	   $nameuser = $this->db->query('SELECT id FROM tbladmin where name="'.$_POST['taikhoan'].'"')->num_rows();	  
	   if($nameuser>0){	  
			$check = 1;
	   }
	  	echo $check;
	}
    public function checkcapcha()
	{		
	   $check = 0;
	   if($_SESSION['img']!=$_POST['capcha']){
		$check = 1;
	   }
	   echo $check;
	}
    public function checkmail()
	{
	   $check = 0;
	   $mail = $this->db->query('SELECT id FROM tbladmin where email="'.$_POST['email'].'"')->num_rows();	  
	   if($mail>0){	  
			$check = 1;
	   }
	   echo $check;
	}
    public function add_thanhvien()
    {
        $ngay=getdate();
		$ngaydang=$ngay['year'].'-'.$ngay['mon'].'-'.$ngay['mday'];
		$macode=md5(uniqid(rand()));
		$email=$_POST['email'];
        if($_POST['id']!='')
        {
        }
        else
        {
            $datatv=array(
                'macode'    =>  $macode,
                'fullname'  =>  $this->input->post('hoten'),
                'dienthoai' =>  $this->input->post('didong'),
                'name'      =>  $this->input->post('taikhoan'),
                'pass'      =>  md5($this->input->post('matkhau')),
                'email'     =>  $this->input->post('email'),
                'role'      => 2,
                'status'    => 0,
                'ngaydang'  =>  $ngaydang,
            );
            $this->site_model->add_tbl('tbladmin',$datatv,'');
            //Gửi mail
			$to=$email;
			$subject='Thông tin kích hoạt tài khoản:';
			$header='Từ: Buôn chung.';
			$noidung="Đường link kích hoạt:\r\n";
			$noidung.="Nhấn đường link sau để kích hoạt tài khoản\r\n";
			$noidung.=base_url()."site/activemember/$macode";
			require_once('class.phpmailer.php');             
			require_once('class.pop3.php');    
			define('GUSER','rasu666@gmail.com');
			define('GPWD','cogangtopweb@2012');
			global $message;
			$this->smtpmailer($to,'rasu666@gmail.com',$header,$subject,$noidung);
			echo $message;	 
        }
    }
    public function activemember($macode)
	{
	    $this->db->where('macode',$macode);
        $sqlactive=$this->db->get('tbladmin');
        if($sqlactive->num_rows() >0)
        {
			$count=$sqlactive->num_rows();
			if($count==1)
			{
		    $data_up=array(
			     'macode'=>'',
				 'status' =>1,
			);
			$this->db->where('macode',$macode);
			$this->db->update('tbladmin',$data_up);
			}
			else
			{
				echo 'Chuỗi sai';
			}			
		}
        redirect(base_url());
    }
	public function tinh()
    {
        //$this->check_cookie();
        $city=$_POST['city'];
        if($city==-1)
        {
            redirect(base_url());    
        }
        else
        {
            //$_SESSION['dachon']='ok';
            $this->db->where('id',$city);
            $tinh=$this->db->get('tbltinh');
            if($tinh->num_rows() >0)
            {
                $row=$tinh->row();
                setcookie("city",$row->id,time() +60*60*30*365,"/");    
            }
            $data['city']=$city;
            $data['content']='includes/main_content';
            $this->load->view('includes/template',$data);
            redirect(base_url());
        }
    }
    public function tinhmore($id)
    {
        $this->db->where('id',$id);
        $tinh2=$this->db->get('tbltinh');
        if($tinh2->num_rows() >0)
        {
            $row2=$tinh2->row();
            setcookie("city",$row2->id,time() +60*60*30*365,"/");    
        }    
        $data['city']=$id;
        $data['content']='includes/main_content';
        $this->load->view('includes/template',$data);
        redirect(base_url());
    }
    public function check_cookie()
    {
        if(isset($_COOKIE['city']))
        {}
        else
        {
            redirect(base_url());
        }
    }	
	public function sanphambyid($id)
    {
        //$this->check_cookie();
        $this->site_model->num_view($id);
        $data['chitiet']=$id;	
        $this->db->where('id',$id);
        $data['query']=$this->db->get('tblbaiviet')->row();
        if($data['query']->meta_title!='')
        {
            $data['header_title']=$data['query']->meta_title;
        }
        else
        {
            $data['header_title']=$data['query']->title;
        }
        if($data['query']->meta_des!='')
        {
            $data['description']=$data['query']->meta_des;
        }
        else
        {
            $data['description']=$data['query']->title;
        }
        if($data['query']->keyword!='')
    	{
    	    $data['keyword']=$data['query']->keyword;
    	}
        
        
        $data['id']=$id;
        $data['chitiet_code_head'] = $id;
		$data['content']='baivietbyid';
        $this->load->view('includes/template',$data);    
    }
    public function baivietbyid($id)
    {
        $this->check_cookie();
        //$this->site_model->num_view($id);
        $data['cbv']=true;
        $data['mbtct']=$id;
        $data['id']=$id;
        $data['chitiet_code_head'] = $id;
		$data['content']='baivietbyid';
        $this->load->view('includes/template',$data);
        
    }	
	public function show_chuyenmuc($id)
    {        
        //$this->check_cookie();
        $this->db->where('id',$id);
        $this->db->select('id,name,meta_title,meta_des,keyword');
        $data['chuyenmuc']=$this->db->get('tblchuyenmuc')->row();
        if($data['chuyenmuc']->keyword!='')
        {
	       $data['keyword']=$data['chuyenmuc']->keyword;
        }
        if($data['chuyenmuc']->meta_title!='')
        {
	       $data['header_title']=$data['chuyenmuc']->meta_title;
    	}
        else
    	{
    	    $data['header_title']=$data['chuyenmuc']->name;
    	}
        if($data['chuyenmuc']->meta_des!='')
    	{
    	    $data['description']=$data['chuyenmuc']->meta_des;
    	}          			
		$data['dm']= $id;			
		$data['content']='chuyenmuc';			
		$this->load->view('includes/template',$data);    		
	  
    } 
    public function load_item_ajax()
	{
	   $html=$this->load->view('process');
	   echo $html;
	}
    public function load_item_ajax1()
	{
	   $html=$this->load->view('process1');
	   echo $html;
	}
    public function show_chuyenmucsub($id)
    {
        $this->db->where('id',$id);
        $this->db->select('id,name,meta_title,meta_des,keyword');
        $data['chuyenmuc']=$this->db->get('tblchuyenmuc')->row();
        if($data['chuyenmuc']->keyword!='')
        {
	       $data['keyword']=$data['chuyenmuc']->keyword;
        }
        if($data['chuyenmuc']->meta_title!='')
        {
	       $data['header_title']=$data['chuyenmuc']->meta_title;
    	}
        else
    	{
    	    $data['header_title']=$data['chuyenmuc']->name;
    	}
        if($data['chuyenmuc']->meta_des!='')
    	{
    	    $data['description']=$data['chuyenmuc']->meta_des;
    	}
        $data['id']=$id;
        $data['content']='chuyenmucsub';			
		$this->load->view('includes/template',$data);              
    }   
	/*---------------------*/
	public function show_tags($tags)
    {
		$start_row=$this->uri->segment(3);
		$per_page=20;
		if(is_numeric($start_row))
		{
			$start_row=$start_row;
		}
		else
		{
			$start_row=0;
		}
		$query=$this->site_model->gettags('tblbaiviet',$tags);			
		//$cat=$this->site_model->gettbl('tblchuyenmuc',$id)->row();			
		$total_rows = $query->num_rows();			
		$this->load->library('pagination');
		$config['base_url'] = site_url().'tags/'.$tags;
		$config['total_rows'] = $total_rows;	
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Sau';
		$config['prev_link'] = 'Trước';
		$config['num_links'] = 4;		
		$config['first_link'] = 'Đầu';
		$config['last_link'] = 'Cuối';    			
		$this->pagination->initialize($config);
		$data['tags']= $tags;	
		$data['active']= '';		
		$data['query']=$this->site_model->gettags_limited('tblbaiviet',$tags,$start_row,$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='tags';			
		$this->load->view('includes/template',$data);    			  
    }
	public function listsukien($sukien=0)
    {					
		if(isset($_POST['txt_sukien'])){
			$sukien=$_POST['txt_sukien'];
		}
		$data['top_content']=0;
		$data['active']= '';
		$data['sukien_id']= $sukien;
		$data['query']=$this->site_model->getlistsukien('tblbaiviet',$sukien);		
		$data['content']='sukien';			
		$this->load->view('template',$data);    			  
    }
    public function dangkyngay()
    {
        $data['header_title']='Đăng ký ngay';
        $data['content']='dangkyngay';
        $data['dangky']=true;			
		$this->load->view('includes/template',$data);
    }
    public function dodangkyhoc()
    {
        $this->form_validation->set_rules('hoten','<span style="color:red;">Họ tên</span>','required');
        $this->form_validation->set_rules('diachi','<span style="color:red;">Địa chỉ</span>','required');
        $this->form_validation->set_rules('dienthoai','<span style="color:red;">Điện thoại</span>','required');
        $this->form_validation->set_rules('email','<span style="color:red;">Email</span>','required|valid_email');
        $this->form_validation->set_rules('songuoidk','<span style="color:red;">Số người đăng ký</span>','required');
        $this->form_validation->set_rules('dangkymh','<span style="color:red;">Đăng ký môn học</span>','required');
        $this->form_validation->set_rules('dangkymh','<span style="color:red;">Thời gian học</span>','required');
        $this->form_validation->set_rules('mabaomat', '<span style="color:red;">Mã bảo mật</span>', 'required|check_captcha');
        $this->form_validation->set_message('required','<span style="color:red;">%s không để trống</span>');
        $this->form_validation->set_message('valid_email','<span style="color:red;">%s không hợp lệ</span>');
        $this->form_validation->set_message('check_captcha','<span style="color:red;">%s không đúng</span>');
        if($this->form_validation->run()==FALSE)
        {
            $data['errors_regíter']=validation_errors();
            $data['header_title']='Đăng ký ngay';
            $data['content']='dangkyngay';
            $data['dangky']=true;			
		    $this->load->view('includes/template',$data);                
        }
        else
        {
            $data_register=array(
                'hoten' =>$this->input->post('hoten'),
                'diachi'    =>  $this->input->post('diachi'),
                'dienthoai'    =>  $this->input->post('dienthoai'),
                'email'    =>  $this->input->post('email'),
                'soluong'    =>  $this->input->post('songuoidk'),
                'monhoc'    =>  $this->input->post('dangkymh'),
                'thoigian'    =>  $this->input->post('thoigian'),
                'noidung'    =>  $this->input->post('noidungkhac'),
                'status'    =>0
            );
            $this->db->insert('tbldangkyhoc',$data_register);
            $data['header_title']='Đăng ký ngay';
            $data['dangky']=true;
            $data['content']='success_dk';			
		    $this->load->view('includes/template',$data);   
        }
    }
	public function listevent()
    {							
		$data['top_content']=0;
		$data['active']= '';		
		$data['query']=$this->site_model->gettbl_sukien('tblsukien','',0,20);		
		$data['content']='list-sukien';			
		$this->load->view('template',$data);    			  
    }
    public function search($txt_search='')
    {		
		$start_row=$this->uri->segment(3);
		if($txt_search==''){
			$txt_search=preg_replace('/([^\pL\.\ ]+)/u', '',$_POST['ten']);	
			//$txt_search=$_POST['txt_search'];			
		}		
		else{		
			$txt_search= str_replace('+',' ',$txt_search);	
		}
		$per_page=15;
		if(is_numeric($start_row))
		{
			$start_row=$start_row;
		}
		else
		{
			$start_row=0;
		}
		$txt_search1= str_replace(' ','+',$txt_search);		
		$query=$this->site_model->search('tblbaiviet',$txt_search);			
		//$cat=$this->site_model->gettbl('tblchuyenmuc',$id)->row();			
		$total_rows = $query->num_rows();			
		$this->load->library('pagination');
		$config['base_url'] = site_url().'ket-qua-tim-kiem/'.$txt_search1;
		$config['total_rows'] = $total_rows;	
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Sau';
		$config['prev_link'] = 'Trước';
		$config['num_links'] = 4;		
		$config['first_link'] = 'Đầu';
		$config['last_link'] = 'Cuối';    			
		$this->pagination->initialize($config);				
		$data['active']= '';
		$data['txt_search']= $txt_search;
		$data['query']=$this->site_model->search_limited('tblbaiviet',$txt_search,$start_row,$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='search';			
		$this->load->view('includes/template',$data);    		
    }
    public function chuyentab()
    {
        $this->checksession();
        $ip_cuoi=$_POST['ip_cuoi'];
        $radiohinhthuc=$_POST['radiohinhthuc'];
        $data['ip_cuoi']=$ip_cuoi;
        $data['radiohinhthuc']=$radiohinhthuc;
        $html=$this->load->view('chuyentab',$data);
        echo $html;
    }
    public function chuyentab2()
    {
        $this->checksession();
        $ip_cuoi=$_POST['ip_cuoi'];
        $radiohinhthuc=$_POST['radiohinhthuc'];
        $hoten=$_POST['hoten'];
        $dienthoai=$_POST['dienthoai'];
        $email=$_POST['email'];  
        $tinh=$_POST['tinh'];
        $quan=$_POST['quan'];
        $phuongxa=$_POST['phuongxa'];
        $duongpho=$_POST['duongpho'];
        $sonha=$_POST['sonha'];
        if(isset($_SESSION['username']))
        {
            $this->db->where('name',$_SESSION['username']);
        }  
        if(isset($_COOKIE['user']))
        {
            $this->db->where('name',$_COOKIE['user']);    
        }
        $admin_ctab=$this->db->get('tbladmin')->row();
        $data_ad_tab=array(
            'fullname'  =>$hoten,
            'dienthoai' =>$dienthoai,
            'email' =>$email
        );
        $this->db->where('id',$admin_ctab->id);
        $this->db->update('tbladmin',$data_ad_tab);
        //uodate dia chi nhan hang
        $data_nhanha_tab=array(
            'sonha' =>$sonha,
            'duongpho'=>$duongpho,
            'phuongxa'=>$phuongxa,
            'tinh'      =>$tinh,
            'quan'      =>$quan
        );
        $this->db->where('name',$admin_ctab->id);
        $this->db->update('tbldiachinhanhang',$data_nhanha_tab);
        $data['ip_cuoi']=$ip_cuoi;
        $data['radiohinhthuc']=$radiohinhthuc;
        //chuyen tab
        $html=$this->load->view('chuyentab2',$data);
        echo $html;
    }
    public function chuyentab3()
    {
        $this->checksession();
        $ip_cuoi=$_POST['ip_cuoi'];
        $radiohinhthuc=$_POST['radiohinhthuc'];
        $yeucauthem=$_POST['yeucauthem'];  
        $data['ip_cuoi']=$ip_cuoi;
        $data['radiohinhthuc']=$radiohinhthuc;
        $data['yeucauthem']=$yeucauthem;
        $html=$this->load->view('chuyentab3',$data);
        echo $html;  
    }
    public function chuyentab4()
    {
        $ip_cuoi=$_POST['ip_cuoi']; 
        $radiohinhthuc=$_POST['radiohinhthuc'];
        if($radiohinhthuc=='cod')
        {
            $hinhthuc='Thu tiền tận nơi';
        }
        else
        {
            $hinhthuc='Thanh toán online';
        }
        $yeucauthem=$_POST['yeucauthem'];   
        $soluongxn=$_POST['soluongxn'];
        $maphieu=$this->rand_string(6);
        $ngay4=getdate();
        $ngaydang=$ngay4['year'].'-'.$ngay4['mon'].'-'.$ngay4['mday'];
        if(isset($_SESSION['username']))
        {
            $this->db->where('name',$_SESSION['username']);
            $admin_or=$this->db->get('tbladmin')->row();
            $this->db->where('name',$admin_or->id);
            $sqldcnh=$this->db->get('tbldiachinhanhang')->row();
            $this->db->where('id',$sqldcnh->quan);
            $quan=$this->db->get('tblquan')->row();
            $this->db->where('id',$sqldcnh->tinh);
            $tinh=$this->db->get('tbltinh')->row();
        }
        $diachinhanhang=$sqldcnh->sonha.'-'.$sqldcnh->duongpho.'-'.$sqldcnh->phuongxa.'-'.$quan->quan.'-'.$tinh->tinh;
        $data_dhweb=array(
            'maphieu'   =>  $maphieu,
            'hinhthuc'  =>  $hinhthuc,
            'hoten'     =>  $admin_or->fullname,
            'dienthoai' =>  $admin_or->dienthoai,
            'email'     =>  $admin_or->email,
            'diachinhanhang'    =>  $diachinhanhang,
            'yeucauthem'    =>  $yeucauthem,
            'sanpham'   =>$ip_cuoi,
            'soluong'   =>$soluongxn,
            'ngaydang'  =>  $ngaydang,
            'name'      =>$admin_or->id,
            'status'    =>0
        );
        $this->db->insert('tbldonhangweb',$data_dhweb);
        //if($radiohinhthuc=='visa')
        //{        
            $this->db->where('id',$ip_cuoi);
            $sqlsanphamem=$this->db->get('tblsanpham')->row();
            if($sqlsanphamem->giakm!='')
            {
                $tkgia=$sqlsanphamem->giakm;
            }
            else
            {
                $tkgia=$sqlsanphamem->gia;
            }                
                    
            $sanpham='<div id="donhangemail_all" style="background:#e3f5ff;padding:20px;">
                <div id="donhangemail" style="background:#fff;border:1px solid #b1d9ee;padding:10px;">
                <p style="text-align:center;padding-top:5px;"><img src="http://buonchung.com/images/logo.png"/></p>
                <p style="text-transform:uppercase;"><b>Xác nhận đặt hàng thành công</b></p>
                <p>Chào&nbsp;'.$admin_or->fullname.'</p>
                <p>BuonChung xác nhận đơn hàng của Quý khách. Để hoàn tất mua hàng, Quý khách đến VP Buônchung tại Hà Nội để thanh toán và nhận mã số phiếu điện tử:</p>
                <p style="color:red;">20 Láng Hạ, Đống Đa, Hà Nội</p>
                <p style="color:red;">Từ 8h -18h thứ 2 đến thứ 7</p>
                <p>Mã đơn hàng&nbsp;<strong>'.$maphieu.'</strong></p>
                <p>Tình trạng:&nbsp;Chưa thanh toán</p>
                <p style="padding-bottom:10px;"><strong>Chi tiết đơn hàng</strong></p>
                <table cellspacing="0" cellpadding="0" style="font-size:12px;color:#333;width:100%">
                    <tr style="background:#c8ebfe;">
                        <td style="border:1px solid #c8ebfe;font-weight:bold;padding:5px 10px;">STT</td>
                        <td style="border:1px solid #c8ebfe;font-weight:bold;padding:5px 10px;">Sản phẩm</td>
                        <td style="border:1px solid #c8ebfe;font-weight:bold;padding:5px 10px;">Giá</td>
                        <td style="border:1px solid #c8ebfe;font-weight:bold;padding:5px 10px;">Số lượng</td>
                        <td style="border:1px solid #c8ebfe;font-weight:bold;padding:5px 10px;">Thành tiền</td>
                    </tr>
                    <tr>
                        <td style="border:1px solid #c8ebfe;padding:5px 10px;">1</td>
                        <td style="border:1px solid #c8ebfe;padding:5px 10px;">'.$sqlsanphamem->title.'</td>
                        <td style="border:1px solid #c8ebfe;padding:5px 10px;">'.number_format($tkgia,0,'.','.').'&nbsp;'.$sqlsanphamem->donvitinh.'</td>  
                        <td style="border:1px solid #c8ebfe;padding:5px 10px;">'.$soluongxn.'</td>
                        <td style="border:1px solid #c8ebfe;padding:5px 10px;">'.number_format($soluongxn*$tkgia,0,'.','.').'&nbsp;'.$sqlsanphamem->donvitinh.'</td>
                    </tr>
                    <tr>
                        <td align="right" style="border:1px solid #c8ebfe;padding:10px;color:#c92525" colspan="5">
                            <b><span style="padding-right:35px;">Tổng tiền</span>'.number_format($soluongxn*$tkgia,0,'.','.').'&nbsp;'.$sqlsanphamem->donvitinh.'</b>
                        </td>
                    </tr>
                </table>
                <p><b>Thông tin khách hàng</b></p>
                <table cellspacing="1" cellpadding="0" border="0" style="margin-top:0;">
                    <tr>
                         <td style="width:160px;">Họ tên</td>
                         <td style="padding:8px 30px;">
                            <b>'.$admin_or->fullname.'</b>
                         </td>
                    </tr>
                    <tr>
                         <td style="width:160px;">Điện thoại</td>
                         <td style="padding:8px 30px;">
                            <b>'.$admin_or->dienthoai.'</b>
                         </td>
                    </tr>
                    <tr>
                        <td style="width:160px;">Địa chỉ</td>
                        <td style="padding:8px 30x;">'.$sqldcnh->sonha.'-'.$sqldcnh->duongpho.'-'.$sqldcnh->phuongxa.'-'.$quan->quan.'-'.$tinh->tinh.'</td>
                    </tr>
                </table>
                <p>Nếu quý khách cần hỗ trợ, vui lòng gọi <span style="color:red;font-weight:bold;font-size:14px;">(01645.820.482)</span> hoặc gửi email đến&nbsp;<a style="color:#15c;" href="mailto:contact@buonchung.com">contact@buonchung.com</a></p>
                <p>Cám ơn Quý khách đã mua sắm trên BuonChung.vn</p>
                <p>(<span style="color:red;">*</span>) Đây là email hệ thống gửi tự động, vui lòng không trả lời lại email này<p>
                <div style="clear:both;"></div>
                <div>
                <div style="clear:both;"></div>
            </div>';
            $toemail=$admin_or->email;
            require_once('class.phpmailer.php');             
            require_once('class.pop3.php');                 
            define('GUSER', 'contact@buonchung.com');          // Email                
            define('GPWD', 'cogangtopweb@2012');                 // Password
            global $message;   
            //var_dump($sanpham);die();                 
            $this->smtpmailer($toemail,"contact@buonchung.com","buonchung.com", "Đặt hàng tại website buonchung.com",$sanpham);
        //}    					    				
        $data['ip_cuoi']=$ip_cuoi;
        $data['radiohinhthuc']=$radiohinhthuc;
        $html=$this->load->view('chuyentab4',$data);
        echo $html;
    }
    public function sitemap()
	{		
	   $doc = new DOMDocument("1.0","utf-8"); 
       $doc->formatOutput = true;
       $r = $doc->createElement("urlset" );
       $r->setAttribute("xmlns", "http://www.sitemaps.org/schemas/sitemap/0.9");
       $doc->appendChild( $r );
       $url = $doc->createElement("url" );
       $name = $doc->createElement("loc" );
       $name->appendChild(
            $doc->createTextNode('http://m.daotaovieclam.edu.vn')
	   );
	   $url->appendChild($name);			
	   $changefreq = $doc->createElement( "changefreq" );
	   $changefreq->appendChild(
	       $doc->createTextNode('daily')
	   );
       $url->appendChild($changefreq);			
	   $priority = $doc->createElement( "priority" );
	   $priority->appendChild(
 			$doc->createTextNode('1.00')
	   );
	   $url->appendChild($priority);			
	   $r->appendChild($url);			       
        $this->db->where('status',1);
		$cate=$this->db->get('tblchuyenmuc');
		if($cate->num_rows()>0)
		{
			foreach($cate->result() as $row)
			{
			    $categoryad='';
                $categoryad=$this->admin_model->getcatlink($row->id);
				$url = $doc->createElement( "url" );
				
				$name = $doc->createElement( "loc" );
				$name->appendChild(
					$doc->createTextNode(site_url($categoryad.'-c'.$row->id.'.html'))
				);
				$url->appendChild($name);
				
				$changefreq = $doc->createElement( "changefreq" );
				$changefreq->appendChild(
					$doc->createTextNode('daily')
				);
				$url->appendChild($changefreq);
				
				$priority = $doc->createElement( "priority" );
				$priority->appendChild(
					$doc->createTextNode('1.00')
				);
				$url->appendChild($priority);
				
				$r->appendChild($url);
			}
		} 
        $this->db->where('status',1);
		$cate=$this->db->get('tblbaiviet');
		if($cate->num_rows()>0)
		{
			foreach($cate->result() as $row)
			{			   
				$url = $doc->createElement( "url" );
				
				$name = $doc->createElement( "loc" );
				$name->appendChild(
					$doc->createTextNode(site_url($row->alias.'-'.$row->id.'.html'))
				);
				$url->appendChild($name);
				
				$changefreq = $doc->createElement( "changefreq" );
				$changefreq->appendChild(
					$doc->createTextNode('daily')
				);
				$url->appendChild($changefreq);
				
				$priority = $doc->createElement( "priority" );
				$priority->appendChild(
					$doc->createTextNode('1.00')
				);
				$url->appendChild($priority);
				
				$r->appendChild($url);
			}
		}   		                    		          
		$doc->save("sitemap.xml");	
	}
    public function thoat()
    {
        setcookie("user"," ",time() - 3600,"/");
		setcookie("pass"," ",time() - 3600,"/");
        if(isset($_SESSION['username']))
        {
            unset($_SESSION['username']);
        }
        redirect(base_url());
    }
    public function smtpmailer($to, $from, $from_name, $subject, $body)
    {
        $mail = new PHPMailer();                  // tạo một đối tượng mới từ class PHPMailer
        $mail->IsSMTP();                         // bật chức năng SMTP
        $mail->SMTPDebug = 0;                      // kiểm tra lỗi : 1 là  hiển thị lỗi và thông báo cho ta biết, 2 = chỉ thông báo lỗi
        $mail->SMTPAuth = true;                  // bật chức năng đăng nhập vào SMTP này
        $mail->CharSet = "UTF-8";
        $mail->SMTPSecure = 'ssl';                
        $mail->Host = 'smtp.zoho.com';         
        $mail->Port = 465;                         
        $mail->Username = GUSER;  
        $mail->Password = GPWD;           
        $mail->SetFrom($from, $from_name);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AddAddress($to);
        if(!$mail->Send())
        {
            $message = 'Gởi mail bị lỗi: '.$mail->ErrorInfo; 
            return false;
        } 
        else 
        {
            $message = 'Thư của bạn đã được gởi đi ';
            return true;
        }
    }
}
?>