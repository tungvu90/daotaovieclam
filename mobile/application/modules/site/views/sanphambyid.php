<?php
if(isset($city))
{
	$city=$city;
}
else
{
	if(isset($_COOKIE['city']))
	{
		$city=$_COOKIE['city'];
	}
    else
    {
        $city=2;
    }
} 
$CI=&get_instance();
$CI->load->model('site/site_model');
if(isset($chitiet))
{    
    ?>
    <div id="sanpham_cate">
    <?php
    $this->db->where('id',$chitiet);
    $sqlchitiettin=$this->db->get('tblsanpham');
    if($sqlchitiettin->num_rows()>0)
    {
        $chitiettin=$sqlchitiettin->row();
        $this->db->where('id',$chitiettin->catid);
        $sqldanhmuctinct=$this->db->get('tblchuyenmucsp');
        if($sqldanhmuctinct->num_rows()>0)
        {
            $danhmuctinct=$sqldanhmuctinct->row();
            $category_tinct='';
            $category_tinct=$CI->site_model->getcatlink($danhmuctinct->id);
        ?>
        <ul class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>" title="Trang chủ">Trang chủ</a><span>/</span></li>
            <li><a href="<?php echo site_url($category_tinct.'-c'.$danhmuctinct->id.'.html'); ?>" title="<?php echo $danhmuctinct->name; ?>"><?php echo $danhmuctinct->name; ?></a><span>/</span></li>
            <li class="active"><span><?php echo $chitiettin->title; ?></span></li>
            <div class="clear"></div>
        </ul>
        <?php 
        }
    }
    ?>
        <div id="sanpham_home_top1" style="margin:7px 10px;text-align: center;">
            <div class="sanpham_home_img">
                <img src="<?php echo linkanh().$chitiettin->image; ?>" title="<?php echo $chitiettin->title; ?>" alt="<?php echo $chitiettin->title; ?>">                                            </div>                                

        </div>
        <div id="sanpham_id_right">
            <div id="chitiet_name_id">
                <h1><?php echo $chitiettin->title; ?>&nbsp;<span style="color:#666;font-weight:normal;">(Mã:<?php echo $chitiettin->id; ?>)</span></h1>
            </div>
            <p id="chitiet_id_ma"></p>
            <p id="giagoc_id"<?php if($chitiettin->giakm==0){ ?>style="text-decoration: none !important;" <?php } ?>>
            <?php 
            if($chitiettin->gia==0)
            {
                echo 'Giá:&nbsp;Liên hệ';     
            }
            else
            {
                echo number_format($chitiettin->gia,0,'.','.').'&nbsp;'.$chitiettin->donvitinh; 
            }
            ?></p>
            <?php 
            if($chitiettin->giakm!=0)
            {
            ?>
            <p id="giakm_id"><?php echo number_format($chitiettin->giakm,0,'.','.').'&nbsp;'.$chitiettin->donvitinh; ?></p>
            <?php 
            }
            ?>                                    
            <div id="func">                
                <div class="v3_SC_select">
                    <div class="v3_SC_boxSX">
                        <img src="<?php echo linkanh().$chitiettin->image; ?>" width="48" height="48" title="<?php echo $chitiettin->title; ?>" >
                        <div id="f1"><?php echo $chitiettin->title; ?></div>
                        <div id="soluong_ct">
                            <select name="soluongquaty">
                                <?php 
                                    for($i=1;$i<26;$i++)
                                    {
                                    ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php 
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div> 
                <div id="total_gia">
                    <p id="baseTotalPrice">Tổng tiền:&nbsp;
                    <?php 
                        if($chitiettin->giakm=='0')
                        {                            
                            echo number_format($chitiettin->gia,0,'.','.').'&nbsp'.$chitiettin->donvitinh;
                        }
                        else
                        {
                            echo number_format($chitiettin->giakm,0,'.','.').'&nbsp'.$chitiettin->donvitinh;    
                        }
                    ?>
                    </p>
                    <div id="datmua">
                        <a href="">Đặt mua</a>
                    </div>
                </div>               
            </div>
            <div id="infoProduct">
                <div class="fea">
                    <div class="damua">
                        <p>Đã mua<span><?php echo $chitiettin->luotmua; ?></span></p>
                    </div>
                    <div class="damua">
                        <?php 
                            if($chitiettin->giakm!=0)
                            {
                                $tktopm=round(100-((($chitiettin->giakm)/($chitiettin->gia))*100));
                            ?>
                            <p>Giảm<span><?php echo $tktopm; ?>%</span></p>
                        <?php 
                        }
                        ?>
                    </div>
                    <div class="damua" style="border-right:none;">                        
                        <p>Lượt xem<span>
                        <?php 
                            echo $chitiettin->view;
                        ?>
                        </span></p>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="diem_ct">
            <div class="diem_ct_l">
                <h2 class="dienct_t">Điểm nổi bật</h2>
                <div class="diem_ct_p">
                    <?php echo $chitiettin->diemnoibat; ?>
                </div>
            </div>
            <div class="diem_ct_l" style="margin-right:0;">
                <h2 class="dienct_t">Điều kiện sử dụng</h2> 
                <div class="diem_ct_p">
                    <?php echo $chitiettin->dieukiensudung; ?>
                </div>
            </div>
            <div class="clear"></div>
        </div><!--End .diemct-->        
        <div id="thongtin_detail">
            <div id="thongtin_left">
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('.danhmuc_item:first').css('display','block'); 
                        $('.fa_tab').click(function(){
                            var name=$(this).attr('title');                            
                            $('.danhmuc_item').css('display','none');                            
                             $('#'+name).css('display','block');
                             $('.fa_tab').removeClass('active');
                             $(this).addClass('active');    
                        });                             
                    });
                </script>
                <div id="tab_new">
                    <div id="tab_top">
                        <ul class="ul_tab_news" class="tab">
                            <li><a class="fa_tab active" title="item_1">Thông tin chi tiết</a></li>
                            <li><a class="fa_tab" title="item_2">Bình luận</a></li>                                                                                  
                        </ul>
                    </div>
                    <div id="tab_main">
                        <div id="item_1" class="danhmuc_item" style="display:none;">  
                            <div id="noidung_id">
                                <?php echo str_replace("/upload","http://buonchung.com/upload",$chitiettin->noidung); ?>
                            </div>                          
                            <div class="clear"></div>
                        </div>
                        <div id="item_2" class="danhmuc_item" style="display:none;"> 
                        <br />                           
                            <div id="fb-root"></div>
                		      <script>(function(d, s, id) {
                		          var js, fjs = d.getElementsByTagName(s)[0];
                                  if (d.getElementById(id)) return;
                                  js = d.createElement(s); js.id = id;
                                  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                  fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                            <html xmlns:fb="http://ogp.me/ns/fb#">
                                <fb:comments href="<?php echo site_url($chitiettin->alias.'-'.$chitiettin->id).'.html';?>" width="300px" num_posts="10" ></fb:comments>
                            </html> 
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>                
            </div>            
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div id="sanphamcungloai">
            <div id="sanphamcl_top">
                <p>Deal Cùng danh mục</p>
            </div>
            <?php 
                $this->db->where('id !=',$chitiettin->id);
                $this->db->where('catid',$chitiettin->catid);
                $this->db->order_by('thutu','desc');
                $this->db->order_by('id','desc');
                $this->db->limit(8);
                $this->db->select('id,title,alias,gia,giakm,thumb,thutu,thutu,status,donvitinh,luotmua,catid');
                $sqlsanphamcungloai=$this->db->get('tblsanpham');
                if($sqlsanphamcungloai->num_rows()>0)
                {
                    $demcl=1;
                    foreach($sqlsanphamcungloai->result() as $itemsanphamcl)
                    {
                    ?>
                    <div class="boxCate">
                        <div class="cate_wrapper">
                            <div class="cate_home_img">
                                <a href="<?php echo site_url($itemsanphamcl->alias.'-'.$itemsanphamcl->id).'.html';?>" title="<?php echo $itemsanphamcl->title; ?>"><img src="<?php echo linkanh().$itemsanphamcl->thumb; ?>" title="<?php echo $itemsanphamcl->title; ?>" alt="<?php echo $itemsanphamcl->title; ?>"></a>
                                <?php 
                                     if($itemsanphamcl->giakm!=0)
                                     {
                                        $tkhome1cl=round(100-((($itemsanphamcl->giakm)/($itemsanphamcl->gia))*100));
                                     ?> 
                                     <div class="cate_home_km"><p>-<?php echo $tkhome1cl; ?><em>%</em></p></div>           
                                    <?php 
                                    }
                                ?>
                            </div>
                            <a href="<?php echo site_url($itemsanphamcl->alias.'-'.$itemsanphamcl->id).'.html';?>" class="vtitle" title="<?php echo $itemsanphamcl->title; ?>"><?php echo $itemsanphamcl->title; ?></a>
                            <p class="cate_home_gia" title="<?php echo $itemsanphamcl->title; ?>">Giá:&nbsp;
                            <?php 
                                if($itemsanphamcl->giakm==0)
                                {
                                    if($itemsanphamcl->gia==0)
                                    {
                                        echo '<span>Liên hệ</span>';
                                    }
                                    else
                                    {
                                    ?>
                                    <span><?php echo number_format($itemsanphamcl->gia,0,'.','.'); ?><em><?php echo $itemsanphamcl->donvitinh; ?></em></span>
                                    <?php 
                                    }
                                }
                                else
                                {
                                    if($itemsanphamcl->giakm==0)
                                    {
                                        echo 'Liên hệ';
                                    }
                                    else
                                    {
                                    ?>
                                    <span><?php echo number_format($itemsanphamcl->giakm,0,'.','.'); ?><em><?php echo $itemsanphamcl->donvitinh; ?></em></span>
                                    <?php 
                                    }   
                                }
                            ?>
                            </p>
                            <div class="vbuyer">
                                Có <strong><?php echo $itemsanphamcl->luotmua; ?></strong>
                                người mua
                            </div>
                        </div>
                    </div>
                    <?php 
                    $demcl++;       
                    }
                }
            ?>
        </div>
        <div class="clear"></div>
    </div>
    <?php   
}
?>