<div id="sanpham_cate">
<ul class="breadcrumb">
    <li><a href="" style="padding-left:0 !important;">Trang chủ</a><span>/</span></li>
    <li class="active"><span>Đăng nhập</span></li>
    <div class="clear"></div>
</ul>
<div id="dangky_form">
    <p><strong>Bạn vui lòng nhập thông tin đăng nhập</strong></p>
        <div class="loidangnhap t_red"></div>
        <table>
            <tr>
                <th>Tài khoản</th>
                <td>
                    <input type="text" name="username" id="username" value="" />
                    <div class="warning_user t_red"></div> 
                </td>
            </tr>
            <tr>
                <th>Mật khẩu:</th>
                <td>
                    <input type="password" name="pass" id="pass" value="" />
                    <div class="warning_pass t_red"></div> 
                </td>
            </tr>            
            <tr>
                <th></th>
                <td><input type="submit" name="submit" class="nut" value="Đăng nhập" onclick="return xuly_dangnhap();" /><input class="nut" type="reset" name="reset" value="Làm lại" /></td>
            </tr>
        </table>
</div>
<script type="text/javascript">
    function xuly_dangnhap()
    {        
        username=$('#username').val();   
        pass=$('#pass').val();
        if(username=='')     
        {
            $('.warning_user').html('Tài khoản không để trống');
            $('#username').focus();
            return false;
        }
        else if(pass=='')
        {
            $('.warning_user').html('');
            $('.warning_pass').html('Mật khẩu không để trống');
            $('#pass').focus();    
        }
        else
        {
            $('.warning_user').html('');
            $('.warning_pass').html('');    
            $.ajax({
                cache:false,    
				url : "<?php echo site_url('site/checklogin/'); ?>",
				type : "post",			
				data : {
					username : username,
                    pass : pass,			
				},
				success : function (html){				                            
                    alert('Đăng nhập thành công');
                    window.location='<?php echo site_url('trang-ca-nhan.html') ?>';                                         
                }
            });
        }
    }
</script>
</div>