<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
?>
<div id="bg_huhu">
<ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Trang chủ</a><span>/</span></li>
        <li><a href="" class="activeh"><?php 
            $tags1=str_replace('-',' ',$tags);
            echo $tags1; ?></a></li>
        <div class="clear"></div>
    </ul>
<div class="main-content" style="background:none;">
	<?php 
	$kt=0;
	foreach($query->result() as $item){
	$kt++;
	$category='';
	$category=$CI->site_model->getcatlink($item->catid);	
	if($item->thumb!=''){
		$thumb=explode('.',$item->thumb);
		$item_thumb=$thumb[0].'_290x185'.'.'.$thumb[1];
	}		
	else{
		$item_thumb='upload/no-img.png';
	}
	$cut_title = '';
	$cut_titles=explode(' ',$item->title);								
	for($j=0;$j<count($cut_titles);$j++){
		$cut_title .= $cut_titles[$j].' ';
		if($j==12){
			$cut_title = $cut_title.'...';
			break;
		}
	}	
	//Kiem tra date
	date_default_timezone_set('Asia/Ho_Chi_Minh');
	$day = date('Y-m-d H:i:s');
	$diff = abs(strtotime($day) - strtotime($item->created_day));
	$years = floor($diff / (365*60*60*24));
	$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
	$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
	$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
	$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
	$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
	if($days==0 and $years==0 and $months==0){
		$daytime = "Cách đây ";
		if($hours!=0){$daytime .= $hours." giờ, ";}
		$daytime .= $minutes." phút";
	}
	else{
		$base=explode(' ',$item->created_day);
		$baseday=explode('-',$base[0]);
		$basetime=substr($base[1],0,-3);
		$daytime=$baseday[2].'/'.$baseday[1].'/'.$baseday[0].' '.$basetime;
	}
	////////////////////////////////////
	?>
	<div class="tintuc_item">
            <a href="<?php echo site_url($item->alias.'-'.$item->id.'.html'); ?>" class="tintuc_item_img"><img src="<?php echo linkanh().$item->thumb; ?>" /></a>
            <a href="<?php echo site_url($item->alias.'-'.$item->id.'.html'); ?>" class="tintuc_item_name"><?php echo $item->title; ?></a>
            <p><?php echo catchuoi($item->mota,600); ?></p>
            <div class="clear"></div>
        </div>
	<?php } ?>
	<div class="clr"></div>
	<div class="pagation">
		<?php echo $pagination; ?>
	</div>	
</div>
<div class="clear"></div>
</div>