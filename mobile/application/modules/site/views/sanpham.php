<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
?>
<div id="panel">
    <p id="panel_title">Danh sách tin đăng</p>
    <div id="panel_main">
        <ul id="panel_top">
            <li><a href="<?php echo site_url('trang-ca-nhan.html'); ?>" <?php if(isset($ctp)){ ?> style="background:#fff;" <?php } ?>>Sửa thông tin</a></li>
            <li><a href="<?php echo site_url('danh-sach-dia-chi.html'); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ cửa hàng">Địa chỉ cửa hàng</a></li>
            <li><a href="">Theo dõi đơn hàng</a></li>
            <li><a href="<?php echo site_url('danh-sach-tin-dang.html'); ?>" <?php if($listtd){ ?> style="background:#fff;" <?php } ?> title="Danh sách tin đăng">Danh sách tin đăng</a></li>
            <li><a href="<?php echo site_url('doi-mat-khau.html') ?>" <?php if(isset($doimk)){ ?> style="background:#fff;" <?php } ?>>Đổi mật khẩu</a></li>
        </ul>
    </div>
    <div id="panel_form">
        <a href="<?php echo site_url('dang-tin.html'); ?>" style="display:block;padding:10px;background:#3883cc;width:80px;margin-bottom:10px;color:#fff;;">Thêm mới</a>
        <table id="listsanpham">
            <tr>
                <th>STT</th>
                <th>Tiêu đề</th>
                <th>Ảnh</th>
                <th>Giá</th>
                <th>Giá khuyễn mại</th>
                <th>Ngày đăng</th>
                <th>Chuyên mục</th>
                <th>Tỉnh thành</th>
                <th>Lượt xem</th>
                <th>Sửa</th>
                <th>Xóa</th>
            </tr>
            <?php 
                if($query->num_rows() >0)
                {
                    $dem=1;
                    foreach($query->result() as $itemquery)
                    {
                    ?>
                    <tr>
                        <td valign="top" style="width:3%;"><?php echo $dem; ?></td>
                        <td valign="top" style="10%"><?php echo $itemquery->title; ?></td>
                        <td valign="top"><img src="<?php echo $itemquery->thumb; ?>" width="50px" /></td>
                        <td valign="top">
                        <?php 
                        if($itemquery->gia==0)
                        {                    
                            echo "Liên hệ";                                                                            
                        }
                        else
                        {
                        echo number_format($itemquery->gia,0,'.','.')?>&nbsp;<?php echo $itemquery->donvitinh; 
                        }
                        ?>
                        </td>
                        <td valign="top">
                        <?php 
                        if($itemquery->giakm==0)
                        {                    
                            echo "Liên hệ";                                                                            
                        }
                        else
                        {
                        echo number_format($itemquery->giakm,0,'.','.')?>&nbsp;<?php echo $itemquery->donvitinh; 
                        }
                        ?>
                        </td>
                        <td valign="top">
                        <?php 
                        $ngay=explode('-',$itemquery->ngaydang);
                        echo $ngay[2].'-'.$ngay[1].'-'.$ngay[0]; 
                        ?></td>
                        <td valign="top">
                        <?php 
        					$cats=$CI->admin_model->gettbl('tblchuyenmucsp',$itemquery->catid);										
        					if($cats->num_rows()> 0){
        					$cat= $cats->row();
        						echo $cat->name;
        					}
        					else{
        						echo 'NULL';
        					}
        			     ?>
                        </td>
                        <td valign="top" style="width:7%;">
                            <?php 
                                $this->db->where('id',$itemquery->tinh);
                                $sqltinh=$this->db->get('tbltinh');
                                if($sqltinh->num_rows()>0)
                                {
                                    $sqltinh=$sqltinh->row();
                                    echo $sqltinh->tinh;
                                }
                                else
                                {
                                    echo '---------';
                                }                                
                            ?>
                        </td>
                        <td valign="top" style="width:3%;">
                            <?php echo $itemquery->view; ?>
                        </td>
                        <td valign="top" style="width:3%;"><a href="<?php echo site_url('site/edittindang/'.$itemquery->id); ?>" title="Edit"><img src="images/Edit.png" title="edit" alt="edit" width="16" /></a></td>
                        <td valign="top" style="width:3%;"><a onclick="return confirm('Bạn có muốn xóa thật không ?');" href="<?php echo site_url('site/xoatin/'.$itemquery->id); ?>" title="Xóa"><img src="images/Delete.png" width="16" title="Xóa" alt="Xóa" /></a></td>
                    </tr>
                    <?php 
                    $dem++;   
                    }
                    ?>
                    <tr>
                <td colspan="11"><p><?php echo $pagination; ?></p></td>
            </tr>
                    <?php
                }
            ?>                        
        </table>    
    </div>
</div>