<div id="sanpham_cate">
    <ul class="breadcrumb">
        <li><a href="" style="padding-left:0 !important;">Trang chủ</a><span>/</span></li>
        <li class="active"><span>Trang cá nhân</span></li>
        <div class="clear"></div>
    </ul>
    <div id="panel_form">
        <?php 
            if(isset($_SESSION['username']))
            {
                $this->db->where('name',$_SESSION['username']);
                $sqlusert=$this->db->get('tbladmin')->row();
            }            
        ?>
        <?php 
            if(isset($thanhcong))
            {
        ?>
        <div class="boxSuccess">
            Cập nhật thông tin tài khoản thành công!
        </div>
        <?php 
        }
        ?>
        <form name="frmsuathongtin" method="post" action="<?php echo site_url('sua-tai-khoan.html') ?>" enctype="multipart/form-data">
            <table>
                <tr>
                    <th>Họ tên</th>
                    <td><input type="text" name="hoten" value="<?php echo $sqlusert->fullname; ?>" />
                    <input type="hidden" name="txtid" value="<?php echo $sqlusert->id; ?>" />
                    </td>
                </tr>
                <tr>
                    <th>Điện thoại</th>
                    <td><input type="text" name="dienthoai" value="<?php echo $sqlusert->dienthoai; ?>" /></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><input type="text" name="email" value="<?php echo $sqlusert->email; ?>" readonly="true" /></td>
                </tr>
                <tr>
                    <th>Ảnh đại diện</th>
                    <td><input type="file" name="avatar" value="" />
                    <input type="hidden" name="anh" value="<?php echo $sqlusert->image; ?>" />
                    <input type="hidden" name="anh_thumb" value="<?php echo $sqlusert->thumb; ?>" /><br /><br />
                    <?php 
                    if($sqlusert->image!='')
                    {
                    ?>
                    <img src="<?php echo linkanh().$sqlusert->thumb; ?>" />
                    <?php    
                    }
                    ?>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td><input type="submit" name="submit" value="Lưu thay đổi" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>