<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
if(isset($_SESSION['username']))
{
     $this->db->where('name',$_SESSION['username']);
}
if(isset($_COOKIE['user']))
{
    $this->db->where('name',$_COOKIE['user']);    
}
$admin=$this->db->get('tbladmin')->row();
$listdc=$CI->site_model->gettablename_all('tbldiachi','id,title,diachi,thutu,status','','name',$admin->id,'','');
?>
<div id="panel">
    <p id="panel_title">Trang cá nhân</p>
    <div id="panel_main">
        <ul id="panel_top">
            <li><a href="<?php echo site_url('trang-ca-nhan.html'); ?>" <?php if(isset($ctp)){ ?> style="background:#fff;" <?php } ?>>Sửa thông tin</a></li>
            <li><a href="<?php echo site_url('dia-chi-cua-hang.html'); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ cửa hàng">Địa chỉ cửa hàng</a></li>
            <li><a href="">Theo dõi đơn hàng</a></li>
            <li><a href="<?php echo site_url('doi-mat-khau.html') ?>" <?php if(isset($doimk)){ ?> style="background:#fff;" <?php } ?>>Đổi mật khẩu</a></li>
        </ul>
    </div>
    <div id="panel_form">  
        <a href="<?php echo site_url('dia-chi-cua-hang.html'); ?>" style="display:block;padding:10px;background:#3883cc;width:80px;margin-bottom:10px;color:#fff;;">Thêm mới</a>      
        <table id="list_dc">
            <tr>
                <th>STT</th>
                <th>Tiêu đề</th>
                <th>Địa chỉ</th>
                <th>Thứ tự</th>
                <th>Trạng thái</th>
                <th>Sửa</th>
                <th>Xóa</th>
            </tr>
            <?php 
                if($listdc->num_rows()>0)
                {
                    $demdc=1;
                    foreach($listdc->result() as $itemlistdc)
                    {
                    ?>
                    <tr>  
                        <td style="width:3%;"><?php echo $demdc; ?></td> 
                        <td><?php echo $itemlistdc->title; ?></td>    
                        <td><?php echo $itemlistdc->diachi; ?></td>     
                        <td style="width:5%;"><?php echo $itemlistdc->thutu; ?></td>
                        <td style="width:10%;"><?php 
                        if($itemlistdc->status==1)
                        {
                            echo 'Hiện thị';
                        }
                        else
                        {
                            echo 'Không hiện thị';
                        }
                         ?></td>
                         <td style="width:3%;"><a href="<?php echo site_url('site/editdiachi/'.$itemlistdc->id); ?>" title="Edit"><img src="images/Edit.png" title="edit" alt="edit" width="16" /></a></td>
                        <td style="width:3%;"><a onclick="return confirm('Bạn có muốn xóa thật không ?');" href="<?php echo site_url('site/xoadiachi/'.$itemlistdc->id); ?>" title="Xóa"><img src="images/Delete.png" width="16" title="Xóa" alt="Xóa" /></a></td>
                    </tr>
                    <?php 
                    $demdc++;
                    }
                }
            ?>
        </table>    
    </div>
</div>