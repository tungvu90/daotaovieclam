<div id="bg_huhu">
<?php 
if(isset($dm))
{
    $this->db->where('id',$dm);
    $this->db->select('id,name,alias,thutu,status');
    $sqlchuyenmuc=$this->db->get('tblchuyenmuc')->row();
    
?>
<script>
$(function(){
    $more=$('#more');
    $start=15;// 20 kết quả mặc định
	$id=<?php echo $dm; ?>;
    $text_default=$more.text();
    $more.click(function(){
        if(!$(this).hasClass('clicked')){
            $(this).addClass('clicked').text('Đang tải dữ liệu...');
            $go_to=$('.main-content .tintuc_item:last').offset().top;
            $.post(
                '<?php echo site_url('site/load_item_ajax/'); ?>',
                {start:$start,id:$id},
                function(data){
                    if(data){
                        $('.main-content').append(data);
                        $more.removeClass('clicked').text($text_default);
                        $('html, body').animate({scrollTop:$go_to},800);
                        $start+=5;// 10 giá trị load ra
                    }else{
                        $more.remove();
                    }
                }
            );
        }
    });
});
</script>
    <ul class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>">Trang chủ</a><span>/</span></li>
        <li><a href="<?php echo site_url($sqlchuyenmuc->alias.'-c'.$sqlchuyenmuc->id.'.html'); ?>" class="activeh"><?php echo $sqlchuyenmuc->name; ?></a></li>
        <div class="clear"></div>
    </ul>
    <div class="main-content" style="background:#fff !important;">
<?php 
    $sqltintuccm="SELECT id,title,image,alias,thumb,mota,thutu,status FROM tblbaiviet WHERE status=1 AND catid=$dm OR ncatid=$dm order by thutu desc,id desc LIMIT 15";
    $sqltintuccm1=$this->db->query($sqltintuccm);
    if($sqltintuccm1->num_rows()>0)
    {
        foreach($sqltintuccm1->result() as $itemtintuccm1)
        {
        ?>
        <div class="tintuc_item">
            <a href="<?php echo site_url($itemtintuccm1->alias.'-'.$itemtintuccm1->id.'.html'); ?>" class="tintuc_item_img"><img src="<?php echo linkanh().$itemtintuccm1->image; ?>" /></a>
            <a href="<?php echo site_url($itemtintuccm1->alias.'-'.$itemtintuccm1->id.'.html'); ?>" class="tintuc_item_name"><?php echo $itemtintuccm1->title; ?></a>
            <p><?php echo catchuoi($itemtintuccm1->mota,300); ?></p>
            <div class="clear"></div>
        </div>
        <?php
        }
    } 
}
?>
    </div>
    <div id="more">Hiển thị thêm</div>
    <div class="clear"></div>
</div>