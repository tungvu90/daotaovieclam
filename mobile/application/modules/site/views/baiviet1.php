<script type="text/javascript" src="tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
	menubar : false,
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
});
</script>
<script language="javascript">
	
	function load_ajax_form(id){
		if($('input.buttonq').hasClass('clicked')){
			$('input.buttonq').removeClass('clicked');
			$('.itemcm .comment').remove();
		}
		else{
			$('.itemcm.cm'+id+' input.buttonq').addClass('clicked');		
			$.ajax({
				url : "<?php echo site_url('site/load_item_ajax1/'); ?>",
				type : "post",
				dateType:"text",
				data : {
					parent : id,
					itemid : <?php echo $item->id; ?>
				},
				success : function (data){
					$('.itemcm.cm'+id).append(data);				
				}
			});
		}
	}
	
	//Phan trang
$(function(){
	$more=$('#more');
    $start=11;
	$id=<?php echo $id; ?>;
    $text_default=$more.text();
    $more.click(function(){
        if(!$(this).hasClass('clicked')){
            $(this).addClass('clicked').text('Đang tải dữ liệu...');
            $go_to=$('.list-cm .itemcm:last').offset().top;
            $.post(
                '<?php echo site_url('site/load_cm_ajax/'); ?>',
                {start:$start,id:$id},
                function(data){
                    if(data){
                        $('.list-cm').append(data);
                        $more.removeClass('clicked').text($text_default);
                        $('html, body').animate({scrollTop:$go_to},800);
                        $start+=10;// 10 giá trị load ra
                    }else{
                        $more.remove();
                    }
                }
            );
        }
    });
});
</script>
<?php
$CI=&get_instance();
$CI->load->model('site/site_model');
//Kiem tra date
date_default_timezone_set('Asia/Ho_Chi_Minh');
$day = date('Y-m-d H:i:s');
$diff = abs(strtotime($day) - strtotime($item->created_day));
$years = floor($diff / (365*60*60*24));
$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
if($days==0 and $years==0 and $months==0){
	$daytime = "Cách đây ";
	if($hours!=0){$daytime .= $hours." giờ, ";}
	$daytime .= $minutes." phút";
}
else{
	$base=explode(' ',$item->created_day);
	$baseday=explode('-',$base[0]);
	$basetime=substr($base[1],0,-3);
	$daytime=$baseday[2].'/'.$baseday[1].'/'.$baseday[0].' '.$basetime;
}
/////////////////////////////////
$category='';
$category=$CI->site_model->getcatlink($item->catid);
$catitem=$CI->site_model->getitem('tblchuyenmuc',$item->catid)->row();
?>
<div class="main-content">
	<div class="category"><a href="<?php echo site_url($category.'-c'.$item->catid.'.html'); ?>"><?php echo $catitem->name; ?></a>
		<p class="date"><?php echo $daytime; ?></p>
	</div>									
	<div class="box-item">			
		<span class="short-title"><?php echo $item->extra; ?></span>
		<?php if($item->sukien!=0){
				$sukien=$CI->site_model->getitem('tblsukien',$item->sukien)->row();
		?>
		<p class="event"><a href="<?php echo base_url().'event/'.$sukien->alias.'-'.$sukien->id;?>"><?php echo $sukien->name;?></a></p>
		<?php } ?>
		<h3 class="title"><?php echo $item->title; ?></h3>
		<div class="sapo">				
			<?php echo $item->sapo; ?>
		</div>
		<?php 		
		if($item->tinlienquan!=''){
			//Xu ly tin lien quan theo lua chon
			$list_lq=$item->tinlienquan;
			$sql3="SELECT id,catid,title,alias FROM tblbaiviet WHERE status=1 AND id IN ($list_lq) ORDER BY id DESC";
			$item_lqs=$this->db->query($sql3);	
			foreach($item_lqs->result() as $item_lq){
			$lq_cat='';
			$lq_cat=$CI->site_model->getcatlink($item_lq->catid);
			?>
				<p class="item-lq"><a href="<?php echo site_url($lq_cat.'/'.$item_lq->alias.'-'.$item_lq->id).'.html';?>"><?php echo $item_lq->title;?></a></p>
			<?php
			}
		}
		?>
		<div class="fulltext">		
			<?php echo $item->fulltext; ?>
			<?php if($item->tacgia!=''){?><p class="tacgia"><?php echo $item->tacgia; ?></p><?php } ?>
			<?php if($item->nguon!=''){?><p class="nguontin"><?php echo $item->nguon; ?></p><?php } ?>
		</div>			
		<?php if($item->tags!=''){ 
		$tags=explode(',',$item->tags);
		?>
		<p class="tags">Từ khóa: <?php 
		$tag='';
		$this->load->helper('locdau');
		for($a=0;$a<count($tags);$a++){
			$tagl=vn_str_filter($tags[$a]);
			$tag.='<a href="'.site_url('tags/'.$tagl).'">'.$tags[$a].'</a>';					
		}
		echo $tag; ?></p>
		<?php } ?>
		<div class="social">			
		<?php $category=''; $category=$CI->site_model->getcatlink($item->catid);?>
			<a title="Link hay!" href="http://linkhay.com/submit"><img src="images/embed-btn.png" width="65" height="20" alt="Link hay!" /></a>
			<div class="fb-like" data-href="<?php echo site_url($category.'/'.$item->alias.'-'.$item->id).'.html';?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>						
		</div>		
		<!-- Comment -->
		<?php 
		$listcms=$CI->site_model->gettbl_comment($item->id,0,10);
		if($listcms->num_rows>0){
		?>
		<h3 class="title-cm-list">Ý kiến bạn đọc</h3>
		<div class="list-cm">
		<?php foreach($listcms->result() as $listcm){
				$base=explode(' ',$listcm->created_day);
				$baseday=explode('-',$base[0]);
				$basetime=substr($base[1],0,-3);
				$daytime=$baseday[2].'/'.$baseday[1].'/'.$baseday[0].' '.$basetime;				
		?>
			<div class="itemcm cm<?php echo $listcm->id; ?>">
				<div class="top">
					<span class="name"><?php echo $listcm->name; ?></span><span class="day">(<?php echo $daytime; ?>)</span>
					<span class="mail"> - email: <?php echo $listcm->mail; ?></span>					
				</div>
				<div class="bottom">
					<p class="cm_intro"><?php echo $listcm->comment; ?></p>
				</div>
				<input type="button" class="buttonq" name="submit" onclick="load_ajax_form(<?php echo $listcm->id; ?>)" value="Trả lời"/>
			</div>			
		<?php 
			$listcm_childs=$CI->site_model->gettbl_comment1($item->id,$listcm->id,5);
		}?>
		</div>
		<?php if($listcms->num_rows>=10){?>
		<div id="more">Xem thêm các bình luận</div>
		<?php } }  ?>
		<h3 class="title-cm">Gửi bình luận</h3>
		<div class="comment">			
			<form name="frmcomment" action="<?php echo site_url('site/result_comment1'); ?>" method="post">			
			<table width="100%">
				<input name="itemid" type="hidden" value="<?php echo $item->id; ?>">
				<tr>
					<td><input name="name" id="name" type="text" value="Họ tên" onfocus="if(this.value  == 'Họ tên') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Họ tên'; } "></td>
					<td><input name="mail" id="mail" type="text" value="Email" onfocus="if(this.value  == 'Email') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Email'; } "></td>
				</tr>
				<tr>
					<td colspan="2"><textarea rows="3" cols="70" name="comment" id="comment" /></textarea></td>				
				</tr>
				<script type="text/javascript">   
					function changeCaptcha()
					{
						document.getElementById("captchaImage").src="captcha_new.php?len="+(Math.random()*4+4);
						document.getElementById("load_img").src="images/indicator_arrows.gif";
						setTimeout(function(){jQuery('#load_img').attr({src : "images/indicator_arrows_static.gif"}); }, 500);
					}
				</script>  
				<tr>
					<td>Mã bảo vệ</td>				
					<td>
						<input type="text" id="mabaove" value="" style="width:100px;margin-right:10px;" name="mabaove" size="5" /> <img src="captcha_new.php" id="captchaImage"/><img id="load_img" src="images/indicator_arrows_static.gif" onclick="changeCaptcha()" style="cursor:pointer;margin-bottom:5px;"/>
					</td>				
				</tr>
				<tr>
					<td colspan="2"><span class="warning"></span>
					<input type="submit" class="button" name="submit" value="Gửi thông tin"/></td>
				</tr>
			</table>
			</form>
		</div>		
		<?php
		if($item->tags!=''){		
			$sql2="SELECT id,catid,title,thumb,alias FROM tblbaiviet where status=1 AND id !=$item->id AND created_day<='$day' AND (";
			for($a=0;$a<count($tags);$a++){
				$tag = trim($tags[$a]);
				$sql2 .=" tags LIKE '%$tag%'";
				if($a<count($tags)-1){
					$sql2 .=" OR";
				}
			}
			$sql2 .=") ORDER BY id DESC LIMIT 6";
			$item_tags=$this->db->query($sql2);			
		?>
		<?php if($item_tags->num_rows()!=0){ ?>
		<div class="box-tin1">															
			<h3><span>Tin liên quan</span></h3>
			<div class="box-cat-inner1">			
				<?php 
				$tag_i=0;
				foreach($item_tags->result() as $item_tag){
				$tag_i++;			
				$tag_cat='';
				$tag_cat=$CI->site_model->getcatlink($item_tag->catid);
				$tag_title = '';
				$tag_titles=explode(' ',$item_tag->title);				
				for($j=0;$j<count($tag_titles);$j++){
					$tag_title .= $tag_titles[$j].' ';
					if($j==10){
						$tag_title = $tag_title.'...';
						break;
					}
				}	
				?>	
					<div class="item">
						<div class="item-inner">
							<div class="swap-img">
								<img src="<?php if($item_tag->thumb!=''){echo $item_tag->thumb;}else{echo 'upload/no-img.png';} ?>">
							</div>
							<a class="title" title="<?php echo $item_tag->title; ?>" href="<?php echo site_url($tag_cat.'/'.$item_tag->alias.'-'.$item_tag->id).'.html';?>"><?php echo $tag_title; ?></a>							
						</div>
					</div>					
				<?php } ?>							
			</div>							
		</div>
		<?php } ?>
		<div class="clr"></div>
		<?php } ?>
		<?php 
		$sql="SELECT id,catid,title,thumb,alias FROM tblbaiviet where status=1 AND created_day<='$day' ORDER BY id DESC";
		$rowskt=$this->db->query($sql)->result();	
		$j = 0;
		$limit=9;
		for($kt = 0; $kt <= count($rowskt); $kt++){
			if($item->id == $rowskt[$kt]->id){break;}
			else{ $j++; }
		}
		$sql1="SELECT id,catid,title,thumb,alias FROM tblbaiviet where status=1 AND id!=$item->id AND created_day<='$day' ORDER BY id DESC";	
		if($j <= $limit-5 and $limit >= 9){		
			$sql1 .=" LIMIT 0,$limit";
		}
		else{		
			$starts = $j-5;
			$sql1 .=" LIMIT $starts,$limit";
		}	
		$tlqs=$this->db->query($sql1);
		if($tlqs->num_rows()){ ?>
		<div class="box-tin1 style2">
			<h3><span>Tin đã đăng</span></h3>
				<div class="box-cat-inner1">
				<?php 			
					foreach($tlqs->result() as $tlq){
					$tlq_cat='';
					$tlq_cat=$CI->site_model->getcatlink($tlq->catid);
					$tlq_title = '';
					$tlq_titles=explode(' ',$tlq->title);				
					for($j=0;$j<count($tlq_titles);$j++){
						$tlq_title .= $tlq_titles[$j].' ';
						if($j==10){
							$tlq_title = $tlq_title.'...';
							break;
						}
					}	
				?>
				<div class="item">
					<div class="item-inner">
						<div class="swap-img">
						<?php 
						if($tlq->thumb!=''){
							$thumb=explode('.',$tlq->thumb);
							$tlq_thumb=$thumb[0].'_290x185'.'.'.$thumb[1];
						}		
						else{
							$tlq_thumb='upload/no-img.png';
						}
						?>
							<img src="<?php echo $tlq_thumb; ?>">
						</div>
						<a class="title" title="<?php echo $tlq->title; ?>" href="<?php echo site_url($tlq_cat.'/'.$tlq->alias.'-'.$tlq->id).'.html';?>"><?php echo $tlq_title; ?></a>							
					</div>
				</div>			
			<?php } ?>
				</div>							
			</div>
		</div>
		<?php } ?>
</div>	