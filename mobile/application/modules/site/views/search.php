<script>
$(function(){
    $more=$('#more');
    $start=15;// 20 kết quả mặc định
	$txt_search='<?php echo $txt_search; ?>';
    $text_default=$more.text();
    $more.click(function(){
        if(!$(this).hasClass('clicked')){
            $(this).addClass('clicked').text('Đang tải dữ liệu...');
            $go_to=$('.main-content .tintuc_item:last').offset().top;
            $.post(
                '<?php echo site_url('site/load_item_ajax1/'); ?>',
                {start:$start,txt_search:$txt_search},
                function(data){
                    if(data){
                        $('.main-content').append(data);
                        $more.removeClass('clicked').text($text_default);
                        $('html, body').animate({scrollTop:$go_to},800);
                        $start+=5;// 10 giá trị load ra
                    }else{
                        $more.remove();
                    }
                }
            );
        }
    });
});
</script>
<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
?>
<div id="bg_huhu">    
<ul class="breadcrumb">
    <li><a href="">Trang chủ</a><span>/</span></li>
    <li style="background:none;"><a>Kết quả tìm kiếm</a></li>            
    <div class="clear"></div>
</ul>
<div class="main-content" style="background:none !important;">
    	<?php 
    	$kt=0;
    	foreach($query->result() as $item){
    	$kt++;    	
    	?>
    	<div class="tintuc_item">
            <a href="<?php echo site_url($item->alias.'-'.$item->id.'.html'); ?>" class="tintuc_item_img"><img src="<?php echo linkanh().$item->thumb; ?>" /></a>
            <a href="<?php echo site_url($item->alias.'-'.$item->id.'.html'); ?>" class="tintuc_item_name"><?php echo $item->title; ?></a>
            <p><?php echo catchuoi($item->mota,600); ?></p>
            <div class="clear"></div>
        </div>
         <?php 
         }
         ?>                   	
        <div class="clear"></div>            
    </div>  
    <div id="more">Hiển thị thêm</div>          
    <div class="clear"></div>            
</div>            