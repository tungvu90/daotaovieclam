<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
$contactinfo=$CI->site_model->gettbl('tblfooter','','')->row();
$address = get_infor_from_address($contactinfo->diachi);
?> 
<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script>
	function initialize() {
		var myLatlng = new google.maps.LatLng(<?php echo $contactinfo->toado; ?>);
		var mapOptions = {
			zoom: 16,
			center: myLatlng
		};
		var map = new google.maps.Map(document.getElementById('div_id'), mapOptions);
		var contentString = "<table><tr><th><?php echo $contactinfo->tencongty; ?></th></tr><tr><td>Ðịa chỉ:&nbsp;<?php echo $contactinfo->diachi; ?></td></tr><tr><td>Điện thoại:&nbsp;<?php echo $contactinfo->dienthoai; ?></td></tr></table>";
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			title: '<?php echo $contactinfo->tencongty; ?>'
		});
		infowindow.open(map, marker);
	}
	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="bg_huhu">    
        <ul class="breadcrumb">
            <li><a href="">Trang chủ</a><span>/</span></li>
            <li style="background:none;"><a>Liên hệ</a></li>
            <div class="clear"></div>
        </ul>             
        <div id="info_contact">
            <ul>
				<li id="name-contact">
				    <h3 style="margin:0;background:none;margin-bottom:10px;color:red;font-size:16px;font-weight:bold;text-transform:uppercase;"><?php echo $contactinfo->tencongty; ?></h3>
				</li>
				<li style="color:#333;"><span><b>Địa chỉ:</b></span>&nbsp;<?php echo $contactinfo->diachi; ?></li>                                            
				<li style="color:#333;"><span><b>Điện thoại:</b></span>&nbsp;<?php echo $contactinfo->dienthoai; ?></li>                                                                					                
                <li style="color:#333;"><span><b>Email:</b></span>&nbsp;<a href="mailto:<?php echo $contactinfo->email; ?>"><?php echo $contactinfo->email; ?></a></li>
            </ul>
            <?php 
                if(isset($errors_register))
                {                                       
                ?>
                    <div id="error_register" style="margin-left:10px;margin-right:10px;">
                        <fieldset>
                            <legend>Thông báo hệ thống</legend>
                            <?php echo $errors_register; ?>
                        </fieldset>
                    </div>
                <?php    
                }
                if(isset($kq))
                {
                ?>
                <div id="error_register" style="margin-left:10px;margin-right:10px;">
                        <fieldset>
                            <legend>Thông báo hệ thống</legend>
                            <p style="color:blue;">Cám ơn bạn đã gửi thông tin liên hệ</p>
                        </fieldset>
                    </div>
                <?php    
                }
            ?>       
            <form method="post" name="frmcontact" action="<?php echo site_url('site/docontact'); ?>">				
				<div class="request-formm">
					<div class="caption">
						<span>Họ tên:</span>
					</div>
					<div class="column">
						<input type="text" name="txthoten" value=""/>
					</div>
				</div>
				<div class="request-formm">
					<div class="caption">
						<span>Địa chỉ:</span>
					</div>
					<div class="column">
						<input type="text" name="txtdc" value=""/>
					</div>
				</div>
				<div class="request-formm">
					<div class="caption">
						<span>Điện thoại:</span>
					</div>
					<div class="column">
						<input type="text" name="txtdt" value=""/>
					</div>
				</div>
				<div class="request-formm">
					<div class="caption">
						<span>Email:</span>
					</div>
					<div class="column">
						<input type="text" name="txtemail" value=""/>
					</div>
				</div>
				<div class="request-formm">
					<div class="caption">
						<span>Nội dung:</span>
					</div>
					<div class="column">
						<textarea rows="5" style="width:100%; " name="txtnd" id="txtnd"></textarea>
					</div>
				</div>
				<div class="request-formm">					
					<input type="submit" class="nut" name="cbg" value="Gửi"/>
					<input type="reset" class="nut" value="Làm lại"/>
				</div>
			</form>
            <div class="clear"></div>
        </div>
<div class="clear"></div>
</div>             