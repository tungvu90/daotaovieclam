
CKEDITOR.editorConfig = function( config )
{
        // Define changes to default configuration here. For example:
         config.language = 'vi';
            
        
        config.toolbar_Full = [
            ['Source','-','Save','NewPage','Preview','-','Templates'],
            ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
            ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
            ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
            '/',
            ['Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','CopyFormatting','RemoveFormat'],
            ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
            ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
            ['BidiLtr', 'BidiRtl' ],
            ['Link','Unlink','Anchor'],
            ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe'],
            '/',
            ['Styles','Format','Font','FontSize'],
            ['TextColor','BGColor'],
            ['Maximize', 'ShowBlocks','-','About']
            ];
        
        config.entities = false;
        config.uiColor= '#44B8C4';
        config.extraPlugins = 'videoembed,tableresize';  
        config.filebrowserBrowseUrl = 'https://daotaovieclam.edu.vn/ckfinder/ckfinders.php';

        config.filebrowserImageBrowseUrl = 'https://daotaovieclam.edu.vn/ckfinder/ckfinders.php?type=Images';

        config.filebrowserFlashBrowseUrl = 'https://daotaovieclam.edu.vn/ckfinder/ckfinders.php?type=Flash';

        config.filebrowserUploadUrl = 'https://daotaovieclam.edu.vn/core/connector/php/connector.php?command=QuickUpload&type=Files';

        config.filebrowserImageUploadUrl = 'https://daotaovieclam.edu.vn/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';

        config.filebrowserFlashUploadUrl = 'https://daotaovieclam.edu.vn/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

};  