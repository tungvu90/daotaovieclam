// Sticky Plugin v1.0.0 for jQuery
// =============
// Author: Anthony Garand
// Improvements by German M. Bravo (Kronuz) and Ruud Kamphuis (ruudk)
// Improvements by Leonardo C. Daronco (daronco)
// Created: 2/14/2011
// Date: 2/12/2012
// Website: http://labs.anthonygarand.com/sticky
// Description: Makes an element on the page stick on the screen as you scroll
//       It will only set the 'top' and 'position' of your element, you
//       might need to adjust the width in some cases.

(function($) {
  var defaults = {
      topSpacing: 0,
      bottomSpacing: 0,
      className: 'is-sticky',
      wrapperClassName: 'sticky-wrapper',
      center: false,
      getWidthFrom: '',
      responsiveWidth: false
    },
    $window = $(window),
    $document = $(document),
    sticked = [],
    windowHeight = $window.height(),
    scroller = function() {
      var scrollTop = $window.scrollTop(),
        documentHeight = $document.height(),
        dwh = documentHeight - windowHeight,
        extra = (scrollTop > dwh) ? dwh - scrollTop : 0;

      for (var i = 0; i < sticked.length; i++) {
        var s = sticked[i],
          elementTop = s.stickyWrapper.offset().top,
          etse = elementTop - s.topSpacing - extra;

        if (scrollTop <= etse) {
          if (s.currentTop !== null) {
            s.stickyElement
              .css('width', '')
              .css('position', '')
              .css('top', '');
            s.stickyElement.trigger('sticky-end', [s]).parent().removeClass(s.className);
            s.currentTop = null;
          }
        }
        else {
          var newTop = documentHeight - s.stickyElement.outerHeight()
            - s.topSpacing - s.bottomSpacing - scrollTop - extra;
          if (newTop < 0) {
            newTop = newTop + s.topSpacing;
          } else {
            newTop = s.topSpacing;
          }
          if (s.currentTop != newTop) {
            s.stickyElement
              .css('width', s.stickyElement.width())
              .css('position', 'fixed')
              .css('top', newTop);

            if (typeof s.getWidthFrom !== 'undefined') {
              s.stickyElement.css('width', $(s.getWidthFrom).width());
            }

            s.stickyElement.trigger('sticky-start', [s]).parent().addClass(s.className);
            s.currentTop = newTop;
          }
        }
      }
    },
    resizer = function() {
      windowHeight = $window.height();

      for (var i = 0; i < sticked.length; i++) {
        var s = sticked[i];
        if (typeof s.getWidthFrom !== 'undefined' && s.responsiveWidth === true) {
          s.stickyElement.css('width', $(s.getWidthFrom).width());
        }
      }
    },
    methods = {
      init: function(options) {
        var o = $.extend({}, defaults, options);
        return this.each(function() {
          var stickyElement = $(this);

          var stickyId = stickyElement.attr('id');
          var wrapperId = stickyId ? stickyId + '-' + defaults.wrapperClassName : defaults.wrapperClassName 
          var wrapper = $('<div></div>')
            .attr('id', stickyId + '-sticky-wrapper')
            .addClass(o.wrapperClassName);
          stickyElement.wrapAll(wrapper);

          if (o.center) {
            stickyElement.parent().css({width:stickyElement.outerWidth(),marginLeft:"auto",marginRight:"auto"});
          }

          if (stickyElement.css("float") == "right") {
            stickyElement.css({"float":"none"}).parent().css({"float":"right"});
          }

          var stickyWrapper = stickyElement.parent();
          stickyWrapper.css('height', stickyElement.outerHeight());
          sticked.push({
            topSpacing: o.topSpacing,
            bottomSpacing: o.bottomSpacing,
            stickyElement: stickyElement,
            currentTop: null,
            stickyWrapper: stickyWrapper,
            className: o.className,
            getWidthFrom: o.getWidthFrom,
            responsiveWidth: o.responsiveWidth
          });
        });
      },
      update: scroller,
      unstick: function(options) {
        return this.each(function() {
          var unstickyElement = $(this);

          var removeIdx = -1;
          for (var i = 0; i < sticked.length; i++)
          {
            if (sticked[i].stickyElement.get(0) == unstickyElement.get(0))
            {
                removeIdx = i;
            }
          }
          if(removeIdx != -1)
          {
            sticked.splice(removeIdx,1);
            unstickyElement.unwrap();
            unstickyElement.removeAttr('style');
          }
        });
      }
    };

  // should be more efficient than using $window.scroll(scroller) and $window.resize(resizer):
  if (window.addEventListener) {
    window.addEventListener('scroll', scroller, false);
    window.addEventListener('resize', resizer, false);
  } else if (window.attachEvent) {
    window.attachEvent('onscroll', scroller);
    window.attachEvent('onresize', resizer);
  }

  $.fn.sticky = function(method) {
    if (methods[method]) {
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }
  };

  $.fn.unstick = function(method) {
    if (methods[method]) {
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method ) {
      return methods.unstick.apply( this, arguments );
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.sticky');
    }

  };
  $(function() {
    setTimeout(scroller, 0);
  });
})(jQuery);
ky").parent):a.parent(),g=function(){var b=a.parent("."+a.data("hcSticky").wrapperClassName);return 0<b.length?(b.css({height:a.outerHeight(!0),width:function(){var c=e(b,"width").value;e(b).remove();return 0<=c.indexOf("%")||"auto"==c?(a.css("width",b.width()),c):a.outerWidth(!0)}()}),b):!1}()||function(){var c=b("<div>",{"class":a.data("hcSticky").wrapperClassName}).css({height:a.outerHeight(!0),width:function(){var b=e(a,"width").value;return 0<=b.indexOf("%")||"auto"==b?(a.css("width",parseFloat(a.css("width"))),b):"auto"==e(a,"margin-left").value?a.outerWidth():a.outerWidth(!0)}(),margin:e(a,"margin-left").value?"auto":null,position:function(){var b=a.css("position");return"static"==b?"relative":b}(),"float":a.css("float")||null,left:e(a,"left").value,right:e(a,"right").value,top:e(a,"top").value,bottom:e(a,"bottom").value});a.wrap(c);return a.parent()}(),f=function(b){a.hasClass(a.data("hcSticky").className)||(b=b||{},a.css({position:"fixed",top:b.top||0,left:b.left||g.offset().left}).addClass(a.data("hcSticky").className),a.data("hcSticky").onStart.apply(this))},k=function(b){b=b||{};a.css({position:b.position||"absolute",top:b.top||0,left:b.left||0}).removeClass(a.data("hcSticky").className);a.data("hcSticky").onStop.apply(this)};e(a).remove();a.css({top:"auto",bottom:"auto",left:"auto",right:"auto"});if(a.outerHeight(!0)>d.height())return this;b(window).load(function(){a.outerHeight(!0)>d.height()&&(g.css("height",a.outerHeight(!0)),a.hcSticky("reinit"))});var m=!1,h=!1;p.on("resize",function(){a.data("hcSticky").responsive&&(h||(h=a.clone().attr("style","").css({visibility:"hidden",height:0,overflow:"hidden",paddingTop:0,paddingBottom:0,marginTop:0,marginBottom:0}),g.after(h)),e(h,"width").value!=e(g,"width").value&&g.width(e(h,"width").value),e(g).remove(),m&&clearTimeout(m),m=setTimeout(function(){m=!1;e(h).remove();h.remove();h=!1},100));"fixed"==a.css("position")?a.css("left",g.offset().left):a.css("left",0);a.width()!=g.width()&&a.css("width",g.width())});a.data("hcSticky",b.extend(a.data("hcSticky"),{f:function(){}}));a.data("hcSticky",b.extend(a.data("hcSticky"),{f:function(e){$referrer=a.data("hcSticky").noContainer?t:a.data("hcSticky").parent?b(a.data("hcSticky").parent):g.parent();if(a.data("hcSticky").on&&!(a.outerHeight(!0)>=$referrer.height())){var d=a.data("hcSticky").innerSticker?b(a.data("hcSticky").innerSticker).position().top:a.data("hcSticky").innerTop?a.data("hcSticky").innerTop:0,h=g.offset().top,m=$referrer.height()-a.data("hcSticky").bottomEnd+(a.data("hcSticky").noContainer?0:h),r=g.offset().top-a.data("hcSticky").top+d,l=a.outerHeight(!0)+a.data("hcSticky").bottom,n=p.height(),s=p.scrollTop(),q=a.offset().top,u=q-s,v;s>=r?m+a.data("hcSticky").bottom-(a.data("hcSticky").followScroll?0:a.data("hcSticky").top)<=s+l-d-(l-d>n-(r-d)&&a.data("hcSticky").followScroll?0<(v=l-n-d)?v:0:0)?k({top:m-l+a.data("hcSticky").bottom-h}):l-d>n&&a.data("hcSticky").followScroll?u+l<=n?"down"==c.direction?f({top:n-l}):0>u&&"fixed"==a.css("position")&&k({top:q-(r+a.data("hcSticky").top-d)-c.distanceY}):"up"==c.direction&&q>=s+a.data("hcSticky").top-d?f({top:a.data("hcSticky").top-d}):"down"==c.direction&&(q+l>n&&"fixed"==a.css("position"))&&k({top:q-(r+a.data("hcSticky").top-d)-c.distanceY}):f({top:a.data("hcSticky").top-d}):k();!0===e&&a.css("top","fixed"==a.css("position")?a.data("hcSticky").top-d:0)}}}));a.data("hcSticky").f(!0);p.on("scroll",a.data("hcSticky").f)})}})})(jQuery);