<!DOCTYPE html>
<html dir="ltr">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <base href="<?php echo base_url(); ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="keywords" content="trung tâm ngoại ngữ hai phong, trung tâm dao tao ngoai ngu tin hoc hai phong, day seo hai phong, day tin hoc van phong, day đồ họa, trung tam ngoai ngu hai phong"/>
    <meta name="description" content="Trung tâm đào tạo tin học ngoại ngữ AMANDA chuyên đào tạo tin học Hải Phòng, Đào tạo ngoại ngữ dạy nghề các môn tin học văn phòng, dạy học ngoại ngữ tiếng hàn, trung, nhật, anh, đức. SEO Website, dạy 3dsmax, dạy autocad, đồ họa máy tính."/>
    <meta name="robots" content="noindex,nofollow" />
    <title>Trung Tâm Tin học Đồ họa Ngoại ngữ tại Hải Phòng | Địa Chỉ Học Uy Tín</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="daotao.ico"/>
    <!-- Custom CSS -->
    <link href="backend/css/style.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <div class="main-wrapper">
      <!-- ============================================================== -->
      <!-- Preloader - style you can find in spinners.css -->
      <!-- ============================================================== -->
      <div class="preloader">
        <div class="lds-ripple">
          <div class="lds-pos"></div>
          <div class="lds-pos"></div>
        </div>
      </div>
      <!-- ============================================================== -->
      <!-- Preloader - style you can find in spinners.css -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Login box.scss -->
      <!-- ============================================================== -->
      <div class="error-box">
        <div class="error-body text-center">
          <h1 class="error-title text-danger">404</h1>
          <h3 class="text-uppercase error-subtitle">PAGE NOT FOUND !</h3>
          <p class="text-muted mt-4 mb-4">
            Xin lỗi. Đường link page không tồn tại !
          </p>
          <a href="<?php echo base_url(); ?>" class="btn btn-danger btn-rounded waves-effect waves-light mb-5 text-white">Quay lại trang chủ</a>
        </div>
      </div>
      <!-- ============================================================== -->
      <!-- Login box.scss -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Page wrapper scss in scafholding.scss -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Page wrapper scss in scafholding.scss -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Right Sidebar -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Right Sidebar -->
      <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- All Required js -->
    <!-- ============================================================== -->
    <script src="backend/assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="backend/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugin js -->
    <!-- ============================================================== -->
    <script>
      $(".preloader").fadeOut();
    </script>
  </body>
</html>
