<div class="box_trai" itemscope="itemscope" itemtype="http://schema.org/WPSideBar">
    <div class="box_trai_top">
        <p><?php echo $this->lang->line('latest_news'); ?></p>
    </div>
    <div class="box_trai_main">
        <?php 
            $this->db->where('status',1);
            $this->db->order_by('sxtt','desc');
            $this->db->order_by('id','desc');
            $this->db->select('id,title,alias');
            $this->db->limit(5);
            $sqltinmoicm=$this->db->get('tblbaiviet');
            if($sqltinmoicm->num_rows()>0)
            {
            ?>
            <ul id="tinmoi_cm">
                <?php 
                    foreach($sqltinmoicm->result() as $itemtinmoicm)
                    {
                    ?>
                    <li><a href="<?php echo site_url($itemtinmoicm->alias.'-'.$itemtinmoicm->id.'.html') ?>" title="<?php echo $itemtinmoicm->title; ?>"><?php echo $itemtinmoicm->title; ?></a></li>
                    <?php    
                    }
                    $sqltinmoicm->free_result();
                ?>                                        
            </ul>
            <?php 
            }
            ?>
    </div>
</div>
<div class="box_trai">
    <div class="box_trai_top">
        <p><?php echo $this->lang->line('online_support'); ?></p>
    </div>
    <div class="box_trai_main">
        <div id="support">
            <?php 
                $this->db->where('status',1);
                $this->db->order_by('thutu','desc');
                $this->db->order_by('id','desc');
                $this->db->select('id,title,yahoo,dienthoai');
                $sqlsupportfet=$this->db->get('tblsupport');
                if($sqlsupportfet->num_rows()>0)
                {
                    foreach($sqlsupportfet->result() as $itemsupportfet)
                    {
                    ?>
                    <div class="support_item">
                        <a href="ymsgr:sendim?<?php echo $itemsupportfet->yahoo; ?>" title="<?php echo $itemsupportfet->title; ?>"><img src="images/phone.png" title="<?php echo $itemsupportfet->title; ?>" alt="<?php echo $itemsupportfet->title; ?>" /></a>
                        <p><?php echo $itemsupportfet->title; ?>:&nbsp;<?php echo $itemsupportfet->dienthoai; ?></p>
                        <div class="clear"></div>
                    </div>
                    <?php
                    }
                    $sqlsupportfet->free_result();
                }
            ?>            
            <div class="clear"></div>
        </div>    
    </div>
</div>
<div id="quangcao_left">
    <?php 
        $this->db->where('status',1);
        $this->db->order_by('thutu','desc');
        $this->db->order_by('id','desc');
        $this->db->select('id,title,anh,link');
        $this->db->limit(5);
        $sqlquangcaoleft=$this->db->get('tblquangcao');
        if($sqlquangcaoleft->num_rows()>0)
        {
            foreach($sqlquangcaoleft->result() as $itemquangcaoleft)
            {
            ?>
            <a href="<?php echo $itemquangcaoleft->link; ?>" target="_blank" title="<?php echo $itemquangcaoleft->title; ?>"><img src="<?php echo $itemquangcaoleft->anh; ?>" title="<?php echo $itemquangcaoleft->title; ?>" alt="<?php echo $itemquangcaoleft->title; ?>" /></a>
            <?php 
            }
        }
        $sqlquangcaoleft->free_result();
    ?>
    <div class="clear"></div>
</div>