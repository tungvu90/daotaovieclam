<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
$doitac=$CI->site_model->gettablename_all('tbldoitac','title,image,link,thutu,status',6,'','','');
$danhmuckhoahochome=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,alias,home,thutu,status',4,'home',1,'');
$danhmuctintuc=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,alias,tintuc,thutu,status',1,'tintuc',1,'');
$khoahochome=$CI->site_model->gettablename_all('tblbaiviet','id,title,alias,thumb,home,thutu,status',3,'home',1,'');
$albumanh=$CI->site_model->gettablename_all('tblalbum','id,album,thutu,status','','','','');
?>
<script type="text/javascript" language="javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
<!-- fire plugin onDocumentReady -->
		<script type="text/javascript" language="javascript">
			$(function() {			
				//	Responsive layout, resizing the items
				$('#foo1').carouFredSel({
					width: '100%',
					direction:'down',
					scroll : {items: 1, duration: 1000, pauseOnHover: true},
				}, {
					transition: true
				});
				//	Responsive layout, resizing the items
				$('#foo2').carouFredSel({
					width: '100%',
					direction:'up',
					scroll : {items: 1, duration: 1500, pauseOnHover: true}, 
				}, {
					transition: true
				});
				//	Responsive layout, resizing the items
				$('#foo3').carouFredSel({
					width: '100%',
					direction:'down',
					scroll : {items: 1, duration: 2000, pauseOnHover: true}, 
					}, {
					transition: true
				});			
			});
		</script>		
<div id="khoahoa_home">
    <div class="item_col">
        <?php 
            $this->db->where('status',1);
            $this->db->where('vitri1',1);
            $this->db->order_by('thutu','desc');
            $this->db->order_by('id','desc');
            $this->db->select('id,title,image,link');
            $sqltextlink1=$this->db->get('tbllink');
            if($sqltextlink1->num_rows()>0)
            {
            ?>
            <div class="list_carousel">   
                <ul id="foo1">
                    <?php 
                        foreach($sqltextlink1->result() as $itemtextlink1)
                        {
                        ?>
                        <li>
                            <div class="khoahochome_item">
                                <div class="khoahochome_item_img">
                                    <a href="<?php echo $itemtextlink1->link; ?>" title="<?php echo $itemtextlink1->title; ?>"><img src="<?php echo $itemtextlink1->image; ?>" title="<?php echo $itemtextlink1->title; ?>" alt="<?php echo $itemtextlink1->title; ?>" /></a>
                                </div>
                                <a href="<?php echo $itemtextlink1->link; ?>" class="khoahochome_item_name" title="<?php echo $itemtextlink1->title; ?>"><?php echo $itemtextlink1->title; ?></a>
                            </div>
                        </li>
                        <?php 
                        }
                        $sqltextlink1->free_result();
                    ?>                    
                </ul>
            </div>
            <?php 
            }
        ?>
    </div>
    <div class="item_col">
        <?php 
            $this->db->where('status',1);
            $this->db->where('vitri2',1);
            $this->db->order_by('thutu','desc');
            $this->db->order_by('id','desc');
            $this->db->select('id,title,image,link');
            $sqltextlink2=$this->db->get('tbllink');
            if($sqltextlink2->num_rows()>0)
            {
            ?>
            <div class="list_carousel">   
                <ul id="foo2">
                    <?php 
                        foreach($sqltextlink2->result() as $itemtextlink2)
                        {
                        ?>
                        <li>
                            <div class="khoahochome_item">
                                <div class="khoahochome_item_img">
                                    <a href="<?php echo $itemtextlink2->link; ?>" title="<?php echo $itemtextlink2->title; ?>"><img src="<?php echo $itemtextlink2->image; ?>" title="<?php echo $itemtextlink2->title; ?>" alt="<?php echo $itemtextlink2->title; ?>" /></a>
                                </div>
                                <a href="<?php echo $itemtextlink2->link; ?>" class="khoahochome_item_name" title="<?php echo $itemtextlink2->title; ?>"><?php echo $itemtextlink2->title; ?></a>
                            </div>
                        </li>
                        <?php 
                        }
                        $sqltextlink2->free_result();
                    ?>                    
                </ul>
            </div>
            <?php 
            }
        ?>
    </div>
    <div class="item_col" style="margin-right:0;">
        <?php 
            $this->db->where('status',1);
            $this->db->where('vitri3',1);
            $this->db->order_by('thutu','desc');
            $this->db->order_by('id','desc');
            $this->db->select('id,title,image,link');
            $sqltextlink3=$this->db->get('tbllink');
            if($sqltextlink3->num_rows()>0)
            {
            ?>
            <div class="list_carousel">   
                <ul id="foo3">
                    <?php 
                        foreach($sqltextlink3->result() as $itemtextlink3)
                        {
                        ?>
                        <li>
                            <div class="khoahochome_item">
                                <div class="khoahochome_item_img">
                                    <a href="<?php echo $itemtextlink3->link; ?>" title="<?php echo $itemtextlink3->title; ?>"><img src="<?php echo $itemtextlink3->image; ?>" title="<?php echo $itemtextlink3->title; ?>" alt="<?php echo $itemtextlink3->title; ?>" /></a>
                                </div>
                                <a href="<?php echo $itemtextlink3->link; ?>" class="khoahochome_item_name" title="<?php echo $itemtextlink3->title; ?>"><?php echo $itemtextlink3->title; ?></a>
                            </div>
                        </li>
                        <?php 
                        }
                        $sqltextlink3->free_result();
                    ?>                    
                </ul>
            </div>
            <?php 
            }
        ?>
    </div>
    <div class="clear"></div>
</div>
<div id="khoahocnb">
    <?php 
        if($danhmuckhoahochome->num_rows()>0)
        {
            $demkhhome=1;
            foreach($danhmuckhoahochome->result() as $itemdanhmuckhoahochome)
            {
                $categorykhhome='';
                $categorykhhome=$CI->site_model->getcatlink($itemdanhmuckhoahochome->id);
            ?>
            <div class="khoahocnb_item" <?php if($demkhhome%2==0){ ?>style="margin-right:0;"<?php } ?>>
                <div class="khoahocnb_item_top">
                    <h3><a href="<?php echo site_url($categorykhhome.'-c'.$itemdanhmuckhoahochome->id.'.html'); ?>" title="<?php echo $itemdanhmuckhoahochome->name; ?>"><?php echo $itemdanhmuckhoahochome->name; ?></a></h3>
                </div>
                <div class="khoahocnb_item_main">
                    <?php 
                        $khoahochome1=$CI->site_model->gettablename_all('tblbaiviet','id,title,alias,mota,catid,thumb,thutu,status',1,'catid',$itemdanhmuckhoahochome->id,'');
                        if($khoahochome1->num_rows()>0)
                        {
                            $khoahochome1=$khoahochome1->row();
                        ?>
                        <div class="khoahoc_item_one">
                            <a href="<?php echo site_url($khoahochome1->alias.'-'.$khoahochome1->id.'.html') ?>" class="khoahoc_item_one_img" title="<?php echo $khoahochome1->title; ?>"><img src="<?php echo $khoahochome1->thumb; ?>" title="<?php echo $khoahochome1->title; ?>" alt="<?php echo $khoahochome1->title; ?>" /></a>
                            <a href="<?php echo site_url($khoahochome1->alias.'-'.$khoahochome1->id.'.html') ?>" class="khoahoc_item_one_name" title="<?php echo $khoahochome1->title; ?>"><?php echo $khoahochome1->title; ?></a>
                            <p><?php echo catchuoi($khoahochome1->mota,180); ?></p>
                            <div class="clear"></div>                                
                        </div>
                        <?php 
                        }
                        $khoahochome2=$CI->site_model->gettablename_all('tblbaiviet','id,title,alias,mota,catid,thumb,thutu,status',5,'catid',$itemdanhmuckhoahochome->id,'');
                        if($khoahochome2->num_rows()>0)
                        {
                            $demkhsub=1;
                        ?>
                        <ul class="khoahocnb_sub">
                            <?php 
                                foreach($khoahochome2->result() as $itemkhoahochome2)
                                {
                                    if($demkhsub>1)
                                    {
                                    ?>
                                    <li><a href="<?php echo site_url($itemkhoahochome2->alias.'-'.$itemkhoahochome2->id.'.html') ?>" title="<?php echo $itemkhoahochome2->title; ?>"><?php echo $itemkhoahochome2->title; ?></a></li>
                                    <?php    
                                    }
                                    $demkhsub++;    
                                }
                            ?>                                                        
                        </ul>
                        <?php 
                        }
                    ?>
                    <div class="clear"></div>
                </div>
                <a class="khoahocnb_item_read" href="<?php echo site_url($categorykhhome.'-c'.$itemdanhmuckhoahochome->id.'.html'); ?>" title="<?php echo $itemdanhmuckhoahochome->name; ?>">>>&nbsp;Đọc thêm</a>
                <div class="clear"></div>
            </div>
            <?php
            $demkhhome++;
            }
            $danhmuckhoahochome->free_result();
        }
    ?>      
    <div class="clear"></div>
</div>
<div id="tt_noe">
    <div id="tintuc">
        <?php 
            if($danhmuctintuc->num_rows()>0)
            {
                $danhmuctintuc=$danhmuctintuc->row();
                $categorytintuch='';
                $categorytintuch=$CI->site_model->getcatlink($danhmuctintuc->id);
            ?>
            <div id="tintuc_top">
                <a href="<?php echo site_url('tin-tuc-c93.html'); ?>">Tin tức mới nhất</a>
            </div>
            <div id="tintuc_main">
                <?php 
                    $this->db->where('status',1);            
                    //$this->db->where('catid',$danhmuctintuc->id);
                    $this->db->order_by('sxtt','desc');
                    $this->db->order_by('id','desc');
                    $this->db->select('id,title,alias,mota,thumb');
                    $this->db->limit(1);
                    $tintuchomeh1=$this->db->get('tblbaiviet');
                    if($tintuchomeh1->num_rows()>0)
                    {
                        $tintuchomeh1=$tintuchomeh1->row();
                        ?>
                        <a href="<?php echo site_url($tintuchomeh1->alias.'-'.$tintuchomeh1->id.'.html') ?>" class="tintuc_name" title="<?php echo $tintuchomeh1->title; ?>"><?php echo $tintuchomeh1->title; ?></a>
                        <a href="<?php echo site_url($tintuchomeh1->alias.'-'.$tintuchomeh1->id.'.html') ?>" class="tintuc_img" title="<?php echo $tintuchomeh1->title; ?>"><img src="<?php echo $tintuchomeh1->thumb; ?>" title="<?php echo $tintuchomeh1->title; ?>" alt="<?php echo $tintuchomeh1->title; ?>" /></a>
                        <p><?php echo catchuoi($tintuchomeh1->mota,300); ?></p>
                    <?php 
                    }
                ?>
                <div class="clear"></div>
                <?php 
                    $this->db->where('status',1);
                    //$this->db->where('catid',$danhmuctintuc->id);
                    $this->db->order_by('sxtt','desc');
                    $this->db->order_by('id','desc');
                    $this->db->select('id,title,alias');
                    $this->db->limit(5);
                    $tintuchomeh2=$this->db->get('tblbaiviet');
                    if($tintuchomeh2->num_rows()>0)
                    {
                        $demtinttuch2=1;
                    ?>
                    <ul id="tintuc_sub">
                        <?php 
                            foreach($tintuchomeh2->result() as $itemtintuchomeh2)
                            {
                                if($demtinttuch2>1)
                                {
                                ?>
                                <li><a href="<?php echo site_url($itemtintuchomeh2->alias.'-'.$itemtintuchomeh2->id.'.html') ?>" title="<?php echo $itemtintuchomeh2->title; ?>"><?php echo $itemtintuchomeh2->title; ?></a></li>
                                <?php
                                }
                                $demtinttuch2++;    
                            }
                            $tintuchomeh2->free_result();
                        ?>                                               
                    </ul>
                    <?php 
                    }
                ?>
            </div>
            <?php 
            }
        ?>
    </div>
    <div id="doitac">
        <div id="doitac_top">
            <p>Đối tác</p>
        </div>
        <div id="doitac_main" style="padding-bottom:7px;">
            <?php 
                if($doitac->num_rows()>0)
                {
                    foreach($doitac->result() as $itemdoitac)    
                    {
                    ?>
                    <div class="doitac_item">
                        <a href="<?php echo $itemdoitac->link; ?>" target="_blank" title="<?php echo $itemdoitac->title; ?>"><img src="<?php echo $itemdoitac->image; ?>" title="<?php echo $itemdoitac->title; ?>" alt="<?php echo $itemdoitac->title; ?>" /></a>
                    </div>
                    <?php    
                    }
                    $doitac->free_result();
                }
            ?>            
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>
<div id="albumanh">
    <div id="albumanh_top">
        <p>Album Ảnh</p>
    </div>
    <div id="albumanh_main">
        <ul id="first-carousel" class="first-and-second-carousel jcarousel-skin-tango">
       <?php 
        if($albumanh->num_rows()>0)
        {
            foreach($albumanh->result() as $itemalbumanh)
            {
                $this->db->where('album_id',$itemalbumanh->id);
                $this->db->select('images');
                $this->db->limit(1);
                $sql_album_img = $this->db->get('tblalbum_images');
                $image = '';
                if($sql_album_img->num_rows()>0){
                    $image = $sql_album_img->row()->images;
                }
            ?>
            <li>
            <div class="album_item">
                <a href="<?php echo site_url(LocDau($itemalbumanh->album).'-tv'.$itemalbumanh->id.'.html'); ?>" title="<?php echo $itemalbumanh->album; ?>"><img src="<?php echo $image; ?>" title="<?php echo $itemalbumanh->album; ?>" alt="<?php echo $itemalbumanh->album; ?>" /></a>
                <a href="<?php echo site_url(LocDau($itemalbumanh->album).'-tv'.$itemalbumanh->id.'.html'); ?>" title="<?php echo $itemalbumanh->album; ?>"><?php echo $itemalbumanh->album; ?></a>
            </div>
            </li>
            <?php 
            }
            $albumanh->free_result();
        }
        ?>        
        </ul>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>