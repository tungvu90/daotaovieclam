<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
$footer=$CI->site_model->gettbl('tblfooter')->row();
$danhmucbv=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,alias,uid,thutu,status',2,'','',0,'');
if($danhmucbv->num_rows()>0)
{
    foreach($danhmucbv->result() as $itemdanhmucbv)
    {
        $categorybv='';
        $categorybv=$CI->site_model->getcatlinktt($itemdanhmucbv->id);
    ?>
    <div class="footer_item">
        <div class="footer_item_top">
            <p><?php echo $itemdanhmucbv->name; ?></p>
        </div>
        <div class="footer_item_main">
            <?php 
                $this->db->where('status',1);
                $this->db->where('catid',$itemdanhmucbv->id);
                $this->db->order_by('id','desc');
                $baiviettin=$this->db->get('tblbaiviet');
                if($baiviettin->num_rows()>0)
                {
                ?>
                <ul>
                    <?php 
                        foreach($baiviettin->result() as $itembaiviettin)
                        {
                        ?>
                        <li><a href="<?php echo site_url($itembaiviettin->alias.'-bv'.$itembaiviettin->id).'.html';?>" title="<?php echo $itembaiviettin->title; ?>"><?php echo $itembaiviettin->title; ?></a></li>  
                        <?php    
                        }
                    ?>                                      
                </ul>
                <?php 
                }
            ?>
        </div>
    </div>
    <?php 
    }
    $danhmucbv->free_result();
}
?>
<div class="footer_item" style="width:160px !important;">
    <div class="footer_item_top">
        <p>Liên kết</p>
    </div>
    <div class="footer_item_main">
        <ul>
            <li><a href="http://nhatdatcity.net" target="_blank" rel="nofollow">Nhà đất city</a></li>                
        </ul>
    </div>
</div>
<div class="footer_item" style="width:290px !important;">
    <div class="footer_item_top">
        <p>Mạng xã hội</p>
    </div>
    <div class="footer_item_main">
        <div id="fanpage">
        <div class="fb-like-box" data-href="<?php echo $footer->fanpage; ?>" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
        </div>
    </div>
</div>
<div class="footer_item" style="margin-right: 0;">
    <div class="footer_item_top">
        <p>Bản đồ</p>
    </div>
    <div class="footer_item_main">
        <div id="map">
        <img src="images/map.jpg" width="248px" height="177px" />
        </div>
    </div>
</div>
<div class="clear"></div>
<div id="copy">
    <?php echo $footer->content; ?>
</div>
<div id="top">                    
</div>             
<script type="text/javascript">
  $(document).ready(function(){
    // hide #top first
    $("#top").hide();
    // fade in #back-top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#top').fadeIn();
        } else {
            $('#top').fadeOut();
        } 
    });        
    // scroll body to 0px on click
    $('#top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
});
</script>