<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
$slider=$CI->site_model->gettablename_all('tblslider','title,image,link,thutu,status','','','','');
$conent_t=$CI->site_model->gettbl('tblfooter','tencongty,status')->row();
$danhmucleft=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,alias,uid,menu,status','','menu',1,0);
$danhmuctrai=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,thumb,alias,uid,trai,status','','trai',1,0);
?>
<div id="content_left">
    <div class="box_left">
        <div class="box_left_top">
            <p><?php echo $this->lang->line('training_courses'); ?></p>
        </div>
        <div class="box_left_main" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
            <?php 
                if($danhmuctrai->num_rows()>0)
                {
                ?>
                <ul id="menu_left">
                    <?php 
                        foreach($danhmuctrai->result() as $itemdanhmuctrai)
                        {
                        ?>
                        <li>
                            <?php 
                                if($itemdanhmuctrai->thumb!='')
                                {
                                ?>
                                <a href="<?php echo site_url($itemdanhmuctrai->alias.'-c'.$itemdanhmuctrai->id.'.html'); ?>" class="menu_left_img" title="<?php echo $itemdanhmuctrai->name; ?>"><img src="<?php echo $itemdanhmuctrai->thumb; ?>" title="<?php echo $itemdanhmuctrai->name; ?>" alt="<?php echo $itemdanhmuctrai->name; ?>" /></a>
                                <?php 
                                }
                            ?>
                             <a href="<?php echo site_url($itemdanhmuctrai->alias.'-c'.$itemdanhmuctrai->id.'.html'); ?>" class="menu_left_name" title="<?php echo $itemdanhmuctrai->name; ?>"><?php echo $itemdanhmuctrai->name; ?></a>
                             <div class="clear"></div>
                             <?php 
                                $this->db->where('status',1);
                                $danhmuctraisub=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,thumb,alias,uid,status','','','',$itemdanhmuctrai->id);
                                if($danhmuctraisub->num_rows()>0)
                                {
                                ?>
                                 <ul class="menu_left_sub">
                                    <?php 
                                        foreach($danhmuctraisub->result() as $itemdanhmuctraisub)
                                        {
                                        ?>
                                        <li>
                                            <?php 
                                                if($itemdanhmuctraisub->thumb!='')
                                                {
                                                ?>
                                                <a class="menu_left_img_sb" title="<?php echo $itemdanhmuctraisub->name; ?>"><img src="<?php echo $itemdanhmuctraisub->thumb; ?>" title="<?php echo $itemdanhmuctraisub->name; ?>" alt="<?php echo $itemdanhmuctraisub->name; ?>" /></a>
                                                <?php 
                                                }
                                            ?>
                                            <a class="menu_left_name_sb" href="<?php echo site_url($itemdanhmuctraisub->alias.'-c'.$itemdanhmuctraisub->id.'.html'); ?>" title="<?php echo $itemdanhmuctraisub->name; ?>"><?php echo $itemdanhmuctraisub->name; ?></a></li>    
                                        <?php    
                                        }
                                        $danhmuctraisub->free_result();
                                    ?>                                                                    
                                 </ul>
                                <?php 
                             }
                             ?>
                        </li>
                        <?php 
                        }
                        $danhmuctrai->free_result();
                    ?>                                   
                </ul>
                <?php 
                }
            ?>
            <script type="text/javascript">
                $(document).ready(function(){                            
                    $('#menu_left > li').hover(function(){
                        $(this).children('.menu_left_sub').css('display','block');                       
                        },function(){
                            $(this).children('.menu_left_sub').css('display','none');    
                        });      
                    });
            </script>
        </div>
    </div>
</div>
<div id="content_right">
    <div id="menu">
        <ul id="menu_top">
            <li><a href="<?php echo base_url(); ?>" title="<?php echo $conent_t->tencongty; ?>"><?php echo $this->lang->line('home'); ?></a></li>
            <?php 
                if($danhmucleft->num_rows()>0)
                {
                    foreach($danhmucleft->result() as $itemdanhmucleft)
                    {                        
                    ?>
                    <li><a href="<?php echo site_url($itemdanhmucleft->alias.'-c'.$itemdanhmucleft->id.'.html'); ?>" title="<?php echo $itemdanhmucleft->name; ?>"><?php echo $itemdanhmucleft->name; ?></a>
                    <?php 
                        $danhmucmenusub=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,thumb,alias,uid,status','','','',$itemdanhmucleft->id);
                        if($danhmucmenusub->num_rows()>0)
                        {
                        ?>
                        <ul class="menu_sub">
                            <?php 
                                foreach($danhmucmenusub->result() as $itemdanhmucmenusub)
                                {
                                ?>
                                <li><a href="<?php echo site_url($itemdanhmucmenusub->alias.'-c'.$itemdanhmucmenusub->id.'.html'); ?>" title="<?php echo $itemdanhmucmenusub->name; ?>"><?php echo $itemdanhmucmenusub->name; ?></a></li>  
                                <?php    
                                }
                                $danhmucmenusub->free_result();
                            ?>                                              
                        </ul>
                        <?php 
                        }
                    ?>
                    </li>
                    <?php    
                    }
                    $danhmucleft->free_result();
                }
            ?>                        
            <li><a rel="nofollow" href="<?php echo site_url('lien-he.html'); ?>" style="border-right:none;" title="<?php echo $this->lang->line('contact'); ?>"><?php echo $this->lang->line('contact'); ?></a></li>
        </ul>
        <script type="text/javascript">
            $(document).ready(function(){                            
                $('#menu_top > li').hover(function(){
                    $(this).children('.menu_sub').css('display','block');                       
                    },function(){
                        $(this).children('.menu_sub').css('display','none');    
                    });      
                });
        </script>
    </div>
    <link rel="stylesheet" href="css/default.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
    <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
    <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider({
            effect: 'fade',
            hover:true
        });
    });
    </script>
    <div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">        
        <?php 
            if($slider->num_rows()>0)
            {
                foreach($slider->result() as $itemslider)
                {
                ?>            
                <a href="<?php echo $itemslider->link; ?>" target="_blank" title="<?php echo $itemslider->title; ?>"><img src="<?php echo $itemslider->image; ?>" title="<?php echo $itemslider->title; ?>" alt="<?php echo $itemslider->title; ?>" /></a>
                <?php 
                }
                $slider->free_result();
            }
        ?>                       
    </div>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>