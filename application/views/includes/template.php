<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml" lang="vi" xml:lang="vi">
<head>
<?php header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', time() + 3600)); ?>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="<?php echo base_url(); ?>">
<title>
<?php
        $CI = &get_instance();
        $CI->load->model('site/site_model');
        $meta = $CI->site_model->gettbl('tblmeta','title,description,keywords,status'); 
        $select_meta=$meta->row();              
        if(isset($header_title))
        {
            echo $header_title;
        }
        else
        {
	       echo $select_meta->title;
        }
    ?>
</title>
<?php 
if(isset($home))
{
?>
<meta content="<?php echo base_url(); ?>" property="og:url">
<meta content="<?php echo $select_meta->title; ?>" property="og:title">
<meta content="<?php echo $select_meta->description; ?>" property="og:description">	
<meta content="https://daotaovieclam.edu.vn/upload/dao-tao-day-nghe-hai-phong.jpg" property="og:image">
<?php 
}
if(isset($ctfb))
{
    $this->db->where('id',$ctfb);
    $sqlsanphamfb=$this->db->get('tblsanpham')->row();    
    ?>
    <meta content="<?php echo site_url($sqlsanphamfb->alias.'-'.$sqlsanphamfb->id.'.html');?>" property="og:url">
    <meta content="<?php echo $sqlsanphamfb->title; ?>" property="og:title">
    <meta content="<?php echo strip_tags($sqlsanphamfb->noidung); ?>" property="og:description">	
    <meta content="<?php echo $sqlsanphamfb->thumb; ?>" property="og:image">
    <?php
}
?>

<meta name="description" content="<?php
    if(isset($description))
    {
        echo $description;
    }
    else
    {
        echo $select_meta->description;
    }?>">
<meta name="keywords" content="<?php 
    if(isset($keyword))
    {
        echo $keyword;    
    }
    else
    {
        echo $select_meta->keywords;    
    }     
    ?>">

<?php 
if(isset($home))
{
?>
<meta content="<?php echo base_url(); ?>" name="twitter:url">
<meta content="<?php echo $select_meta->title; ?>" name="twitter:title">
<meta content="<?php echo $select_meta->description; ?>" name="twitter:description">	
<meta content="http://daotaovieclam.edu.vn/upload/dao-tao-day-nghe-hai-phong.jpg" name="twitter:image">
<?php 
}
if(isset($ctfb))
{
    $this->db->where('id',$ctfb);
    $sqlsanphamfb=$this->db->get('tblsanpham')->row();    
    ?>
    <meta content="<?php echo site_url($sqlsanphamfb->alias.'-'.$sqlsanphamfb->id.'.html');?>" name="twitter:url">
    <meta content="<?php echo $sqlsanphamfb->title; ?>" name="twitter:title">
    <meta content="<?php echo strip_tags($sqlsanphamfb->noidung); ?>" name="twitter:description">	
    <meta content="<?php echo $sqlsanphamfb->thumb; ?>" name="twitter:image">
    <?php
}
?>	

<meta name="twitter:description" content="<?php
    if(isset($description))
    {
        echo $description;
    }
    else
    {
        echo $select_meta->description;
    }?>">


    <?php
        function catchuoi($chuoi,$gioihan){
        if(strlen($chuoi)<=$gioihan)
        {
            return $chuoi;
        }
        else{
        if(strpos($chuoi," ",$gioihan) > $gioihan){
            $new_gioihan=strpos($chuoi," ",$gioihan);
            $new_chuoi = substr($chuoi,0,$new_gioihan)."...";
            return $new_chuoi;
        }
        $new_chuoi = substr($chuoi,0,$gioihan)."...";
            return $new_chuoi;
        }
    }
    ?>           
<meta name="author" content="daotaoonline">
<meta name="copyright" content="daotaoonline">
<meta name="robots" content="index,follow">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/owl.carousel.css" rel="stylesheet" type="text/css" />
<link href="daotao.ico" rel="shortcut icon" type="image/x-icon"/>
<!--<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">-->
<?php 
if(isset($home))
{
?>
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.daotaovieclam.edu.vn">
<?php    
}
if(isset($contact))
{
?>
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.daotaovieclam.edu.vn/lien-he.html">
<?php    
}
if(isset($chuyenmuc))
{
    $this->db->where('id',$chuyenmuc);
    $this->db->select('id,alias');
    $sqlchuyenmucalter=$this->db->get('tblchuyenmuc')->row();
    $linkalter=site_url($sqlchuyenmucalter->alias.'-c'.$sqlchuyenmucalter->id.'.html');
    $linkalter1=str_replace('daotaovieclam.edu.vn','m.daotaovieclam.edu.vn',$linkalter);
?>
<link rel="alternate" media="only screen and (max-width: 640px)" href="<?php echo $linkalter1; ?>">
<?php
}
if(isset($chitiettu))
{
    $this->db->where('id',$chitiettu);
    $this->db->select('id,alias');
    $sqlchitietbvalter=$this->db->get('tblbaiviet')->row();
    $linkbvalter=site_url($sqlchitietbvalter->alias.'-'.$sqlchitietbvalter->id.'.html');
    $linkbvalter1=str_replace('daotaovieclam.edu.vn','m.daotaovieclam.edu.vn',$linkbvalter);
?>
<link rel="alternate" media="only screen and (max-width: 640px)" href="<?php echo $linkbvalter1; ?>">
<?php       
}
if(isset($dangky))
{
?>
<link rel="alternate" media="only screen and (max-width: 640px)" href="https://m.daotaovieclam.edu.vn/dang-ky-ngay.html">
<?php    
}
?>
<link href="css/skin.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script>
    jQuery(document).ready(function($) {
    	var owl = jQuery('.owl-carousel'); // save reference to variable
    	owl.owlCarousel({
    	  items:1,
    	  nav:true,
    	  loop:true,
    	  autoplay:true,
    	  autoplayTimeout:3000,
    	  autoplayHoverPause:true
    	});
    });
</script>
<script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {            
        	jQuery('.first-and-second-carousel').jcarousel({
        	   auto:3,
               scroll:1,
               animation:'slow',
                wrap: 'circular',
                animation:1000,                 
        	});        	        
        });        
    </script>
<?php 
$this->db->where('status',1);
$this->db->select('googleantic');
$googlett=$this->db->get('tblfooter');
if($googlett->num_rows()>0)
{
    echo $googlett->row()->googleantic;
}
if(isset($chitiet_code_head)){
    $this->db->where('id',$chitiet_code_head);
    $this->db->select('code_head');
    $code_header_artice = $this->db->get('tblbaiviet');
    if($code_header_artice->num_rows()>0){
        echo $code_header_artice->row()->code_head;   
    }
}
?>
<body itemscope="itemscope" itemtype="http://schema.org/WebPage">
    
<?php 
$this->db->where('status',1);
$this->db->where('vitri2',1);
$this->db->select('linkphai,anhphai');
$sqlquangcaoright=$this->db->get('tblfooter');
if($sqlquangcaoright->num_rows()>0)
{
    $quangcaoright=$sqlquangcaoright->row();
?>
<div id="divAdRight" style="display: block; position: fixed; top: 0px;">
    <a href="<?php echo $quangcaoright->linkphai; ?>" title="ảnh phải" rel="nofollow"><img src="<?php echo $quangcaoright->anhphai; ?>" width="120" alt="ảnh phải" /></a>
</div>
<?php 
}
?>
<?php 
$this->db->where('status',1);
$this->db->where('vitri1',1);
$this->db->select('linktrai,anhtrai');
$sqlquangcaoleft=$this->db->get('tblfooter');
if($sqlquangcaoleft->num_rows()>0)
{
    $quangcaoleft=$sqlquangcaoleft->row();
?>    
<div id="divAdLeft" style="display: block; position: fixed; top: 0px;">
    <a href="<?php echo $quangcaoleft->linktrai; ?>" title="ảnh trái" rel="nofollow"><img src="<?php echo $quangcaoleft->anhtrai; ?>" width="120" alt="ảnh trái" /></a>
</div>
<?php 
}
?>
<script>
    function FloatTopDiv()
    {
        startLX = ((document.body.clientWidth -MainContentW)/2)-LeftBannerW-LeftAdjust , startLY = TopAdjust+80;
        startRX = ((document.body.clientWidth -MainContentW)/2)+MainContentW+RightAdjust , startRY = TopAdjust+80;
        var d = document;
        function ml(id)
        {
            var el=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];
            el.sP=function(x,y){this.style.left=x + 'px';this.style.top=y + 'px';};
            el.x = startRX;
            el.y = startRY;
            return el;
        }
        function m2(id)
        {
            var e2=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];
            e2.sP=function(x,y){this.style.left=x + 'px';this.style.top=y + 'px';};
            e2.x = startLX;
            e2.y = startLY;
            return e2;
        }
        window.stayTopLeft=function()
        {
            if (document.documentElement && document.documentElement.scrollTop)
                var pY =  document.documentElement;
            else if (document.body)
                var pY =  document.body;
            if (document.body.scrollTop > 30){startLY = 3;startRY = 3;} else {startLY = TopAdjust;startRY = TopAdjust;};
            ftlObj.y += (pY+startRY-ftlObj.y)/16;
            ftlObj.sP(ftlObj.x, ftlObj.y);
            ftlObj2.y += (pY+startLY-ftlObj2.y)/16;
            ftlObj2.sP(ftlObj2.x, ftlObj2.y);
            setTimeout("stayTopLeft()", 1);
        }
        ftlObj = ml("divAdRight");
        //stayTopLeft();
        ftlObj2 = m2("divAdLeft");
        stayTopLeft();
    }
    function ShowAdDiv()
    {
        var objAdDivRight = document.getElementById("divAdRight");
        var objAdDivLeft = document.getElementById("divAdLeft");
        if (document.body.clientWidth < 1000)
        {
            objAdDivRight.style.display = "none";
            objAdDivLeft.style.display = "none";
        }
        else
        {
            objAdDivRight.style.display = "block";
            objAdDivLeft.style.display = "block";
            FloatTopDiv();
        }
    }
</script>
<script>
document.write("<script type='text/javascript' language='javascript'>MainContentW = 1000;LeftBannerW = 120;RightBannerW = 120;LeftAdjust = 15;RightAdjust = 5;TopAdjust = 10;ShowAdDiv();window.onresize=ShowAdDiv;;<\/script>");
</script>
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
FB.init({
appId : '1628978170653064',
status : true, // check login status
cookie : true, // enable cookies to allow the server to access the session
xfbml : true // parse XFBML
});
};
(function() {
var e = document.createElement('script');
e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
e.async = true;
document.getElementById('fb-root').appendChild(e);
}());
</script>

    <div id="wrapper" class="container">
        <div id="header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
            <?php $this->load->view('includes/header') ?>
        </div><!--End #header-->
        <div id="content">
            <div id="content_top">
                <?php $this->load->view('includes/content_top') ?>
            </div><!--End #content_top-->  
            <div id="bg_search">
                <a rel="nofollow" href="<?php echo site_url('dang-ky-ngay.html'); ?>" id="dangkyhoc" title="Đăng ký ngay">Đăng ký ngay</a>         
                <div id="search">
                    <form name="frmsearch" method="POST" action="<?php echo site_url('ket-qua-tim-kiem.html'); ?>">
                        <input type="text" name="ten" value="" placeholder="Nhập tên khóa học cần tìm" />
                        <input type="submit" name="submit" value="Tìm kiếm" />
                    </form>
                </div> 
                <div class="clear"></div>
            </div>           
            <div id="content_main"<?php if(isset($home)){}else{ ?> style="margin-top:20px;" <?php } ?>>
                <?php $this->load->view($content) ?>
                <div class="clear"></div>
            </div><!--End #content_main-->
            <div class="clear"></div>
        </div>
        <div id="footer" itemscope="itemscope" itemtype="http://schema.org/WPFooter">
            <?php $this->load->view('includes/footer') ?>
        </div><!--End #footer-->
        <div class="clear"></div>
    </div>
    <!-- Messenger Plugin chat Code -->
    <div id="fb-root"></div>

    <!-- Your Plugin chat code -->
    <div id="fb-customer-chat" class="fb-customerchat">
    </div>

    <script>
      var chatbox = document.getElementById('fb-customer-chat');
      chatbox.setAttribute("page_id", "1761738037379858");
      chatbox.setAttribute("attribution", "biz_inbox");
    </script>

    <!-- Your SDK code -->
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v12.0'
        });
      };

      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>
</body>

</html>