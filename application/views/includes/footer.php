<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
$footer=$CI->site_model->gettbl('tblfooter','giaychungnhan,tencongty,diachi,dienthoai,hotline,email,theh,fanpage,status')->row();
$danhmucfooter=$CI->site_model->gettablename_all('tblchuyenmuc','id,name,alias,uid,footer,thutu,status',3,'footer',1,0);
?>
<div id="footer_top">
    <?php 
        if($danhmucfooter->num_rows()>0)
        {
            foreach($danhmucfooter->result() as $itemdanhmucfooter)
            {                
            ?>
            <div class="footer_item">
                <p><a href="<?php echo site_url($itemdanhmucfooter->alias.'-c'.$itemdanhmucfooter->id.'.html'); ?>" class="footer_title" title="<?php echo $itemdanhmucfooter->name; ?>"><?php echo $itemdanhmucfooter->name; ?></a></p>
                <?php 
                    $tindanhmucfooter=$CI->site_model->gettablename_all('tblbaiviet','id,title,alias,catid,thutu,status',5,'catid',$itemdanhmucfooter->id,'');
                    if($tindanhmucfooter->num_rows()>0)
                    {
                    ?>
                    <ul>
                        <?php 
                            foreach($tindanhmucfooter->result() as $itemtindanhmucfooter)
                            {
                            ?>
                            <li><a href="<?php echo site_url($itemtindanhmucfooter->alias.'-'.$itemtindanhmucfooter->id).'.html';?>" title="<?php echo $itemtindanhmucfooter->title; ?>">-&nbsp;<?php echo $itemtindanhmucfooter->title; ?></a></li>
                            <?php    
                            }
                        ?>                                            
                    </ul>
                    <?php 
                    }
                ?>
            </div>
            <?php 
            }
        }
    ?>
    <div class="footer_item" style="margin-right:0;background:none;;">
        <?php echo ThongTin($footer->diachi,$footer->dienthoai,$footer->hotline,$footer->email) ?>
    <br>
    <p><a href="" class="footer_title">Chính sách công ty </a></p>
    <p style="font-size:12px; display:inline;"><a style="display:inline;color:#555;" rel="nofollow" href="http://daotaovieclam.edu.vn/bao-mat-thong-tin-203.html">Bảo mật thông tin</a> | <a style="display:inline;color:#555;" rel="nofollow" href="http://daotaovieclam.edu.vn/quy-che-hoat-dong-206.html">Quy chế hoạt động</a> | <a style="display:inline;color:#555;" rel="nofollow" href="http://daotaovieclam.edu.vn/huong-dang-dang-ky-hoc-207.html">Hướng dẫn đăng ký học</a> | <a style="display:inline;color:#555;" rel="nofollow" href="http://daotaovieclam.edu.vn/chinh-sach-hoan-tien-hoc-phi-205.html">Hoàn Tiền</a></p>
    </div>
    
    <div class="clear"></div>
</div>
<div id="footer_main">
    <div id="footer_left">
        <p><strong><?php echo $footer->giaychungnhan; ?></strong></p>
        <p><strong><?php echo $footer->tencongty; ?></strong></p>
        <p><strong>Địa chỉ</strong>:&nbsp;<?php echo $footer->diachi; ?></p>
        <p><strong>Điện thoại</strong>:&nbsp;<?php echo $footer->dienthoai; ?></p>
        <p style="display:inline"><strong>Email</strong>:&nbsp;<?php echo $footer->email; ?></p>

    </div>
    <div id="fanpage">
       <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=1628978170653064";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
        <div class="fb-like-box" data-href="<?php echo $footer->fanpage; ?>" data-width="300" data-height="170" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
    </div>
    <?php 
        if(isset($home))
        {
        ?>
        <div id="tukhoa">
            <p>Từ khóa</p>
            <?php 
                echo $footer->theh;    
            ?>
        </div>
        <?php 
        }
    ?>
    <div class="clear"></div>
</div>
<div class="clear"></div>