<?php 
class Site_model extends CI_Model
{	
	public function __construct()
    {
        parent::__construct();
    }
	public function gettbl($tbl,$select)
    {
        $this->db->where('status',1);
        $this->db->order_by("id","desc");
        $this->db->select($select);
		$query = $this->db->get($tbl);
		return $query;     
    }
    public function gettablename_all($table_name,$dataall_selct,$limit,$data_key,$data_value,$uid)
    {
        $this->db->where('status',1);        
        if($data_key!='' && $data_value!='')
        {
            $this->db->where($data_key,$data_value);
        }          
        $this->db->order_by('thutu','desc');
        $this->db->order_by('id','desc');
        if($dataall_selct!='')
        {
            $this->db->select($dataall_selct);
        }
        if($uid!='')
        {
            $this->db->where('uid',$uid);
        }
        if($limit!='')
        {
            $this->db->limit($limit);
        }
        $sqlall=$this->db->get($table_name);
        return $sqlall;
    } 
    public function getdanhmuchome()
    {
        $results = $this->memcached_library->get('select_dm');	
        if (!$results) 
        {		
            $sqltt="SELECT id,name,alias,home,uid,thutu,status FROM tblchuyenmucsp WHERE home=1 AND status=1";
            $select_dm=$this->db->query($sqltt)->result();	             				
            $this->memcached_library->add('select_dm', $select_dm,36000);	           		
        }
        else 
        {				
            $select_dm=$results;		
        }
        var_dump($select_dm);
    }
    public function num_view($id)
    {
        $this->db->where('id',$id);
        $new = $this->db->get('tblbaiviet');
        $new = $new->row();
        $view = $new->hits + 1;
        $data = array(
            'hits'  =>$view
        );
        $this->db->where('status',1);
        $this->db->where('id',$id);
        $this->db->update('tblbaiviet',$data);
    }  
	public function gettbl1($tbl)
	{		
		$this->db->where('status',1);
        if(isset($_SESSION['username']))
        {
            $this->db->where('name',$_SESSION['username']);
            $admin=$this->db->get('tbladmin')->row();
            $this->db->where('name',$admin->id);
        }
        if(isset($_COOKIE['user']))
        {
            $this->db->where('name',$_COOKIE['user']);
            $admin=$this->db->get('tbladmin')->row();
            $this->db->where('name',$admin->id);
        }
		$this->db->order_by("id","desc");
		$query = $this->db->get($tbl);
		return $query;
	}
    public function gettbl_limited1($tbl,$limit,$start_row)
	{
	    $this->db->where('status',1);
        if(isset($_SESSION['username']))
        {
            $this->db->where('name',$_SESSION['username']);
            $admin=$this->db->get('tbladmin')->row();
            $this->db->where('name',$admin->id);
        }
        if(isset($_COOKIE['user']))
        {
            $this->db->where('name',$_COOKIE['user']);
            $admin=$this->db->get('tbladmin')->row();
            $this->db->where('name',$admin->id);
        }
		$this->db->order_by("id","desc");
        $this->db->limit($limit,$start_row);
		$query = $this->db->get($tbl);
		return $query;
	}	
    public function gettable($table_name)
    {
        $ngaydang=getdate();
        $ngay=$ngaydang['year'].'-'.$ngaydang['mon'].'-'.$ngaydang['mday'];
        $this->db->where('status',1);
        $this->db->where('ngaydang',$ngay);
        if(isset($_COOKIE['city']))
        {
            $this->db->where('tinh',$_COOKIE['city']);    
        }
        $this->db->order_by("thutu","desc");
        $this->db->order_by("id","desc");
		$query = $this->db->get($table_name);
		return $query;    
    }
    public function gettable_limited($table_name,$limit,$start_row)
    {
        $ngaydang=getdate();
        $ngay=$ngaydang['year'].'-'.$ngaydang['mon'].'-'.$ngaydang['mday'];
        $this->db->where('status',1);
        $this->db->where('ngaydang',$ngay);
        if(isset($_COOKIE['city']))
        {
            $this->db->where('tinh',$_COOKIE['city']);    
        }
        $this->db->order_by("thutu","desc");
        $this->db->order_by("id","desc");
        $this->db->limit($limit,$start_row);
		$query = $this->db->get($table_name);
		return $query;        
    }
    public function checklogin($u,$p)
     {
    
    	 $this->db->where('status',1);
    
    	 $this->db->where('name',$u);
    
    	 $this->db->where('pass',$p);
    
    	 $check=$this->db->get('tbladmin');
    
    	 return $check;
    
     }
	public function add_tbl($tbl,$data,$id)
    {		
        if($id=='')
        {
            $this->db->insert($tbl,$data);
        }
        else
        {
            $this->db->where('id',$id);
            $this->db->update($tbl,$data);    
        }
    }  
	public function gettbl_comment($itemid,$parent,$limit)
	{		
		$this->db->where('status',1);
		if($itemid!=''){
		  $this->db->where('itemid',$itemid);
		}		
		if($parent>=0){			
		  $this->db->where('parent',$parent);
		}
		if($limit!=''){
		  $this->db->limit($limit,0);
		}
		$this->db->order_by("id","desc");
		$query = $this->db->get('tblcomment');
		return $query;
	}
	public function gettbl_comment1($itemid,$parent,$limit)
    {
        $this->db->where('status',1);
        $this->db->where('itemid',$itemid);
        $this->db->where('parent',$parent);
		$this->db->limit($limit,0);
        $this->db->order_by('id','desc');
        $sql=$this->db->get('tblcomment');
        foreach($sql->result() as $listcm)
        {		
			$base=explode(' ',$listcm->created_day);
			$baseday=explode('-',$base[0]);
			$basetime=substr($base[1],0,-3);
			$daytime=$baseday[2].'/'.$baseday[1].'/'.$baseday[0].' '.$basetime;
			echo'			
			<div style="margin-left:10px;" class="itemcm cm'.$listcm->id.' child">
			<img class="ico-top-cm" src="images/ico-top-cm.png">
				<div class="top">'.
					'<span class="name">'.$listcm->name.'</span><span class="day">('.$daytime.')</span>
					<span class="mail"> - email: '.$listcm->mail.'</span>					
				</div>
				<div class="bottom">
					<p class="cm_intro">'.$listcm->comment.'</p>
				</div>
				<input type="button" class="buttonq" name="submit" onclick="load_ajax_form('.$listcm->id.')" value="Trả lời"/>
			</div>';
            $this->gettbl_comment1($itemid,$listcm->id,$limit);    
        }
        return true;
    }
	public function gettbl_sukien($tbl,$checked,$start_row,$limit)
	{		
		$this->db->where('status',1);
		if($checked!=''){
			$this->db->where('checked',$checked);
		}
		$this->db->limit($limit,0);
		$this->db->order_by("id","desc");
		$query = $this->db->get($tbl);
		return $query;
	}
	public function getitem_hits($tbl,$limit,$check)
	{	
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$day = date('Y-m-d H:i:s');	      
		//$timezone = +6;
		//$day = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
		$sql  ="SELECT id,alias,catid,title,sapo,hits,created_day,thumb FROM $tbl WHERE status = 1";
		if($check==1){
			$sql .=" AND DATEDIFF('$day',created_day)<=1";
		}
		else if($check==2){
			$sql .=" AND DATEDIFF('$day',created_day)<=7";
		}
		else{
			$sql .=" AND DATEDIFF('$day',created_day)<=30";
		}
		$sql .=" order by hits desc limit 0,$limit";
		$query=$this->db->query($sql);
		return $query;
	}
	public function getitem_hits_cat($tbl,$id,$limit)
	{	
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$day = date('Y-m-d H:i:s');	      
		//$timezone = +6;
		//$day = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
		$sql = "SELECT id,alias,catid,title,sapo,hits,created_day,thumb FROM $tbl WHERE status = 1 AND (catid = $id OR catid IN(select id from tblchuyenmuc where uid = $id)) AND DATEDIFF('$day',created_day)<=1 order by hits desc limit 0,$limit";
		$query=$this->db->query($sql);
		return $query;
	}
	public function getitem_hits_cat_welk($tbl,$id,$limit)
	{	
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$day = date('Y-m-d H:i:s');	      		
		$sql = "SELECT id,alias,catid,title,sapo,hits,created_day,thumb FROM $tbl WHERE status = 1 AND (catid = $id OR catid IN(select id from tblchuyenmuc where uid = $id)) AND DATEDIFF('$day',created_day)<=7 order by hits desc limit 0,$limit";
		$query=$this->db->query($sql);
		return $query;
	}
	public function getsukien($tbl,$limit,$show)
	{		
		if($show!=''){
			$checked = $this->db->query("select id,checked,status from tblsukien where checked=1 and status=1 and showed=1 order by id desc limit 0,1")->row();		
		}
		else{ $checked = $this->db->query("select id,checked,status from tblsukien where checked=1 and status=1 order by id desc limit 0,1")->row();}
		//$this->db->join('tblsukien', 'tblsukien.id = tblbaiviet.sukien');
		if($checked!=''){
			$this->db->where("sukien",$checked->id);
		}			
		if($limit!=''){
			$this->db->limit($limit,0);
		}
		$this->db->order_by("tblbaiviet.id","desc");				
		$query = $this->db->get($tbl);
		return $query;
	}
	public function getitem($tbl,$id)
	{		
		$this->db->where("id",$id);
		$this->db->where("status",1);
		$query = $this->db->get($tbl);
		return $query;
	}		
	public function getsubcat($tbl,$uid)
	{
		if($uid!=''){
			$this->db->where('uid',$uid);
		}
		$this->db->where('status',1);
		$query = $this->db->get($tbl);
		return $query;
	}

    public function getlist($id)
    {					
		$sql="select id,alias,title,thumb,catid,status from tblbaiviet where catid = $id OR ncatid='$id' AND status = 1 order by id desc";
		$query=$this->db->query($sql);
		return $query;	
    }
	public function gettags($tbl,$tags)
	{		
		$tags=str_replace('-',' ',$tags);		
		$this->db->where('status',1);
		$this->db->like("tags",$tags);
		$query = $this->db->get($tbl);
		return $query;
	}	
	public function getlistsukien($tbl,$sukien)
	{				
		$this->db->select('id,title,alias,catid,thumb,sapo');
		$this->db->where('status',1);
		$this->db->where("sukien",$sukien);
		$query = $this->db->get($tbl);
		return $query;
	}	
	public function gettbl_limit_vip($tbl,$id,$vip,$limit)
	{
		if($id!=''){
			$this->db->where('id',$id);
		}		
		if($vip!=''){
			$this->db->where('vip',$vip);
		}
		if($limit!=''){
			$this->db->limit($limit,0);
		}
		$this->db->where('status',1);
		$this->db->order_by("created_day,id","desc");
		$query = $this->db->get($tbl);
		return $query;
	}	
	public function hits($tbl,$id){		
		$sql="UPDATE $tbl SET hits=hits+1 WHERE id='$id'";
		$this->db->query($sql);
	}
	public function gettbl_limited($tbl,$id,$start_row,$limit)
	{						
		$sql="SELECT * FROM $tbl WHERE status=1 AND catid = '$id' OR ncatid = '$id' order by thutu desc,id desc limit $start_row,$limit";	
		$query=$this->db->query($sql);
		return $query;
	}
    public function gettbl_limited2($tbl,$id,$start_row,$limit)
	{						
		$sql="SELECT id,alias,catid,title,image,thumb,status FROM $tbl WHERE catid = $id AND status = 1 limit $start_row,$limit";	
		$query=$this->db->query($sql);
		return $query;
	}	
	public function gettbl_limited_cat($tbl,$start_row,$limit)
	{		
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$day = date('Y-m-d H:i:s');				
		$sql="SELECT bv.id,bv.alias,catid,title,hits,created_day,bv.image,bv.thumb,bv.status FROM $tbl as bv JOIN tblchuyenmuc as cm ON cm.id = bv.catid WHERE (catid IN (1,8,13,20,27) OR catid IN(select id  from tblchuyenmuc where uid IN (1,8,13,20,27))) AND '$day'>=created_day AND bv.status = 1 AND cm.status=1 order by created_day desc limit $start_row,$limit";
		$query=$this->db->query($sql);
		return $query;
	}	
	public function gettags_limited($tbl,$tags,$start_row,$limit)
	{		
		$tags=str_replace('-',' ',$tags);
		$sql="select id,alias,catid,title,mota,thumb,status,created_day from $tbl where status = 1 AND tags LIKE '%$tags%' order by id desc limit  $start_row,$limit";		
		$query=$this->db->query($sql);
		return $query;
	}	
	public function gettbl_hot_limited($tbl,$id,$start_row,$limit)
	{		
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$day = date('Y-m-d H:i:s');	 
		$sql="select bv.id,bv.alias,catid,title,sapo,bv.thumb,bv.status FROM $tbl as bv JOIN tblchuyenmuc as cm ON cm.id = bv.catid WHERE bv.status = 1 AND cm.status=1 AND feature=1 AND created_day<='$day'";
		if($id!=''){
			$sql .=" AND (catid = $id OR catid IN(select id from tblchuyenmuc where uid = $id))";
		}		
		$sql .=" order by bv.id desc limit $start_row,$limit";
		$query=$this->db->query($sql);
		return $query;
	}	
	public function getvideo($tbl,$start_row,$limit)
	{		
		$sql="select * from $tbl where status = 1 order by id desc limit $start_row,$limit";
		$query=$this->db->query($sql);
		return $query;
	}
	// Đệ quy link thân thiện
	public function getcatlink($uid)
    {					
		$catlink=0;
        $this->db->where('id',$uid);
        $sql1=$this->db->get('tblchuyenmuc');
        if($sql1->num_rows() >0)
        {           		
            foreach($sql1->result() as $items)
            {                            
                $catlink = $this->getcatlink($items->uid); 
				$catlink .= '/'.$items->alias;								
            }   
		return $catlink;
        }				
    }    
	public function search($tbl,$tags)
	{		
		$tags=str_replace('-',' ',$tags);		
		$this->db->where('status',1);
		$this->db->like("title",$tags);
		$query = $this->db->get($tbl);
		return $query;
	}	
	public function search_limited($tbl,$tags,$start_row,$limit)
	{		
		$tags=str_replace('-',' ',$tags);
		$sql="select id,alias,catid,title,mota,thumb,status,created_day from $tbl where status = 1 AND title LIKE '%$tags%' order by id desc limit  $start_row,$limit";		
		$query=$this->db->query($sql);
		return $query;
	}	
}
?>