<?php 
class Site extends MX_Controller
{
	public function __construct()
	{
		parent::__construct(); 		
		$this->load->model('site_model');	
        $this->load->model('admin/admin_model');
        $this->load->helper('images');	
        $this->load->library('form_validation');
        $this->lang->load("vi", "vietnamese");
        //$this->load->library('memcached_library');
        //$this->output->enable_profiler(TRUE);	
	}   
	public function index()
    { 
        //$this->check_cookie();
		$data['home'] = true;		
		$data['active'] ='';
        $_SESSION['base_url']=base_url();
        $data['content']='includes/main_content';
        $this->load->view('includes/template',$data);
    }
    public function dangkyngay()
    {
        $this->load->helper('mobile');
        $mobilect = detect_mobile();
        if($mobilect === true){            
            $link=site_url('dang-ky-ngay.html');
            $link1=str_replace('daotaovieclam.edu.vn','m.daotaovieclam.edu.vn',$link);
            redirect($link1);             
        }else{
            $data['header_title']='Đăng ký ngay';
            $data['dangky']=true;
            $data['content']='dangkyngay';			
    		$this->load->view('includes/template',$data);
        }
    }
    public function dodangkyhoc()
    {
        $this->form_validation->set_rules('hoten','<span style="color:red;">Họ tên</span>','required');
        $this->form_validation->set_rules('diachi','<span style="color:red;">Địa chỉ</span>','required');
        $this->form_validation->set_rules('dienthoai','<span style="color:red;">Điện thoại</span>','required');
        $this->form_validation->set_rules('email','<span style="color:red;">Email</span>','required|valid_email');
        $this->form_validation->set_rules('songuoidk','<span style="color:red;">Số người đăng ký</span>','required');
        $this->form_validation->set_rules('dangkymh','<span style="color:red;">Đăng ký môn học</span>','required');
        $this->form_validation->set_rules('dangkymh','<span style="color:red;">Thời gian học</span>','required');
        $this->form_validation->set_rules('mabaomat', '<span style="color:red;">Mã bảo mật</span>', 'required|check_captcha');
        $this->form_validation->set_message('required','<span style="color:red;">%s không để trống</span>');
        $this->form_validation->set_message('valid_email','<span style="color:red;">%s không hợp lệ</span>');
        $this->form_validation->set_message('check_captcha','<span style="color:red;">%s không đúng</span>');
        if($this->form_validation->run()==FALSE){
            $data['errors_regíter']=validation_errors();
            $data['header_title']='Đăng ký ngay';
            $data['dangky']=true;
            $data['content']='dangkyngay';			
		    $this->load->view('includes/template',$data);                
        }else{
            $data_register=array(
                'hoten' =>$this->input->post('hoten'),
                'diachi'    =>  $this->input->post('diachi'),
                'dienthoai'    =>  $this->input->post('dienthoai'),
                'email'    =>  $this->input->post('email'),
                'soluong'    =>  $this->input->post('songuoidk'),
                'monhoc'    =>  $this->input->post('dangkymh'),
                'thoigian'    =>  $this->input->post('thoigian'),
                'noidung'    =>  $this->input->post('noidungkhac'),
                'status'    =>0
            );
            $this->db->insert('tbldangkyhoc',$data_register);
            $data['header_title']='Đăng ký ngay';
            $data['dangky']=true;
            $data['content']='success_dk';			
		    $this->load->view('includes/template',$data);   
        }
    }
    public function emailgiamgia()
    {
        $mailsale=$_POST['mailsale'];    
        require_once('class.phpmailer.php'); 
        require_once('class.pop3.php'); 
        define('GUSER', 'contact@buonchung.com');         
        define('GPWD', 'cogangtopweb@2012');  
        $this->db->where('status',1);
        $this->db->where('gia !=',0);
        $this->db->where('giakm !=',0);
        $this->db->order_by('id','random');
        $this->db->limit(1);
        $sqlsanpham=$this->db->get('tblsanpham')->row();
        $noidung1='<p><b>Thông tin Email giảm giá từ website buonchung.com</b></p>
        <table>
            <tr>                              
               <td> 
                    <p><strong>Tên sản phẩm:&nbsp;'.$sqlsanpham->title.'</strong></p>    
                    <p>Giá gốc:&nbsp;'.number_format($sqlsanpham->gia,0,'.','.').'&nbsp;'.$sqlsanpham->donvitinh.'</p>
                    <p>Giá khuyễn mại:&nbsp;'.number_format($sqlsanpham->giakm,0,'.','.').'&nbsp;'.$sqlsanpham->donvitinh.'</p>
               </td>
            </tr>
        </table>
        <div id="noidung_em">
            <p><b>Thông tin chi tiết</b></p>
            '.$sqlsanpham->noidung.' 
            <div style="clear:both;"></div>   
        </div>
        ';             
        global $message;    
        $this->smtpmailer($mailsale, "contact@buonchung.com", "Buôn chung", "Thông tin Email giảm giá từ website buonchung.com",$noidung1);
        echo $message;
    }
    public function lienhe()
    {
        $this->load->helper('mobile');
        $mobilect = detect_mobile();
        if($mobilect === true){            
            $link=site_url('lien-he.html');
            $link1=str_replace('daotaovieclam.edu.vn','m.daotaovieclam.edu.vn',$link);
            redirect($link1);             
        }else
        {
            $this->load->helper('map');
            $data['header_title']='Liên hệ';
            $data['contact']=true;
    		$data['content']='lienhe';			
    		$this->load->view('includes/template',$data);
        } 
    }
    public function docontact()
    {
        $this->load->helper('map');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('txthoten','<span style="color:red;">Họ tên</span>','required');    
        $this->form_validation->set_rules('txtdc','<span style="color:red;">Địa chỉ</span>','required'); 
        $this->form_validation->set_rules('txtdt','<span style="color:red;">Điện thoại</span>','required'); 
        $this->form_validation->set_rules('txtemail','<span style="color:red;">Email</span>','required|valid_email');
        $this->form_validation->set_message('required','<span style="color:red;">%s không để trống</span>'); 
        $this->form_validation->set_message('valid_email','<span style="color:red;">%s không hợp lệ</span>'); 
        if($this->form_validation->run()==FALSE){            
            $data['header_title']='Liên hệ';
            $data['errors_register'] = validation_errors();
		    $data['content']='lienhe';			
		    $this->load->view('includes/template',$data);         
        }else{
            $date = getdate();
    		$sDate = $date['year'].'-'.$date['mon'].'-'.$date['mday'];
    		$insert_data = array(
				'hoten'	=> $this->input->post('txthoten'),
				'diachi'	=> $this->input->post('txtdc'),
				'dienthoai'	=> $this->input->post('txtdt'),
				'email'	=>	$this->input->post('txtemail'),
				'noidung'	=> $this->input->post('txtnd'),
				'ngaylienhe'	=> $sDate,
				'status'	=>0
            ); 
            $this->db->insert('tbllienhe',$insert_data);
            $data['header_title']='Liên hệ';
            $data['kq']=true; 
            $data['content']='lienhe';			
		    $this->load->view('includes/template',$data);   
        }
    }
    public function theodoidonhang()
    {
        $this->checksession();
       //$this->load->helper('status');
        $start_row=$this->uri->segment(3);
		$per_page=15;
		if(is_numeric($start_row)){
			$_SESSION['start_row']=$start_row;					
		}else{
			$_SESSION['start_row']=0;				
		}	
		$query=$this->site_model->gettbl1('tbldonhangweb');			
		$total_rows = $query->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = site_url().'/site/theodoidonhang/';
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Prev';
		$config['num_links'] = 4;
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';    		
		$this->pagination->initialize($config);
        $data['theodoi']=true;
		$data['query']=$this->site_model->gettbl_limited1('tbldonhangweb',$per_page,$_SESSION['start_row']);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='theodoidonhang';			
		$this->load->view('includes/template',$data);    
    }
    public function checkdonhangcp()
    {
        $sodt=$_POST['sodt'];
        $radioship=$_POST['radioship'];
        $soluong=$_POST['soluong'];
        $id_cp=$_POST['id_cp'];
        $ngay=getdate();
        $ngaydang=$ngay['year'].'-'.$ngay['mon'].'-'.$ngay['mday'];
        if(isset($_SESSION['username'])){
            $this->db->where('name',$_SESSION['username']);
            $sqladmindencuh=$this->db->get('tbladmin')->row();
            $hoten=$sqladmindencuh->fullname;
            $email=$sqladmindencuh->email;
            $name=$sqladmindencuh->id;
        }else{
            $hoten='';
            $email='';
            $name='';
        }
        $data_dh=array(
            'maphieu'   =>$this->rand_string(6),
            'hinhthuc'  =>  'Đến cửa hàng mua',
            'hoten' =>  $hoten,
            'dienthoai'=>$sodt,
            'email' =>$email,
            'diachi'=>$radioship,
            'sanpham'   =>$id_cp,
            'soluong'   => $soluong,
            'ngaydang'  =>  $ngaydang,
            'name'  =>  $name,
            'status'    =>0
        );  
        $this->db->insert('tbldonhangweb',$data_dh);                                                          
    }    
    public function editnhanhang($id)
    {
        $this->checksession();
        $data['id']=$id;
        $data['diachimore']=true;
        $data['content']='diachich';
        $this->load->view('includes/template',$data);    
    }
    public function editdiachi($id)
    {
        $this->checksession();
        $data['diachimore']=true;
        $data['id']=$id;
        $data['content']='diachich';
        $this->load->view('includes/template',$data);    
    }
    public function xoadiachi($id)
    {
        $this->db->delete('tbldiachi',array('id'=>$id));
        redirect(site_url('danh-sach-dia-chi.html'));
    }
    public function loadcate2($id)
    {
        $district=$this->site_model->gettablename_all('tblquan','id,quan,tinh,thutu,status','','tinh',$id,'','');
        echo '<option value="-1">--Chuyên quận huyện--</option>';
	    foreach($district->result() as $item)
	    {
	       echo '<option value="'.$item->id.'"'.set_select('cmbquan',$item->id).'>'.$item->quan.'</option>';
	    }
    }        
    public function doimatkhau()
    {
        $this->checksession();
        $data['doimk']=true;
        $data['content']='doimatkhau';
        $this->load->view('includes/template',$data);
    }
    public function dodoimatkhau()
    {
        $this->checksession();
        $this->form_validation->set_rules('matkhaucu','<span style="color:red;">Mật khẩu cũ</span>','required'); 
        $this->form_validation->set_rules('matkhaumoi','<span style="color:red;">Mật khẩu mới</span>','required|matches[matkhaucure]');
        $this->form_validation->set_message('required','<span style="color:red;">%s không để trống</span>');
        $this->form_validation->set_message('matches','<span style="color:red;">%s không trùng nhau</span>');
        if($this->form_validation->run()==FALSE)
        {
            $data['error_register']=validation_errors();   
            $data['content']='doimatkhau';
            $data['doimk']=true;
            $this->load->view('includes/template',$data); 
        }
        else
        {
            $matkhau=md5($_POST['matkhaucu']);
            $this->db->where('pass',$matkhau);
            $d=$this->db->get('tbladmin');
            if($d->num_rows() >0)
            {
                $data_mk=array(
                    'pass'=>md5($this->input->post('matkhaumoi'))
                );  
                $this->db->where('id',$this->input->post('id')); 
                $this->db->update('tbladmin',$data_mk); 
                $data['doimk']=true;
                $data['thanhcong']=true;  
                $data['content']='doimatkhau';                  
                $this->load->view('includes/template',$data);    
            }   
            else
            {
                $data['error_register']='<p>Mật khẩu cũ không đúng</p>';   
                $data['content']='doimatkhau';
                $this->load->view('includes/template',$data);     
            }
        }
    }
    public function quenmatkhau()
    {
        $this->check_cookie();
        $data['quenmk']=true;
        $data['content']='quenmatkhau';
        $this->load->view('includes/template',$data);    
    }
    public function doquenmatkhau()
    {
        $this->check_cookie();
        $this->form_validation->set_rules('email','<span style="color:red;">Email</span>','required|valid_email');
        $this->form_validation->set_message('required','<span style="color:red;">%s không để trống</span>');
        $this->form_validation->set_message('valid_email','<span style="color:red;">%s không hợp lệ</span>');
        if($this->form_validation->run() == FALSE)
        {
            $data['quenmk']=true;
            $data['error_register']=validation_errors();  
            $data['content']='quenmatkhau';
            $this->load->view('includes/template',$data);   
        }
        else
        {
            $email = $_POST['email'];
            $this->db->where('email',$email);
            $u = $this->db->get('tbladmin');
            if($u->num_rows() > 0)
            {
                $u=$u->row();
                $matkhau=$this->rand_string(10);
                $data = array(
                    'pass' =>md5($matkhau),
                );    
                $this->db->where('email',$email);
                $this->db->update('tbladmin',$data);
                require_once('class.phpmailer.php'); 
                require_once('class.pop3.php'); 
                define('GUSER', 'rasu666@gmail.com');         
                define('GPWD', 'cogangtopweb@2012');  
                $noidung='<p><b>Thông tin đăng nhập từ website buonchung.com</b></p>
                <p>Tên đăng nhập:<b>'.$u->name.'</p>
                <p>Mật khẩu:<b>'.$matkhau.'</b></p>
                ';             
                global $message;    
                $this->smtpmailer($email, "rasu666@gmail.com", "Buôn chung", "Quên mật khẩu đăng nhập buonchung.com",$noidung);
                echo $message;
                $data['quenmk']=true;
                $data['thanhcong']=true;                
                $data['content']='quenmatkhau';
                $this->load->view('includes/template',$data);  
            }
            else
            {
                $data['quenmk']=true;
                $data['error_register']='<p>Email không tồn tại trong hệ thống</p>';  
                $data['content']='quenmatkhau';
                $this->load->view('includes/template',$data);       
            }
        } 
    }
    public function rand_string($length)
	  {
	   $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; 
	   $str='';
	   $size=strlen($chars);
	   for($i=0;$i<$length;$i++)
	   {
		$str.=$chars[rand(0,$size-1)];
	   }
	   return $str;
	  }
    public function dotaikhoan()
    {
        $this->checksession();
        $id=$this->input->post('txtid');
        $config['upload_path'] = './upload/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']	= '1000000';
		$config['max_width']  = '0';
		$config['max_height']  = '0';
		$this->load->library('upload', $config);
        if(!is_dir($config['upload_path'])){
			mkdir($config['upload_path'], 0755, TRUE);
		}
		if(!is_dir($config['upload_path'].'resized/')){
			mkdir($config['upload_path'].'resized/', 0755, TRUE);
		}
        if ( ! $this->upload->do_upload('avatar'))
		{
			if($id==''){
				$image='';
				$thumb='';
			}
			else{
				$image=$this->input->post('anh');
				$thumb=$this->input->post('anh_thumb');
			}
		}
        else
        {
            $image_data = $this->upload->data();
		    $name_img='upload/'.$image_data['file_name'];  
        	$temp=explode('.',$image_data['file_name']);
			if($temp[1]=='jpeg' or $temp[1]=='JPEG'){
				$temp[1]='jpg';
			}
			$temp[1]=strtolower($temp[1]);
			$thumb='upload/resized/'.$temp[0].'_thumb'.'.'.$temp[1];
			$image='upload/'.$image_data['file_name'];
			$imageThumb = new Image($name_img);
			$imageThumb->resize(50,50,'crop');
			$imageThumb->save($temp[0].'_thumb', './upload/resized');       
        }
        $data=array(
            'fullname' =>  $this->input->post('hoten'),
            'dienthoai' =>  $this->input->post('dienthoai'),
            'image' =>$image,
            'thumb' =>$thumb
        );   
        $this->site_model->add_tbl('tbladmin',$data,$id); 
        $data['thanhcong']=true;
         $data['ctp']=true;
        $data['content']='panel';
        $this->load->view('includes/template',$data);
    }
    public function checksession()
    {
        if(isset($_SESSION['username']) || isset($_COOKIE['user']))
        {}
        else
        {
            redirect(site_url('dang-nhap.html'));
        }
    }
    public function dangnhap()
    {
        $data['header_title']='Đăng nhập hệ thống';  
        $data['dangnhap_r']=true;
        $data['content']='dangnhap';
        $this->load->view('includes/template',$data);  
    }
    public function checklogin()
    {
        $username=$_POST['username'];
        $pass=$_POST['pass'];
        $this->db->where('name',$username);
        $this->db->where('pass',md5($pass));
        $user=$this->db->get('tbladmin');
        if($user->num_rows() >0){
            echo 'true';
            $_SESSION['username']=$username;
            $_SESSION['id']=$user->row()->id;
        }else{
            echo 'false';
        }
    }
    public function checkluu()
    {
        $username=$_POST['username'];
        $pass=$_POST['pass'];
        $luu=$_POST['luu'];
        $expire=time()+60*60*24*365; // Tổng số giây có được từ thời có máy chủ unix, tức là thời gian sẽ là ngày mai nếu tính từ thời điểm hiện tại
		setcookie("user",$username, $expire,"/");
		setcookie("pass",$pass,time() +60*60*30*365,"/");
    }
    public function dangky()
    {
        $data['header_title']='Đăng ký thành viên';
        $data['dangky_r']=true;
        $data['content']='dangky';
        $this->load->view('includes/template',$data);
    }
    public function checkdangky()
	{
	   $check = 0;
	   $nameuser = $this->db->query('SELECT id FROM tbladmin where name="'.$_POST['taikhoan'].'"')->num_rows();	  
	   if($nameuser>0){	  
			$check = 1;
	   }
	  	echo $check;
	}
    public function checkcapcha()
	{		
	   $check = 0;
	   if($_SESSION['img']!=$_POST['capcha']){
		$check = 1;
	   }
	   echo $check;
	}
    public function checkmail()
	{
	   $check = 0;
	   $mail = $this->db->query('SELECT id FROM tbladmin where email="'.$_POST['email'].'"')->num_rows();	  
	   if($mail>0){	  
			$check = 1;
	   }
	   echo $check;
	}
    public function add_thanhvien()
    {
        $ngay=getdate();
		$ngaydang=$ngay['year'].'-'.$ngay['mon'].'-'.$ngay['mday'];
		$macode=md5(uniqid(rand()));
		$email=$_POST['email'];
        if($_POST['id']!='')
        {
        }
        else
        {
            $datatv=array(
                'macode'    =>  $macode,
                'fullname'  =>  $this->input->post('hoten'),
                'dienthoai' =>  $this->input->post('didong'),
                'name'      =>  $this->input->post('taikhoan'),
                'pass'      =>  md5($this->input->post('matkhau')),
                'email'     =>  $this->input->post('email'),
                'role'      => 2,
                'status'    => 0,
                'ngaydang'  =>  $ngaydang,
            );
            $this->site_model->add_tbl('tbladmin',$datatv,'');
            //Gửi mail
			$to=$email;
			$subject='Thông tin kích hoạt tài khoản:';
			$header='Từ: Buôn chung.';
			$noidung="Đường link kích hoạt:\r\n";
			$noidung.="Nhấn đường link sau để kích hoạt tài khoản\r\n";
			$noidung.=base_url()."site/activemember/$macode";
			require_once('class.phpmailer.php');             
			require_once('class.pop3.php');    
			define('GUSER','rasu666@gmail.com');
			define('GPWD','cogangtopweb@2012');
			global $message;
			$this->smtpmailer($to,'rasu666@gmail.com',$header,$subject,$noidung);
			echo $message;	 
        }
    }
    public function activemember($macode)
	{
	    $this->db->where('macode',$macode);
        $sqlactive=$this->db->get('tbladmin');
        if($sqlactive->num_rows() >0)
        {
			$count=$sqlactive->num_rows();
			if($count==1)
			{
		    $data_up=array(
			     'macode'=>'',
				 'status' =>1,
			);
			$this->db->where('macode',$macode);
			$this->db->update('tbladmin',$data_up);
			}
			else
			{
				echo 'Chuỗi sai';
			}			
		}
        redirect(base_url());
    }
	public function tinh()
    {
        //$this->check_cookie();
        $city=$_POST['city'];
        if($city==-1)
        {
            redirect(base_url());    
        }
        else
        {
            //$_SESSION['dachon']='ok';
            $this->db->where('id',$city);
            $tinh=$this->db->get('tbltinh');
            if($tinh->num_rows() >0)
            {
                $row=$tinh->row();
                setcookie("city",$row->id,time() +60*60*30*365,"/");    
            }
            $data['city']=$city;
            $data['content']='includes/main_content';
            $this->load->view('includes/template',$data);
            redirect(base_url());
        }
    }
    public function tinhmore($id)
    {
        $this->db->where('id',$id);
        $tinh2=$this->db->get('tbltinh');
        if($tinh2->num_rows() >0)
        {
            $row2=$tinh2->row();
            setcookie("city",$row2->id,time() +60*60*30*365,"/");    
        }    
        $data['city']=$id;
        $data['content']='includes/main_content';
        $this->load->view('includes/template',$data);
        redirect(base_url());
    }
    public function check_cookie()
    {
        if(isset($_COOKIE['city']))
        {}
        else
        {
            redirect(base_url());
        }
    }	
	public function sanphambyid($id)
    {
        $this->check_cookie();
        $this->site_model->num_view($id);
        $data['chitiet']=$id;	
        $this->db->where('id',$id);
        $data['query']=$this->db->get('tblsanpham')->row();
        if($data['query']->meta_title!='')
        {
            $data['header_title']=$data['query']->meta_title;
        }
        else
        {
            $data['header_title']=$data['query']->title;
        }
        if($data['query']->meta_des!='')
        {
            $data['description']=$data['query']->meta_des;
        }
        else
        {
            $data['description']=$data['query']->title;
        }
        if($data['query']->keyword!='')
    	{
    	    $data['keyword']=$data['query']->keyword;
    	}
        $data['cty']=true;
        $data['ctfb']=$id;
		$data['content']='sanphambyid';
        $this->load->view('includes/template',$data);    
    }
    public function thuvienbyid($id)
    {
        $data['id']=$id;
        $data['content']='thuvien';		
        $this->load->view('includes/template',$data);    
    }
    public function baivietbyid($id)
    {
        $this->load->helper('mobile');
        $mobilect = detect_mobile();
        if($mobilect === true){
            $this->db->where('id',$id);
            $sqldientoct=$this->db->get('tblbaiviet')->row();
            $link=site_url($sqldientoct->alias.'-'.$sqldientoct->id.'.html');
            $link1=str_replace('daotaovieclam.edu.vn','m.daotaovieclam.edu.vn',$link);
            redirect($link1);             
        }
        else
        {
            $data['id']=$id;
            $this->db->where('id',$data['id']);
            $data['queryh']=$this->db->get('tblbaiviet');
            if($data['queryh']->num_rows()>0)
            {
                $data['queryh']=$data['queryh']->row();
                if($data['queryh']->meta_title!='')
            	{
            	    $data['header_title']=$data['queryh']->meta_title;
            	}
            	else
            	{     	    
                    $data['header_title']=$data['queryh']->title;                                                         
            	}
            	if($data['queryh']->keyword!='')
            	{
            	    $data['keyword']=$data['queryh']->keyword;
            	}
            	if($data['queryh']->meta_des!='')
            	{
            	    $data['description']=$data['queryh']->meta_des;
            	}
            	else
            	{    	   
                    $data['description']=$data['queryh']->title;                       
            	}      
        		$data['top_content']='';	
        		$data['only_item']='';				
        		$this->site_model->hits('tblbaiviet',$id);
        		$data['item']= $this->site_model->getitem('tblbaiviet',$id)->row();
        		$check_catids=$this->site_model->getitem('tblchuyenmuc',$data['item']->catid)->row();
        		$check_catid=$check_catids->id;
        		if($check_catids->uid!=0){
        			$check_catid=$check_catids->uid;
        		}
        		$data['active']=$check_catid;
        		$CI=&get_instance();
        		$data['title_site']=htmlspecialchars($data['item']->title);
        		//$data['sapo_site']=htmlspecialchars($data['item']->sapo);
        		$data['image_site']=htmlspecialchars($data['item']->thumb);
        		$category=''; $category=$CI->site_model->getcatlink($data['item']->catid);
        		$data['link_site']=site_url($category.'/'.$data['item']->alias.'-'.$id).'.html';
                $data['chitiettu']=$id;
                $data['chitiet_code_head']=$id;
        		$data['content']='baiviet';		
                $this->load->view('includes/template',$data);
            }
            else
            {
                redirect(base_url());    
            }
        }    
    }	
	public function show_chuyenmuc($id)
    {
        $this->load->helper('mobile');
        $mobilect = detect_mobile();
        if($mobilect === true){ 
            $this->db->where('id',$id);
            $sqlchuyenmucmobile=$this->db->get('tblchuyenmuc')->row();
            $link=site_url($sqlchuyenmucmobile->alias.'-c'.$sqlchuyenmucmobile->id.'.html');
            $link1=str_replace('daotaovieclam.edu.vn','m.daotaovieclam.edu.vn',$link);
            redirect($link1);             
        }
        else
        {
    		$this->db->where('id',$id);
            $this->db->select('id,name,meta_title,meta_des,keyword');
            $data['chuyenmuc']=$this->db->get('tblchuyenmuc')->row();
            if($data['chuyenmuc']->keyword!='')
            {
    	       $data['keyword']=$data['chuyenmuc']->keyword;
            }
            if($data['chuyenmuc']->meta_title!='')
            {
    	       $data['header_title']=$data['chuyenmuc']->meta_title;
        	}
            else
        	{
        	    $data['header_title']=$data['chuyenmuc']->name;
        	}
            if($data['chuyenmuc']->meta_des!='')
        	{
        	    $data['description']=$data['chuyenmuc']->meta_des;
        	}          
    		$cat=$this->site_model->getcatlink($id);
    		$start_row=$this->uri->segment(count(explode('/',$cat)));		
    		$per_page=16;
    		if(is_numeric($start_row))
    		{
    			$start_row=$start_row;
    		}
    		else
    		{
    			$start_row=0;
    		}
    		$query=$this->site_model->getlist($id);			
    		$cats=$this->site_model->getitem('tblchuyenmuc',$id)->row();		
    		$total_rows = $query->num_rows();			
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().$cat.'-c'.$id;
    		$config['total_rows'] = $total_rows;	
    		$config['per_page'] = $per_page;		
    		$config['uri_segment'] =count(explode('/',$cat));		
    		$config['next_link'] = 'Sau';
    		$config['prev_link'] = 'Trước';
    		$config['num_links'] = 4;		
    		$config['first_link'] = 'Đầu';
    		$config['last_link'] = 'Cuối';    		
    		$this->pagination->initialize($config);
    		$data['chuyenmuc']= $id;
    		$data['query']=$this->site_model->gettbl_limited('tblbaiviet',$id,$start_row,$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='chuyenmuc';			
    		$this->load->view('includes/template',$data);
        }    		
	  
    }
	/*---------------------*/
	public function show_tags($tags)
    {
        $this->load->helper('mobile');
        $mobilect = detect_mobile();
        if($mobilect === true){            
            $link=site_url('tags/'.trim($tags));
            $link1=str_replace('daotaovieclam.edu.vn','m.daotaovieclam.edu.vn',$link);
            redirect($link1);             
        }
        else
        {
            $tags1=str_replace('-',' ',$tags);
    		$start_row=$this->uri->segment(3);
    		$per_page=20;
    		if(is_numeric($start_row))
    		{
    			$start_row=$start_row;
    		}
    		else
    		{
    			$start_row=0;
    		}
    		$query=$this->site_model->gettags('tblbaiviet',$tags);        				        
            $data['header_title']=$tags1;    			
    		$total_rows = $query->num_rows();			
    		$this->load->library('pagination');
    		$config['base_url'] = site_url().'tags/'.$tags;
    		$config['total_rows'] = $total_rows;	
    		$config['per_page'] = $per_page;
    		$config['uri_segment'] =3;
    		$config['next_link'] = 'Sau';
    		$config['prev_link'] = 'Trước';
    		$config['num_links'] = 4;		
    		$config['first_link'] = 'Đầu';
    		$config['last_link'] = 'Cuối';    			
    		$this->pagination->initialize($config);
    		$data['tags']= $tags;	
    		$data['active']= '';		
    		$data['query']=$this->site_model->gettags_limited('tblbaiviet',$tags,$start_row,$per_page);
    		$data['pagination']= $this->pagination->create_links();	
    		$data['content']='tags';			
    		$this->load->view('includes/template',$data);
        }    			  
    }
	public function listsukien($sukien=0)
    {					
		if(isset($_POST['txt_sukien'])){
			$sukien=$_POST['txt_sukien'];
		}
		$data['top_content']=0;
		$data['active']= '';
		$data['sukien_id']= $sukien;
		$data['query']=$this->site_model->getlistsukien('tblbaiviet',$sukien);		
		$data['content']='sukien';			
		$this->load->view('template',$data);    			  
    }
	public function listevent()
    {							
		$data['top_content']=0;
		$data['active']= '';		
		$data['query']=$this->site_model->gettbl_sukien('tblsukien','',0,20);		
		$data['content']='list-sukien';			
		$this->load->view('template',$data);    			  
    }
	public function search($txt_search='')
    {		
		$start_row=$this->uri->segment(3);
		if($txt_search==''){
			$txt_search=preg_replace('/([^\pL\.\ ]+)/u', '',$_POST['ten']);	
			//$txt_search=$_POST['txt_search'];			
		}		
		else{		
			$txt_search= str_replace('+',' ',$txt_search);	
		}
		$per_page=20;
		if(is_numeric($start_row))
		{
			$start_row=$start_row;
		}
		else
		{
			$start_row=0;
		}
		$txt_search1= str_replace(' ','+',$txt_search);		
		$query=$this->site_model->search('tblbaiviet',$txt_search);			
		//$cat=$this->site_model->gettbl('tblchuyenmuc',$id)->row();			
		$total_rows = $query->num_rows();			
		$this->load->library('pagination');
		$config['base_url'] = site_url().'ket-qua-tim-kiem/'.$txt_search1;
		$config['total_rows'] = $total_rows;	
		$config['per_page'] = $per_page;
		$config['uri_segment'] =3;
		$config['next_link'] = 'Sau';
		$config['prev_link'] = 'Trước';
		$config['num_links'] = 4;		
		$config['first_link'] = 'Đầu';
		$config['last_link'] = 'Cuối';    			
		$this->pagination->initialize($config);				
		$data['active']= '';
		$data['txt_search']= $txt_search;
		$data['query']=$this->site_model->search_limited('tblbaiviet',$txt_search,$start_row,$per_page);
		$data['pagination']= $this->pagination->create_links();	
		$data['content']='search';			
		$this->load->view('includes/template',$data);    		
    }
    public function thoat()
    {
        setcookie("user"," ",time() - 3600,"/");
		setcookie("pass"," ",time() - 3600,"/");
        if(isset($_SESSION['username']))
        {
            unset($_SESSION['username']);
        }
        redirect(base_url());
    }
    public function smtpmailer($to, $from, $from_name, $subject, $body)
    {
        $mail = new PHPMailer();                  // tạo một đối tượng mới từ class PHPMailer
        $mail->IsSMTP();                         // bật chức năng SMTP
        $mail->SMTPDebug = 0;                      // kiểm tra lỗi : 1 là  hiển thị lỗi và thông báo cho ta biết, 2 = chỉ thông báo lỗi
        $mail->SMTPAuth = true;                  // bật chức năng đăng nhập vào SMTP này
        $mail->CharSet = "UTF-8";
        $mail->SMTPSecure = 'ssl';                
        $mail->Host = 'smtp.zoho.com';         
        $mail->Port = 465;                         
        $mail->Username = GUSER;  
        $mail->Password = GPWD;           
        $mail->SetFrom($from, $from_name);
        $mail->Subject = $subject;
        $mail->Body = $body;
        $mail->AddAddress($to);
        if(!$mail->Send())
        {
            $message = 'Gởi mail bị lỗi: '.$mail->ErrorInfo; 
            return false;
        } 
        else 
        {
            $message = 'Thư của bạn đã được gởi đi ';
            return true;
        }
    }
}
?>