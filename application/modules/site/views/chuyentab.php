<div id="listchuyentab2">
<div class="popup-header1" style="background:url(images/xac_nhan_thong_tin_ca_nhan.png) top left no-repeat !important;">
        <a class="close_popup1" href="javascript:void(0)"></a>
</div>
<div id="hinhthuc_main">
    <p class="hinhthuc_tan_noi">Thông tin tại khoản hiện tại</p>
    <div id="hinthuc_tannoi_left">
        <p>Thông tin liên hệ</p>
        <?php 
        if(isset($_SESSION['username']))
        {
            $this->db->where('name',$_SESSION['username']);
            $admin_ht=$this->db->get('tbladmin')->row();
        }
        ?>
        <table>
            <tr>
                <th>Họ và tên:</th>
                <td><input type="text" name="hoten" id="hoten" value="<?php echo $admin_ht->fullname; ?>" /></td>
            </tr>
            <tr>
                <th>Điện thoại:</th>
                <td><input style="width:155px;" type="text" name="dienthoai" id="dienthoai" value="<?php echo $admin_ht->dienthoai; ?>" /><span>Dùng liên hệ khi giao hàng</span></td>
            </tr>
            <tr>
                <th>Email:</th>
                <td><input type="text" name="email" id="email" value="<?php echo $admin_ht->email; ?>" /></td>
            </tr>
        </table>
    </div>
    <div id="hinthuc_tannoi_right">
        <p>Địa chỉ nhận hàng</p>
        <?php 
        if(isset($_SESSION['username']))
        {
            $this->db->where('name',$_SESSION['username']);
            $admin_ht=$this->db->get('tbladmin')->row();
            $this->db->where('name',$admin_ht->id);
            $sqldiachinhanhang=$this->db->get('tbldiachinhanhang')->row();
        }
        ?>
        <table>
            <tr>
                <th>Tỉnh thành:</th>
                <td>
                <select name="tinhthanh_dh" id="cmbtinh">
                    <option value="-1">--Chọn tỉnh thành--</option>
                    <?php 
                        $this->db->select('id,tinh');
                        $sqltinhtab=$this->db->get('tbltinh');
                        if($sqltinhtab->num_rows()>0)
                        {
                            foreach($sqltinhtab->result() as $itemtinhtab)
                            {
                                if($sqldiachinhanhang->tinh==$itemtinhtab->id)
                                {
                                ?>
                                <option value="<?php echo $itemtinhtab->id; ?>" selected="selected"><?php echo $itemtinhtab->tinh; ?></option>
                                <?php    
                                }
                                else
                                {
                                ?>
                                <option value="<?php echo $itemtinhtab->id; ?>"><?php echo $itemtinhtab->tinh; ?></option>
                                <?php    
                                }                                
                            }
                            $sqltinhtab->free_result();
                        }
                    ?>
                </select></td>
            </tr>
            <tr>
                <th>Quận/Huyện:</th>
                <td>
                <select name="quyenhuyen_dh" id="cmbquan">
                    <option value="-1">--Chọn Quận/Huyện--</option>
                    <?php 
                        $this->db->select('id,quan');
                        $sqlquantab=$this->db->get('tblquan');
                        if($sqlquantab->num_rows()>0)
                        {
                            foreach($sqlquantab->result() as $itemquantab)
                            {
                                if($sqldiachinhanhang->quan==$itemquantab->id)
                                {
                                ?>
                                <option value="<?php echo $itemquantab->id; ?>" selected="selected"><?php echo $itemquantab->quan; ?></option>
                                <?php    
                                }
                                else
                                {
                                ?>
                                <option value="<?php echo $itemquantab->id; ?>"><?php echo $itemquantab->quan; ?></option>
                                <?php    
                                }                                
                            }
                            $sqlquantab->free_result();
                        }
                    ?>
                </select></td>
                <script language="javascript">
 			    jQuery(document).ready(function() {
				    jQuery('#cmbtinh').change(function() {
   					    giatri = this.value;
       					jQuery('#cmbquan').load('<?php echo site_url().'/site/loadcate2/'; ?>' + giatri);                        
       	 			});
     			});
      		</script>

            </tr>
            <tr>
                <th>Phường/Xã</th>
                <td>
                    <input type="phuongxa" name="phuongxa" id="phuongxa" value="<?php echo $sqldiachinhanhang->phuongxa; ?>" />
                </td>
            </tr>
            <tr>
                <th>Đường phố</th>
                <td><input type="text" name="duongpho" id="duongpho" value="<?php echo $sqldiachinhanhang->duongpho; ?>" />
                <input type="hidden" name="ip_cuoi" id="ip_cuoi" value="<?php echo $ip_cuoi; ?>" />
                <input type="hidden" name="radiohinhthuc" id="radiohinhthuc" value="<?php echo $radiohinhthuc; ?>" />
                </td>
            </tr> 
            <tr>
                <th>Số nhà</th>
                <td><input type="text" name="sonha" id="sonha" value="<?php echo $sqldiachinhanhang->sonha; ?>" /></td>
            </tr>           
        </table>
    </div>
    <div class="clear"></div>
    <div id="hinhthuc_footer">
        <p id="hinthuc_tab">Tiếp tục</p>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("#hinthuc_tab").click(function(){
                $ip_cuoi=jQuery("#ip_cuoi").val();
                $radiohinhthuc=jQuery("#radiohinhthuc").val();
                hoten=jQuery("#hoten").val();
                dienthoai=jQuery("#dienthoai").val();
                email=jQuery("#email").val(); 
                tinh=jQuery("#cmbtinh").val();
                quan=jQuery("#cmbquan").val();
                phuongxa=jQuery("#phuongxa").val();
                duongpho=jQuery("#duongpho").val();
                sonha=jQuery("#sonha").val();  
                if(hoten=='')
                {
                    alert('Bạn chưa nhập họ tên');
                    hoten.focus();
                    return false;
                }
                else if(dienthoai=='')
                {
                    alert('Bạn chưa nhập điện thoại');
                    dienthoai.focus();
                    return false;    
                } 
                else if(email.search('@')==-1)
                {
                    alert('Email không hợp lệ');
                    email.focus();
                    return false;    
                }
                else if(tinh==-1)
                {
                    alert('Bạn chưa chọn tỉnh thành');
                    return false;
                }
                else if(quan==-1)
                {
                    alert('Bạn chưa chọn quận huyện');
                    return false;
                }
                else if(phuongxa==='')
                {
                    alert('Bạn chưa nhập Phường/Xã');
                    phuongxa.focus();
                    return false;    
                }
                else if(duongpho==='')
                {
                    alert('Bạn chưa nhập đường phố');
                    duongpho.focus();
                    return false;    
                }
                else
                {
                    jQuery.ajax({
                        cache:false,                                
                        type : "POST",
                        data:{ip_cuoi : ip_cuoi,radiohinhthuc : radiohinhthuc,hoten : hoten,dienthoai : dienthoai,email : email,tinh : tinh,quan : quan,phuongxa : phuongxa,duongpho : duongpho,sonha : sonha},  
                        url : "<?php echo site_url('site/chuyentab2/'); ?>",
                        success:function(html){
                            jQuery("#listchuyentab2").html(html);                                                                         
                        }  
                    });    
                }
            });    
        });
    </script>
    <div class="clear"></div>
</div>
</div>