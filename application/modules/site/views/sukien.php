<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
$sukien_name=$CI->site_model->getitem('tblsukien',$sukien_id)->row()->name;
$sukiens=$CI->site_model->gettbl_sukien('tblsukien','',0,15);
?>
<div class="main-content">
<div class="box-sukien">
	<form name="frmsukien" action="<?php echo site_url('result-event.html'); ?>" method="post">
		<select class="sukien" name="txt_sukien">
			<?php foreach($sukiens->result() as $sukien){
			if($sukien->id==$sukien_id){
			?>
			<option value="<?php echo $sukien->id;?>" selected><?php echo $sukien->name;?></option>		
			<?php } else{?>
			<option value="<?php echo $sukien->id;?>"><?php echo $sukien->name;?></option>						
			<?php }} ?>
		</select>	
		<button class="button_w" type="submit">Thực hiện</button>
	</form>
</div>
<h3 class="sukien">Sự kiện: <?php echo $sukien_name; ?></h3>
<?php if($query->num_rows()>0){ ?>
<ul class="list-sukien">
	<?php 
	$kt=0;
	foreach($query->result() as $item){
	$kt++;
	$category='';
	$category=$CI->site_model->getcatlink($item->catid);		
	////////////////////////////////////
	if($item->thumb!=''){
		$thumb=explode('.',$item->thumb);
		$item_thumb=$thumb[0].'_160x150'.'.'.$thumb[1];
	}		
	else{
		$item_thumb='upload/no-img.png';
	}
	$cut_sapo = '';
	$cut_sapos=explode(' ',$item->sapo);				
	for($j=0;$j<count($cut_sapos);$j++){
		$cut_sapo .= $cut_sapos[$j].' ';
		if($j==35){
			$cut_sapo = $cut_sapo.'...';
			break;
		}
	}	
	?>
	<li>
		<img src="<?php echo $item_thumb; ?>" width="76">
		<a class="title" title="<?php echo $item->title; ?>" href="<?php echo site_url($category.'/'.$item->alias.'-'.$item->id).'.html';?>"><?php echo $item->title; ?></a>													
		<p class="sapo">
			<?php echo $cut_sapo; ?>
		</p>		
	</li>
	<?php } ?>
</ul>	
<?php } else{?>
<p class="warning">Tin tức đang được cập nhật</p>
<?php } ?>
</div>