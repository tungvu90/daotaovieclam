<div id="panel">
    <p id="panel_title">Trang cá nhân</p>
    <div id="panel_main">
        <ul id="panel_top">
            <li><a href="<?php echo site_url('trang-ca-nhan.html'); ?>" <?php if(isset($ctp)){ ?> style="background:#fff;" <?php } ?>>Sửa thông tin</a></li>
            <?php 
                if(isset($_SESSION['username']))
                {
                    $this->db->where('name',$_SESSION['username']);
                    $admin_pn=$this->db->get('tbladmin')->row();
                }
                $this->db->where('name',$admin_pn->id);
                $nhanhang=$this->db->get('tbldiachinhanhang')->row();
                if(count($nhanhang)==0)
                {
                ?>
                <li><a href="<?php echo site_url('site/diachinhanhang/'); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ nhận hàng">Địa chỉ nhận hàng</a></li>
                <?php
                }
                else
                {
                    //$nhanhang=$nhanhang->row();
                ?>
                <li><a href="<?php echo site_url('site/editnhanhang/'.$nhanhang->id); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ nhận hàng">Địa chỉ nhận hàng</a></li>
                <?php
                }
            ?>
            <li><a href="<?php echo site_url('theo-doi-don-hang.html'); ?>" title="Theo dõi đơn hàng" <?php if(isset($theodoi)){ ?> style="background:#fff;"<?php } ?>>Theo dõi đơn hàng</a></li>           
            <li><a href="<?php echo site_url('doi-mat-khau.html') ?>" <?php if(isset($doimk)){ ?> style="background:#fff;" <?php } ?>>Đổi mật khẩu</a></li>
        </ul>
    </div>
    <div id="panel_form" style="position:relative;">        
        <?php 
            if(isset($thanhcong))
            {
        ?>
        <div class="boxSuccess">
            Cập nhật địa chỉ thành công!
        </div>
        <?php 
        }
        if(isset($error_register))
        {
        ?>
        <div id="error_register">
			<fieldset style="text-align: left;background-color: #F5EFC9;">
    		<legend style="font-weight: bold; color:#F00" accesskey="Q">Thông báo hệ thống</legend>
				<?php echo $error_register;?>
			 </fieldset>
		 </div>
        <?php    
        }
        ?>
        <form name="frmdiachimore" method="post" action="<?php echo site_url('xu-ly-dia-chi-nhan-hang.html'); ?>" enctype="multipart/form-data">
            <?php 
    			if(isset($id))
    			{    			     
    				$this->db->where('id',$id);
    				$item=$this->db->get('tbldiachinhanhang')->row();                                                 
    			}
    		?>
            <table>
                <tr>
                    <th>Số nhà</th>
                    <td><input type="text" name="sonha" value="<?php if(isset($id)){ echo $item->sonha;}; ?>" />
                    <input type="hidden" id="id" name="id" value="<?php if(isset($id)){ echo $item->id;} ?>" />
                    </td>
                </tr> 
                <tr>
                    <th>Đường/Phố</th>
                    <td><input type="text" name="duongpho" value="<?php if(isset($id)){ echo $item->duongpho;}; ?>" /></td>
                </tr> 
                <tr>
                    <th>Phường/Xã</th>
                    <td><input type="text" name="phuongxa" value="<?php if(isset($id)){ echo $item->phuongxa;}; ?>" /></td>
                </tr>   
                <tr>
                    <th>Tỉnh/Thành Phố</th>
                    <td>
                    <select name="tinhhome" id="cmbtinh">
                        <option value="-1">--Chọn tỉnh thành--</option>
                        <?php 
                            $this->db->select('id,tinh');
                            $sqltinhdh=$this->db->get('tbltinh');
                            if($sqltinhdh->num_rows()>0)
                            {
                                foreach($sqltinhdh->result() as $itemtinhdh)
                                {                                    
                                    if(isset($id))
                                    {
                                        if($item->tinh==$itemtinhdh->id)
                                        {
                                        ?>
                                        <option value="<?php echo $itemtinhdh->id; ?>" selected="selected"><?php echo $itemtinhdh->tinh; ?></option>
                                        <?php    
                                        }   
                                        else
                                        {
                                        ?>
                                        <option value="<?php echo $itemtinhdh->id; ?>"><?php echo $itemtinhdh->tinh; ?></option>
                                        <?php    
                                        } 
                                    }
                                    else
                                    {
                                    ?>
                                    <option value="<?php echo $itemtinhdh->id; ?>"><?php echo $itemtinhdh->tinh; ?></option>
                                    <?php 
                                    }   
                                }
                                $sqltinhdh->free_result();
                            }
                        ?>
                    </select>
                    </td>
                </tr>  
                <tr>
                    <th>Quận Huyện</th>
                    <td>
                        <select name="quan" id="cmbquan">
                            <option value="-1">--Chọn quận huyện--</option>    
                            <?php 
                                if(isset($id))
                                {
                                    $this->db->select('id,quan');
                                    $sqlquandh=$this->db->get('tblquan');
                                    if($sqlquandh->num_rows()>0)    
                                    {
                                        foreach($sqlquandh->result() as $itemquandh)
                                        {
                                            if($item->quan==$itemquandh->id)
                                            {
                                            ?>
                                            <option value="<?php echo $itemquandh->id; ?>" selected="selected"><?php echo $itemquandh->quan; ?></option>
                                            <?php    
                                            }
                                            else
                                            {
                                            ?>
                                            <option value="<?php echo $itemquandh->id; ?>"><?php echo $itemquandh->quan; ?></option>
                                            <?php    
                                            } 
                                        }
                                        $sqlquandh->free_result();   
                                    }
                                }
                            ?>
                        </select>
                    </td>
                </tr>                      
                <tr>
                    <th></th>
                    <td><input type="submit" name="submit" value="Lưu thay đổi" /></td>
                </tr>
            </table>
            <script language="javascript">
 			    jQuery(document).ready(function() {
				    jQuery('#cmbtinh').change(function() {
   					    giatri = this.value;
       					jQuery('#cmbquan').load('<?php echo site_url().'/site/loadcate2/'; ?>' + giatri);                        
       	 			});
     			});
      		</script>
        </form>
    </div>
</div>