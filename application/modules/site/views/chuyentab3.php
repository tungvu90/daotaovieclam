<div id="listchuyentab3">
<div class="popup-header1" style="background:url(images/xac_nhan_don_hang.png) top left no-repeat !important;">
    <a class="close_popup1" href="javascript:void(0)"></a>
</div>
<div id="hinhthuc_main">
    <div id="listchuyentab4">
    <?php 
    if(isset($_SESSION['username']))
    {
    ?>
    <div class="xacnhan4">
        <div class="thongtinkh">
            <?php                 
                    $this->db->where('name',$_SESSION['username']);
                    $this->db->select('id,fullname,email,dienthoai');
                    $sqlthanhvien4=$this->db->get('tbladmin')->row();
                    $this->db->where('name',$sqlthanhvien4->id);
                    $sqldiachinhanhang4=$this->db->get('tbldiachinhanhang')->row();
        
            ?>
            <p><strong>Thông tin khách hàng</strong></p>
            <p>Họ tên:<span><?php echo $sqlthanhvien4->fullname; ?></span></p>
            <p>Email:<span><?php echo $sqlthanhvien4->email; ?></span></p>
            <p>Điện thoại:<span><?php echo $sqlthanhvien4->dienthoai; ?></span></p>
            <p>Địa chỉ:<span><?php echo $sqldiachinhanhang4->sonha; ?>-<?php echo $sqldiachinhanhang4->duongpho; ?>-<?php echo $sqldiachinhanhang4->phuongxa; ?>
            -<?php 
                $this->db->where('id',$sqldiachinhanhang4->quan);
                $sqlquan4=$this->db->get('tblquan')->row();
                echo $sqlquan4->quan;
            ?>
            -
            <?php 
                $this->db->where('id',$sqldiachinhanhang4->tinh);
                $sqltinh4=$this->db->get('tbltinh')->row();
                echo $sqltinh4->tinh;
            ?>
            </span></p>
        </div>
        <div class="hinhthucthanhtoan4">
            <p>Hình thức thanh toán&nbsp;
            <span>
            <?php 
                if($radiohinhthuc=='cod')
                {
                    echo 'Thu tiền tận nới';
                }
                else
                {
                    echo 'Thanh toán online';
                }
            ?>
            </span>
            </p>
        </div>
        <div class="hinhthucthanhtoan4">
            <p>Hình thức nhận hàng&nbsp;
            <span>Buonchung gửi mã số phiếu qua email</span>
            </p>
        </div>
        <div class="hinhthucthanhtoan4">
            <p>Thông tin sản phẩm</p>
            <table id="thongtinsp4">
                <tr>
                    <th>Sản phẩm</th>
                    <th>Ảnh</th>                    
                    <th>Số lượng</th>
                    <th>Giá bán</th>                    
                </tr>
                <?php 
                if(isset($ip_cuoi))
                {
                    $this->db->where('id',$ip_cuoi);
                    $this->db->select('title,thumb,gia,giakm,donvitinh');
                    $sqlsanphamdh4=$this->db->get('tblsanpham');
                    if($sqlsanphamdh4->num_rows()>0)
                    {
                        $sqlsanphamdh4=$sqlsanphamdh4->row();
                        ?>
                        <tr>
                            <td><?php echo $sqlsanphamdh4->title; ?></td>
                            <td><img src="<?php echo $sqlsanphamdh4->thumb; ?>" title="<?php echo $sqlsanphamdh4->title; ?>" alt="<?php echo $sqlsanphamdh4->title; ?>" width="50px" /></td>                            
                            <td>
                                <input type="text" name="soluongxn" id="soluongxn" value="1" />
                            </td>
                            <td>
                            <input type="hidden" name="radiohinhthuc" id="radiohinhthuc" value="<?php echo $radiohinhthuc; ?>" />
                            <input type="hidden" name="ip_cuoi" id="ip_cuoi" value="<?php echo $ip_cuoi; ?>" />                    
                            <input type="hidden" name="yeucauthem" id="yeucauthem" value="<?php echo $yeucauthem; ?>" />
                            <span>
                            <?php 
                                if($sqlsanphamdh4->giakm!=0)
                                {
                                    echo number_format($sqlsanphamdh4->giakm,0,'.','.').'&nbsp;'.$sqlsanphamdh4->donvitinh;    
                                }
                                else
                                {
                                    echo number_format($sqlsanphamdh4->gia,0,'.','.').'&nbsp;'.$sqlsanphamdh4->donvitinh;    
                                }
                            ?>
                            </span>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>                
            </table>
            <div class="clear">
            <div id="thanhtoan_visa">
                <img src="images/icon_dt5.png" title="Đặt mua" alt="Đặt mua" />
                <div class="loaduser" style="padding-top:10px;"></div>
            </div>
            <script type="text/javascript">
                jQuery(document).ready(function(){
                    jQuery("#thanhtoan_visa").click(function(){
                        ip_cuoi=jQuery("#ip_cuoi").val();     
                        radiohinhthuc=jQuery("#radiohinhthuc").val();
                        yeucauthem=jQuery("#yeucauthem").val();  
                        soluongxn=jQuery("#soluongxn").val(); 
                        jQuery(".loaduser").html('<img src="images/loader.gif" alt="Uploading......."/>');
                        jQuery.ajax({
                            cache:false,                                
                            type : "POST",
                            data:{ip_cuoi : ip_cuoi,radiohinhthuc : radiohinhthuc,yeucauthem : yeucauthem,soluongxn : soluongxn},  
                            url : "<?php echo site_url('site/chuyentab4/'); ?>",
                            success:function(html){
                                jQuery(".loaduser").html('');
                                jQuery("#listchuyentab4").html(html);                                                                         
                            }  
                        });
                    });    
                });
            </script>
            </div>
        </div>
    </div>
    <?php 
    }
    ?>
    </div>
</div>
</div>