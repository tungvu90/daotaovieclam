<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
?>
<div id="panel">
    <p id="panel_title">Danh sách tin đăng</p>
    <div id="panel_main">
        <ul id="panel_top">
            <li><a href="<?php echo site_url('trang-ca-nhan.html'); ?>" <?php if(isset($ctp)){ ?> style="background:#fff;" <?php } ?>>Sửa thông tin</a></li>
            <?php 
                if(isset($_SESSION['username']))
                {
                    $this->db->where('name',$_SESSION['username']);
                    $admin_pn=$this->db->get('tbladmin')->row();
                }
                $this->db->where('name',$admin_pn->id);
                $nhanhang=$this->db->get('tbldiachinhanhang')->row();
                if(count($nhanhang)==0)
                {
                ?>
                <li><a href="<?php echo site_url('site/diachinhanhang/'); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ nhận hàng">Địa chỉ nhận hàng</a></li>
                <?php
                }
                else
                {            
                ?>
                <li><a href="<?php echo site_url('site/editnhanhang/'.$nhanhang->id); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ nhận hàng">Địa chỉ nhận hàng</a></li>
                <?php
                }
            ?>
            <li><a href="<?php echo site_url('theo-doi-don-hang.html'); ?>" title="Theo dõi đơn hàng" <?php if(isset($theodoi)){ ?> style="background:#fff;"<?php } ?>>Theo dõi đơn hàng</a></li>
            <li><a href="<?php echo site_url('doi-mat-khau.html') ?>" <?php if(isset($doimk)){ ?> style="background:#fff;" <?php } ?>>Đổi mật khẩu</a></li>
        </ul>
    </div>
    <div id="panel_form">        
        <table id="listsanpham">
            <tr>
                <th>Mã đơn hàng</th>
                <th>Hình thức</th>
                <th>Sản phẩm</th>
                <th>Ngày mua</th>
                <th>Số lượng</th>
                <th>Thành tiền</th>                
                <th>Trạng thái</th>                
            </tr>
            <?php 
                if($query->num_rows() >0)
                {
                    $dem=1;
                    foreach($query->result() as $itemquery)
                    {
                    ?>
                    <tr>                        
                        <td valign="top" style="10%"><?php echo $itemquery->maphieu; ?></td>
                        <td valign="top"><?php echo $itemquery->hinhthuc; ?></td>
                        <td valign="top"><?php
                        $this->db->where('id',$itemquery->sanpham);
                        $sqlspdh=$this->db->get('tblsanpham')->row();
                        echo $sqlspdh->title; ?></td>
                        <td valign="top"><?php 
                        $datadh=explode('-',$itemquery->ngaydang);
                        echo $datadh[2].'-'.$datadh[1].'-'.$datadh[0]; ?></td>
                        <td valign="top"><?php echo $itemquery->soluong; ?></td> 
                        <td><span style="color:red;font-weight:bold;font-size:18px;"><?php 
                        if($sqlspdh->giakm!='')
                        {
                            echo number_format($itemquery->soluong*$sqlspdh->giakm,0,'.','.').'&nbsp;'.$sqlspdh->donvitinh; 
                        }
                        else
                        {
                            echo number_format($itemquery->soluong*$sqlspdh->gia,0,'.','.').'&nbsp;'.$sqlspdh->donvitinh;
                        }
                        ?></span></td>
                        <td>
                        <?php 
                            if($itemquery->status=='')
                            {
                                echo 'Chưa thanh toán';
                            }
                            else
                            {
                                echo 'Đã thanh toán';
                            }
                        ?>
                        </td>                       
                    </tr>
                    <?php 
                    $dem++;   
                    }
                    ?>
                    <tr>
                <td colspan="11"><p><?php echo $pagination; ?></p></td>
            </tr>
                    <?php
                }
            ?>                        
        </table>    
    </div>
</div>