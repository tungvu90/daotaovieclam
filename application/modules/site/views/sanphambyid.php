<link href="css/skin.css" rel="stylesheet" type="text/css" />
<link href="css/skin-ie7.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.elevateZoom-2.2.3.min.js"></script> 
<script type="text/javascript" src="js/jquery.jcarousel.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {     
    	jQuery('.first-and-second-carousel').jcarousel({
    	   auto:0,
           scroll:1,
           animation:'slow',
            wrap: 'circular',
            animation:1000,                 
    	}); 
        jQuery('.jcarousel-skin-ie7').jcarousel({
             auto:0,
           scroll:1,
           animation:'slow',
            wrap: 'circular',
            animation:2000,       
        });              	             	        
    });                      
</script>
<?php
if(isset($city))
{
	$city=$city;
}
else
{
	if(isset($_COOKIE['city']))
	{
		$city=$_COOKIE['city'];
	}
} 
$CI=&get_instance();
$CI->load->model('site/site_model');
if(isset($chitiet))
{
    $this->db->where('id',$chitiet);
    $sqlchitiettin=$this->db->get('tblsanpham');
    if($sqlchitiettin->num_rows()>0)
    {
        $chitiettin=$sqlchitiettin->row();
        $this->db->where('id',$chitiettin->catid);
        $sqldanhmuctinct=$this->db->get('tblchuyenmucsp');
        if($sqldanhmuctinct->num_rows()>0)
        {
            $danhmuctinct=$sqldanhmuctinct->row();
            $category_tinct='';
            $category_tinct=$CI->site_model->getcatlink($danhmuctinct->id);
        ?>
        <ul class="menubreakcumb">
            <li><a href="<?php echo base_url(); ?>" title="Trang chủ">Trang chủ</a></li>
            <li><a href="<?php echo site_url($category_tinct.'-c'.$danhmuctinct->id.'.html'); ?>" title="<?php echo $danhmuctinct->name; ?>"><?php echo $danhmuctinct->name; ?></a></li>
            <li style="background: none;"><span><?php echo $chitiettin->title; ?></span></li>
            <div class="clear"></div>
        </ul>
        <?php 
        }
    }
    ?>
    <div id="sanpham_id">
        <div id="sanpham_id_left">
            <img id="zoom_03" src="<?php echo $chitiettin->image; ?>" data-zoom-image="<?php echo $chitiettin->image; ?>" width="480"  />
            <div id="gallery_01" style="width:480px;float:left;">
                <ul id="first-carousel" class="first-and-second-carousel jcarousel-skin-tango">
                    <li><a href="tester slide-content" class="elevatezoom-gallery" data-image="<?php echo $chitiettin->image; ?>" data-zoom-image="<?php echo $chitiettin->image; ?>"><img src="<?php echo $chitiettin->image; ?>" width="64px" height="64px" /></a></li>
                    <?php 
                    if(!empty($chitiettin->image1)){
                        $images1 =json_decode($chitiettin->image1); 
                        if(is_array($images1) && count($images1)>0){
                            foreach ($images1 as $image1) {
                            ?>
                            <li><a href="tester slide-content" class="elevatezoom-gallery" data-image="<?php echo  base_url().'tmp'.'/' . $image1; ?>" data-zoom-image="<?php echo  base_url().'tmp'.'/' . $image1; ?>"><img src="<?php echo  base_url().'tmp'.'/' . $image1; ?>" width="64px" height="64px" /></a></li>
                            <?php 
                            }
                        }
                    }
                    ?>
                </ul>
            </div>
            <script type="text/javascript">
$(document).ready(function () {
$("#zoom_03").elevateZoom({gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: "active", imageCrossfade: true, loadingIcon: "http://www.elevateweb.co.uk/spinner.gif"}); 

$("#zoom_03").bind("click", function(e) {  
  var ez =   $('#zoom_03').data('elevateZoom');
  ez.closeAll(); //NEW: This function force hides the lens, tint and window	
	$.fancybox(ez.getGalleryList());
  return false;
}); 

}); 

</script>                                   

        </div>
        <div id="sanpham_id_right">
            <div id="chitiet_name_id">
                <h1><?php echo $chitiettin->title; ?></h1>
            </div>
            <p id="chitiet_id_ma">Mã sản phẩm:&nbsp;<?php echo $chitiettin->id; ?></p>
            <p id="giagoc_id"<?php if($chitiettin->giakm==0){ ?>style="text-decoration: none !important;" <?php } ?>>
            <?php 
            if($chitiettin->gia==0)
            {
                echo 'Giá:&nbsp;Liên hệ';     
            }
            else
            {
                echo number_format($chitiettin->gia,0,'.','.').'&nbsp;'.$chitiettin->donvitinh; 
            }
            ?></p>
            <?php 
            if($chitiettin->giakm!=0)
            {
            ?>
            <p id="giakm_id"><?php echo number_format($chitiettin->giakm,0,'.','.').'&nbsp;'.$chitiettin->donvitinh; ?></p>
            <?php 
            }
            ?>
            <div id="soluong_id">
                <p id="soluong_id_l">Số lượng</p>
                <p id="soluong_select">
                    <select name="soluong_i">
                        <?php 
                            $i=1;
                            for($i=1;$i<26;$i++)
                            {
                                ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php
                                $i++;
                            }
                        ?>
                    </select>
                </p>
                <div class="clear"></div>    
            </div>
            <div id="muahang">  
                <link href="css/popupct.css" rel="stylesheet" type="text/css" />
                <script type="text/javascript" src="js/jquery.popupMiendatweb.min.js"></script>
                <script type="text/javascript" language="JavaScript">
	$(function(){
		/* khởi tạo popup */
        $('input[rel*=buonchungPopup]').showPopup({ 
        	top : 200, //khoảng cách popup cách so với phía trên
        	closeButton: ".close_popup" , //khai báo nút close cho popup
			scroll : false, //cho phép scroll khi mở popup, mặc định là không cho phép
        	onClose:function(){            	
        		//sự kiện cho phép gọi sau khi đóng popup, cho phép chúng ta gọi 1 số sự kiện khi đóng popup, bạn có thể để null ở đây
        	}
        });	
	});
    $(function(){
		/* khởi tạo popup */
        $('input[rel*=buonchungorder]').showPopup({ 
        	top : 200, //khoảng cách popup cách so với phía trên
        	closeButton: ".close_popup1" , //khai báo nút close cho popup
			scroll : false, //cho phép scroll khi mở popup, mặc định là không cho phép
        	onClose:function(){            	
        		//sự kiện cho phép gọi sau khi đóng popup, cho phép chúng ta gọi 1 số sự kiện khi đóng popup, bạn có thể để null ở đây
        	}
        });	
	});
</script> 
                <?php 
                    $data['ip_cuoi']=$chitiettin->id;
                    $this->load->view('order_visa',$data); 
                ?>
                <div id="muahang_right">
                    <input type="button" id="open_popup" name="open_popup" rel="buonchungPopup" href="#popup_content" value="Đến cửa hàng mua"/>                                        
                </div>  
<div id="popup_content" class="popup">
	<div class="popup-header">
	   <a class="close_popup" href="javascript:void(0)"></a>	
	</div>
	<div class="info_popup">    
		<div id="nhapsodt">
            <p><strong>Nhập số điện thoại</strong></p>
            <p>Được dùng để truy xuất đơn hàng của bạn tại cửa hàng</p>
            <?php
                if(isset($_SESSION['username']))
                {
                    $this->db->where('name',$_SESSION['username']);  
                    $this->db->select('id,dienthoai');
                    $sqldienthoaiauthor=$this->db->get('tbladmin')->row();  
                    $dienthoaiauthor=$sqldienthoaiauthor->dienthoai;
                }    
                elseif(isset($_COOKIE['user']))
                {
                    $this->db->where('name',$_COOKIE['user']);  
                    $this->db->select('id,dienthoai');
                    $sqldienthoaiauthor=$this->db->get('tbladmin')->row();  
                    $dienthoaiauthor=$sqldienthoaiauthor->dienthoai;
                } 
                else
                {
                    $dienthoaiauthor='';    
                }                               
                
            ?>
            <input type="text" name="sodt" id="sodt" value="<?php echo $dienthoaiauthor; ?>" />
        </div>
        <div id="diachi_more">
            <?php                 
                $this->db->select('id,title,diachi,thutu,status');
                $sqlcoso=$this->db->get('tbldiachi');
                if($sqlcoso->num_rows()>0)
                {
                    foreach($sqlcoso->result() as $itemcoso)
                    {
            ?>
            <div class="diachi_radio_item">
                <div class="diachi_radio_item_l">
                    <input type="radio" name="radioship" value="<?php echo $itemcoso->id; ?>" />
                </div>
                <div class="diachi_radio_item_r">
                    <label><?php echo $itemcoso->title; ?></label>
                    <span><?php echo $itemcoso->diachi; ?></span>
                </div>
                <div class="clear"></div>
            </div>   
            <?php 
                }
                $sqlcoso->free_result();
            }
            ?>         
        </div>
        <div id="donhang_cty">
            <div id="donhang_top">
                <p id="donhang_img"><img title="<?php echo $chitiettin->title; ?>" alt="<?php echo $chitiettin->title; ?>" src="<?php echo $chitiettin->thumb; ?>"/></p>
                <p id="donhang_name"><?php echo $chitiettin->title; ?> với 
                <?php 
                if($chitiettin->giakm!=0)
                {
                    echo number_format($chitiettin->giakm,0,'.','.').'&nbsp;'.$chitiettin->donvitinh; 
                }
                elseif($chitiettin->giakm==0)
                {
                    if($chitiettin->gia!=0)
                    {
                        echo number_format($chitiettin->gia,0,'.','.').'&nbsp;'.$chitiettin->donvitinh;
                    }    
                }                
                ?></p>
            </div>
            <p id="donhang_gia">Giá:&nbsp;<span>
            <?php 
                if($chitiettin->giakm!=0)
                {
                    echo number_format($chitiettin->giakm,0,'.','.').'&nbsp;'.$chitiettin->donvitinh; 
                }
                elseif($chitiettin->giakm==0)
                {
                    if($chitiettin->gia==0)
                    {
                        echo 'Liên hệ';
                    }
                    else
                    {
                        echo number_format($chitiettin->gia,0,'.','.').'&nbsp;'.$chitiettin->donvitinh;
                    }    
                }                
                ?>
            </span></p>
            <p id="donhang_sl">
                Số lượng:
                <span>
                    <select name="soluong" id="soluong">
                        <?php 
                            $j=1;
                            for($j=1;$j<26;$j++)
                            {
                                ?>
                                <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                                <?php
                                $j++;    
                            }
                        ?>
                    </select>
                </span>
            </p>
            <p id="dongia_total">Tổng tiền:<span>
            <?php 
                if($chitiettin->giakm!=0)
                {
                    echo number_format($chitiettin->giakm,0,'.','.').'&nbsp;'.$chitiettin->donvitinh; 
                }
                elseif($chitiettin->giakm==0)
                {
                    if($chitiettin->gia==0)
                    {
                        echo 'Liên hệ';
                    }
                    else
                    {
                        echo number_format($chitiettin->gia,0,'.','.').'&nbsp;'.$chitiettin->donvitinh;
                    }    
                }                
                ?>
            </span></p>
            <p id="dongia_datmua"><a id="dathangtaicty" title="Đặt mua cho <?php echo $chitiettin->title; ?>"><img src="images/icon_dm.png" /></a></p>
            <input type="hidden" name="id_cp" id="id_cp" value="<?php echo $chitiettin->id; ?>" />
            <script type="text/javascript">
                jQuery(document).ready(function(){
                    jQuery("#dathangtaicty").click(function(){
                         sodt=jQuery("#sodt").val();                           
                         radioship=jQuery('input[name=radioship]:checked').val(); 
                         soluong=jQuery("#soluong").val();
                         id_cp=jQuery("#id_cp").val();
                         if(sodt=='')
                         {
                            alert('Điện thoại không để trống');
                            $("#sodt").focus();
			                 return false;
                         }  
                         else if(!radioship)
                         {
                            alert('Bạn chua chon cửa hàng');
                            return false; 
                         }                                             
                         else
                         {
                            jQuery.ajax({
                                cache:false,                                
			                    type : "POST",						
							    data:{sodt : sodt,radioship : radioship,soluong : soluong,id_cp : id_cp},
                                url : "<?php echo site_url('site/checkdonhangcp/'); ?>",
                                success:function(html){
                                    alert('Bạn đặt hàng thành công');
                                    window.location.reload();                                                                         
                                }
                            });   
                         }
                    });
                });
            </script>
            <div class="clear"></div>
        </div>      
        <div class="clear"></div>
	</div>
<div class="clear"></div>
</div>

                <div class="clear"></div>
            </div>
            <div id="call_me">
                <?php                     
                    $this->db->where('id',$chitiettin->nguoidang);                    
                    $this->db->select('id,dienthoai');
                    $sqlcalauthor=$this->db->get('tbladmin');
                    if($sqlcalauthor->num_rows()>0)
                    {
                        $callauthor=$sqlcalauthor->row();
                        $authordt=$callauthor->dienthoai;
                    }
                    else
                    {
                        $authordt='';    
                    }
                ?>
                <p>Gọi đặt hàng ngay:<span><?php echo $authordt; ?><label>(Mã sp:&nbsp;<?php echo $chitiettin->id; ?>)</label></span></p>
            </div>
            <div id="view_id">
                <?php 
                        $itemct=explode('-',$chitiettin->ngayhethan);
				        $namct=$itemct['0'];
				        $thangct=$itemct['1'];
				        $ngayct=$itemct['2'];
                    ?>
                    <script type="text/javascript">
                        $(function () {
                            var austDay = new Date();                                
                            var austDay = new Date();
                            austDay = new Date(<?php echo $namct?>, <?php echo $thangct;?> - 1, <?php echo $ngayct;?>);
                            $('#defaultCountdownct').countdown({until: austDay, format: 'HMS'});
                        });
                    </script>
                <p style="float: left;">Đã mua:&nbsp;<strong><?php echo $chitiettin->luotmua; ?></strong><span>|</span>Lượt xem:&nbsp;<?php echo $chitiettin->view; ?><span>|</span></p>
                <?php 
                    $dateluict=date("Y-m-d");
                    if(strtotime($chitiettin->ngayhethan) < strtotime($dateluict))
                    {
                    ?>
                    <p class="chitiet_id_lock">Vẫn còn mua được hàng</p>   
                    <?php    
                    }
                    else
                    {
                    ?>
                    <div style="float: left;color:#333;" id="defaultCountdownct"></div>   
                    <?php 
                    }
                ?>
                <div class="clear"></div>
                <div class="fb-like" data-href="<?php echo site_url($chitiettin->alias.'-'.$chitiettin->id).'.html';?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
        <div class="diem_ct">
            <div class="diem_ct_l">
                <h2>Điểm nổi bật</h2>
                <div class="diem_ct_p">
                    <?php echo $chitiettin->diemnoibat; ?>
                </div>
            </div>
            <div class="diem_ct_l" style="margin-right:0;">
                <h2>Điều kiện sử dụng</h2> 
                <div class="diem_ct_p">
                    <?php echo $chitiettin->dieukiensudung; ?>
                </div>
            </div>
            <div class="clear"></div>
        </div><!--End .diemct-->
        <div id="cungmuc">
            <div id="cungmuc_top">
                <p id="cungmuc_l">Có thể bạn thích</p>
                <p id="cungmuc_vien"></p>
            </div>
            <div id="cungmuc_main">
                <?php
                    if($chitiettin->catid!='' || $chitiettin->catid!=0)
                    {
                    $this->db->where('status',1); 
                    $this->db->where('id !=',$chitiettin->id);                
                    $this->db->where('catid',$chitiettin->catid);
                    $this->db->order_by('thutu','desc');
                    $this->db->order_by('id','desc');
                    $this->db->select('id,title,alias,catid,gia,giakm,donvitinh,thumb,thutu,status');
                    $sanphamlike=$this->db->get('tblsanpham');
                    if($sanphamlike->num_rows() >0)
                    {
                    ?>
                    <ul id="second-carousel" class="first-and-second-carousel jcarousel-skin-ie7">
                        <?php 
                            foreach($sanphamlike->result() as $itemsanphamlike)
                            {
                            ?>
                            <li>
                                <div class="sanphamlike_item">
                                    <a href="<?php echo site_url($itemsanphamlike->alias.'-'.$itemsanphamlike->id).'.html';?>" class="sanphamlike_item_img" title="<?php echo $itemsanphamlike->title; ?>"><img src="<?php echo $itemsanphamlike->thumb; ?>" title="<?php echo $itemsanphamlike->title; ?>" alt="<?php echo $itemsanphamlike->title; ?>" /></a>
                                    <a href="<?php echo site_url($itemsanphamlike->alias.'-'.$itemsanphamlike->id).'.html';?>" class="sanphamlike_item_name" title="<?php echo $itemsanphamlike->title; ?>"><?php echo $itemsanphamlike->title; ?></a>
                                    <p class="sanphamlike_item_giakm"><?php echo number_format($itemsanphamlike->giakm,0,'.','.').'&nbsp;'.$itemsanphamlike->donvitinh; ?></p>
                                    <p class="sanphamlike_item_gia"><?php echo number_format($itemsanphamlike->gia,0,'.','.').'&nbsp;'.$itemsanphamlike->donvitinh; ?></p>
                                    <div class="clear"></div>
                                </div>
                            </li>
                            <?php 
                            }
                            $sanphamlike->free_result();
                        ?>
                    </ul>
                    <?php 
                    }
                }
                ?>
            </div>
            <div class="clear"></div>
        </div><!--End #cungmuc-->
        <div id="thongtin_detail">
            <div id="thongtin_left">
                <script type="text/javascript">
                    $(document).ready(function(){
                        $('.danhmuc_item:first').css('display','block'); 
                        $('.fa_tab').click(function(){
                            var name=$(this).attr('title');                            
                            $('.danhmuc_item').css('display','none');                            
                             $('#'+name).css('display','block');
                             $('.fa_tab').removeClass('active');
                             $(this).addClass('active');    
                        });                             
                    });
                </script>
                <div id="tab_new">
                    <div id="tab_top">
                        <ul class="ul_tab_news" class="tab">
                            <li><a class="fa_tab active" title="item_1">Thông tin chi tiết</a></li>
                            <li><a class="fa_tab" title="item_2">Bình luận</a></li>                                                                                  
                        </ul>
                    </div>
                    <div id="tab_main">
                        <div id="item_1" class="danhmuc_item" style="display:none;">  
                            <div id="noidung_id">
                                <?php echo $chitiettin->noidung; ?>
                            </div>                          
                            <div class="clear"></div>
                        </div>
                        <div id="item_2" class="danhmuc_item" style="display:none;"> 
                        <br />                           
                            <div id="fb-root"></div>
                		      <script>(function(d, s, id) {
                		          var js, fjs = d.getElementsByTagName(s)[0];
                                  if (d.getElementById(id)) return;
                                  js = d.createElement(s); js.id = id;
                                  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                                  fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                            <html xmlns:fb="http://ogp.me/ns/fb#">
                                <fb:comments href="<?php echo site_url($chitiettin->alias.'-'.$chitiettin->id).'.html';?>" width="800" num_posts="10" ></fb:comments>
                            </html> 
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>                
            </div>
            <div id="thongtin_right">
                <div class="deal_right">
                    <div class="deal_right_top">
                        <h2>Deal mới</h2>
                    </div>
                    <div class="deal_right_main">
                        <?php 
                            $this->db->where('status',1);
                            $this->db->where('tinh',$city);
                            $this->db->where('ngaydang >',date('Y-m-d', strtotime('-7 days')));
                            $this->db->order_by('thutu','desc');
                            $this->db->order_by('id','desc');
                            $this->db->select('id,title,alias,tinh,thumb,gia,giakm,donvitinh,ngaydang,thutu,status');
                            $this->db->limit(5);
                            $sqlxuhuong=$this->db->get('tblsanpham');
                            if($sqlxuhuong->num_rows() >0)
                            {
                                $demxuhuong=1;
                                foreach($sqlxuhuong->result() as $itemxuhuong)
                                {
                                    $cate_xuhuong=$sqlxuhuong->num_rows();
                                ?>            
                                <div class="xuhuong_item" <?php if($demxuhuong==$cate_xuhuong){ ?>style="border-bottom: none;"<?php } ?>>
                                    <a href="<?php echo site_url($itemxuhuong->alias.'-'.$itemxuhuong->id).'.html';?>" class="xuhuong_item_img"><img title="<?php echo $itemxuhuong->title; ?>" alt="<?php echo $itemxuhuong->title; ?>" src="<?php echo $itemxuhuong->thumb; ?>" /></a>
                                    <p class="xuhuong_item_name">
                                        <span><?php echo $demxuhuong; ?>.</span>
                                        <a href="<?php echo site_url($itemxuhuong->alias.'-'.$itemxuhuong->id).'.html';?>" title="<?php echo $itemxuhuong->title; ?>"><?php echo $itemxuhuong->title; ?></a>
                                        <div class="clear"></div>
                                    </p>
                                    <p class="xuhuong_gia">
                                    <?php 
                                        if($itemxuhuong->giakm!='')
                                        {
                                        ?>
                                        <?php echo number_format($itemxuhuong->giakm,0,'.','.').'&nbsp;'.$itemxuhuong->donvitinh; ?><span><?php echo number_format($itemxuhuong->gia,0,'.','.').'&nbsp;'.$itemxuhuong->donvitinh; ?></span>
                                        <?php 
                                        }
                                        else
                                        {
                                            echo number_format($itemxuhuong->gia,0,'.','.').'&nbsp;'.$itemxuhuong->donvitinh;    
                                        }
                                    ?>
                                    </p>
                                    <div class="clear"></div>
                                </div>
                                <?php 
                                $demxuhuong++;
                                }
                                $sqlxuhuong->free_result();
                            }
                        ?>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <?php   
}
?>