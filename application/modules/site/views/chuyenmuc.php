<?php 
$CI=&get_instance();
$CI->load->model('site/site_model');
?>
<div id="left">
    <?php $this->load->view('includes/left') ?>
    <div class="clear"></div>
</div>
<div id="right">
    <div class="box_right">
        <div class="box_right_top">
            <?php 
                if(isset($chuyenmuc))
                {
                    $this->db->where('id',$chuyenmuc);
                    $this->db->select('id,name,theh');
                    $sqlchuyenmuc=$this->db->get('tblchuyenmuc')->row();
                    $categorycm='';
                    $categorycm=$CI->site_model->getcatlink($sqlchuyenmuc->id);
                    ?>
                    <a href="<?php echo site_url($categorycm.'-c'.$sqlchuyenmuc->id.'.html'); ?>" title="<?php echo $sqlchuyenmuc->name; ?>"><?php echo $sqlchuyenmuc->name; ?></a>
                    <?php
                }
            ?>        
        </div>
        <div class="box_right_main">
            <ul class="menubreakcumb">
                <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                <?php 
                    if(isset($chuyenmuc))
                    {
                    ?>
                    <li style="background:none;"><span><?php echo $sqlchuyenmuc->name; ?></span></li>
                    <?php 
                    }
                ?>
                <div class="clear"></div>
            </ul>
            <?php 
                if($query->num_rows()>0)
                {
                    foreach($query->result() as $itemtintucall)
                    {
                        $categoryd='';
                        $categoryd=$CI->site_model->getcatlink($itemtintucall->catid);
                    ?>
                    <div class="tintuc_all">
                        <a href="<?php echo site_url($itemtintucall->alias.'-'.$itemtintucall->id.'.html') ?>" class="tintuc_all_img"><img title="<?php echo $itemtintucall->title; ?>" alt="<?php echo $itemtintucall->title; ?>" src="<?php echo $itemtintucall->thumb; ?>" /></a>
                        <a href="<?php echo site_url($itemtintucall->alias.'-'.$itemtintucall->id.'.html') ?>" class="tintuc_all_name"><?php echo $itemtintucall->title; ?></a>
                        <p><?php echo catchuoi($itemtintucall->mota,70); ?></p>                    
                        <div class="clear"></div>
                    </div>
                    <?php    
                    }
?>
<div class="clear"></div>
<div class="pagination">
                <?php echo $pagination; ?>
            </div>
<?php
                }
                else
                {
                ?>
                <p style="text-align:center;font-size:12px;">Dữ liệu đang cập nhật</p>
                <?php    
                }
            ?>              
            <div class="clear"></div>
            <div id="theh">
                <p style="font-size:18px !important;line-height:35px;border-bottom:1px solid #ddd;float:unset;">Các từ khóa</p>
                <div style="padding-top:5px;">
                <?php 
                    if(isset($chuyenmuc))
                    {
                        echo $sqlchuyenmuc->theh;
                    }
                ?>
                <div class="clear"></div>
                </div>
            </div>      
        </div>
    </div>
    <div class="clear"></div>
</div>