<div id="panel">
    <p id="panel_title">Trang cá nhân</p>
    <div id="panel_main">
        <ul id="panel_top">
            <li><a href="<?php echo site_url('trang-ca-nhan.html'); ?>" <?php if(isset($ctp)){ ?> style="background:#fff;" <?php } ?>>Sửa thông tin</a></li>
            <?php 
                if(isset($_SESSION['username']))
                {
                    $this->db->where('name',$_SESSION['username']);
                    $admin_pn=$this->db->get('tbladmin')->row();
                }
                $this->db->where('name',$admin_pn->id);
                $nhanhang=$this->db->get('tbldiachinhanhang')->row();
                if(count($nhanhang)==0)
                {
                ?>
                <li><a href="<?php echo site_url('site/diachinhanhang/'); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ nhận hàng">Địa chỉ nhận hàng</a></li>
                <?php
                }
                else
                {            
                ?>
                <li><a href="<?php echo site_url('site/editnhanhang/'.$nhanhang->id); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ nhận hàng">Địa chỉ nhận hàng</a></li>
                <?php
                }
            ?>
            <li><a href="<?php echo site_url('theo-doi-don-hang.html'); ?>" title="Theo dõi đơn hàng" <?php if(isset($theodoi)){ ?> style="background:#fff;"<?php } ?>>Theo dõi đơn hàng</a></li>
            <li><a href="<?php echo site_url('doi-mat-khau.html') ?>" <?php if(isset($doimk)){ ?> style="background:#fff;" <?php } ?>>Đổi mật khẩu</a></li>
        </ul>
    </div>
    <div id="panel_form">
        <?php 
            if(isset($_SESSION['username']))
            {
                $this->db->where('name',$_SESSION['username']);
                $sqlusert=$this->db->get('tbladmin')->row();
            }
            if(isset($_COOKIE['user']))
            {
                $this->db->where('name',$_COOKIE['user']);
                $sqlusert=$this->db->get('tbladmin')->row();    
            }
        ?>
        <?php 
            if(isset($thanhcong))
            {
        ?>
        <div class="boxSuccess">
            Cập nhật thông tin tài khoản thành công!
        </div>
        <?php 
        }
        ?>
        <form name="frmsuathongtin" method="post" action="<?php echo site_url('sua-tai-khoan.html') ?>" enctype="multipart/form-data">
            <table>
                <tr>
                    <th>Họ tên</th>
                    <td><input type="text" name="hoten" value="<?php echo $sqlusert->fullname; ?>" />
                    <input type="hidden" name="txtid" value="<?php echo $sqlusert->id; ?>" />
                    </td>
                </tr>
                <tr>
                    <th>Điện thoại</th>
                    <td><input type="text" name="dienthoai" value="<?php echo $sqlusert->dienthoai; ?>" /></td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td><input type="text" name="email" value="<?php echo $sqlusert->email; ?>" readonly="true" /></td>
                </tr>
                <tr>
                    <th>Ảnh đại diện</th>
                    <td><input type="file" name="avatar" value="" />
                    <input type="hidden" name="anh" value="<?php echo $sqlusert->image; ?>" />
                    <input type="hidden" name="anh_thumb" value="<?php echo $sqlusert->thumb; ?>" /><br /><br />
                    <?php 
                    if($sqlusert->image!='')
                    {
                    ?>
                    <img src="<?php echo $sqlusert->thumb; ?>" />
                    <?php    
                    }
                    ?>
                    </td>
                </tr>
                <tr>
                    <th></th>
                    <td><input type="submit" name="submit" value="Lưu thay đổi" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>