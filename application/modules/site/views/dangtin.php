<link href="css/ui-lightness/jquery-ui-1.10.4.custom.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="css/uploadify.css" media="all" /> 
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>	
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.uploadify.min.js" type="text/javascript"></script>
<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
$tinh=$CI->admin_model->gettbl('tbltinh','');
?>
<div id="panel">
    <p id="panel_title">Trang cá nhân</p>
    <div id="panel_main">
        <ul id="panel_top">
            <li><a href="<?php echo site_url('trang-ca-nhan.html'); ?>" <?php if(isset($ctp)){ ?> style="background:#fff;" <?php } ?>>Sửa thông tin</a></li>
            <li><a href="<?php echo site_url('danh-sach-dia-chi.html'); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ cửa hàng">Địa chỉ cửa hàng</a></li>
            <li><a href="">Theo dõi đơn hàng</a></li>
            <li><a href="<?php echo site_url('danh-sach-tin-dang.html'); ?>" <?php if(isset($listtd)){ ?> style="background:#fff;" <?php } ?> title="Danh sách tin đăng">Danh sách tin đăng</a></li>
            <li><a href="<?php echo site_url('doi-mat-khau.html') ?>" <?php if(isset($doimk)){ ?> style="background:#fff;" <?php } ?>>Đổi mật khẩu</a></li>
        </ul>
    </div>
    <div id="panel_form">
        <?php 
            if(isset($_SESSION['username']))
            {
                $this->db->where('name',$_SESSION['username']);
                $sqlusert=$this->db->get('tbladmin')->row();
            }
            if(isset($_COOKIE['user']))
            {
                $this->db->where('name',$_COOKIE['user']);
                $sqlusert=$this->db->get('tbladmin')->row();    
            }
        ?>
        <?php 
            if(isset($thanhcong))
            {
        ?>
        <div class="boxSuccess">
            Cập nhật thông tin sản phẩm thành công!
        </div>
        <?php 
        }
        if(isset($error_register))
        {
        ?>
        <div id="error_register">
			<fieldset style="text-align: left;background-color: #F5EFC9;">
    		<legend style="font-weight: bold; color:#F00" accesskey="Q">Thông báo hệ thống</legend>
				<?php echo $error_register;?>
			 </fieldset>
		 </div>
        <?php    
        }
        ?>
        <form name="frmdangtin" method="post" action="<?php echo site_url('xu-ly-dang-tin.html') ?>" enctype="multipart/form-data">
            <?php 
			if(isset($id))
			{
				$this->db->where('id',$id);
				$item=$this->db->get('tblsanpham')->row();
				$id=$item->id;            
			}
		?>    
            <table>
                <tr>
                    <th>Tên sản phẩm</th>
                    <td>
                    <input type="text" name="title" value="<?php if(isset($id)){ echo $item->title;}else{ echo set_value('title');} ?>" />
                    <input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />
                    </td>
                </tr>
                <tr>
                    <th>Alias</th>
                    <td><input type="text" name="alias" value="<?php if(isset($id)){ echo $item->alias;} ?>" />
                    </td>
                </tr>
                <tr>
                    <th>Chuyên mục 1</th>
                    <td>
                    <?php 
				        if(isset($id))
                        { 
                            $CI->admin_model->selectCtrlsp($item->catid,'catid', 'forFormDim');
                        }
				        else{
					       $CI->admin_model->selectCtrlsp('','catid', 'forFormDim');
				        }
			         ?>
            </td>
                </tr>
                <tr>
                    <th>Chuyên mục 2</th>
                    <td>
                    <?php 
				        if(isset($id))
                        { 
                            $CI->admin_model->selectCtrlsp($item->ncatid,'ncatid1', 'forFormDim');
                        }
				        else
                        {
					       $CI->admin_model->selectCtrlsp('','ncatid1', 'forFormDim');
				        }
			         ?>
                    </td>
                </tr>
                <tr>
                    <th>Chuyên mục 3</th>
                   	<td>
                       <?php 
            				if(isset($id)){ $CI->admin_model->selectCtrlsp($item->ncatid1,'ncatid2', 'forFormDim');}
            				else{
            					$CI->admin_model->selectCtrlsp('','ncatid2', 'forFormDim');
            				}
            			?>
                    </td>	
                </tr>
                <tr>
                    <th>Tỉnh thành</th>
                    <td>
                        <select name="city">
                            <option value="-1">--Chọn tỉnh thành--</option>
                            <?php 
                                if(isset($id))
                                {
                                    if($tinh->num_rows() >0)
                                    {
                                        foreach($tinh->result() as $itemtinh)
                                        {
                                            if($item->tinh==$itemtinh->id)
                                            {
                                            ?>
                                            <option selected="selected" value="<?php echo $itemtinh->id; ?>"><?php echo $itemtinh->tinh; ?></option>
                                            <?php  
                                            } 
                                            else
                                            {
                                            ?>
                                            <option value="<?php echo $itemtinh->id; ?>"><?php echo $itemtinh->tinh; ?></option>
                                            <?php    
                                            } 
                                        }
                                    }
                                }
                                else
                                {
                                    if($tinh->num_rows() >0)
                                    {
                                        foreach($tinh->result() as $itemtinh)
                                        {
        
                                            ?>
                                            <option value="<?php echo $itemtinh->id; ?>"><?php echo $itemtinh->tinh; ?></option>
                                            <?php   
                                        }
                                    }    
                                }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
        			<th>Ảnh đại diện</th>
        			<td><?php if(isset($id)){?>
        			<input type="hidden" name="image" style="width:250px;" value="<?php echo $item->image; ?>"> 
        			<input type="hidden" name="thumb" value="<?php echo $item->thumb; ?>"><br /><br />
        			<?php if($item->thumb !=''){?>
        			<img src="<?php echo $item->thumb; ?>" width="250"/><br />
        			<?php }} 			
        			?>
        			<input type="file" name="image" value="" />
        			</td>
        		</tr>
                <tr>
            <th>Ảnh  trưng bày</th>
            <td>
            <div class="row-field">
		<div class="field-content">
			<div id="portfolio" class="block-input-append">                   
                    <div class="title-clear"></div>
                    <div id="queue"></div>
                    <input id="file_upload" name="file_upload" type="file" multiple="true"/>                    
                     <ul id="sortable">  
                        <div class="ci-message"></div>
                        <?php 
                        if(!empty($item->image1)){
                        $images1 =json_decode($item->image1); 
                        if(is_array($images1) && count($images1)>0){
                            foreach ($images1 as $image1) {	
                            ?>
                                    <li>                                       
                                           <img class="img-thumb" width="131px"
                                               src="<?php echo  base_url().'tmp'.'/' . $image1; ?>" />                                   
                                                
                                        <input type="hidden" name="image_filename[]"
                                               value="<?php echo $image1 ?>" /> <br />			
                                       
                                        <a href="javascript:void(0)" class="remove" onclick="removeImage(this)" >Remove</a>
                                    </li>
                            <?php
                            }
                        }
                        }
                        ?>                        
                    </ul>
                </div>			
		</div>
	</div>
            </td>
        </tr>
        <tr>
            <th>Giá</th>
            <td><input type="text" name="gia" value="<?php if(isset($id)){ echo $item->gia;}else{ echo set_value('gia');} ?>" /></td>
        </tr>
        <tr>
            <th>Giá khuyến mại</th>
            <td><input type="text" name="giakm" value="<?php if(isset($id)){ echo $item->giakm;}else{ echo set_value('giakm');} ?>" /></td>
        </tr>
        <tr>
            <th>Đơn vị tính</th>
            <td><input type="text" name="donvitinh" value="<?php if(isset($id)){ echo $item->donvitinh;}else{ echo set_value('donvitinh');} ?>" /></td>
        </tr>
        <tr>
            <th>Thông tin chi tiết</th>
            <td>
                <textarea rows="5" cols="70" name="thongtinct" id="thongtinct"><?php if(isset($id)){ echo $item->noidung;}else{ echo set_value('thongtinct');} ?></textarea>
            </td>
        </tr>
        <tr>
            <th>Điểm nổi bật</th>
            <td>
                <textarea rows="5" cols="70" name="diennb" id="diennb"><?php if(isset($id)){ echo $item->diemnoibat;}else{ echo set_value('diennb');} ?></textarea>
            </td>
        </tr>
        <tr>
            <th>Điều kiện sử dụng</th>
            <td colspan="2">
                <textarea rows="5" cols="70" name="dieukiensd" id="dieukiensd"><?php if(isset($id)){ echo $item->dieukiensudung;}else{ echo set_value('dieukiensd');} ?></textarea>
            </td>
        </tr>
        <tr>
            <?php 
				if(isset($id)){
					$ngay1=explode('-',$item->ngayhethan);
					$created_day1=$ngay1[2].'-'.$ngay1[1].'-'.$ngay1[0];	
				}
                else
                {
                    $created_day1='';
                }
	       ?>
            <th>Ngày hết hạn</th>
            <td><input id="created_day1" type="text" name="created_day1" value="<?php echo $created_day1; ?>" /></td>
        </tr>
        <script type="text/javascript">   
                     function changeCaptcha()
						{
				            document.getElementById("captchaImage").src="captcha_new.php?len="+(Math.random()*4+4);
                            document.getElementById("load_img").src="images/indicator_arrows.gif";
                            setTimeout(function(){jQuery('#load_img').attr({src : "images/indicator_arrows_static.gif"}); }, 500);
						}
                    </script>     
            <tr>
                <th>Mã bảo vệ</th>
                <td><img src="captcha_new.php" id="captchaImage"/><img id="load_img" src="images/indicator_arrows_static.gif" onclick="changeCaptcha()" style="cursor:pointer;margin-bottom:5px;"/></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="text" value="" style="width:100px;margin-right:10px;" name="mabaove" id="mabaove" size="5" />
                <span class="require">Nhập chính sác ảnh hiện thị phía trên</span>
                <div class="warning t_red"></div>
                </td>
            </tr>						
                <tr>
                    <th></th>
                    <td><input type="submit" name="submit" value="Lưu thay đổi" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script >
<script type="text/javascript">
jQuery(function($){

	$.datepicker.regional['vi'] = {

		closeText: 'Đóng',

		prevText: '&#x3c;Trước',

		nextText: 'Tiếp&#x3e;',

		currentText: 'Hôm nay',

		monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',

		'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],

		monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',

		'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],

		dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],

		dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],

		dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],

		weekHeader: 'Tu',

		dateFormat: 'dd-mm-yy',

		firstDay: 0,

		isRTL: false,

		showMonthAfterYear: false,

		yearSuffix: ''};
		
	$.datepicker.setDefaults($.datepicker.regional['vi']);
});
$(function() {
    $( "#created_day1" ).datepicker();
	$.datepicker.setDefaults($.datepicker.regional['vi']);
});
</script>
<script type="text/javascript"> 
$(function() { 
	var editor = CKEDITOR.replace('thongtinct', { 
		filebrowserBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html"; ?>', 
		filebrowserImageBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Images";?>', 
		filebrowserFlashBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Flash" ?>', 
		filebrowserUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>', 
		filebrowserImageUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";?>', 
		filebrowserFlashUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";?>', 
		filebrowserWindowWidth : '800', 
		filebrowserWindowHeight : '480' 
	}); 
	CKFinder.setupCKEditor( editor, "<?php echo base_url().'ckfinder/'?>" ); 
	}) 
</script>
<script type="text/javascript"> 
$(function() { 
	var editor1 = CKEDITOR.replace('diennb', { 
		filebrowserBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html"; ?>', 
		filebrowserImageBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Images";?>', 
		filebrowserFlashBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Flash" ?>', 
		filebrowserUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>', 
		filebrowserImageUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";?>', 
		filebrowserFlashUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";?>', 
		filebrowserWindowWidth : '800', 
		filebrowserWindowHeight : '480' 
	}); 
	CKFinder.setupCKEditor( editor1, "<?php echo base_url().'ckfinder/'?>" ); 
	}) 
</script>
<script type="text/javascript"> 
$(function() { 
	var editor2 = CKEDITOR.replace('dieukiensd', { 
		filebrowserBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html"; ?>', 
		filebrowserImageBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Images";?>', 
		filebrowserFlashBrowseUrl : '<?php echo base_url()."ckfinder/ckfinder.html?Type=Flash" ?>', 
		filebrowserUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files"?>', 
		filebrowserImageUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images";?>', 
		filebrowserFlashUploadUrl : '<?php echo base_url()."ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash";?>', 
		filebrowserWindowWidth : '800', 
		filebrowserWindowHeight : '480' 
	}); 
	CKFinder.setupCKEditor( editor2, "<?php echo base_url().'ckfinder/'?>" ); 
	}) 
</script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script >
<script type="text/javascript" src="js/mootools-core.js"></script >
<script type="text/javascript" src="js/mootools-more.js"></script >
<script type="text/javascript">
	 window.addEvent('domready', function(){
        reNewItem();
       
    });
	<?php $timestamp = time();?>
	$(function() {
		$('#file_upload').uploadify({
			'formData'      : {
				'timestamp' : '<?php echo $timestamp; ?>',
				'token'     : '<?php echo md5('unique_salt' . $timestamp); ?>',
				'sessionid' : '<?php echo session_id(); ?>'
			},
			'buttonText' 	: 'Chèn ảnh ...',
			'width'   		: 180,
            'auto'          : true,
            'multi'         : true,
			'swf'           : 'images/uploadify.swf',
			'uploader'      : '<?php echo base_url() ?>uploadify.php',
            'onProgress'   : function(file, e) {
            
            },
			'onUploadSuccess' : function(file, data, response) {
				// Et ici
               $("#gallery").load("ajax/gallery.php");
                  var data =  $.parseJSON(data);
                  var item = data.files;
                  if (data.success) {
              
                       var html = '<li>';                                   
                                    html += '<img class="img-thumb" width= "140px" src="<?php echo base_url() . 'tmp/'; ?>'+item.filename+'" />';
                                    html += '<input type="hidden" name="image_id[]" value="0" />';
                                    html += '<input type="hidden" name="image_filename[]" value="'+item.filename+'" /><br/>';				
                                    html +='<a href="javascript:void(0)" class="remove" onclick="removeImage(this)" >Remove</a>';
                           html += '</li>';
                              console.log(html); 
                     $('#sortable').append(html);
                     $('#sortable li:last-child ').find('a.edit').data('title', item.title);								 
                        reNewItem();
                               
                   
                   }
			} 
		});
	});
       
    function removeImage(el)
    {
   
    
        jQuery(el).parent().fadeOut(function(){
             jQuery('.ci-message').append('Delete photo').show().fadeOut();
            jQuery(this).remove();
       });
    }	 
     function reNewItem(){      
        new Sortables('#sortable', {
            clone: true,
            revert: true,
            opacity: 0.3
        });
    }
</script>