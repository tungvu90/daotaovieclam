<div id="panel">
    <p id="panel_title">Trang cá nhân</p>
    <div id="panel_main">
        <ul id="panel_top">
            <li><a href="<?php echo site_url('trang-ca-nhan.html'); ?>" <?php if(isset($ctp)){ ?> style="background:#fff;" <?php } ?>>Sửa thông tin</a></li>
            <?php 
                if(isset($_SESSION['username']))
                {
                    $this->db->where('name',$_SESSION['username']);
                    $admin_pn=$this->db->get('tbladmin')->row();
                }
                $this->db->where('name',$admin_pn->id);
                $nhanhang=$this->db->get('tbldiachinhanhang')->row();
                if(count($nhanhang)==0)
                {
                ?>
                <li><a href="<?php echo site_url('site/diachinhanhang/'); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ nhận hàng">Địa chỉ nhận hàng</a></li>
                <?php
                }
                else
                {
                    //$nhanhang=$nhanhang->row();
                ?>
                <li><a href="<?php echo site_url('site/editnhanhang/'.$nhanhang->id); ?>" <?php if(isset($diachimore)){ ?> style="background:#fff;" <?php } ?> title="Địa chỉ nhận hàng">Địa chỉ nhận hàng</a></li>
                <?php
                }
            ?>
            <li><a href="<?php echo site_url('theo-doi-don-hang.html'); ?>" title="Theo dõi đơn hàng" <?php if(isset($theodoi)){ ?> style="background:#fff;"<?php } ?>>Theo dõi đơn hàng</a></li>
            <li><a href="<?php echo site_url('doi-mat-khau.html') ?>" <?php if(isset($doimk)){ ?> style="background:#fff;" <?php } ?>>Đổi mật khẩu</a></li>
        </ul>
    </div>
    <div id="panel_form">
        <?php 
            if(isset($_SESSION['username']))
            {
                $this->db->where('name',$_SESSION['username']);
                $sqlusert=$this->db->get('tbladmin')->row();
            }
            if(isset($_COOKIE['user']))
            {
                $this->db->where('name',$_COOKIE['user']);
                $sqlusert=$this->db->get('tbladmin')->row();    
            }
        ?>
        <?php 
            if(isset($thanhcong))
            {
        ?>
        <div class="boxSuccess">
            Cập nhật thông tin tài khoản thành công!
        </div>
        <?php 
        }
        if(isset($error_register))
        {
        ?>
        <div id="error_register">
			<fieldset style="text-align: left;background-color: #F5EFC9;">
    		<legend style="font-weight: bold; color:#F00" accesskey="Q">Thông báo hệ thống</legend>
				<?php echo $error_register;?>
			 </fieldset>
		 </div>
        <?php    
        }
        ?>
        <form name="frmsuathongtin" method="post" action="<?php echo site_url('xu-ly-doi-mat-khau.html') ?>" enctype="multipart/form-data">
            <table>
                <tr>
                    <th>Tài khoản</th>
                    <td><label><?php echo $sqlusert->name; ?></label>
                    <input type="hidden" name="id" value="<?php echo $sqlusert->id; ?>" />
                    </td>
                </tr>
                <tr>
                    <th>Mật khẩu cũ</th>
                    <td><input type="password" name="matkhaucu" value="" />
                    </td>
                </tr>
                <tr>
                    <th>Mật khẩu mới</th>
                    <td><input type="password" name="matkhaumoi" value="" /></td>
                </tr>
                <tr>
                    <th>Xác nhận mật khẩu</th>
                    <td><input type="password" name="matkhaucure" value="" /></td>
                </tr>
                <tr>
                    <th></th>
                    <td><input type="submit" name="submit" value="Lưu thay đổi" /></td>
                </tr>
            </table>
        </form>
    </div>
</div>