<?php
$CI=&get_instance();
$CI->load->model('site/site_model');
//Kiem tra date
date_default_timezone_set('Asia/Ho_Chi_Minh');
$day = date('Y-m-d H:i:s');
$diff = abs(strtotime($day) - strtotime($item->created_day));
$years = floor($diff / (365*60*60*24));
$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
$hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24)/ (60*60));
$minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
$seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));
if($days==0 and $years==0 and $months==0){
	$daytime = "Cách đây ";
	if($hours!=0){$daytime .= $hours." giờ, ";}
	$daytime .= $minutes." phút";
}
else{
	$base=explode(' ',$item->created_day);
	$baseday=explode('-',$base[0]);
	$basetime=substr($base[1],0,-3);
	$daytime=$baseday[2].'/'.$baseday[1].'/'.$baseday[0].' '.$basetime;
}
/////////////////////////////////
$category='';
$category=$CI->site_model->getcatlink($item->catid);
$catitem=$CI->site_model->getitem('tblchuyenmuc',$item->catid)->row();
?>
<div id="left">
    <?php $this->load->view('includes/left') ?>
    <div class="clear"></div>
</div>
<div id="right">
<div class="box_right">
	<div class="box_right_top"><a href="<?php echo site_url($catitem->alias.'-c'.$catitem->id.'.html'); ?>"><?php echo $catitem->name; ?></a>
		
	</div>									
	<div class="box_right_main">
    <ul class="menubreakcumb">
            <li><a href="">Trang chủ</a></li>
            <li><span><?php echo $catitem->name; ?></span></li>
            <li style="background:none;"><span><?php echo $item->title; ?></span></li>
            <div class="clear"></div>
        </ul>
        <div id="keugoi">					
		<h1 class="title"><?php echo $item->title; ?></h1>
		<p class="date"><?php echo $daytime; ?>&nbsp;|&nbsp;<?php echo $item->ngaydang; ?>&nbsp;|&nbsp;<?php echo $item->hits ?> lượt xem</p>		
		<div class="fulltext">		
			<?php echo $item->fulltext; ?>
			<?php if($item->tacgia!=''){?><p class="tacgia"><strong>Tác giả</strong>:&nbsp;<?php echo $item->tacgia; ?></p><?php } ?>
			<?php if($item->nguon!=''){?><p class="nguontin"><strong>Nguồn</strong>:&nbsp;<?php echo $item->nguon; ?></p><?php } ?>
		</div>
        <!--Chia Sẻ-->
                  <div id="chiase">
                  	  <span id="title">Chia sẻ</span>
                      <!-- AddThis Button BEGIN @@ Chèn vào chổ mà bạn muốn nó hiển thị -->
<div class="addthis_toolbox addthis_default_style">
<a class="addthis_button_google"></a>
<a class="addthis_button_facebook"></a>
<a class="addthis_button_yahoobkm"></a>
<a class="addthis_button_zingme"></a>
<a class="addthis_button_govn"></a>
<a class="addthis_button_email"></a>
<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
<a class="addthis_counter addthis_pill_style"></a>
</div>
<script type="text/javascript" src="https://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4f50c8104ae535bf"></script>
<!-- AddThis Button END -->		 
                   </div>
                  <!--Chia Sẻ-->			
		<?php if($item->tags!=''){ 
		$tags=explode(',',$item->tags);
		?>
		<p class="tags">Từ khóa: <?php 
		$tag='';
		$this->load->helper('locdau');
		for($a=0;$a<count($tags);$a++){
			$tagl=LocDau($tags[$a]);
			$tag.='<a href="'.site_url('tags/'.$tagl).'">'.$tags[$a].'</a>,';					
		}
		echo $tag; ?></p>
		<?php } ?>
		
		<!-- Comment -->
					
		<?php
		if($item->tags!=''){		
			$sql2="SELECT id,catid,title,thumb,alias FROM tblbaiviet where status=1 AND id !=$item->id AND created_day<='$day' AND (";
			for($a=0;$a<count($tags);$a++){
				$tag = trim($tags[$a]);
				$sql2 .=" tags LIKE '%$tag%'";
				if($a<count($tags)-1){
					$sql2 .=" OR";
				}
			}
			$sql2 .=") ORDER BY id DESC LIMIT 6";
			$item_tags=$this->db->query($sql2);			
		?>
		
		<?php } ?>
		</div>
        <div id="fb" style="padding-left:9px;">       
	       <div id="fb-root"></div>
		      <script>(function(d, s, id) {
		          var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                  fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
            <html xmlns:fb="https://ogp.me/ns/fb#">
                <fb:comments href="<?php echo site_url($item->alias.'-'.$item->id).'.html';?>" width="700" num_posts="10" ></fb:comments>
            </html>
            <!--fb-->
        </div>
        <div id="tinmoi_ct">
            <p id="tinmoi_ct_title">Tin mới</p>
            <?php 
                $this->db->where('status',1);
                $this->db->order_by('id','desc');
                $this->db->select('id,catid,title,alias');
                $this->db->limit(5);
                $tinmoict=$this->db->get('tblbaiviet');
                if($tinmoict->num_rows()>0)
                {
                    foreach($tinmoict->result() as $itemtinmoict)
                    {
                    ?>
                    <p class="item-lq"><a href="<?php echo site_url($itemtinmoict->alias.'-'.$itemtinmoict->id).'.html';?>"><?php echo $itemtinmoict->title;?></a></p>
                    <?php    
                    }
                }
            ?>
        </div>	 
        <div id="lienquan_ct">
            <p id="lienquan_ct_title">Tin liên quan</p>	
        <?php 		
		if($item->tinlienquan!=''){
			//Xu ly tin lien quan theo lua chon
            $tinlqfor=explode(',',$item->tinlienquan);
            foreach($tinlqfor as $itemlqhy)
            {
			//$list_lq=$item->tinlienquan;
			$sql3="SELECT id,catid,title,alias FROM tblbaiviet WHERE status=1 AND id=$itemlqhy ORDER BY id DESC";
			$item_lqs=$this->db->query($sql3);	
			foreach($item_lqs->result() as $item_lq){			
			?>
				<p class="item-lq"><a href="<?php echo site_url($item_lq->alias.'-'.$item_lq->id).'.html';?>"><?php echo $item_lq->title;?></a></p>
			<?php
			}
            }
		}
		?>
        </div>
        <div class="clear"></div>
		</div>
	
</div>
    <div class="clear"></div>
</div>