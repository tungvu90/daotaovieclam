<script type="text/javascript" src="tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    tinymce.init({
    selector: "textarea",
    menubar : false,
    plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | link image"
});
</script>
<div id="left">
    <?php $this->load->view('includes/left') ?>
    <div class="clear"></div>
</div>
<div id="right">
    <div class="box_right">
        <div class="box_right_top">
            <p>Đăng ký ngay</p>
        </div>
        <div class="box_right_main">
            <ul class="menubreakcumb">
                <li><a href="">Trang chủ</a></li>
                <li style="background:none;"><span>Đăng ký ngay</span></li>
                <div class="clear"></div>
            </ul>
            <div id="dangkyngay">
            <script type="text/javascript" src="https://secure.jotform.me/jsform/52573603709458"></script>
                <?php 
    			     if(isset($errors_regíter))
    			     {
    			     ?>
                    <div id="error_register">
        			     <fieldset style="text-align: left;background-color: #F5EFC9;">
                		<legend style="font-weight: bold; color:#F00" accesskey="Q">Thông báo hệ thống</legend>
            				<?php echo $errors_regíter;?>
            			 </fieldset>
        			 </div>
    			 <?php
                 }?>
                <!--<form name="frmdangkyhoc" method="POST" action="<?php echo site_url('site/dodangkyhoc'); ?>">
                    <table>
                        <tr>
                            <td><label>Họ tên</label></td>
                            <td><input type="text" name="hoten" value="" /></td>
                        </tr>
                        <tr>
                            <td><label>Địa chỉ</label></td>
                            <td><input type="text" name="diachi" value="" /></td>
                        </tr>
                        <tr>
                            <td><label>Điện thoại</label></td>
                            <td><input type="text" name="dienthoai" value="" /></td>
                        </tr>
                        <tr>
                            <td><label>Email</label></td>
                            <td><input type="text" name="email" value="" /></td>
                        </tr>
                        <tr>
                            <td><label>Số người đăng ký</label></td>
                            <td><input type="text" name="songuoidk" value="" /></td>
                        </tr>
                        <tr>
                            <td><label>Đăng ký môn học</label></td>
                            <td><input type="text" name="dangkymh" value="" /></td>
                        </tr>
                        <tr>
                            <td><label>Thời gian học</label></td>
                            <td><input type="text" name="thoigian" value="" /></td>
                        </tr>
                        <tr>
                            <td valign="top"><label>Nội dung khác</label></td>
                            <td><textarea name="noidungkhac"></textarea></td>
                        </tr>
                        <script type="text/javascript">
    						function changeCaptcha()
    						{
    								document.getElementById("captchaImage").src="captcha_new.php?len="+(Math.random()*4+4);
    								 document.getElementById("load_img").src="images/indicator_arrows.gif";
    								 setTimeout(function(){jQuery('#load_img').attr({src : "images/indicator_arrows_static.gif"}); }, 500);
    						}
    						</script>
                        <tr>
                            <td style="padding-top:7px;"><label>Mã bảo vệ</label></td>
                            <td style="padding-top:7px;">
                            <img src="captcha_new.php" id="captchaImage" ><img id="load_img" src="images/indicator_arrows_static.gif" onclick="changeCaptcha()" style="cursor:pointer;margin-bottom:5px;"/>
                            <br />
                            <input type="text" size="5"  name="mabaomat"  style="width:100px;margin-top:5px;" value="<?php echo set_value('mabaomat'); ?>">
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="padding-top:10px;">
                                <input type="submit" name="submit" value="Đăng ký" />
                                <input type="reset" name="reset" value="Làm lại" />
                            </td>
                        </tr>
                    </table>
                </form>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</div>    