<div class="box_right">
	<div class="box_right_top">
        <?php 
            if(isset($id))
            {
                $this->db->where('id',$id);
                $sqlthuvienid=$this->db->get('tblalbum');
                if($sqlthuvienid->num_rows()>0)
                {
                    $sqlthuvienid=$sqlthuvienid->row();
                ?>
                <a href="<?php echo site_url(LocDau($sqlthuvienid->album).'-tv'.$sqlthuvienid->id.'.html'); ?>"><?php echo $sqlthuvienid->album; ?></a>
                <?php    
                }
            }
        ?>        
    </div>									
	<div class="box_right_main">
        <ul class="menubreakcumb">
            <li><a href="">Trang chủ</a></li>
            <?php 
                if(isset($id))
                {
                ?>            
                <li style="background:none;"><span><?php echo $sqlthuvienid->album; ?></span></li>
                <?php 
                }
            ?>
            <div class="clear"></div>
        </ul>
        <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
		<script src="js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
       
			<script type="text/javascript" charset="utf-8">
			$(document).ready(function(){
				$("area[rel^='prettyPhoto']").prettyPhoto();
				
				$(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({});
				$(".gallery:gt(0) a[rel^='prettyPhoto']").prettyPhoto({});
		
				$("#custom_content a[rel^='prettyPhoto']:first").prettyPhoto({
					custom_markup: '<div id="map_canvas" style="width:260px; height:265px"></div>',
					changepicturecallback: function(){ initialize(); }
				});

				$("#custom_content a[rel^='prettyPhoto']:last").prettyPhoto({
					custom_markup: '<div id="bsap_1259344" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div><div id="bsap_1237859" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6" style="height:260px"></div><div id="bsap_1251710" class="bsarocks bsap_d49a0984d0f377271ccbf01a33f2b6d6"></div>',
					changepicturecallback: function(){ _bsap.exec(); }
				});
			});
			</script>
            <style>
			.wide {
				border-bottom: 1px #000 solid;
				width: 4000px;
			}
			
			.fleft { float: left; margin: 0 20px 0 0; }
			
			.cboth { clear: both; }
			
			#main {
				background: #fff;
				margin: 0 auto;
				padding: 30px;
				width: 1000px;
			}
		</style>
<!--big-box-->
<style>
.error
{
	color:red;
}
</style>
        <div id="click_more">
            <p>Click vào hình để xem ảnh lớn hơn</p>
            <div id="content_box" class="gallery clearfix">
            <?php 
                if(isset($id)){
                    $this->db->where('album_id',$id);
                    $sqlimages = $this->db->get('tblalbum_images')->result();
                    foreach ($sqlimages as $anh){
                    ?>
                    <div class="album_itemg">
                        <a title="<?php echo $anh->title; ?>" href="<?php echo  base_url().$anh->images; ?>" rel="prettyPhoto[gallery]"><img src="<?php echo  base_url().$anh->images; ?>" title="<?php echo $anh->title; ?>" alt="<?php echo $anh->title; ?>" /> </a>
                        <span><?php echo $anh->title; ?></span>
                        
                    </div>
                    <?php        
                    }
                }
            ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>    