<div id="listchuyentab3">
<div class="popup-header1" style="background:url(images/chon_tt.png) top left no-repeat !important;">
    <a class="close_popup1" href="javascript:void(0)"></a>
</div>
<div id="hinhthuc_main">
    <?php 
            if($radiohinhthuc=='cod')
            {
             ?>
    <div class="hinhthuc3_item">
        <div class="hinhthuc3_item_l">
            <img src="images/icon_tannoi.png" />
        </div>
        <div class="hinhthuc3_item_r">
            <input type="radio" name="thutannoi" value="" checked="checked" />            
            <p>
            Buôn chung giao phiếu và thu tiền <span>(Miễn phí)</span>
            <label>Trong vòng 1-3 ngày, Buonchung sẽ đến địa chỉ của bạn để thu tiền và gửi mã số phiếu qua Email</label></p>
            <div class="clear"></div>
            <input type="hidden" name="ip_cuoi" id="ip_cuoi" value="<?php echo $ip_cuoi; ?>" />
            <input type="hidden" name="radiohinhthuc" id="radiohinhthuc" value="<?php echo $radiohinhthuc; ?>" />
            <textarea name="yeucauthem" id="yeucauthem" placeholder="Nếu Quý khách có yêu cầu đặc biệt, vui lòng thêm thông tin để Buonchung phục vụ tốt hơn"></textarea>
        </div>
    </div>
    <div id="hinhthuc_footer3">
        <p id="hinthuc_tab3">Tiếp tục</p>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("#hinthuc_tab3").click(function(){
                ip_cuoi=jQuery("#ip_cuoi").val();
                radiohinhthuc=jQuery("#radiohinhthuc").val();
                yeucauthem=jQuery("#yeucauthem").val(); 
                jQuery.ajax({
                    cache:false,                                
                    type : "POST",
                    data:{ip_cuoi : ip_cuoi,radiohinhthuc : radiohinhthuc,yeucauthem : yeucauthem},  
                    url : "<?php echo site_url('site/chuyentab3/'); ?>",
                    success:function(html){
                        jQuery("#listchuyentab3").html(html);                                                                         
                    }  
                });       
            });
        });
    </script>
    <?php 
    }
    else
    {
    ?>
    <div class="hinhthuc3_item">
        <div class="hinhthuc3_item_l">
            <img src="images/sms.png" />
        </div>
        <div class="hinhthuc3_item_r">
            <input type="radio" name="thutannoi" value="" checked="checked" />            
            <p>
            Nhận mã số phiếu qua email
            <label>Bạn sẽ nhận được mã số phiếu qua SMS và Email ngay sau khi thanh toán thành công.</label>
            <label>Hãy mang theo mã số này để cung cấp khi sử dụng dịch vụ.</label>
            </p>
            <div class="clear"></div> 
            <input type="hidden" name="ip_cuoi" id="ip_cuoi" value="<?php echo $ip_cuoi; ?>" />
            <input type="hidden" name="radiohinhthuc" id="radiohinhthuc" value="<?php echo $radiohinhthuc; ?>" /> 
            <textarea style="display: none;" name="yeucauthem" id="yeucauthem" placeholder="Nếu Quý khách có yêu cầu đặc biệt, vui lòng thêm thông tin để Buonchung phục vụ tốt hơn"></textarea>          
        </div>
    </div>
    <div id="hinhthuc_footer3">
        <p id="hinthuc_tab3">Tiếp tục</p>
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery("#hinthuc_tab3").click(function(){
                ip_cuoi=jQuery("#ip_cuoi").val();
                radiohinhthuc=jQuery("#radiohinhthuc").val();
                yeucauthem=jQuery("#yeucauthem").val(); 
                jQuery.ajax({
                    cache:false,                                
                    type : "POST",
                    data:{ip_cuoi : ip_cuoi,radiohinhthuc : radiohinhthuc,yeucauthem : yeucauthem},  
                    url : "<?php echo site_url('site/chuyentab3/'); ?>",
                    success:function(html){
                        jQuery("#listchuyentab3").html(html);                                                                         
                    }  
                });       
            });
        });
    </script>
    <?php
    }
    ?>
</div>
</div>