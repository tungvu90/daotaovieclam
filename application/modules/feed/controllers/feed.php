<?php 
class Feed extends MX_Controller {
       
      public function __construct()
      {
        parent::__construct(); 
        $this->load->helper('xml');
        $this->load->helper('text');
        $this->load->model('posts_model');
      }
  public function index()
    {
        $data['feed_name'] = 'Đào tạo việc làm'; // your website
        $data['encoding'] = 'UTF-8'; // the encoding
        $data['feed_url'] = 'http://daotaovieclam.edu.vn/feed'; // the url to your feed
        $data['page_description'] = 'Đào tạo dạy nghề tại Hải Phòng | Dạy học nghề thực tế'; // some description
        $data['page_language'] = 'en-en'; // the language
        $data['creator_email'] = 'mail@me.com'; // your email
        $data['posts'] = $this->posts_model->getPosts();  
        header("Content-Type: application/rss+xml"); // important!
        $this->load->view('rss', $data);
    }
}
?>