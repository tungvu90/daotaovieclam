<?php 
class Admin_model extends CI_Model
{	
	public function __construct()
    {
        parent::__construct();
    }
	
	public function gettbl($tbl,$id)
	{
		if($id!=''){
			$this->db->where('id',$id);
		}			
		if($tbl=='site'){
			$this->db->order_by('category_id','asc');			
		}		
		$query = $this->db->get($tbl);
		return $query;
	}
    public function getOldImage($id){
        $sql="select anh from tblalbum where id=".$id;
		$query=$this->db->query($sql);
        return $query->row();
    }
    public function add_tbl_image($tbl,$data,$id)
    {        
        if($id=='')
        {
            $this->db->insert($tbl,$data);
            
        }else
        {
            $this->db->where('id',$id);
            $this->db->update($tbl,$data);    
        }
    }
	public function gettbl_listbv($tbl)
	{		
		$this->db->where('status',1);
		$query = $this->db->get($tbl);
		return $query;
	}
	public function gettbl_data($tbl,$id)
	{
		if($id!=''){
			$this->db->where('site_id',$id);
		}			
		$query = $this->db->get($tbl);
		return $query;
	}
	public function add_tbl($tbl,$data,$id)
    {
		//var_dump($data['title']);die;
        if($id=='')
        {
            $this->db->insert($tbl,$data);
        }
        else
        {
            $this->db->where('id',$id);
            $this->db->update($tbl,$data);    
        }
    }  
	public function add_tbl1($tbl,$data,$catid)
    {
		$extra = $data["extra"];
		$element_ex = $data["element_delete"];
		$name = $data["field_name"];
		$sql="select * from $tbl where field_name='$name' AND site_id=".$catid;
		$query=$this->db->query($sql);
		
        if($query->num_rows)
        {							
			if($data["field_name"] == 'name'){				
				$sql="UPDATE $tbl SET extra='$extra', element_delete='$element_ex' WHERE field_name='name' AND site_id=".$catid;
				$this->db->query($sql);
			}			
			if($data["field_name"] == 'sapo'){				
				$sql="UPDATE $tbl SET extra='$extra', element_delete='$element_ex' WHERE field_name='sapo' AND site_id=".$catid;
				$this->db->query($sql);
			}
			if($data["field_name"] == 'description'){				
				$sql="UPDATE $tbl SET extra='$extra', element_delete='$element_ex' WHERE field_name='description' AND site_id=".$catid;
				$this->db->query($sql);
			}
        }
        else
        {		
			$this->db->insert($tbl,$data);
        }
    }  
	
	public function show_category($catid,$parent_id="0",$insert_text="-")
    {
        $this->db->where('uid',$parent_id);
        $this->db->order_by('id','asc');
        $sql=$this->db->get('tblchuyenmuc');
        foreach($sql->result() as $itemcat)
        {		
			if($itemcat->id==$catid){
				echo "<option selected=\"selected\" value='".$itemcat->id."'>".$insert_text.$itemcat->name."</option>";				
			}
			else{
				echo "<option value='".$itemcat->id."'>".$insert_text.$itemcat->name."</option>";				
			}
            $this->show_category($catid,$itemcat->id,$insert_text."---");    
        }
        return true;
    }
    public function selectCtrl($catid,$name,$class)
    {
        echo "<select name='".$name."' class='".$class." form-control'>\n";
		if($name=='ncatid1' or $name=='ncatid2'){
			echo "<option value='0'>---------------------</option>";
		}
		else{
			echo "<option value='0'>-- Chọn chuyên mục --</option>";
		}
        $this->show_category($catid);
        echo "</select>";
    }
    public function show_categorysp($catid,$parent_id="0",$insert_text="-")
    {
        $this->db->where('uid',$parent_id);
        $this->db->order_by('id','asc');
        $sql=$this->db->get('tblchuyenmucsp');
        foreach($sql->result() as $itemcat)
        {		
			if($itemcat->id==$catid){
				echo "<option selected=\"selected\" value='".$itemcat->id."'>".$insert_text.$itemcat->name."</option>";				
			}
			else{
				echo "<option value='".$itemcat->id."'>".$insert_text.$itemcat->name."</option>";				
			}
            $this->show_categorysp($catid,$itemcat->id,$insert_text."---");    
        }
        return true;
    }
    public function selectCtrlsp($catid,$name,$class)
    {
        echo "<select name='".$name."' class='".$class."'>\n";
		if($name=='ncatid1' or $name=='ncatid2'){
			echo "<option value='0'>---------------------</option>";
		}
		else{
			echo "<option value='0'>-- Chọn chuyên mục --</option>";
		}
        $this->show_categorysp($catid);
        echo "</select>";
    }
	// Đệ quy link thân thiện
	public function getcatlink($uid)
    {				
		$catlink=0;
        $this->db->where('id',$uid);
        $sql1=$this->db->get('tblchuyenmuc');
        if($sql1->num_rows() >0)
        {           		
            foreach($sql1->result() as $items)
            {                            
                $catlink = $this->getcatlink($items->uid); 
				$catlink .= '/'.$items->alias;								
            }   
		return $catlink;
        }				
    }
	public function getcatlinksp($uid)
    {
        $catlink=0;
        $this->db->where('id',$uid);
        $sql1=$this->db->get('tblchuyenmucsp');
        if($sql1->num_rows() >0)
        {           		
            foreach($sql1->result() as $items)
            {                            
                $catlink = $this->getcatlinksp($items->uid); 
				$catlink .= '/'.$items->alias;								
            }   
		return $catlink;
        }    
    }
	public function gettblsub($tbl,$id)
	{		
		if($id!=''){
			$this->db->where('uid',$id);
		}
		$query = $this->db->get($tbl);
		return $query;
	}
		
	public function gettbl_limited($tbl,$start_row,$limit)
	{
	    $sql="select * from $tbl order by id desc limit  $start_row,$limit";
		$query=$this->db->query($sql);
		return $query;
	}	
	public function gettbl_listbv_limited($tbl,$start_row,$limit)
	{
	    $sql="select id,title,catid from $tbl where status=1 order by id desc limit $start_row,$limit";
		$query=$this->db->query($sql);
		return $query;
	}
	public function gettbl_search_limited($tbl,$start_row,$limit)
	{	   	
		if(isset($_SESSION['txt_search']) and $_SESSION['txt_search']!='Nhập từ khóa tìm kiếm')
        {                       		
			$this->db->like('title',$_SESSION['txt_search']);			
		}
		if(isset($_SESSION['search_catid']) and $_SESSION['search_catid']!=0)
        {             			
			$this->db->where('catid',$_SESSION['search_catid']);					
		}
		if(isset($_SESSION['search_user']) and $_SESSION['search_user']!=0)
        {             			
			$this->db->where('taikhoan',$_SESSION['search_user']);					
		}
		if(isset($_SESSION['search_status']) and $_SESSION['search_status']!=-1)
        {             			
			$this->db->where('status',$_SESSION['search_status']);		
		}     
		if($tbl=='tblbaiviet'){
		    if(isset($_SESSION['quyen'])){
		        if($_SESSION['quyen'] == 3 || $_SESSION['quyen'] == 4){
		            $this->db->where('uid',$_SESSION['author']);
		        }
		    }
		}
		$this->db->order_by('id','DESC');
		if($limit!=''){
			$this->db->limit($limit,$start_row);			
		}		
		$query=$this->db->get($tbl);		
		return $query;
	}
    public function gettbl_donhang_limited($tbl,$start_row,$limit)
    {        
        $this->db->order_by('id','DESC');
		if($limit!=''){
			$this->db->limit($limit,$start_row);			
		}		
		$query=$this->db->get($tbl);		
		return $query;    
    }
	public function gettbl_search_limited_user($tbl,$start_row,$limit)
	{	   	
		if(isset($_SESSION['txt_search']) and $_SESSION['txt_search']!='Nhập từ khóa tìm kiếm')
        {                       		
			$this->db->like('title',$_SESSION['txt_search']);
		}
		if(isset($_SESSION['search_catid']) and $_SESSION['search_catid']!=0)
        {             			
			$this->db->where('catid',$_SESSION['search_catid']);
		}
		if($tbl=='tblbaiviet'){
		    if(isset($_SESSION['quyen'])){
		        if($_SESSION['quyen'] == 3 || $_SESSION['quyen'] == 4){
		            $this->db->where('uid',$_SESSION['author']);
		        }
		    }
		}
	    //$this->db->where('status',0);
		$this->db->order_by('id','DESC');
		if($limit!=''){
			$this->db->limit($limit,$start_row);			
		}		
		$query=$this->db->get($tbl);		
		return $query;
	}
	public function gettbl_search_limited_usersp($tbl,$start_row,$limit)
	{	   
	
		if(isset($_SESSION['txt_searchsp']) and $_SESSION['txt_searchsp']!='Nhập từ khóa tìm kiếm')
        {                       		
			$this->db->like('title',$_SESSION['txt_searchsp']);
		}
        if(isset($_SESSION['txt_dienthoaisp']) and $_SESSION['txt_dienthoaisp']!='Nhập số điện thoại tìm kiếm')
        {
            $this->db->like('dienthoai',$_SESSION['txt_dienthoaisp']);    
        }
		if(isset($_SESSION['search_catidsp']) and $_SESSION['search_catidsp']!=0)
        {             			
			$this->db->where('catid',$_SESSION['search_catidsp']);
		}
		//$this->db->where('status',0);
		$this->db->order_by('id','DESC');
		if($limit!=''){
			$this->db->limit($limit,$start_row);			
		}		
		$query=$this->db->get($tbl);		
		return $query;
	}
	public function del_tbl($tbl,$id){
		$sql="DELETE FROM $tbl WHERE id=".$id;                
		$result=$this->db->query($sql);		
		return $result;
	}
	public function checkstatus($tbl,$action,$id)
	{
		$sql="UPDATE $tbl SET status='$action' WHERE id='$id'";
		$this->db->query($sql);
	}
	//Check admin
	public function getlogin($name,$pass)
    {
        $this->db->where('taikhoan',$name);
        $this->db->where('matkhau',md5($pass));
        $sql=$this->db->get('tbladmin1');
        if($sql->num_rows()==1)
        {
            return TRUE;
        }
    }
    public function checkfileds($name)
    {
        $this->db->where('name',$name);
        $sql=$this->db->get('tbladmin');
        if($sql->num_rows()==1)
        {
            return TRUE;
        }
    }
	//Gan Flag
	public function flags($id,$nguoidang,$flag){		
		$sql="UPDATE tblbaiviet SET nguoidang=$nguoidang,flag=$flag WHERE id='$id'";
		$this->db->query($sql);
	}
	/////////////////////////////////////////	
	public function add_tbldata($data)
    {
		$title= $data['title'];
		//Kiem tra trung bai viet
		$this->db->select('title');
		$this->db->where('title',$title);
        $query=$this->db->get('news');				
		if($query->num_rows()==0){
		//////////////
		$tags='';
		$base_tags = explode(',',trim($data['tags']));
		for($b=0;$b<count($base_tags);$b++){
			$tags .= trim($base_tags[$b]);
			if($b<count($base_tags)-1){
				$tags .= ',';
			}
		}		
			$data1=array(			
				'title'  	=>  $data['title'],					
				'alias'  	=>  $data['alias'],					
				'sapo'  	=>  $data['sapo'],																					
				'fulltext'  =>  $data['fulltext'],				
				'tags'  	=>  $tags,				
				'thumb'  	=>  $data['image'],
				'uid'  		=>  $data['user_id'],
				'catid'  	=>  $data['catid'],
				'nguon'  	=>  $data['nguon'],
				'status'  	=>  $data['status']
			); 
			$this->db->insert('news',$data1);
			return $query;	  
		}
    }  
	
	public function insert_data_query()
    {		
		//Kiem tra trung bai viet
		$sql="select * from news order by id desc";
		$query=$this->db->query($sql);
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$day = date('Y-m-d H:i:s');	
		//$timezone = +6;
		//$day = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
		foreach($query->result() as $qry){
			$this->db->select('title');
			$this->db->where('title',$qry->title);			
			$sql=$this->db->get('tblbaiviet');				
			if($sql->num_rows()==0){
				$data=array(			
					'title'  	=>  $qry->title,	
					'alias'		=>  $qry->alias,
					'sapo'  	=>  $qry->sapo,																					
					'fulltext'  =>  $qry->fulltext,
					'created_day'  =>  $day,
					'thumb'  	=>  $qry->thumb,
					'uid'  		=>  $qry->uid,
					'catid'  	=>  $qry->catid,
					'tags'  	=>  $qry->tags,
					'nguon'  	=>  $qry->nguon,
					'status'  	=>  $qry->status
				); 
				$this->db->insert('tblbaiviet',$data);
			}
		}
		$this->db->empty_table('news');
    }  
	
	public function check_link($url,$host='')
	{
		if((strpos($url,'http://')===false) and (preg_match_all('/http:\/\/(.*)\.([a-z]+)\//',$host,$matches,PREG_SET_ORDER)))
		{
			while ($url{0}=='/'){
				$url=substr($url,1);
			}
			if($matches[0][0]{strlen($matches[0][0])-1}!='/'){
				$matches[0][0]=$matches[0][0].'/';
			}
			$url = $matches[0][0].$url;
		}
		return $url;
	}
	public function file_get_contents_curl($url) {	
		$ch = curl_init();
	
		curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);		
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	
		$data = curl_exec($ch);
		curl_close($ch);
	
		return $data;
	}
}
?>