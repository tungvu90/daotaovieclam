<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">
          <?php 
                if(isset($id))
                {
                    echo 'Sửa liên hệ';
                }
                else
                {
                    echo 'Thêm liên hệ';    
                }
            ?>
      </h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/lienhe'); ?>">Thông tin liên hệ</a></li>
            <li class="breadcrumb-item active" aria-current="page">
            <?php 
                if(isset($id)) {
                    echo 'Sửa';
                }else{
                    echo 'Thêm mới';    
                }
            ?>
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <?php 
                if(isset($error))
                {
                    echo '<div class="warning">'.$error.'</div>';
                }
            ?>
            <form name="frmslider" action="<?php echo site_url('admin/add_lienhe'); ?>" method="post" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header" style="display:table;">
                        <p class="text-end" style="display:table-cell;">
                            <a href="<?php echo site_url('admin/lienhe'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                        </p>
                    </div>
                    <div class="card-body">
                        <?php 
                			if(isset($id))
                			{
                				$this->db->where('id',$id);
                				$sqlcheck=$this->db->get('tbllienhe')->row();
                				$id=$sqlcheck->id;            
                			}
                		?>    
                		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />	
                		<div class="row">
                    	    <div class="col-6">
                    	        <div class="form-group row">
                    	            <label for="hoten" class="col-sm-12 control-label col-form-label"><strong>Họ tên</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="hoten" value="<?php if(isset($id)) { echo $sqlcheck->hoten; }; ?>" />
                                    </div>
                	            </div>
            	            </div>
            	            <div class="col-6">
                    	        <div class="form-group row">
                    	            <label for="diachi" class="col-sm-12 control-label col-form-label"><strong>Địa chỉ</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="diachi" value="<?php if(isset($id)) { echo $sqlcheck->diachi; }; ?>" />
                                    </div>
                	            </div>
            	            </div>
            	            <div class="col-6">
                    	        <div class="form-group row">
                    	            <label for="dienthoai" class="col-sm-12 control-label col-form-label"><strong>Điện thoại</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="dienthoai" value="<?php if(isset($id)) { echo $sqlcheck->dienthoai; }; ?>" />
                                    </div>
                	            </div>
            	            </div>
            	            <div class="col-6">
                    	        <div class="form-group row">
                    	            <label for="email" class="col-sm-12 control-label col-form-label"><strong>Email</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="email" value="<?php if(isset($id)) { echo $sqlcheck->email; }; ?>" />
                                    </div>
                	            </div>
            	            </div>
        	            </div>
        	            <div class="row">
        	                <div class="col-12">
                    	        <div class="form-group row">
                    	            <label for="noidung" class="col-sm-12 control-label col-form-label"><strong>Nội dung</strong></label>
                                    <div class="col-12">
                                        <textarea name="noidung" class="form-control"><?php if(isset($id)) { echo $sqlcheck->noidung; }; ?></textarea>
                                    </div>
                	            </div>
            	            </div>
    	                </div>
    	                <div class="row">
    	                    <div class="col-6">
    	                        <?php 
                                    $ngaydanglh=explode('-',$sqlcheck->ngaylienhe);
                                    $created_day=$ngaydanglh[2].'/'.$ngaydanglh[1].'/'.$ngaydanglh[0];
                                ?>
                                <label for="ngaylienhe" class="col-sm-12 control-label col-form-label"><strong>Ngày liên hệ</strong></label>
    	                        <div class="input-group">
        					        <input class="form-control" id="created_day" type="text" placeholder="Ngày tạo" name="ngaylienhe" value="<?php echo $created_day; ?>" />
        					        <div class="input-group-append">
                                        <span class="input-group-text h-100"><i class="mdi mdi-calendar"></i></span>
                                    </div>
                                </div>
	                        </div>
	                        <div class="col-6">
    	                        <div class="form-group row">
                    	            <label for="status" class="col-sm-12 control-label col-form-label"><strong>Trạng thái</strong></label>
                                    <div class="col-12">
                                        <?php 
                        					 if(isset($id))
                        					 {							 
                        					?>
                    						<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" <?php if($sqlcheck->status==1): ?>checked="checked"<?php endif; ?> />
                        						<?php         
                        					 }
                        						else
                        						{
                        						?>
                    							<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" />
                        						<?php             
                        					}
                    					?>	
                    					<label class="form-check-label mb-0" for="customControlAutosizing1">Xuất bản</label>
                                    </div>
                	            </div>
	                        </div>
	                    </div>
                    </div>
                    <div class="card-footer">
                        <center>
                		<?php 
                			if(isset($id))
                			{
                			?>			
            				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Lưu thay đổi</button>										
                			<?php    
                			}
                			else
                			{
                			?>			
            				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Nhập tin</button>					
                			<?php 
                			}
                		?>
                		<a href="<?php echo site_url('admin/lienhe'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                		</center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script >
<script type="text/javascript">
jQuery(function($){

	$.datepicker.regional['vi'] = {

		closeText: 'Đóng',

		prevText: '&#x3c;Trước',

		nextText: 'Tiếp&#x3e;',

		currentText: 'Hôm nay',

		monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',

		'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],

		monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',

		'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],

		dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],

		dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],

		dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],

		weekHeader: 'Tu',

		dateFormat: 'dd/mm/yy',

		firstDay: 0,

		isRTL: false,

		showMonthAfterYear: false,

		yearSuffix: ''};
		
	$.datepicker.setDefaults($.datepicker.regional['vi']);
});
$(function() {
	$( "#created_day" ).datepicker();
	$.datepicker.setDefaults($.datepicker.regional['vi']);
});
</script>
