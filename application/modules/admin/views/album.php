<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Album Ảnh</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Album Ảnh
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_album'); ?>">
            <div class="card">
                <div class="card-header" style="display:table;">
                    <p class="text-end" style="display:table-cell;">
                        <a href="<?php echo site_url('admin/frmalbum'); ?>" class="btn-sm btn btn-success text-white"><i class="fas fa-plus"></i> Thêm mới</a> 
                        <input type="submit" name="submit" value="Xóa" class="btn btn-danger btn-sm text-white" />
                    </p>
                </div>
                <div class="card-body">
                    <table width="100%" class="table">
                        <thead>
                            <tr>
                                <td scope="col"><input type="checkbox" onclick="checkall('checkbox', this)" name="check" class="form-check-input"/></td>
                                <td scope="col">Album</td>                
                                <td scope="col">Trạng thái</td>
                        		<td scope="col">#ID</td>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php 
                            if($query->num_rows() >0)
                            {
                        	?>
                            <?php 
                                $stt=0;
                        		foreach($query->result() as $item)
                                {
                        		$stt++;
                            ?>
                        		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
                        			<td scope="row">
                        			<div id="request-form">
                        				<input type="checkbox" name="checkbox[]" class="checkbox form-check-input" value="<?php echo $item->id; ?>" />
                        			</div>
                        			</td>
                                    <td><a href="<?php echo site_url('admin/edit_album/'.$item->id.'.html'); ?>"><?php echo $item->album; ?></a></td>								
                        			<td>						
                        			  <?php 
                        			  if($item->status=='1')
                        			  {
                        			  ?>
                        			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblalbum','album')"><img src="images/toolbar/tick.png"></a>
                        			  <?php
                        			  }
                        			  else
                        			  {
                        			  ?>
                        			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblalbum','album')"><img src="images/toolbar/publish_x.png"></a>
                        			  <?php
                        			  }
                        			  ?>			
                        			</td>       
                        			<td><?php echo $item->id; ?></td>
                        		</tr>		
                            <?php 
                            }
                            ?>
                        	<?php 
                        }
                        else
                        {
                        ?>
                        <tr><td></td><tr>
                        <?php    
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="clr"></div>
                    <div class="pagation">
                    	<?php echo $pagination; ?>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
