<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
//$category=$CI->admin_model->gettbl('tblchuyenmuc','')->result();	
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">
          <?php echo isset($id) ? 'Sửa chuyên mục' : 'Thêm chuyên mục' ?>
      </h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/chuyenmuc'); ?>">Chuyên mục</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                <?php echo isset($id) ? 'Sửa' : 'Thêm mới' ?>
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form name="frmdanhmuc" action="<?php echo site_url('xu-ly-chuyen-muc.html'); ?>" method="post" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header" style="display:table;">
                        <p class="text-end" style="display:table-cell;">
                            <a href="<?php echo site_url('admin/chuyenmuc'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                        </p>
                    </div>
                    <div class="card-body">
                        <?php 
                            if(isset($error))
                            {
                                echo '<div class="warning">'.$error.'</div>';
                            }
                            ?>
                            <div class="content-inner1">
                            		<?php 
                            			if(isset($id))
                            			{
                            				$this->db->where('id',$id);
                            				$item=$this->db->get('tblchuyenmuc')->row();
                            				$id=$item->id;            
                            			}
                            		?>    
                            		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />
                            		<div class="row">
                            		    <div class="col-6">
                            		        <div class="form-group row">
                            		            <label for="name" class="col-sm-12 control-label col-form-label"><strong>Name</strong></label>
                            		            <div class="col-sm-12">
                            		                <input type="text" class="form-control" name="name" value="<?php if(isset($id)) {echo $item->name;} ?>" />
                        		                </div>
                        		            </div>
                        		        </div>
                        		        <div class="col-6">
                            		        <div class="form-group row">
                            		            <label for="alias" class="col-sm-12 control-label col-form-label"><strong>Đường dẫn thân thiện</strong></label>
                            		            <div class="col-sm-12">
                            		                <input type="text" class="form-control" name="alias" value="<?php if(isset($id)) {echo $item->alias;} ?>" />
                        		                </div>
                        		            </div>
                        		        </div>
                        		    </div>
                        			<div class="row">
                        			    <div class="col-12">
                        			        <div class="form-group row">
                        			            <label for="image" class="col-sm-12 control-label col-form-label"><strong>Ảnh đại diện</strong></label>
                        			            <div class="col-sm-12">
                        			                <?php if(isset($id)){?>
                                        			<input type="hidden" name="image" style="width:250px;" value="<?php echo $item->image; ?>"> 
                                        			<input type="hidden" name="thumb" value="<?php echo $item->thumb; ?>">
                                        			<?php if($item->thumb !=''){?>
                                        			<img src="<?php echo $item->thumb; ?>" width="25" style="display:block;"><br />
                                        			<?php }} 			
                                        			?>
                                        			<input type="file" name="image" value="" />
                    			                </div>
                    			            </div>
                    			        </div>
                    			    </div>
                        			<div class="row">
                        			    <div class="col-6">
                        			        <div class="form-group row">
                        			            <label for="uid" class="col-sm-12 control-label col-form-label"><strong>Chuyên mục cha</strong></label>
                        			            <div class="col-sm-12">
                        			                <?php 
                                        				if(isset($id)){ $CI->admin_model->selectCtrl($item->uid,'uid', 'forFormDim');}
                                        				else{
                                        					$CI->admin_model->selectCtrl('','uid', 'forFormDim');
                                        				}
                                        			?>		
                    			                </div>
                    			            </div>
                    			        </div>
                    			        <div class="col-6">
                    			            <label class="col-sm-12 control-label col-form-label"><strong>Chọn vị trí</strong></label>
                			                <?php 
                                			if(isset($id))
                                			{							 
                                			?>
                                				<input class="news_checkbox form-check-input" type="checkbox" name="menu" value="1" <?php if($item->menu==1): ?>checked="checked"<?php endif; ?> />				
                                				<?php         
                                			}
                                			else
                                			{
                                			?>
                                				<input class="news_checkbox form-check-input" type="checkbox" name="menu" value="1" />				
                                			<?php             
                                			}
                                			?>	
                                			<label class="form-check-label mb-0">Hiển thị menu</label>
                                            <?php 
                                			if(isset($id))
                                			{							 
                                			?>
                                				<input class="news_checkbox form-check-input" type="checkbox" name="trai" value="1" <?php if($item->trai==1): ?>checked="checked"<?php endif; ?> />			
                                				<?php         
                                			}
                                			else
                                			{
                                			?>
                                				<input class="news_checkbox form-check-input" type="checkbox" name="trai" value="1" />			
                                			<?php             
                                			}
                                			?>	
                                			<label class="form-check-label mb-0">Hiển thị Left</label>
                                            <?php 
                                			if(isset($id))
                                			{							 
                                			?>
                                				<input class="news_checkbox form-check-input" type="checkbox" name="home" value="1" <?php if($item->home==1): ?>checked="checked"<?php endif; ?> />				
                                				<?php         
                                			}
                                			else
                                			{
                                			?>
                                				<input class="news_checkbox form-check-input" type="checkbox" name="home" value="1" />		
                                			<?php             
                                			}
                                			?>	
                                			<label class="form-check-label mb-0">Hiển thị Home</label>
                                            <?php 
                                			if(isset($id))
                                			{							 
                                			?>
                                				<input class="news_checkbox form-check-input" type="checkbox" name="tintuc" value="1" <?php if($item->tintuc==1): ?>checked="checked"<?php endif; ?> />			
                                				<?php         
                                			}
                                			else
                                			{
                                			?>
                                				<input class="news_checkbox form-check-input" type="checkbox" name="tintuc" value="1" />				
                                			<?php             
                                			}
                                			?>	
                                			<label class="form-check-label mb-0">Hiển thị Tin tức</label>
                                            <?php 
                                			if(isset($id))
                                			{							 
                                			?>
                                				<input class="news_checkbox form-check-input" type="checkbox" name="footer" value="1" <?php if($item->footer==1): ?>checked="checked"<?php endif; ?> />				
                                				<?php         
                                			}
                                			else
                                			{
                                			?>
                                				<input class="news_checkbox form-check-input" type="checkbox" name="footer" value="1" />			
                                			<?php             
                                			}
                                			?>
                                			<label class="form-check-label mb-0">Hiển thị Footer</label>
                			            </div>
                    			    </div>
                            		<div class="row">
                            		    <div class="col-12">
                            		        <div class="form-group row">
                        			            <label for="theh" class="col-sm-12 control-label col-form-label"><strong>Thẻ Heading</strong></label>
                        			            <div class="col-sm-12">
                        			                <textarea class="form-control" name="theh" id="theh"><?php if(isset($id)){ echo $item->theh;}?></textarea>
                    			                </div>
                    			            </div>
                            		    </div>
                        		    </div>
                        		    <div class="row">
                        		        <div class="col-4">
                        		            <div class="form-group row">
                        			            <label for="meta_title" class="col-sm-12 control-label col-form-label"><strong>Meta Title</strong></label>
                        			            <div class="col-sm-12">
                        			                <textarea class="form-control" name="meta_title"><?php if(isset($id)){ echo $item->meta_title;}?></textarea>
                    			                </div>
                    			            </div>
                    		            </div>
                    		            <div class="col-4">
                        		            <div class="form-group row">
                        			            <label for="meta_des" class="col-sm-12 control-label col-form-label"><strong>Meta Description</strong></label>
                        			            <div class="col-sm-12">
                        			                <textarea class="form-control" name="meta_des"><?php if(isset($id)){ echo $item->meta_des;}?></textarea>
                    			                </div>
                    			            </div>
                    		            </div>
                    		            <div class="col-4">
                        		            <div class="form-group row">
                        			            <label for="keyword" class="col-sm-12 control-label col-form-label"><strong>Keyword</strong></label>
                        			            <div class="col-sm-12">
                        			                <textarea class="form-control" name="keyword"><?php if(isset($id)){ echo $item->keyword;}?></textarea>
                    			                </div>
                    			            </div>
                    		            </div>
                    		        </div>
                    		        <div class="row">
                    		            <div class="col-6">
                    		                <div class="form-group row">
                    		                    <label for="thutu" class="col-sm-12 control-label col-form-label"><strong>Thứ tự</strong></label>
                    		                    <div class="col-sm-12">
                    		                        <input class="form-control" type="text" name="thutu" value="<?php if(isset($id)){ echo $item->thutu;}?>" />
                		                        </div>
                		                    </div>
                		                </div>
                		                <div class="col-6">
                		                    <div class="form-group row">
                		                        <label for="thutu" class="col-sm-12 control-label col-form-label"><strong>Trạng thái</strong></label>
                		                        <div class="col-sm-12">
                		                            <?php 
                                        			 if(isset($id))
                                        			 {							 
                                        			?>
                                        				<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />			
                                        				<?php         
                                        			 }
                                        				else
                                        				{
                                        				?>
                                        					<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" checked="checked" />
                                        				<?php             
                                        			}
                                        			?>
                                        			<label class="form-check-label mb-0">Xuất bản</label>
            		                            </div>
            		                        </div>
            		                    </div>
                    		        </div>
                            </div>
                    </div>
                    <div class="card-footer">
                        <center>
                    		<?php 
                    			if(isset($id))
                    			{
                    			?>			
                    				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Lưu thay đổi</button>				
                    			<?php    
                    			}
                    			else
                    			{
                    			?>			
                    				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Nhập tin</button>				
                    			<?php 
                    			}
                    		?>
                    		<a href="<?php echo site_url('admin/chuyenmuc'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                		</center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Tích hợp jck soạn thảo-->
<script type="text/javascript">
//CKEDITOR.replace('theh');	
var editor1 = CKEDITOR.replace('theh', {
      extraAllowedContent: 'div'
});
</script>