<h3 class="header">Thêm Tin dạng quận huyện</h3>
<?php 
if(isset($error))
{
    echo '<div class="warning">'.$error.'</div>';
}
?>
<div class="content-inner">
	<form name="frmvideo" action="<?php echo site_url('xu-ly-quan.html'); ?>" method="post" enctype="multipart/form-data">
		<?php 
			if(isset($id))
			{
				$this->db->where('id',$id);
				$item=$this->db->get('tblquan')->row();
				$id=$item->id;            
			}
		?>    
		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />	
		<div class="gray">
		<table class="tab1">
		<tr>
			<td width="250"><strong>Quận huyện</strong></td>
			<td>
				<input type="text" name="quan" value="<?php if(isset($id)){ echo $item->quan;} ?>" />
			</td>
		</tr>	
		<tr class="second">
			<td><strong>Tỉnh thành</strong></td>
			<td>
				<select name="tinhthanh">
                    <option value="-1">--Chọn tỉnh thành--</option>
                    <?php 
                        $this->db->select('id,tinh');
                        $sqltinhq=$this->db->get('tbltinh');
                        foreach($sqltinhq->result() as $itemtinhq)
                        {
                            if(isset($id))
                            {
                                if($item->tinh==$itemtinhq->id)
                                {
                                ?>
                                <option value="<?php echo $itemtinhq->id; ?>" selected="selected"><?php echo $itemtinhq->tinh; ?></option>
                                <?php    
                                }
                                else
                                {
                                ?>
                                <option value="<?php echo $itemtinhq->id; ?>"><?php echo $itemtinhq->tinh; ?></option>
                                <?php    
                                }
                            }
                            else
                            {
                            ?>
                            <option value="<?php echo $itemtinhq->id; ?>"><?php echo $itemtinhq->tinh; ?></option>
                            <?php 
                            }   
                        }
                    ?>
                </select>
			</td>
		</tr>	
		<tr class="second">
			<td><strong>Thứ tự</strong></td>
			<td>
				<input style="width:50px;" type="text" name="thutu" value="<?php if(isset($id)){ echo $item->thutu;} ?>" />
			</td>
		</tr>	
		<tr><td width="200">
			<strong>Trạng thái</strong></td>
			<td>
			<p class="message_head">									
					<?php 
					 if(isset($id))
					 {							 
					?>
						<input class="news_checkbox" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />Xuất bản				
						<?php         
					 }
						else
						{
						?>
							<input class="news_checkbox" type="checkbox" name="status" value="1" checked="checked"/>Xuất bản
						<?php             
					}
					?>			
				</p>
		</td></tr>
		</table>
		</div>
		<div class="gray">
		<center>
		<?php 
			if(isset($id))
			{
			?>			
				<input class="button" type="submit" name="submit" value="Lưu thay đổi" />						
			<?php    
			}
			else
			{
			?>			
				<input class="button" type="submit" name="submit" value="Nhập tin" />					
			<?php 
			}
		?>
		</center>
		</div>
	</form>
	<div class="clr"></div>
</div>