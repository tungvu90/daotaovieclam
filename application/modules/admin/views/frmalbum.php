<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">
          <?php echo isset($id) ? 'Sửa album' : 'Thêm album' ?>
    </h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/album'); ?>">Album Ảnh</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                <?php echo isset($id) ? 'Sửa' : 'Thêm mới' ?>
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header" style="display:table">
                    <?php 
                        if(isset($id)) {
                        ?>
                        <p class="text-end" style="display:table-cell;">
                            <button id="save_info" class="btn btn-info btn-sm"><i class="mdi mdi-content-save"></i> Cập nhật thông tin</button>
                            <a href="<?php echo site_url('admin/album'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                        </p>
                        <div style="clear:both"></div>
                        <?php
                        }else{
                            ?>
                            <p class="text-end" style="display:table-cell;">
                            <a href="<?php echo site_url('admin/album'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                            </p>
                            <?php
                        }
                    ?>
                </div>
                <div class="card-body">
                    <?php
                        if(isset($error)) {
                            echo '<div class="warning">'.$error.'</div>';
                        }
                        ?>
                    	<form name="frmslider" id="image_upload_form" action="<?php echo site_url('admin/add_album'); ?>" method="post" enctype="multipart/form-data" class="dropzone">
                    		<?php 
                    			if(isset($id)) {
                    				$this->db->where('id',$id);
                    				$sqlcheck=$this->db->get('tblalbum')->row();
                    				$id=$sqlcheck->id;            
                    				$this->db->where('album_id',$id);
                    				$sql_images = $this->db->get('tblalbum_images')->result();
                    			}
                    		?>    
                    		<input type="hidden" name="id" id="id" value="<?php if(isset($id)) { echo $id; }; ?>" />
                    		<div class="row">
                    		    <div class="col-6">
                    		        <div class="form-group">
                    		            <label for="title" class="col-sm-12 control-label col-form-label"><strong>Title</strong></label>
                    		            <div class="col-sm-12">
                    		                <input type="text" class="form-control" id="title" name="title" value="<?php if(isset($id)) { echo $sqlcheck->album; }; ?>" />
                		                </div>
                		            </div>
                		        </div>
                		        <div class="col-6">
                		            <div class="form-group">
                		                <label for="ordernum" class="col-sm-12 control-label col-form-label"><strong>Thứ tự</strong></label>
                		                <div class="col-sm-12">
                    		                <input type="text" class="form-control" id="ordernum" name="ordernum" value="<?php if(isset($id)) { echo $sqlcheck->thutu; }; ?>" />
                		                </div>
            		                </div>
            		            </div>
                		    </div>
                		    <div class="row">
                		        <div class="col-12">
                		            <div class="form-group">
                    		            <label for="status" class="col-sm-12 control-label col-form-label"><strong>Trạng thái</strong></label>
                    		            <div class="col-sm-12">
                    		                <?php 
                    					 if(isset($id)) {							 
                    					    ?>
                    						<input class="news_checkbox news_checkbox_edit form-check-input" type="checkbox" name="status" value="1" <?php if($sqlcheck->status==1): ?>checked="checked"<?php endif; ?> />			
                    						<?php         
                    					 } else {
                    						?>
                    						<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" />
                    						<?php             
                    					}
                    					?>	
                    					<label class="form-check-label mb-0" for="customControlAutosizing1">Xuất bản</label>
                		                </div>
                		            </div>
            		            </div>
            		            <div class="col-12">
            		                <label class="col-sm-12 control-label col-form-label"><strong>Ảnh</strong></label>
            		                <div class="col-sm-12">
            		                    
        		                    </div>
        		                </div>
            		        </div>
                    	</form>
                        	<br>
                        	<?php 
                        	    if(isset($id)){
                        	    ?>
                        	    <div class="row">
                        	        <div class="col-12">
                        	            <h3>Danh sách hình ảnh</h3>
                        	        </div>
                        	    </div>
                        	    <div class="row">
                        	        <?php 
                        	        $dem_img = 1;
                        	        foreach($sql_images as $item_images){
                            	        ?>
                                    	   <div class="item_image col-3">
                                    	       <button class="delete_image btn btn-danger btn-sm text-white" data-id="<?php echo $item_images->id; ?>">Xoá</button>
                                    	       <img src="<?php echo $item_images->images; ?>" >
                                    	       <div class="item_image_<?php echo $item_images->id; ?>">
                                    	            <textarea class="form-control image_title" name="image_title" data-id="<?php echo $item_images->id; ?>" placeholder="Nhập tên hình ảnh"><?php echo $item_images->title; ?></textarea>
                                    	       </div>
                                    	   </div>
                            	        <?php
                                	    if($dem_img%4 == 0){
                                	        echo '<div style="clear:both;"></div>';
                                	    }
                                	    ++$dem_img;
                            	    }
                            	    ?>
                        	    </div>
                            	    <?php
                        	    }
                        	?>
                        	<div class="clr"></div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    new Dropzone("#image_upload_form", { 
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        dictDefaultMessage: "Chọn hình ảnh",
        thumbnailWidth: 700,
		maxFilesize: 2, // MB
		init: function() {
			this.on("complete", function(file, responseText) {
				location.reload()
			});
		}
	});
	$(function() {
	    $('.image_title').keyup(function() {
	        var image_title = $(this).val();
	        var image_id = $(this).data('id');
	        $.ajax({
    			url:"<?php echo site_url('admin/do_edit_image_title')?>",
    			type:"POST",
    			data:{id : image_id,title : image_title},
    			success: function(html) {
    			    $(this).val().focus();
               	    $(".item_image_" + image_id).html(html);
           		},
               	error: function() {
                    alert("Invalide!");
                }
    		});
	    });
	    $("#save_info").click(function(){
	       var title = $('#title').val();
	       var id = $('#id').val();
	       var ordernum = $("#ordernum").val();
	       var status = 0;
	       if ($('.news_checkbox_edit').is(":checked")){
	           status = 1;
	       }
	       $.ajax({
    			url:"<?php echo site_url('admin/do_edit_album')?>",
    			type:"POST",
    			data:{id : id,title : title,ordernum : ordernum,status : status},
    			success: function(data) {
               	    alert('Cập nhật thành công');
               	    window.location.reload();
           		},
               	error: function() {
                    alert("Invalide!");
                }
    		});
	    });
	    $('.delete_image').click(function(){
	       var images_id = $(this).data('id');
	       var checkstr =  confirm('Bạn có thực sự muốn xoá ảnh này không?');
	       if(checkstr == true) {
    	       $.ajax({
        			url:"<?php echo site_url('admin/delete_image')?>",
        			type:"POST",
        			data:{id : images_id},
        			success: function(data) {
                   	    alert('Xoá ảnh thành công');
                   	    window.location.reload();
               		},
                   	error: function() {
                        alert("Invalide!");
                    }
        		});
	       } else{
	           return false;
	       }
	    });
	});
</script>