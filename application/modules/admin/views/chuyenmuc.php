<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Chuyên mục</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Chuyên mục
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_chuyenmuc'); ?>">
                <div class="card">
                    <div class="card-header" style="display:table;">
                        <p class="text-end" style="display:table-cell;">
                            <a href="<?php echo site_url('them-chuyen-muc.html'); ?>" class="btn-sm btn btn-success text-white"><i class="fas fa-plus"></i> Thêm mới</a> 
                            <input type="submit" name="submit" value="Xóa" class="btn btn-danger btn-sm text-white" />
                        </p>
                    </div>
                    <div class="card-body">
                        <table width="100%" class="table">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" onclick="checkall('checkbox', this)" name="check" class="form-check-input"/></th>        
                                    <th>Name</th>
                                    <th>Alias</th>
                            		<th>Chuyên mục cha</th>
                                    <th>Hiển thị Menu</th>
                                    <th>Hiển thị Left</th>
                                    <th>Hiển thị Home</th>
                                    <th>Hiển thị Tin tức</th>
                                    <th>Hiển thị Footer</th>
                                    <th>Thứ tự</th>       
                                    <th>Trạng thái</th>
                            		<th>#ID</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php 
                            if($query->num_rows() >0)
                            {
                        ?>	
                            <?php 
                                $stt=0;
                        		foreach($query->result() as $item)
                                {
                        		$stt++;
                            ?>
                        		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
                        			<td style="text-align:center;">
                        			<div id="request-form">
                        				<input type="checkbox" name="checkbox[]" class="checkbox form-check-input" value="<?php echo $item->id; ?>" />
                        			</div>
                        			</td>					
                        			<td width="15%"><a href="<?php echo site_url('admin/edit_chuyenmuc/'.$item->id.'-'.url_title($item->alias).'.html'); ?>"><?php echo $item->name; ?></a></td>			
                        			<td><?php echo $item->alias; ?></td>
                        			<td>
                        			<?php 
                        				if($item->uid != 0)
                        				{
                        					$this->db->where('id',$item->uid);
                        					$chuyenmuc=$this->db->get('tblchuyenmuc')->row();					
                        					echo $chuyenmuc->name;            
                        				}
                        				else{
                        					echo 'Không';
                        				}
                        			?>  
                        			</td>
                        			<td style="text-align:center;"><?php echo $item->menu!=0?'Có':'Không'; ?></td>
                                    <td style="text-align:center;"><?php echo $item->trai!=0?'Có':'Không'; ?></td>
                                    <td style="text-align:center;"><?php echo $item->home!=0?'Có':'Không'; ?></td>
                                    <td style="text-align:center;"><?php echo $item->tintuc!=0?'Có':'Không'; ?></td>
                                    <td style="text-align:center;"><?php echo $item->footer!=0?'Có':'Không'; ?></td>
                        			<td style="text-align:center;"><?php echo $item->thutu; ?></td>
                        			<td style="text-align:center;">						
                        			  <?php 
                        			  if($item->status=='1')
                        			  {
                        			  ?>
                        			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblchuyenmuc','chuyenmuc')"><img src="images/toolbar/tick.png"></a>
                        			  <?php
                        			  }
                        			  else
                        			  {
                        			  ?>
                        			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblchuyenmuc','chuyenmuc')"><img src="images/toolbar/publish_x.png"></a>
                        			  <?php
                        			  }
                        			  ?>			
                        			</td>       
                        			<td style="text-align:center;"><?php echo $item->id; ?></td>
                        		</tr>
                            <?php 
                            }
                            ?>
                        <?php 
                        }
                        else
                        {
                        ?>
                        <tr><td colspan="6"></td></tr>
                        <?php    
                        }
                        ?>
                        </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="pagation">
                        	<?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
