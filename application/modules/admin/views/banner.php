﻿<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_banner'); ?>">
<p class="sidebar"><a href="<?php echo site_url('them-banner.html'); ?>">Thêm mới</a> <input type="submit" name="submit" value="Xóa" /></p>
<table width="100%">
    <tr class="title">
        <td width="5%" style="text-align:center;"><input type="checkbox" onclick="checkall('checkbox', this)" name="check"/></td>        
        <td>Tên</td>       
        <td>Tên file</td>    
        <td>Tỉnh thành</td>    
        <td width="25%">Link liên kết</td>
		<td width="5%">Vị trí</td>
        <td width="8%">Trạng thái</td>
		<td width="5%" style="text-align:center;">id</td>
    </tr>
	<?php 
    if($query->num_rows() >0)
    {
	?>
    <?php 
        $stt=0;
		foreach($query->result() as $item)
        {
		$stt++;
    ?>
		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
			<td style="text-align:center;">
				<div id="request-form">
					<input type="checkbox" name="checkbox[]" class="checkbox" value="<?php echo $item->id; ?>" />
				</div>
			</td>					
			<td><a href="<?php echo site_url('admin/edit_banner/'.$item->id.'.html'); ?>"><?php echo $item->name; ?></a></td>							
			<td><?php echo $item->file; ?></td>	
            <td>
            <?php 
                $CI=&get_instance();
                $CI->load->model('admin/admin_model');
                $tinh=$CI->admin_model->gettbl('tbltinh',$item->tinh);
                if($tinh->num_rows()>0)
                {
                    echo $tinh=$tinh->row()->tinh;
                }
                else
                {
                    echo 'Không';
                }
            ?>
            </td>						
			<td><?php echo $item->link; ?></td>
			<td style="text-align:center;"><?php echo strtoupper($item->vitri); ?></td>
			<td style="text-align:center;">						
			  <?php 
			  if($item->status=='1')
			  {
			  ?>
			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblbanner','banner')"><img src="images/toolbar/tick.png"></a>
			  <?php
			  }
			  else
			  {
			  ?>
			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblbanner','banner')"><img src="images/toolbar/publish_x.png"></a>
			  <?php
			  }
			  ?>			
			</td>       
			<td style="text-align:center;"><?php echo $item->id; ?></td>
		</tr>		
    <?php 
    }
    ?>
	<?php 
}
else
{
?>
<tr><td></td><tr>
<?php    
}
?>
</table>
</form>
<div class="clr"></div>
<div class="pagation">
	<?php echo $pagination; ?>
</div>
