<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
?>
<?php 
$this->db->where('status',1);
$this->db->where('name',$_SESSION['name']);
$admin=$this->db->get('tbladmin');	
$admin=$admin->row(); 
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">
          <?php echo isset($id) ? 'Sửa thành viên' : 'Thêm thành viên'; ?>
      </h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/tbladmin1'); ?>">Thành viên</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                <?php echo isset($id) ? 'Sửa thành viên' : 'Thêm thành viên'; ?>
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <?php 
                if(isset($error)) {
                    echo '<div class="warning">'.$error.'</div>';
                }
            ?>
            <form name="frmdanhmuc" action="<?php echo site_url('admin/add_admin1'); ?>" method="post" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header" style="display:table;">
                        <p class="text-end" style="display:table-cell;">
                            <a href="<?php echo site_url('admin/tbladmin1'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                        </p>
                    </div>
                    <div class="card-body">
                        <?php 
                			if(isset($id))
                			{
                				$this->db->where('id',$id);
                				$item=$this->db->get('tbladmin1')->row();
                				$id=$item->id;            
                			}
                		?>    		
                		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />
                		<div class="row">
                    	    <div class="col-6">
                                <div class="form-group row">
                                    <label for="taikhoan" class="col-sm-12 control-label col-form-label"><strong>Tài khoản</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="taikhoan" value="<?php if(isset($id)) {echo $item->taikhoan;} ?>" />
                                    </div>
                                </div>
                            </div>
                            <?php 
                                if(isset($id)){
                                    
                                }else{
                                ?>
                                <div class="col-6">
                                    <div class="form-group row">
                                        <label for="matkhau" class="col-sm-12 control-label col-form-label"><strong>Mật khẩu</strong></label>
                                        <div class="col-12">
                                            <input type="password" class="form-control" name="matkhau" value="<?php if(isset($id)){ echo $item->matkhau;} ?>" />
                                        </div>
                                    </div>
                                </div>
                                <?php 
                                }
                            ?>
                    	    <div class="col-6">
                                <div class="form-group row">
                                    <label for="hoten" class="col-sm-12 control-label col-form-label"><strong>Họ tên</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="hoten" value="<?php if(isset($id)) {echo $item->hoten;} ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row">
                                    <label for="diachi" class="col-sm-12 control-label col-form-label"><strong>Địa chỉ</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="diachi" value="<?php if(isset($id)){ echo $item->diachi;} ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row">
                                    <label for="dienthoai" class="col-sm-12 control-label col-form-label"><strong>Điện thoại</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="dienthoai" value="<?php if(isset($id)){ echo $item->dienthoai;} ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row">
                                    <label for="email" class="col-sm-12 control-label col-form-label"><strong>Email</strong></label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" name="email" value="<?php if(isset($id)){ echo $item->email;} ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group row">
                                    <label for="quyen" class="col-sm-12 control-label col-form-label"><strong>Quyền hạn</strong></label>
                                    <div class="col-12">
                                        <select name="quyen" class="form-control">
                                            <option value="-1">--Chọn quyền--</option>
                                            <?php 
                                                $this->db->where('status',1);   
                                                $sqlrole=$this->db->get('tblrole'); 
                                                if($sqlrole->num_rows()>0)
                                                {
                                                    if(isset($id))
                                                    {
                                                        foreach($sqlrole->result() as $itemrole)
                                                        {
                                                            if($itemrole->id==$item->role)
                                                            {
                                                            ?>
                                                            <option value="<?php echo $itemrole->id; ?>" selected="selected"><?php echo $itemrole->name; ?></option>
                                                            <?php    
                                                            }
                                                            else
                                                            {
                                                            ?>
                                                            <option value="<?php echo $itemrole->id; ?>"><?php echo $itemrole->name; ?></option>
                                                            <?php    
                                                            }
                                                        }        
                                                    }
                                                    else
                                                    {
                                                        foreach($sqlrole->result() as $itemrole)
                                                        {
                                                            ?>
                                                            <option value="<?php echo $itemrole->id; ?>"><?php echo $itemrole->name; ?></option>
                                                            <?php    
                                                        }
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                    	</div>
                    	<div class="row">
                    	    <div class="col-12">
                    	        <div class="form-group row">
                                    <label for="status" class="col-sm-12 control-label col-form-label"><strong>Trạng thái</strong></label>
                                    <div class="col-12">
                                        <?php 
                            			 if(isset($id))
                            			 {							 
                            			?>
                            				<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />
                            				<?php         
                            			 }
                            				else
                            				{
                            				?>
                            					<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" />
                            				<?php             
                            			}
                            			?>
                            			<label class="form-check-label mb-0">Kích hoạt</label>
                                    </div>
                                </div>
                    	    </div>
                    	</div>
                    </div>
                    <div class="card-footer">
                        <center>
                    		<?php 
                    			if(isset($id))
                    			{
                    			?>			
                				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Lưu thay đổi</button>										
                    			<?php    
                    			}
                    			else
                    			{
                    			?>			
                				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Nhập tin</button>					
                    			<?php 
                    			}
                    		?>
                    		<a href="<?php echo site_url('admin/tbladmin1'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                		</center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>