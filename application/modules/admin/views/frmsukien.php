﻿<h3 class="header">Thêm Sự kiện</h3>
<?php 
if(isset($error))
{
    echo '<div class="warning">'.$error.'</div>';
}
?>
<div class="content-inner">
	<form name="frmsukien" action="<?php echo site_url('xu-ly-sukien.html'); ?>" method="post" enctype="multipart/form-data">
		<?php 
			if(isset($id))
			{
				$this->db->where('id',$id);
				$item=$this->db->get('tblsukien')->row();
				$id=$item->id;            
			}
		?>    
		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />	
		<div class="gray">
		<table class="tab1">		
		<tr><td width="200">
			<strong>Tên sự kiện</strong></td>
			<td>
			<input type="text" name="name" value="<?php if(isset($id)) { echo $item->name; }; ?>" />			
		</td></tr>
		<tr><td width="200">
			<strong>Bí danh tiêu đề</strong></td>
			<td>
				<input type="text" name="alias" value="<?php if(isset($id)) { echo $item->alias; }; ?>" />
		</td></tr>
		<tr><td width="200">			
			<td>
			<p class="message_head">									
					<?php 
					 if(isset($id))
					 {							 
					?>
						<input class="news_checkbox" type="checkbox" name="showed" value="1" <?php if($item->showed==1): ?>checked="checked"<?php endif; ?> />Hiển thị trang chủ				
						<?php         
					 }
						else
						{
						?>
							<input class="news_checkbox" type="checkbox" name="showed" value="1" />Hiển thị trang chủ
						<?php             
					}
					?>			
				</p>
		</td></tr>
		<tr><td width="200">			
			<td>
			<p class="message_head">									
					<?php 
					 if(isset($id))
					 {							 
					?>
						<input class="news_checkbox" type="checkbox" name="checked" value="1" <?php if($item->checked==1): ?>checked="checked"<?php endif; ?> />Sự kiện chính				
						<?php         
					 }
						else
						{
						?>
							<input class="news_checkbox" type="checkbox" name="checked" value="1" />Sự kiện chính
						<?php             
					}
					?>			
				</p>
		</td></tr>
		<tr><td width="200">
			<strong>Trạng thái</strong></td>
			<td>
			<p class="message_head">									
					<?php 
					 if(isset($id))
					 {							 
					?>
						<input class="news_checkbox" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />Xuất bản				
						<?php         
					 }
						else
						{
						?>
							<input class="news_checkbox" type="checkbox" name="status" value="1" checked="checked" />Xuất bản
						<?php             
					}
					?>			
				</p>
		</td></tr>
		</table>
		</div>
		<div class="gray">
		<center>
		<?php 
			if(isset($id))
			{
			?>			
				<input class="button" type="submit" name="submit" value="Lưu thay đổi" />						
			<?php    
			}
			else
			{
			?>			
				<input class="button" type="submit" name="submit" value="Nhập tin" />					
			<?php 
			}
		?>
		</center>
		</div>
	</form>
	<div class="clr"></div>
</div>
