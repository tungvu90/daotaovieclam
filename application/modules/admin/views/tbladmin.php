﻿<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_thanhvien'); ?>">
<p class="sidebar"><a href="<?php echo site_url('them-thanh-vien.html'); ?>">Thêm mới</a> <input type="submit" name="submit" value="Xóa" /></p>
<table width="100%">
    <tr class="title">
        <td width="5%" style="text-align:center;"><input type="checkbox" onclick="checkall('checkbox', this)" name="check"/></td>        
        <td>Tài khoản</td>
        <td width="10%">Mật khẩu</td>
        <td>Email</td>
		<td width="15%">Họ tên</td>    
        <td width="10%">Trạng thái</td>
		<td width="8%" style="text-align:center;">id</td>
    </tr>
<?php 
    if($query->num_rows() >0)
    {
?>
    <?php 
        $stt=0;
		foreach($query->result() as $item)
        {
		$stt++;			
    ?>
		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
			<td style="text-align:center;">
			<div id="request-form">
				<input type="checkbox" name="checkbox[]" class="checkbox" value="<?php echo $item->id; ?>" />
			</div>
			</td>					
			<td><a href="<?php echo site_url('admin/edit_thanhvien/'.$item->id); ?>"><?php echo $item->name; ?></a></td>			
			<td><?php echo $item->pass; ?></td>
            <td><?php echo $item->email; ?></td>
			<td><?php echo $item->fullname; ?></td>						
			<td style="text-align:center;">						
			  <?php 
			  if($item->status=='1')
			  {
			  ?>
			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tbladmin','tbladmin')"><img src="images/toolbar/tick.png"></a>
			  <?php
			  }
			  else
			  {
			  ?>
			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tbladmin','tbladmin')"><img src="images/toolbar/publish_x.png"></a>
			  <?php
			  }
			  ?>			
			</td>          
			<td style="text-align:center;"><?php echo $item->id; ?></td>
		</tr>
    <?php 
    }
}
else
{
?>
<tr>
	<td></td>						
	<td></td>						
	<td></td>						
	<td></td>						
	<td></td>						
	<td></td>						
	<td></td>						
	<td></td>						
</tr>
<?php    
}
?>
</table>
</form>
<div class="clr"></div>
<div class="pagation">
	<?php echo $pagination; ?>
</div>
