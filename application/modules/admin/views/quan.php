<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_quan'); ?>">
<p class="sidebar"><a href="<?php echo site_url('them-quan.html'); ?>">Thêm mới</a> <input type="submit" name="submit" value="Xóa" /></p>
<table width="100%">
    <tr class="title">
        <td width="5%" style="text-align:center;"><input type="checkbox" onclick="checkall('checkbox', this)" name="check"/></td>        
        <td width="30%">Quận huyện</td>
        <td width="30%">Tỉnh</td>
        <td width="20%">Thứ tự</td>
        <td>Trạng thái</td>
		<td width="8%" style="text-align:center;">id</td>
    </tr>
	<?php 
    if($query->num_rows() >0)
    {
	?>
    <?php 
        $stt=0;
		foreach($query->result() as $item)
        {
		$stt++;
    ?>
		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
			<td style="text-align:center;">
			<div id="request-form">
				<input type="checkbox" name="checkbox[]" class="checkbox" value="<?php echo $item->id; ?>" />
			</div>
			</td>					
			<td><a href="<?php echo site_url('admin/edit_quan/'.$item->id.'.html'); ?>"><?php echo $item->quan; ?></a></td>						
            <td>
            <?php 
            $this->db->where('id',$item->tinh);
            $sqltinh=$this->db->get('tbltinh')->row();
            echo $sqltinh->tinh; ?></td>
            <td><?php echo $item->thutu; ?></td>
			<td style="text-align:center;">						
			  <?php 
			  if($item->status=='1')
			  {
			  ?>
			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblquan','quan')"><img src="images/toolbar/tick.png"></a>
			  <?php
			  }
			  else
			  {
			  ?>
			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblquan','quan')"><img src="images/toolbar/publish_x.png"></a>
			  <?php
			  }
			  ?>			
			</td>       
			<td style="text-align:center;"><?php echo $item->id; ?></td>
		</tr>		
    <?php 
    }
    ?>
	<?php 
}
else
{
?>
<tr><td></td><tr>
<?php    
}
?>
</table>
</form>
<div class="clr"></div>
<div class="pagation">
	<?php echo $pagination; ?>
</div>
