<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
$thongtinseo=$CI->admin_model->gettbl('tblmeta','')->row();
$footer=$CI->admin_model->gettbl('tblfooter','')->row();	
$this->db->where('status',1);
$this->db->where('taikhoan',$_SESSION['name']);
$admin=$this->db->get('tbladmin1')->row();	
?>
<aside class="left-sidebar" data-sidebarbg="skin5">
    <div class="scroll-sidebar">
      <nav class="sidebar-nav">
        <ul id="sidebarnav" class="pt-4">
          <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo site_url('admin/'); ?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a>
          </li>
          <?php 
            if($admin->role==1)
            {
            ?>      
              <li class="sidebar-item">
                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo site_url('admin/tbladmin1'); ?>" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Administrators</span></a>
              </li>
          <?php 
            }
          ?>
          <li class="sidebar-item">
            <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-format-list-numbers"></i><span class="hide-menu">Quản trị bài viết</span></a>
            <ul aria-expanded="false" class="collapse first-level">
             <?php 
                if($admin->role==1)
                {
                ?>
                  <li class="sidebar-item">
                    <a href="<?php echo site_url('admin/chuyenmuc/'); ?>" class="sidebar-link"><i class="mdi mdi-format-list-numbers"></i><span class="hide-menu">Chuyên mục</span></a>
                  </li>
              <?php 
                }
                if($admin->role==1 || $admin->role==3 || $admin->role==4)
                {
                ?>
                  <li class="sidebar-item">
                    <a href="<?php echo site_url('admin/baiviet/'); ?>" class="sidebar-link"><i class="mdi mdi-pencil"></i><span class="hide-menu">Bài viết</span></a
                    >
                  </li>
              <?php 
                }
              ?>
            </ul>
          </li>
          <?php             
            if($admin->role==1)
            {
            ?>
            <li class="sidebar-item">
                <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-image-multiple"></i><span class="hide-menu">Quản lý hình ảnh</span></a>
                <ul aria-expanded="false" class="collapse first-level">
                    <li class="sidebar-item">
                        <a class="sidebar-link" href="<?php echo site_url('admin/slider'); ?>" aria-expanded="false"><i class="mdi mdi-file-image"></i><span class="hide-menu">Slider</span></a>
                     </li>
                     <li class="sidebar-item">
                        <a class="sidebar-link" href="<?php echo site_url('admin/album'); ?>" aria-expanded="false"><i class="mdi mdi-image-multiple"></i><span class="hide-menu">Album ảnh</span></a>
                      </li>
                      <li class="sidebar-item">
                        <a class="sidebar-link" href="<?php echo site_url('admin/quangcao'); ?>" aria-expanded="false"><i class="mdi mdi-image-broken-variant"></i><span class="hide-menu">Quảng cáo</span></a>
                      </li>
                </ul>
            </li>
          <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo site_url('admin/support'); ?>" aria-expanded="false"><i class="mdi mdi-cellphone-iphone"></i><span class="hide-menu">Hỗ trợ trực tuyến</span></a>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo site_url('admin/doitac'); ?>" aria-expanded="false"><i class="fas fa-people-carry"></i><span class="hide-menu">Quản lý Đối tác</span></a>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo site_url('admin/link'); ?>" aria-expanded="false"><i class="mdi mdi-link-variant"></i><span class="hide-menu">Quản lý Text Link</span></a>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo site_url('admin/dangkyhoc'); ?>" aria-expanded="false"><i class="mdi mdi-blur-linear"></i><span class="hide-menu">Đăng ký học</span></a>
          </li>
          <li class="sidebar-item">
            <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-move-resize-variant"></i><span class="hide-menu">Other</span></a>
            <ul aria-expanded="false" class="collapse first-level">
              <li class="sidebar-item">
                <a href="<?php if (count($thongtinseo)==0){echo site_url('them-thong-tin-seo.html');}
    	         else{
    	             echo site_url('admin/edit_seo/'.$thongtinseo->id);
              	}
                ?>" class="sidebar-link"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Thông tin seo</span></a>
              </li>
              <li class="sidebar-item"><a href="<?php if (count($footer)==0){echo site_url('them-footer.html');}
    	         else{
    	             echo site_url('admin/edit_footer/'.$footer->id);
              	}
                ?>" class="sidebar-link"><i class="mdi mdi-multiplication-box"></i><span class="hide-menu">Thông tin công ty</span></a></li>
              <li class="sidebar-item">
                <a href="<?php echo site_url('admin/lienhe'); ?>" class="sidebar-link"><i class="mdi mdi-calendar-check"></i><span class="hide-menu">Thông tin liên hệ</span></a>
              </li>
            </ul>
          </li>
          <?php 
            }
        ?>
        </ul>
      </nav>
    </div>
</aside>