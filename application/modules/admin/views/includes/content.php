<?php 
$this->db->where('status',1);
$this->db->where('taikhoan',$_SESSION['name']);
$admin=$this->db->get('tbladmin1')->row();	
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Dashboard</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Dashboard
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6 col-lg-2 col-xlg-3">
      <div class="card card-hover">
        <div class="box bg-dark text-center">
          <h1 class="font-light text-white">
            <i class="mdi mdi-view-dashboard"></i>
          </h1>
          <h6 class="text-white"><a class="text-white" href="<?php echo site_url('admin'); ?>">Dashboard</a></h6>
        </div>
      </div>
    </div>
    <?php if($admin->role==1){ ?>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <div class="card card-hover">
            <div class="box bg-cyan text-center">
              <h1 class="font-light text-white">
                <i class="mdi mdi-account"></i>
              </h1>
              <h6 class="text-white"><a class="text-white" href="<?php echo site_url('admin/tbladmin1'); ?>">Administrators</a></h6>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xlg-3">
          <div class="card card-hover">
            <div class="box bg-success text-center">
              <h1 class="font-light text-white">
                <i class="mdi mdi-format-list-numbers"></i>
              </h1>
              <h6 class="text-white"><a class="text-white" href="<?php echo site_url('admin/chuyenmuc'); ?>">Chuyên mục</a></h6>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <div class="card card-hover">
            <div class="box bg-info text-center">
              <h1 class="font-light text-white">
                <i class="mdi mdi-file-image"></i>
              </h1>
              <h6 class="text-white"><a class="text-white" href="<?php echo site_url('admin/slider'); ?>">Slider</a></h6>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <div class="card card-hover">
            <div class="box bg-secondary text-center">
              <h1 class="font-light text-white">
                <i class="mdi mdi-contact-mail"></i>
              </h1>
              <h6 class="text-white"><a class="text-white" href="<?php echo site_url('admin/lienhe'); ?>">Thông tin liên hệ</a></h6>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xlg-3">
          <div class="card card-hover">
            <div class="box bg-danger text-center">
              <h1 class="font-light text-white">
                <i class="mdi mdi-image-multiple"></i>
              </h1>
              <h6 class="text-white"><a class="text-white" href="<?php echo site_url('admin/album'); ?>">Album ảnh</a></h6>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <div class="card card-hover">
            <div class="box bg-success text-center">
              <h1 class="font-light text-white">
                <i class="mdi mdi-image-broken-variant"></i>
              </h1>
              <h6 class="text-white"><a class="text-white" href="<?php echo site_url('admin/quangcao'); ?>">Quảng cáo</a></h6>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-2 col-xlg-3">
          <div class="card card-hover">
            <div class="box bg-info text-center">
              <h1 class="font-light text-white">
                <i class="fas fa-people-carry"></i>
              </h1>
              <h6 class="text-white"><a class="text-white" href="<?php echo site_url('admin/doitac'); ?>">Đối tác</a></h6>
            </div>
          </div>
        </div>
    <?php }
    if($admin->role==1 || $admin->role==3 || $admin->role==4){
    ?>
    <div class="col-md-6 col-lg-2 col-xlg-3">
      <div class="card card-hover">
        <div class="box bg-warning text-center">
          <h1 class="font-light text-white">
            <i class="fas fa-edit"></i>
          </h1>
          <h6 class="text-white"><a class="text-white" href="<?php echo site_url('them-bai-viet.html'); ?>">Đăng bài</a></h6>
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-2 col-xlg-3">
      <div class="card card-hover">
        <div class="box bg-primary text-center">
          <h1 class="font-light text-white">
            <i class="mdi mdi-pencil"></i>
          </h1>
          <h6 class="text-white"><a class="text-white" href="<?php echo site_url('admin/baiviet/'); ?>">Bài viết</a></h6>
        </div>
      </div>
    </div>
    <?php 
    }
    ?>
  </div>
  <?php if($admin->role==1){ ?>
  <div class="row">
    <div class="col-lg-6">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Bài viết mới nhất</h4>
        </div>
        <div class="comment-widgets scrollable">
            <?php 
                $this->db->where('status',1);
                $this->db->order_by('id','DESC');
                $this->db->limit(3);
                $sql_news = $this->db->get('tblbaiviet');
                if($sql_news->num_rows() > 0){
                    foreach($sql_news->result() as $item_new){
                    ?>
                  <div class="d-flex flex-row comment-row mt-0">
                    <div class="p-2">
                      <img src="<?php echo $item_new->thumb; ?>" alt="<?php echo $item_new->title; ?>" width="50" height="50" class="rounded-circle"/>
                    </div>
                    <div class="comment-text w-100">
                      <h6 class="font-medium"><?php echo $item_new->title; ?></h6>
                      <span class="mb-3 d-block"><?php echo $item_new->mota; ?></span>
                      <div class="comment-footer">
                        <span class="text-muted float-end"><i class="mdi mdi-calendar-check"></i> <?php echo date('d/m/Y H:i',strtotime($item_new->ngaydang)); ?></span>
                        <a href="<?php echo site_url('admin/edit_baiviet/'.$item_new->id.'-'.url_title($item_new->alias)); ?>" type="button" class="btn btn-cyan btn-sm text-white">
                            <i class="fas fa-edit"></i> Sửa
                        </a>
                        <a target="_blank" href="<?php echo site_url($item_new->alias.'-'.$item_new->id.'.html'); ?>" type="button" class="btn btn-success btn-sm text-white">
                            <i class="fas fa-angle-right"></i> Chi tiết
                        </a>
                        <button type="button" class="btn btn-danger btn-sm text-white"><i class="mdi mdi-delete"></i> Xoá</button>
                      </div>
                    </div>
                  </div>
                <?php 
                    }
                }
          ?>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Bài viết xem nhiều</h4>
        </div>
        <div class="comment-widgets">
            <?php 
                $this->db->where('status',1);
                $this->db->order_by('hits','DESC');
                $this->db->limit(3);
                $sql_view = $this->db->get('tblbaiviet');
                if($sql_view->num_rows() > 0){
                    foreach($sql_view->result() as $item_view){
                    ?>
                      <div class="d-flex flex-row comment-row">
                        <div class="p-2">
                          <img src="<?php echo $item_view->thumb; ?>" alt="<?php echo $item_view->title; ?>" width="50" height="50" class="rounded-circle"/>
                        </div>
                        <div class="comment-text w-100">
                          <h6 class="font-medium"><?php echo $item_view->title; ?></h6>
                          <span class="mb-3 d-block">
                              <?php echo $item_view->mota; ?>
                          </span>
                          <div class="comment-footer">
                            <span class="text-muted float-end">
                                <i class=" fas fa-eye"></i>:<?php echo $item_view->hits; ?>
                                <i class="mdi mdi-calendar-check"></i> <?php echo date('d/m/Y H:i',strtotime($item_view->ngaydang)); ?>
                            </span>
                            <a href="<?php echo site_url('admin/edit_baiviet/'.$item_view->id.'-'.url_title($item_view->alias)); ?>" type="button" class="btn btn-cyan btn-sm text-white">
                                <i class="fas fa-edit"></i> Sửa
                            </a>
                            <a target="_blank" href="<?php echo site_url($item_view->alias.'-'.$item_view->id.'.html'); ?>" type="button" class="btn btn-success btn-sm text-white">
                                <i class="fas fa-angle-right"></i> Chi tiết
                            </a>
                            <button type="button" class="btn btn-danger btn-sm text-white"><i class="mdi mdi-delete"></i> Xoá</button>
                          </div>
                        </div>
                      </div>
                    <?php 
                    }
                }
          ?>
        </div>
      </div>
    </div>
  </div>
  <?php 
    }
  ?>
  <div class="row">
      <div class="col-12">
          <div class="card">
              <div class="card-body b-l calender-sidebar">
                <div id="calendar"></div>
              </div>
          </div>
      </div>
  </div>
</div>