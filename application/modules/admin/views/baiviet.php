﻿<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
$this->db->where('status',1);
$this->db->where('taikhoan',$_SESSION['name']);
$admin=$this->db->get('tbladmin1')->row();
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Bài viết</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Bài viết
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="form-search">
                <form name="frmsearch" method="post" action="<?php echo site_url('admin/user_baiviet');?>">
                    <div class="row">
                        <div class="col-3">
                            <input class="text-search form-control" name="txt_search" type="text" value="<?php if(isset($_SESSION['txt_search'])){ echo $_SESSION['txt_search'];}else{ echo 'Nhập từ khóa tìm kiếm'; }?>" 
                    onfocus="if(this.value  == 'Nhập từ khóa tìm kiếm') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Nhập từ khóa tìm kiếm'; } " />  
                        </div>
                        <div class="col-3">
                            <?php 
                            	if(isset($_SESSION['search_catid'])){
                            		$CI->admin_model->selectCtrl($_SESSION['search_catid'],'catid', 'search_catid');
                            	}
                            	else{ $CI->admin_model->selectCtrl('','catid', 'search_catid'); }
                                //List thành viên	
                                $list_user=$this->db->get('tbladmin1')->result();
                            ?>  
                        </div>
                        <div class="col-3">
                            <select name="search_user" class="form-control">
                                <option value="0">-- Chọn tác giả --</option>
                                <?php foreach($list_user as $luser){?>
                                <option value="<?php echo $luser->id; ?>" <?php if(isset($_SESSION['search_user']) and $_SESSION['search_user']==$luser->id){ ?>selected="selected"<?php } ?>><?php echo $luser->hoten; ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <select name="search_status" class="form-control">
                                	<option value="-1">-- Trạng thái --</option>
                                	<option value="1" <?php if(isset($_SESSION['search_status']) and $_SESSION['search_status']==1){ ?>selected="selected"<?php } ?>>Đã đăng</option>
                                	<option value="0" <?php if(isset($_SESSION['search_status']) and $_SESSION['search_status']==0){ ?>selected="selected"<?php } ?>>Chưa đăng</option>
                                </select>  
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <button class="button_w btn btn-primary text-white" type="submit" name="submit"><i class="fas fa-search"></i> Tìm kiếm</button>
                        </div>
                    </div>
                </form>
            </div>
            <form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_baiviet'); ?>">
            <div class="card">
                <div class="card-header" style="display:table;">
                    <p class="text-end" style="display:table-cell;">
                        <a href="<?php echo site_url('them-bai-viet.html'); ?>" class="btn-sm btn btn-success text-white"><i class="fas fa-plus"></i> Thêm mới</a> 
                        <?php if($admin->role==1){ ?><input class="btn btn-danger btn-sm text-white" type="submit" name="submit" value="Xóa" /><?php } ?>
                    </p>
                </div>
                <div class="card-body">
                        <table width="100%" class="table">
                            <thead class="thead-light">
                            <tr class="title">
                                <td width="5%" style="text-align:center;"><input type="checkbox" class="form-check-input" onclick="checkall('checkbox', this)" name="check"/></td>
                                <td>Ảnh</td>        
                                <td>Tiêu đề</td>
                        		<td style="text-align:center;" width="10%">Ngày đăng</td>
                        		<td style="text-align:center;" width="5%">Thời gian</td>
                        		<?php 			
                        			if($admin->role==1 or $admin->role == 3 or $admin->role == 4){
                        			?>
                        		<td width="5%">Trạng thái</td>
                        		<?php } ?>
                                <td width="12%">Chuyên mục</td>        	               
                        		<td width="10%">Tác giả</td>
                        		<?php if($admin->role==1){ ?>
                        		<td width="10%">Ngày sửa</td>
                        		<?php } ?>		
                        		<td width="5%">Người sửa</td>
                        		<?php
                        				if($admin->role==1){
                        			?>
                        		<td width="5%">Lượt đọc</td>		
                        		<?php 
                        				}
                        		?>
                        		<td width="5%" style="text-align:center;">#ID</td>
                            </tr>
                            </thead>
                        	<?php 
                        	if($query->num_rows() >0)
                        	{
                        	?>
                            <?php 
                                $stt=0;	
                        		date_default_timezone_set('Asia/Ho_Chi_Minh');
                        		$day = date('Y-m-d H:i:s');	
                        		foreach($query->result() as $item)
                                {
                        		$stt++;	
                        		$nguoidangs=$CI->admin_model->gettbl('tbladmin1',$item->nguoidang);
                            ?>
                        		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
                        			<td style="text-align:center;">
                        			<div id="request-form">
                        				<input type="checkbox" name="checkbox[]" class="checkbox form-check-input" value="<?php echo $item->id; ?>" />
                        			</div>
                        			</td>
                                    <td><img src="<?php echo $item->image;  ?>" width="100" /></td>					
                        			<td><a href="<?php echo site_url('admin/edit_baiviet/'.$item->id.'-'.url_title($item->alias)); ?>"><?php echo $item->title; ?></a></td>			
                        			<td style="text-align:center;"><?php 
                        			$ngays=explode(' ',$item->created_day);
                        			$ngay=explode('-',$ngays[0]);
                        			$created_day=$ngay[2].'-'.$ngay[1].'-'.$ngay[0];			
                        			echo $created_day; ?></td>
                        			<td style="text-align:center;"><?php echo $ngays[1]; ?></td>
                        			<?php if($admin->role==1 or $admin->role==3 or $admin->role==4){ ?>
                        			<td style="text-align:center;">
                        			    	<?php 
                        			    if($admin->role==4){
                        			        if($item->status=='1')
                                			  {					
                                    			  if($day < $item->created_day){
                                    			  ?>
                                    			  <a class="status"><img src="images/toolbar/publish_y.png"></a>
                                    			  <?php
                                    			  }else{
                                    			  ?>
                                    			  <a class="status"><img src="images/toolbar/tick.png"></a>
                                    			  <?php }
                                			  }
                                			  else
                                			  {
                                			  ?>
                                			        <a class="status"><img src="images/toolbar/publish_x.png"></a>
                                			  <?php
                                			  }
                        			    }else{
                                			  if($item->status=='1')
                                			  {					
                                    			  if($day < $item->created_day){
                                    			  ?>
                                    			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblbaiviet','baiviet')"><img src="images/toolbar/publish_y.png"></a>
                                    			  <?php
                                    			  }else{
                                    			  ?>
                                    			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblbaiviet','baiviet')"><img src="images/toolbar/tick.png"></a>
                                    			  <?php }
                                			  }
                                			  else
                                			  {
                                			  ?>
                                			        <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblbaiviet','baiviet')"><img src="images/toolbar/publish_x.png"></a>
                                			  <?php
                                			  }
                        			    }
                        			  ?>
                        			</td>    
                        			<?php } ?>
                        			<td><?php 
                        					$cats=$CI->admin_model->gettbl('tblchuyenmuc',$item->catid);										
                        					if($cats->num_rows()> 0){
                        					$cat= $cats->row();
                        						echo $cat->name;
                        					}
                        					else{
                        						echo 'NULL';
                        					}
                        			?></td>
                        			<td style="text-align:center;">
                        			<?php 										
                        				$users=$CI->admin_model->gettbl('tbladmin1',$item->uid);										
                        				if($users->num_rows()> 0){
                        				$user= $users->row();
                        					echo $user->hoten;
                        				}
                        				else{
                        					echo '';
                        				}
                        			?>
                        			</td>
                        			<?php
                        				if($admin->role==1){
                        			?>
                        			<td style="text-align:center;"><?php echo $item->ngaydang; ?></td>
                        			<?php } ?>			
                        			<td style="text-align:center;">
                        				<?php 																								
                        					if($nguoidangs->num_rows()> 0){
                        					$nguoidang= $nguoidangs->row();
                        						echo $nguoidang->hoten;
                        					}
                        					else{
                        						echo '';
                        					}
                        				?>
                        			</td>
                        			<?php
                        				if($admin->role==1){
                        			?>
                        			<td style="text-align:center;"><?php echo $item->hits; ?></td>
                        			<?php 
                        				}
                        			?>
                        			<td style="text-align:center;"><?php echo $item->id; ?></td>
                        		</tr>		
                            <?php 
                            }
                            ?>
                        	<?php 
                        	}
                        	else
                        	{
                        	?>
                        	<tr><td colspan="8">Dữ liệu đang cập nhật</td></tr>
                        	<?php    
                        	}
                        	?>
                        </table>
                        <div class="clr"></div>
                        <div class="pagation">
                        	<?php echo $pagination; ?>
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>