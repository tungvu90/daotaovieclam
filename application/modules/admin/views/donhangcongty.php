<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
$this->db->where('status',1);
$this->db->where('name',$_SESSION['name']);
$admin=$this->db->get('tbladmin')->row();
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<div class="form-search">
<form name="frmsearch" method="post" action="<?php echo site_url('admin/user_donhangcongty') ?>">
<input class="text-searchsp" name="txt_dienthoaisp" type="text" value="<?php if(isset($_SESSION['txt_dienthoaisp'])){ echo $_SESSION['txt_dienthoaisp'];}else{ echo 'Nhập số điện thoại tìm kiếm'; }?>" 
onfocus="if(this.value  == 'Nhập số điện thoại tìm kiếm') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Nhập số điện thoại tìm kiếm'; } " />
<input class="button_w" type="submit" name="submit" value="Tìm kiếm" />
</form>
</div>
<form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_donhangcongty'); ?>">
<p class="sidebar"><a href="<?php echo site_url('them-san-pham.html'); ?>">Thêm mới</a> <input type="submit" name="submit" value="Xóa" /></p>
<table width="100%" style="min-height:350px;display:block;">
    <tr class="title">
        <td width="5%" style="text-align:center;"><input type="checkbox" onclick="checkall('checkbox', this)" name="check"/></td>        
		<td style="text-align:center;" width="10%">Điện thoại</td>
		<td width="5%">Cửa hàng</td>
        <td width="5%">Sản phẩm</td>
        <td width="5%">Số lượng</td>
        <td style="width:5%;">Thành tiền</td>
        <td width="12%">Ngày đăng</td> 
        <td style="width:3%;">Thứ tự</td>       	               	
		<td width="3%">Trạng thái</td>	
    </tr>
	<?php 
	if($query->num_rows() >0)
	{
	?>
    <?php 
        $stt=0;	
		foreach($query->result() as $item)
        {
		$stt++;			
    ?>
		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
			<td style="text-align:center;">
			<div id="request-form">
				<input type="checkbox" name="checkbox[]" class="checkbox" value="<?php echo $item->id; ?>" />
			</div>
			</td>
            <td><a href="<?php echo site_url('admin/edit_donhangcty/'.$item->id); ?>"><?php echo $item->dienthoai; ?></a></td>	
            <td>
            <?php 
                $this->db->where('id',$item->cuahang); 
                $this->db->select('id,title,diachi');
                $sqlcuahang=$this->db->get('tbldiachi');
                if($sqlcuahang->num_rows()>0)
                {
                    $cuahang=$sqlcuahang->row();
                    echo '<b>'.$cuahang->title.'</b>:&nbsp;'.$cuahang->diachi;
                }        
            ?></td>						
			<td style="text-align:center;">
            <?php 
                $this->db->where('id',$item->sanpham);
                $this->db->select('id,title,gia,giakm,donvitinh');
                $sqlsanphamdh=$this->db->get('tblsanpham');
                if($sqlsanphamdh->num_rows()>0)
                {
                    $sanphamdh=$sqlsanphamdh->row();
                    echo $sanphamdh->title; 
                }
            ?></td>
            <td><?php echo $item->soluong; ?></td>	
            <td style="color:red;font-weight:bold;;">
            <?php 
            if($sanphamdh->giakm!='')
            {                
                echo number_format(($item->soluong)*($sanphamdh->giakm),0,'.','.').'&nbsp;'.$sanphamdh->donvitinh; 
            }
            else
            {
                echo number_format(($item->soluong)*($sanphamdh->gia),0,'.','.').'&nbsp;'.$sanphamdh->donvitinh; 
            }
            ?></td>
            <td><?php 
            $ngaydh=explode('-',$item->ngaydang);
            echo $ngaydh[2].'-'.$ngaydh[1].'-'.$ngaydh[0]; ?></td>		
            <td><?php echo $item->thutu; ?></td>	
			<td style="text-align:center;">						
			  <?php 
			  if($item->status=='1')
			  {					
			     echo 'Đã duyệt';
			  }
			  else
			  {
			     echo 'Chưa duyệt';
			  }
			  ?>
			</td>                
		</tr>		
    <?php 
    }
    ?>
	<?php 
	}
	else
	{
	?>
	<tr><td colspan="8">Dữ liệu đang cập nhật</td></tr>
	<?php    
	}
	?>
</table>

</form>
<div class="clr"></div>
<div class="pagation">
	<?php echo $pagination; ?>
</div>