<h3 class="header">Nội dung Seo</h3>
<?php 
if(isset($error))
{
    echo '<div class="warning">'.$error.'</div>';
}
?>
<div class="content-inner">
	<form name="frmfooter" action="<?php echo site_url('xu-ly-don-hang-cong-ty.html'); ?>" method="post" enctype="multipart/form-data">
		<?php 
			if(isset($id))
			{
				$this->db->where('id',$id);
				$item=$this->db->get('tbldonhangcongty')->row();
				$id=$item->id;            
			}
		?>    
		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />
        <input type="hidden" name="idsp" value="<?php echo $item->sanpham; ?>" />	
		<div class="gray">
		<table class="tab1">
        <tr>
            <td><strong>Điện thoại</strong></td>
            <td><input type="text" name="dienthoai" value="<?php if(isset($id)){echo $item->dienthoai;} ?>" readonly="true" /></td>
        </tr>
		<tr>
			<td><strong>Cửa hàng</strong></td>
			<td>
		          <select name="cuahang">
                    <?php 
                        $this->db->select('id,diachi');
                        $sqlcuahang=$this->db->get('tbldiachi');
                        if($sqlcuahang->num_rows()>0)
                        {                            
                            foreach($sqlcuahang->result() as $itemcuahang)
                            {                                
                                if($itemcuahang->id==$item->cuahang)
                                {
                                ?>
                                <option value="<?php echo $itemcuahang->id;?>" selected="selected"><?php echo $itemcuahang->diachi;?></option>
                                <?php    
                                }
                                else
                                {
                                ?>
                                <option value="<?php echo $itemcuahang->id;?>"><?php echo $itemcuahang->diachi;?></option>
                                <?php    
                                }
                            }
                            $sqlcuahang->free_result();
                        }
                    ?>                    
                  </select>
			</td>
		</tr>
        <tr>
			<td><strong>Số lượng</strong></td>
			<td>
		          <input style="width:30px;text-align: center;padding:3px;" type="text" name="soluong" value="<?php echo $item->soluong; ?>" />
			</td>
		</tr>	
        <tr><td width="200">
			<strong>Trạng thái</strong></td>
			<td>									
				<?php 
				 if(isset($id))
				 {							 
				?>
					<input class="news_checkbox" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />Đã duyệt				
					<?php         
				 }
					else
					{
					?>
						<input class="news_checkbox" type="checkbox" name="status" value="1" checked="checked" />Đã duyệt
					<?php             
				}
				?>							
		</td></tr>	
		</table>
		</div>
		<div class="gray">
		<center>
		<?php 
			if(isset($id))
			{
			?>			
				<input class="button" type="submit" name="submit" value="Lưu thay đổi" />						
			<?php    
			}
			else
			{
			?>			
				<input class="button" type="submit" name="submit" value="Nhập" />	
                <input class="button" type="reset" name="reset" value="Làm lại" />				
			<?php 
			}
		?>
		</center>
		</div>
	</form>
	<div class="clr"></div>
</div>