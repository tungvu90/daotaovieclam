<!DOCTYPE html>
<html dir="ltr" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <base href="<?php echo base_url(); ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="keywords" content="trung tâm ngoại ngữ hai phong, trung tâm dao tao ngoai ngu tin hoc hai phong, day seo hai phong, day tin hoc van phong, day đồ họa, trung tam ngoai ngu hai phong"/>
    <meta name="description" content="Trung tâm đào tạo tin học ngoại ngữ AMANDA chuyên đào tạo tin học Hải Phòng, Đào tạo ngoại ngữ dạy nghề các môn tin học văn phòng, dạy học ngoại ngữ tiếng hàn, trung, nhật, anh, đức. SEO Website, dạy 3dsmax, dạy autocad, đồ họa máy tính." />
    <meta name="robots" content="noindex,nofollow" />
    <title>Quản lý hệ thống</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="backend/assets/images/favicon.png"/>
    <!-- Custom CSS -->
    <link href="backend/assets/libs/flot/css/float-chart.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="backend/css/style.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="backend/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css"/>
    <link href="backend/assets/libs/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet"/>
    <link href="backend/assets/extra-libs/calendar/calendar.css" rel="stylesheet" />
    <link href="backend/assets/libs/toastr/build/toastr.min.css" rel="stylesheet" />
    <link href="css/dropzone.min.css" rel="stylesheet">
    <script src="backend/assets/libs/jquery/dist/jquery.min.js"></script>
     <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="ckfinder/ckfinder.js"></script>
	<script src="js/dropzone.min.js"></script>
	<script src="backend/assets/libs/toastr/build/toastr.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
      <div class="lds-ripple">
        <div class="lds-pos"></div>
        <div class="lds-pos"></div>
      </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
      <!-- ============================================================== -->
      <!-- Topbar header - style you can find in pages.scss -->
      <!-- ============================================================== -->
      <?php $this->load->view('includes/header'); ?>
      <!-- ============================================================== -->
      <!-- End Topbar header -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Left Sidebar - style you can find in sidebar.scss  -->
      <!-- ============================================================== -->
      <?php $this->load->view('includes/left'); ?>
      <!-- ============================================================== -->
      <!-- End Left Sidebar - style you can find in sidebar.scss  -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Page wrapper  -->
      <!-- ============================================================== -->
      <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <?php $this->load->view($content); ?>
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <?php $this->load->view('includes/footer') ?>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- End Page wrapper  -->
      <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="backend/js/jquery.ui.touch-punch-improved.js"></script>
    <script src="backend/js/jquery-ui.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="backend/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <script src="backend/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="backend/assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="backend/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="backend/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="backend/js/custom.min.js"></script>
    <!--This page JavaScript -->
    <!-- <script src="../dist/js/pages/dashboards/dashboard1.js"></script> -->
    <!-- Charts js Files -->
    <script src="backend/assets/libs/flot/excanvas.js"></script>
    <script src="backend/assets/libs/flot/jquery.flot.js"></script>
    <script src="backend/assets/libs/flot/jquery.flot.pie.js"></script>
    <script src="backend/assets/libs/flot/jquery.flot.time.js"></script>
    <script src="backend/assets/libs/flot/jquery.flot.stack.js"></script>
    <script src="backend/assets/libs/flot/jquery.flot.crosshair.js"></script>
    <script src="backend/assets/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <script src="backend/js/pages/chart/chart-page-init.js"></script>
    <script src="backend/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="backend/assets/libs/moment/min/moment.min.js"></script>
    <script src="backend/assets/libs/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="backend/js/pages/calendar/cal-init.js"></script>
    <script src="backend/assets/libs/fullcalendar/dist/locale/vi.js"></script>
    <script>
        $('#calendar').fullCalendar({
          locale: 'vi'
        });
    </script>
  </body>
</html>