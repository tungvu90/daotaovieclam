<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
$this->db->where('status',1);
$this->db->where('taikhoan',$_SESSION['name']);
$admin=$this->db->get('tbladmin1')->row();
?>
<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<?php 
if($admin->role==1)
{
?>
<div class="form-search">
<form name="frmsearch" method="post" action="<?php echo site_url('admin/user_sanpham') ?>">
<input class="text-searchsp" name="txt_searchsp" type="text" value="<?php if(isset($_SESSION['txt_searchsp'])){ echo $_SESSION['txt_searchsp'];}else{ echo 'Nhập từ khóa tìm kiếm'; }?>" 
onfocus="if(this.value  == 'Nhập từ khóa tìm kiếm') { this.value = ''; } " onblur="if(this.value == '') { this.value = 'Nhập từ khóa tìm kiếm'; } " />
<?php 
	if(isset($_SESSION['search_catidsp'])){
		$CI->admin_model->selectCtrlsp($_SESSION['search_catidsp'],'catidsp', 'search_catidsp');
	}
	else{ $CI->admin_model->selectCtrlsp('','catidsp', 'search_catidsp'); }
 ?>
<input class="button_w" type="submit" name="submit" value="Tìm kiếm" />
</form>
</div>
<?php 
}
?>
<form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_sanpham'); ?>">
<p class="sidebar"><a href="<?php echo site_url('them-san-pham.html'); ?>">Thêm mới</a> <input type="submit" name="submit" value="Xóa" /></p>
<table width="100%" style="min-height:350px;display:block;">
    <tr class="title">
        <td width="5%" style="text-align:center;"><input type="checkbox" onclick="checkall('checkbox', this)" name="check"/></td>        
        <td>Tiêu đề</td>
        <td>Ảnh</td>
		<td style="text-align:center;" width="10%">Ngày đăng</td>
		<?php 			
			if($admin->role==1 or $admin->role==3){
			?>
		<td width="5%">Trạng thái</td>
		<?php } ?>
        <td width="5%">Home</td>
        <td width="5%">Top</td>
        <td width="12%">Chuyên mục</td> 
        <td>Tỉnh thành</td>       	               	
		<td width="5%">Người sửa</td>	
		<td width="5%">Lượt xem</td>		
		<td width="5%" style="text-align:center;">id</td>
    </tr>
	<?php 
	if($query->num_rows() >0)
	{
	?>
    <?php 
        $stt=0;	
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$day = date('Y-m-d H:i:s');	
		foreach($query->result() as $item)
        {
		$stt++;	
		$nguoidangs=$CI->admin_model->gettbl('tbladmin',$item->nguoidang);
    ?>
		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
			<td style="text-align:center;">
			<div id="request-form">
				<input type="checkbox" name="checkbox[]" class="checkbox" value="<?php echo $item->id; ?>" />
			</div>
			</td>
            <td><a href="<?php echo site_url('admin/edit_sanpham/'.$item->id.'-'.url_title($item->alias)); ?>"><?php echo $item->title; ?></a></td>	
            <td><img src="<?php echo $item->thumb; ?>" width="100px" /></td>						
			<td style="text-align:center;"><?php 
			$ngays=explode(' ',$item->ngaydang);
			$ngay=explode('-',$ngays[0]);
			$created_day=$ngay[2].'-'.$ngay[1].'-'.$ngay[0];			
			echo $created_day; ?></td>
			<?php if($admin->role==1 or $admin->role==3){ ?>
			<td style="text-align:center;">						
			  <?php 
			  if($item->status=='1')
			  {					
			  ?>
			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblsanpham','sanpham')"><img src="images/toolbar/tick.png"></a>
			  <?php
			  }
			  else
			  {
			  ?>
			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tblsanpham','sanpham')"><img src="images/toolbar/publish_x.png"></a>
			  <?php
			  }
			  ?>
			</td>    
			<?php } ?>
            <td>
                <?php 
                if($item->home=='1')
                {
                    echo 'Hiện thị';
                }
                else
                {
                    echo 'Không';
                }
                ?>
            </td>
            <td>
                <?php 
                if($item->top=='1')
                {
                    echo 'Hiện thị';
                }
                else
                {
                    echo 'Không';
                }
                ?>
            </td>
			<td><?php 
					$cats=$CI->admin_model->gettbl('tblchuyenmucsp',$item->catid);										
					if($cats->num_rows()> 0){
					$cat= $cats->row();
						echo $cat->name;
					}
					else{
						echo 'NULL';
					}
			?></td>
            <td>
                <?php 
                $tinh=$CI->admin_model->gettbl('tbltinh',$item->tinh);
                if($tinh->num_rows()>0)
                {
                    echo $tinh=$tinh->row()->tinh;    
                }
                else
                {
                    echo 'Không';
                }
                ?>
            </td>			
			<td style="text-align:center;">
            <?php 
            $this->db->where('id',$item->nguoidang);
            $sqladmindang=$this->db->get('tbladmin1');
            if($sqladmindang->num_rows()>0)
            {
                $sqladmindang=$sqladmindang->row();
                echo $sqladmindang->taikhoan;    
            }
             ?></td>							
			<td style="text-align:center;"><?php echo $item->view; ?></td>
			<td style="text-align:center;"><?php echo $item->id; ?></td>
		</tr>		
    <?php 
    }
    ?>
	<?php 
	}
	else
	{
	?>
	<tr><td colspan="8">Dữ liệu đang cập nhật</td></tr>
	<?php    
	}
	?>
</table>

</form>
<div class="clr"></div>
<div class="pagation">
	<?php echo $pagination; ?>
</div>