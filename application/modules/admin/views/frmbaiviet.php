﻿<?php 

$CI=&get_instance();

$CI->load->model('admin/admin_model');	

$baiviet=$CI->admin_model->gettbl('tblbaiviet','');	

$this->db->where('status',1);

$this->db->where('taikhoan',$_SESSION['name']);

$admin=$this->db->get('tbladmin1')->row();		

?>

<?php 

if(isset($error))

{

    echo '<div class="warning">'.$error.'</div>';

}

?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">
          <?php echo isset($id) ? 'Sửa bài viết' : 'Thêm bài viết' ?>
      </h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/baiviet'); ?>">Bài viết</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                <?php echo isset($id) ? 'Sửa' : 'Thêm mới' ?>
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form class="form-horizontal" name="frmtintuc" action="<?php echo site_url('xu-ly-bai-viet.html'); ?>" method="post" enctype="multipart/form-data">
            <div class="card">
                <div class="card-header" style="display:table;">
                    <p class="text-end" style="display:table-cell;">
                        <a href="<?php echo site_url('admin/baiviet'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                    </p>
                </div>
                <div class="card-body">
                    <div class="content-inner1">
                    		<?php 
                    			if(isset($id)) {
                    				$this->db->where('id',$id);
                    				$item=$this->db->get('tblbaiviet')->row();
                    				$id=$item->id;            
                    			}
                    		?>    
                    		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />		
                    		<input type="hidden" name="uid" value="<?php if(isset($id)) { echo $item->uid; }else{ echo $admin->id;}?>" />
                    		<div class="row">
                    		    <div class="col-6">
                    		        <div class="form-group row">
                    		            <label for="title" class="col-sm-12 control-label col-form-label"><strong>Tiêu đề</strong></label>
                    		            <div class="col-sm-12">
                    		                <input type="text" class="form-control" name="title" placeholder="Tiêu đề" value="<?php if(isset($id)) {echo htmlspecialchars($item->title);} ?>" />
                		                </div>
                		            </div>
                		            <div class="form-group row">
                		                <label for="alias" class="col-sm-12 control-label col-form-label"><strong>Đường dẫn thân thiện</strong></label>
                		                <div class="col-sm-12">
                		                    <input type="text" class="form-control" name="alias" placeholder="Đường dẫn thân thiện" value="<?php if(isset($id)){ echo $item->alias;} ?>" />
            		                    </div>
            		                </div>
            		                <div class="form-group row">
            		                    <label for="catid" class="col-sm-12 control-label col-form-label"><strong>Chuyên mục</strong></label>
            		                    <div class="col-sm-12">
            		                        <?php 
                                				if(isset($id)){ $CI->admin_model->selectCtrl($item->catid,'catid', 'forFormDim');}
                                				else{
                                					$CI->admin_model->selectCtrl('','catid', 'forFormDim');
                                				}
                                			?>
        		                        </div>
        		                    </div>
        		                    <div class="form-group row">
        		                        <label for="ncatid" class="col-sm-12 control-label col-form-label"><strong>Chuyên mục 2</strong></label>
        		                        <div class="col-sm-12">
        		                            <?php 
                            				if(isset($id)){ $CI->admin_model->selectCtrl($item->ncatid,'ncatid', 'forFormDim');}
                            				else{
                            					$CI->admin_model->selectCtrl('','ncatid', 'forFormDim');
                            				}
                            			?>
    		                            </div>
    		                        </div>
    		                        <div class="form-group row">
    		                            <label for="image" class="col-sm-12 control-label col-form-label"><strong>Ảnh đại diện</strong></label>
    		                            <div class="col-sm-12">
    		                                 <?php if(isset($id)){?>
                                			<input type="hidden" name="image" style="width:250px;" value="<?php echo $item->image; ?>"> 
                                			<input type="hidden" name="thumb" value="<?php echo $item->thumb; ?>"><br /><br />
                                			<?php if($item->thumb !=''){?>
                                			<img src="<?php echo $item->thumb; ?>" width="250"><br />
                                			<?php }
                                			    } 			
                                			?>
                                			<input type="file" name="image" value="" />
		                                </div>
		                            </div>
                		        </div>
                		        <div class="col-6">
                		            <?php if($admin->role==1 or $admin->role==3){ ?>
                        		        <div class="form-group row">
                        		            <label class="col-sm-12 control-label col-form-label"><strong>Bài viết thuộc các nhóm tin:</strong></label>
                        		            <div class="col-12">
                        		                <?php 
                                					 if(isset($id)) {							 
                            					    ?>
                                						<input class="form-check-input" type="checkbox" name="home" value="1" <?php if($item->home==1): ?>checked="checked"<?php endif; ?> />	
                                						<?php         
                                					 }
                            						else {
                            						?>
                        							<input class="form-check-input" type="checkbox" name="home" value="1" />
                            						<?php             
                            					}
                    					        ?>
                    					        <label class="form-check-label mb-0" for="customControlAutosizing1">Hiện thị home</label>
                    					        <?php 
                    					        if(isset($id)){	
                    					        ?>
                    						    <input class="form-check-input" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />				
                    						    <?php         
                    					        }else{
                    						    ?>
                    							<input class="form-check-input" type="checkbox" name="status" value="1" />
                    						<?php             
                    
                    					   }
                    
                    					?>	
                    					<label class="form-check-label mb-0" for="customControlAutosizing1">Xuất bản</label>
                        		            </div>
                    		            </div>
                		            <?php } ?>
                		            <div class="form-group row">
                		                <label class="col-sm-12 control-label col-form-label"><strong>Tags:</strong> <cite class="badge bg-danger">Từ khóa cách nhau bởi dấu phẩy ','</cite></label>
                		                <div class="col-12">
                		                    <textarea class="form-control" name="tags" /><?php if(isset($id)) {echo $item->tags;} ?></textarea>
            		                    </div>
            		                </div>
            		                <div class="form-group row">
            		                    <label class="col-sm-12 control-label col-form-label"><strong>Ngày tạo</strong> ((Ngày-tháng-năm giờ:phút:giây))</label>
            		                    <div class="col-12">
            		                        <?php 
                        					if(isset($id) and $item->ngaydang!='0000-00-00 00:00:00'){
                        						$ngays=explode(' ',$item->created_day);
                        						$ngay=explode('-',$ngays[0]);
                        						$created_day=$ngay[2].'/'.$ngay[1].'/'.$ngay[0];	
                        						$time=$ngays[1];
                        					} else{							
                        						date_default_timezone_set('Asia/Ho_Chi_Minh');
                        						$created_day = date('d/m/Y');							
                        						$time = date('H:i:s');;
                        					}
                        					?>
                        					<div class="row">
                        					    <div class="col-6">
                        					        <div class="input-group">
                            					        <input class="form-control mydatepicker" type="text" placeholder="Ngày tạo" name="created_day" value="<?php echo $created_day; ?>" />
                            					        <div class="input-group-append">
                                                            <span class="input-group-text h-100"><i class="mdi mdi-calendar"></i></span>
                                                        </div>
                                                    </div>
                    					        </div>
                    					        <div class="col-6">
                    					            <input id="time" class="form-control" type="text" name="time" value="<?php echo $time; ?>" />
                					            </div>
                        					</div>
        		                        </div>
        		                    </div>
                		        </div>
                		    </div>
                    		<div class="row">
                    		    <div class="col-12">
                		            <div class="form-group row">
                		                <label for="mota" class="col-sm-12 control-label col-form-label"><strong>Mô tả</strong></label>
                		                <div class="col-sm-12">
                		                    <textarea class="form-control" name="mota" rows="5" cols="80" placeholder="Mô tả"><?php if(isset($id)){ echo $item->mota;} ?></textarea>
            		                    </div>
            		                </div>
        		                </div>
        		                <div class="col-12">
                		            <div class="form-group row">
                		                <label for="fulltext" class="col-sm-12 control-label col-form-label"><strong>Nội dung</strong></label>
                		                <div class="col-sm-12">
                		                    <textarea class="form-control" rows="5" cols="70" name="fulltext" id="txt_content" /><?php if(isset($id)) {echo $item->fulltext;} ?></textarea>
            		                    </div>
            		                </div>
        		                </div>
                		    </div>
                		    <div class="row">
                		        <div class="col-6">
                		            <div class="form-group row">
                        		        <label for="tacgia" class="col-sm-12 control-label col-form-label"><strong>Tác giả bài viết</strong></label>
                        		        <div class="col-sm-12">
                        		            <input type="text" class="form-control" name="tacgia" placeholder="Tác giả bài viết" value="<?php if(isset($id)){ echo $item->tacgia;} ?>" />
                        		        </div>
                        		    </div>
                		        </div>
                		        <div class="col-6">
                		            <div class="form-group row">
                        		        <label for="nguon" class="col-sm-12 control-label col-form-label"><strong>Nguồn tin</strong></label>
                        		        <div class="col-sm-12">
                        		            <input type="text" class="form-control" name="nguon" placeholder="Nguồn tin" value="<?php if(isset($id)){ echo $item->nguon;} ?>" />
                        		        </div>
                        		    </div>
            		            </div>
                		    </div>
                		    <div class="row">
                    		        <div class="col-6">
                    		            <label for="meta_title" class="col-sm-12 control-label col-form-label"><strong>Meta Title</strong></label>
                        		        <div class="col-sm-12">
                        		            <textarea name="meta_title" class="form-control" cols="98" rows="5"><?php if(isset($id)){ echo $item->meta_title;} ?></textarea>
                        		        </div>
                    		        </div>
            		            <div class="col-6">
                		            <label for="meta_des" class="col-sm-12 control-label col-form-label"><strong>Meta Description</strong></label>
                    		        <div class="col-sm-12">
                    		            <textarea name="meta_des" class="form-control" cols="98" rows="5"><?php if(isset($id)){ echo $item->meta_des;} ?></textarea>
                    		        </div>
            		            </div>
            		        </div>
                            <div class="row">
                                <div class="col-6">
                                    <label for="keyword" class="col-sm-12 control-label col-form-label"><strong>Keyword</strong></label>
                                    <div class="col-sm-12">
                    		            <textarea name="keyword" class="form-control" cols="98" rows="5"><?php if(isset($id)){ echo $item->keyword;} ?></textarea>
                    		        </div>
                		        </div>
                		        <div class="col-6">
                                    <label for="code_head" class="col-sm-12 control-label col-form-label"><strong>Mã chèn trong Head</strong></label>
                                    <div class="col-sm-12">
                    		            <textarea name="code_head" class="form-control" cols="98" rows="5"><?php if(isset($id)){ echo $item->code_head;} ?></textarea>
                    		        </div>
                		        </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="thutu" class="col-sm-12 control-label col-form-label"><strong>Thứ tự</strong></label>
                                        <div class="col-sm-12">
                        		            <input type="text" class="form-control" name="thutu" value="<?php if(isset($id)){ echo $item->thutu;} ?>" />
                        		        </div>
                    		        </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="thutu" class="col-sm-12 control-label col-form-label"><strong>Sắp xếp tin tức</strong></label>
                                        <div class="col-sm-12">
                        		            <input type="text" class="form-control" name="sxtt" value="<?php if(isset($id)){ echo $item->sxtt;} ?>" />
                        		        </div>
                    		        </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <strong>Chèn bài liên quan</strong>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-2">
                                                    <div class="form-group">
                                                        <input type="text" class="form-control" name="listbaiviet" id="load-list" value="<?php if(isset($id)){ echo $item->tinlienquan;} ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-10">
                                                    <p class="badge bg-danger">
                        							    <cite>Nhập id bài viết, các id ngăn cách nhau bởi dấu phẩy ','</cite>
                        							</p>
                                                </div>
                                            </div>
                							<div id="popup">							
                
                							<div id="popup-content">
                
                								<?php 
                
                									$dem1='0';
                
                									$start_row=$this->uri->segment(4);
                
                									$per_page=5;
                
                									if(is_numeric($start_row)=='')
                
                									{
                
                										$start_row=0;
                
                									}
                
                									$CI=&get_instance();
                
                									$CI->load->model('admin/admin_model');
                
                									$listbv=$CI->admin_model->gettbl_listbv('tblbaiviet');
                
                									$total_rows = $listbv->num_rows();
                
                									$CI->load->library('pagination');				
                
                									$config['base_url'] = site_url().'/admin/search_page/';
                
                									$config['total_rows'] = $total_rows;
                
                									$config['per_page'] = $per_page;
                
                									$config['uri_segment'] =4;
                
                									$config['next_link'] = 'Tiếp';
                
                									$config['prev_link'] = 'Sau';
                
                									$config['num_links'] = 4;
                
                									$config['first_link'] = 'Đầu';
                
                									$config['last_link'] = 'Cuối';
                
                									$config['full_tag_open']='<div class="pagination1">';
                
                									$config['full_tag_close']='</div>';
                
                									$CI->pagination->initialize($config);
                
                									$data['query']=$CI->admin_model->gettbl_listbv_limited('tblbaiviet',$start_row,$per_page);
                
                									$data['pagination1']= $CI->pagination->create_links();										
                
                								?>
                								<div class="thing1">
                									<?php $this->load->view('includes/ct-tab',$data); ?>
                								</div>        
                							</div>
                						</div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                    </div>
                </div>
                <div class="card-footer">
                    <center>
                		<?php 
                			if(isset($id)){
                			?>			
            				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Lưu thay đổi</button>						
                			<?php    
                			}
                			else {
                			?>			
                			<button class="btn btn-info text-white btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Nhập tin</button>
                			<?php 
                			}
                		?>
                		<a href="<?php echo site_url('admin/baiviet'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
            		</center>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script >

<script type="text/javascript">

jQuery(function($){



	$.datepicker.regional['vi'] = {



		closeText: 'Đóng',



		prevText: '&#x3c;Trước',



		nextText: 'Tiếp&#x3e;',



		currentText: 'Hôm nay',



		monthNames: ['Tháng Một', 'Tháng Hai', 'Tháng Ba', 'Tháng Tư', 'Tháng Năm', 'Tháng Sáu',



		'Tháng Bảy', 'Tháng Tám', 'Tháng Chín', 'Tháng Mười', 'Tháng Mười Một', 'Tháng Mười Hai'],



		monthNamesShort: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6',



		'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],



		dayNames: ['Chủ Nhật', 'Thứ Hai', 'Thứ Ba', 'Thứ Tư', 'Thứ Năm', 'Thứ Sáu', 'Thứ Bảy'],



		dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],



		dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],



		weekHeader: 'Tu',



		dateFormat: 'dd/mm/yy',



		firstDay: 0,



		isRTL: false,



		showMonthAfterYear: false,



		yearSuffix: ''};

		

	$.datepicker.setDefaults($.datepicker.regional['vi']);

});

$(function() {
/*datwpicker*/
      jQuery(".mydatepicker").datepicker();
      jQuery("#datepicker-autoclose").datepicker({
        autoclose: true,
        todayHighlight: true,
      });

	$.datepicker.setDefaults($.datepicker.regional['vi']);

});

</script>

<script type="text/javascript">
CKEDITOR.replace('txt_content');
</script>