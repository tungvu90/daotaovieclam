﻿<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
$tinh=$CI->admin_model->gettbl('tbltinh','');
?>
<h3 class="header">Thêm quảng cáo</h3>
<?php 
if(isset($error))
{
    echo '<div class="warning">'.$error.'</div>';
}
?>
<div class="content-inner">
	<form name="frmslider" action="<?php echo site_url('xu-ly-banner.html'); ?>" method="post" enctype="multipart/form-data">
		<?php 
			if(isset($id))
			{
				$this->db->where('id',$id);
				$item=$this->db->get('tblbanner')->row();
				$id=$item->id;            
			}
		?>    
		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />	
		<div class="gray">
		<table width="100%"><tr>
		<td>
			<table class="tab1">
			<tr><td width="200">
				<strong>Tên đơn vị</strong></td>
				<td>
				<input type="text" name="name" value="<?php if(isset($id)) { echo $item->name; }; ?>" />
			</td></tr>
			<tr class="second"><td>
				<strong>File quảng cáo</strong></td>
				<td><select name="type" style="width:100px;">
					<option value="image">Image</option>
					<option value="flash">Flash</option>
				</select>
				<input type="file" name="file" value="" />
				<?php if(isset($id)){?>				
				<input style="margin-top:5px;" type="text" name="file" value="<?php echo $item->file; ?>">	
				<?php
                } 			
				?>				
			</td></tr>
			<tr><td width="200">
				<strong>Đường dẫn liên kết</strong></td>
				<td>
				<input type="text" name="link" value="<?php if(isset($id)) { echo $item->link; } else{ echo 'http://';}; ?>" />
			</td></tr>
            <tr>
                <td width="200">
                    <strong>Tỉnh thành</strong>
                </td>    
                <td>
                    <select name="tinhthanh">
                        <option value="-1">--Chọn tỉnh thành--</option>
                        <?php 
                            if($tinh->num_rows()>0)
                            {
                                foreach($tinh->result() as $itemtinh)
                                {
                                    if($item->tinh==$itemtinh->id)
                                    {
                                    ?>
                                    <option selected="selected" value="<?php echo $itemtinh->id; ?>"><?php echo $itemtinh->tinh; ?></option>
                                    <?php    
                                    }
                                    else
                                    {
                                    ?>
                                    <option value="<?php echo $itemtinh->id; ?>"><?php echo $itemtinh->tinh; ?></option>
                                    <?php    
                                    }                                                                                
                                }
                            }
                        ?>
                    </select>
                </td>
            </tr>
			<tr class="second"><td width="200">
				<strong>Trạng thái</strong></td>
				<td>
				<p class="message_head">									
					<?php 
					 if(isset($id))
					 {							 
					?>
						<input class="news_checkbox" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />Xuất bản				
						<?php         
					 }
						else
						{
						?>
							<input class="news_checkbox" type="checkbox" name="status" checked="checked" value="1" />Xuất bản
						<?php             
					}
					?>			
				</p>
			</td></tr>
			<tr><td width="200">
				<strong>Vị trí quảng cáo</strong></td>
				<td>											
					<select name="vitri" style="width:50px;">
						<?php if(isset($id)){ ?>
						<option value="<?php echo $item->vitri; ?>" selected="selected" ><?php echo strtoupper($item->vitri); ?></option>
						<?php } ?>
						<option value="a1">A1</option>
						<option value="a2">A2</option>						
					</select>
			</td></tr>
			</table>
		</td>
		<td valign="top">
			<table width="100%">
				<tr><td>
					<p class="message_head"><strong>Kích thước file: </strong><br /><cite>Đơn vị pixel, (width x height)</cite></p>					
					<p class="message_head" style="color:red;">A1 (Home): 250 x 302</p>			
					<p class="message_head" style="color:red;">A2 (top): 1220 x 100</p>										
				</td></tr>
			</table>
		</td></tr>
		</table>
		</div>
		<div class="gray">
		<center>
		<?php 
			if(isset($id))
			{
			?>			
				<input class="button" type="submit" name="submit" value="Lưu thay đổi" />						
			<?php    
			}
			else
			{
			?>			
				<input class="button" type="submit" name="submit" value="Nhập tin" />					
			<?php 
			}
		?>
		</center>
		</div>
	</form>
	<div class="clr"></div>
</div>
