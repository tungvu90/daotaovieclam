<?php 
$CI=&get_instance();
$CI->load->model('admin/admin_model');
//$category=$CI->admin_model->gettbl('tblchuyenmuc','')->result();	
?>
<h3 class="header">Thêm chuyên mục sản phẩm</h3>
<?php 
if(isset($error))
{
    echo '<div class="warning">'.$error.'</div>';
}
?>
<div class="content-inner1">
	<form name="frmdanhmuc" action="<?php echo site_url('xu-ly-chuyen-mucsp.html'); ?>" method="post" enctype="multipart/form-data">
		<?php 
			if(isset($id))
			{
				$this->db->where('id',$id);
				$item=$this->db->get('tblchuyenmucsp')->row();
				$id=$item->id;            
			}
		?>    
		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />
		<div class="gray">
		<table class="tab1">
		<tr>
			<td width="150"><strong>Name</strong></td>
			<td><input type="text" name="name" width="250" value="<?php if(isset($id)) {echo $item->name;} ?>" /></td>
		</tr>
		<tr class="second">
			<td><strong>Đường dẫn thân thiện</strong><br /></td>
			<td><input type="text" name="alias" value="<?php if(isset($id)){ echo $item->alias;} ?>" /></td>
		</tr>
		<tr class="second">
			<td><strong>Chuyên mục cha</strong></td>
			<td><?php 
				if(isset($id)){ $CI->admin_model->selectCtrlsp($item->uid,'uid', 'forFormDim');}
				else{
					$CI->admin_model->selectCtrlsp('','uid', 'forFormDim');
				}
			?>		
			<?php 
			if(isset($id))
			{							 
			?>
				<input class="news_checkbox" type="checkbox" name="menu" value="1" <?php if($item->menu==1): ?>checked="checked"<?php endif; ?> />Hiển thị menu				
				<?php         
			}
			else
			{
			?>
				<input class="news_checkbox" type="checkbox" name="menu" value="1" />Hiển thị menu				
			<?php             
			}
			?>	
            <?php 
			if(isset($id))
			{							 
			?>
				<input class="news_checkbox" type="checkbox" name="hover" value="1" <?php if($item->hover==1): ?>checked="checked"<?php endif; ?> />Hover				
				<?php         
			}
			else
			{
			?>
				<input class="news_checkbox" type="checkbox" name="hover" value="1"/>Hover				
			<?php             
			}
			?>	
            <?php 
			if(isset($id))
			{							 
			?>
				<input class="news_checkbox" type="checkbox" name="home" value="1" <?php if($item->home==1): ?>checked="checked"<?php endif; ?> />Home				
				<?php         
			}
			else
			{
			?>
				<input class="news_checkbox" type="checkbox" name="home" value="1"/>Home				
			<?php             
			}
			?>	
			</td>
		</tr>
        <tr>
            <td><strong>Meta Title</strong></td>
            <td><textarea rows="5" cols="70" name="meta_title"><?php if(isset($id)){ echo $item->meta_title;}?></textarea></td>
        </tr>
        <tr>
            <td><strong>Meta Description</strong></td>
            <td><textarea rows="5" cols="70" name="meta_des"><?php if(isset($id)){ echo $item->meta_des;}?></textarea></td>
        </tr>
        <tr>
            <td><strong>Keyword</strong></td>
            <td><textarea rows="5" cols="70" name="keyword"><?php if(isset($id)){ echo $item->keyword;}?></textarea></td>
        </tr>		
		<tr>
			<td><strong>Thứ tự<strong></td>
			<td>​			
				<input style="width:50px" type="text" name="thutu" value="<?php if(isset($id)){ echo $item->thutu;}?>" />			
			</td>
		</tr>
		<tr>
			<td><strong>Trạng thái</strong></td>
			<td>
			<?php 
			 if(isset($id))
			 {							 
			?>
				<input class="news_checkbox" type="checkbox" name="status" value="1" <?php if($item->status==1): ?>checked="checked"<?php endif; ?> />Xuất bản				
				<?php         
			 }
				else
				{
				?>
					<input class="news_checkbox" type="checkbox" name="status" value="1" checked="checked" />Xuất bản
				<?php             
			}
			?></td>
		</tr>
		</table>
		</div>		
		<div class="gray">
		<center>
		<?php 
			if(isset($id))
			{
			?>			
				<input class="button" type="submit" name="submit" value="Lưu thay đổi" />						
			<?php    
			}
			else
			{
			?>			
				<input class="button" type="submit" name="submit" value="Nhập tin" />					
			<?php 
			}
		?>
		</center>
		</div>
	</form>
	<div class="clr"></div>
</div>
