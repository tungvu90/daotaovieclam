<script type="text/javascript">
	$(document).ready(function(){
		$('#frmxoaall').submit(function(){
			if(!$('#request-form input[type="checkbox"]').is(':checked')){
  				alert("Bạn phải chọn ít nhất 1 bản ghi.");
 			 return false;
			}
		});
	});

	function checkall(class_name,obj)
	{
		var items=document.getElementsByClassName(class_name);
		if(obj.checked == true)
		{
			for(i=0;i<items.length;i++)
				items[i].checked=true;
		}
		else
		{
			for(i=0;i<items.length;i++)
				items[i].checked=false;					
				
		}
	}	
</script>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">
          Thông tin liên hệ
      </h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                Thông tin liên hệ
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <form name="frmxoaall" id="frmxoaall" method="post" action="<?php echo site_url('admin/del_lienhe'); ?>">
                <div class="card">
                    <div class="card-header" style="display:table;">
                        <p class="text-end" style="display:table-cell;">
                            <input type="submit" name="submit" value="Xóa" class="btn btn-danger btn-sm text-white" />
                        </p>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" onclick="checkall('checkbox', this)" name="check" class="form-check-input"/></th>
                                    <th>Ngày đăng</th>        
                                    <th>Họ tên</th>
                                    <th>Điện thoại</th>
                                    <th>Email</th>
                                    <th>Trạng thái</th>
                            		<th>#ID</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php 
                                if($query->num_rows() >0)
                                {
                            	?>
                                <?php 
                                    $stt=0;
                            		foreach($query->result() as $item)
                                    {
                            		$stt++;
                                ?>
                            		<tr class="<?php echo $stt%2 ? 'odd' : 'even'; ?>">
                            			<td>
                            			<div id="request-form">
                            				<input type="checkbox" name="checkbox[]" class="checkbox form-check-input" value="<?php echo $item->id; ?>" />
                            			</div>
                            			</td>
                                        <td><?php 
                                        $getlienhe=explode('-',$item->ngaylienhe);
                                        echo $getlienhe[2].'-'.$getlienhe[1].'-'.$getlienhe[0]; ?></td>					
                            			<td><a href="<?php echo site_url('admin/edit_lienhe/'.$item->id.'.html'); ?>"><?php echo $item->hoten; ?></a></td>            						
                            			<td><?php echo $item->dienthoai; ?></td>
                                        <td><?php echo $item->email; ?></td>
                            			<td style="text-align:center;">						
                            			  <?php 
                            			  if($item->status=='1')
                            			  {
                            			  ?>
                            			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tbllienhe','lienhe')"><img src="images/toolbar/tick.png"></a>
                            			  <?php
                            			  }
                            			  else
                            			  {
                            			  ?>
                            			  <a class="status" onclick="check_status(<?php echo $item->id; ?>,'tbllienhe','lienhe')"><img src="images/toolbar/publish_x.png"></a>
                            			  <?php
                            			  }
                            			  ?>			
                            			</td>       
                            			<td><?php echo $item->id; ?></td>
                            		</tr>		
                                <?php 
                                }
                                ?>
                            	<?php 
                            }
                            else
                            {
                            ?>
                            <tr><td></td><tr>
                            <?php    
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer">
                        <div class="pagation">
                        	<?php echo $pagination; ?>
                        </div>
                    </div>
                </div>        
            </form>
        </div>
    </div>
</div>