<?php 
$this->db->where('status',1);
$this->db->where('id',$_SESSION['author']);
$admin=$this->db->get('tbladmin1');	
$admin=$admin->row(); 
?>
?>
<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">Đổi mật khẩu</h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/tbladmin1'); ?>">Administrators</a></li>
            <li class="breadcrumb-item active" aria-current="page">
              Đổi mật khẩu
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header" style="display:table;">
                    <p class="text-end" style="display:table-cell;">
                        <a href="<?php echo site_url('admin/tbladmin1'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                    </p>
                </div>
                <div class="card-body">
            		<input type="hidden" name="id" value="<?php echo $admin->id; ?>" />
            		<div class="row">
                	    <div class="col-12">
                            <div class="form-group row">
                                <label for="taikhoan" class="col-sm-12 control-label col-form-label"><strong>Tài khoản</strong></label>
                                <div class="col-12">
                                    <input type="text" class="form-control" name="taikhoan" value="<?php echo $admin->taikhoan; ?>" disabled/>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group row">
                                <label for="password" class="col-sm-12 control-label col-form-label"><strong>Mật khẩu cũ</strong></label>
                                <div class="col-12">
                                    <input type="password" id="password" class="form-control" name="password" value="" />
                                </div>
                            </div>
                        </div>
                	    <div class="col-12">
                            <div class="form-group row">
                                <label for="password" class="col-sm-12 control-label col-form-label"><strong>Mật khẩu  mới</strong></label>
                                <div class="col-12">
                                    <input type="password" id="password_new" class="form-control" name="password_new" value="" />
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group row">
                                <label for="re_password" class="col-sm-12 control-label col-form-label"><strong>Xác nhận mật khẩu mới</strong></label>
                                <div class="col-12">
                                    <input type="password" id="re_password" class="form-control" name="re_password_new" value="" />
                                </div>
                            </div>
                        </div>
                	</div>
                </div>
                <div class="card-footer">
                    <center>
        				<button class="btn btn-info btn-sm" type="submit" name="submit" id="change_password"/><i class="mdi mdi-content-save"></i> Cập nhật</button>										
                		<a href="<?php echo site_url('admin/tbladmin1'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
            		</center>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
       $('#change_password').on('click',function(){
           var password = $("#password").val();
           var password_new = $("#password_new").val();
           var re_password_new = $("#re_password").val();
           if(password ===''){
               toastr.error('Mật khẩu cũ không để trống','Thông báo');
               $("#password").focus();
               return false;
           }else if(password_new === ''){
               toastr.error('Mật khẩu mới không để trống','Thông báo');
               $("#password_new").focus();
               return false;
           }else if(password_new !== re_password_new){
               toastr.error('Xác nhận mật khẩu mới không giống nhau','Thông báo');
               $("#re_password_new").focus();
               return false;
           }else{
               $.ajax({
                    url : "<?php echo site_url('admin/update_change_password')?>",
                    type: "POST",
                    dataType: "JSON",
                    data:{password: password,password_new: password_new},
                    success: function(data){
                        if(data.status == 200) {
                            toastr.success(data.success,'Thông báo');
                            setTimeout(function() { 
                                window.location.href="<?php echo site_url('admin/tbladmin1'); ?>"
                            }, 2000);
                        }else {
                            toastr.warning(data.error,'Thông báo');
                            $("#password").focus();
                        }
                    }
                }); 
           }
       });
    });
</script>