<?php 
    if(isset($id)){
        $this->db->where('id',$id);
        $sql = $this->db->get('tblalbum_images');
        if($sql->num_rows() > 0) {
            $item=$sql->row();
            ?>
            <textarea name="image_title" class="image_title" data-id="<?php echo $item->id; ?>" placeholder="Nhập tên hình ảnh"><?php echo $item->title; ?></textarea>
            <?php
        }
    }
?>
<script>
    $('.image_title').keyup(function() {
	        var image_title = $(this).val();
	        var image_id = $(this).data('id');
	        $.ajax({
    			url:"<?php echo site_url('admin/do_edit_image_title')?>",
    			type:"POST",
    			data:{id : image_id,title : image_title},
    			success: function(html) {
               	    $(".item_image_" + image_id).html(html);
           		},
               	error: function() {
                    alert("Invalide!");
                }
    		});
	    });
</script>