<div class="page-breadcrumb">
  <div class="row">
    <div class="col-12 d-flex no-block align-items-center">
      <h4 class="page-title">
          <?php echo isset($id) ? 'Sửa text link' : 'Thêm text link' ?>
    </h4>
      <div class="ms-auto text-end">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="<?php echo site_url('admin/link'); ?>">Text link</a></li>
            <li class="breadcrumb-item active" aria-current="page">
                <?php echo isset($id) ? 'Sửa' : 'Thêm mới' ?>
            </li>
          </ol>
        </nav>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <?php 
            if(isset($error)) {
                echo '<div class="warning">'.$error.'</div>';
            }
            ?>
            <form name="frmslider" action="<?php echo site_url('admin/add_link'); ?>" method="post" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header" style="display:table">
                        <p class="text-end" style="display:table-cell;">
                            <a href="<?php echo site_url('admin/link'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                        </p>
                    </div>
                    <div class="card-body">
                        <?php 
                			if(isset($id))
                			{
                				$this->db->where('id',$id);
                				$sqlcheck=$this->db->get('tbllink')->row();
                				$id=$sqlcheck->id;            
                			}
                		?>    
                		<input type="hidden" name="id" value="<?php if(isset($id)) { echo $id; }; ?>" />	
                		<div class="row">
                		    <div class="col-12">
                		        <div class="form-group row">
                		            <label for="title" class="col-sm-12 control-label col-form-label"><strong>Tiêu đề</strong></label>
                		            <div class="col-sm-12">
                		                <input class="form-control" type="text" name="title" value="<?php if(isset($id)) { echo $sqlcheck->title; }; ?>" />    
            		                </div>
            		            </div>
                		    </div>
                		    <div class="col-12">
                		        <div class="form-group row">
                		            <label for="image" class="col-sm-12 control-label col-form-label"><strong>Ảnh đại diện</strong></label>
                		            <div class="col-sm-12">
                		                <?php if(isset($id)){?>
                            			<input type="hidden" name="image" value="<?php echo $sqlcheck->image; ?>">	
                            			<img src="<?php echo $sqlcheck->image; ?>" width="300"><br />
                            			<?php } 			
                            			?>
                            			<input type="file" name="image" value="" />
            		                </div>
            		            </div>
            		        </div>
                		</div>
                		<div class="row">
                		    <div class="col-4">
                		        <div class="form-group row">
                		            <label for="vitri1" class="col-sm-12 control-label col-form-label"><strong>Vị trí 1</strong></label>
                		            <div class="col-sm-12">
                		                <?php 
                    					 if(isset($id))
                    					 {							 
                    					?>
                    						<input class="news_checkbox form-check-input" type="checkbox" name="vitri1" value="1" <?php if($sqlcheck->vitri1==1): ?>checked="checked"<?php endif; ?> />
                    						<?php         
                    					 }
                    						else
                    						{
                    						?>
                    							<input class="news_checkbox form-check-input" type="checkbox" name="vitri1" value="1" />
                    						<?php             
                    					}
                    					?>
                    					<label class="form-check-label mb-0">Hiện thị</label>
            		                </div>
            		            </div>
                		    </div>
                		    <div class="col-4">
                		        <div class="form-group row">
                		            <label for="vitri2" class="col-sm-12 control-label col-form-label"><strong>Vị trí 2</strong></label>
                		            <div class="col-sm-12">
                		                <?php 
                    					 if(isset($id))
                    					 {							 
                    					?>
                    						<input class="news_checkbox form-check-input" type="checkbox" name="vitri2" value="1" <?php if($sqlcheck->vitri2==1): ?>checked="checked"<?php endif; ?> />
                    						<?php         
                    					 }
                    						else
                    						{
                    						?>
                    							<input class="news_checkbox form-check-input" type="checkbox" name="vitri2" value="1" />
                    						<?php             
                    					}
                    					?>
                    					<label class="form-check-label mb-0">Hiện thị</label>
            		                </div>
            		            </div>
                		    </div>
                		    <div class="col-4">
                		        <div class="form-group row">
                		            <label for="vitri3" class="col-sm-12 control-label col-form-label"><strong>Vị trí 3</strong></label>
                		            <div class="col-sm-12">
                		                <?php 
                    					 if(isset($id))
                    					 {							 
                    					?>
                    						<input class="news_checkbox form-check-input" type="checkbox" name="vitri3" value="1" <?php if($sqlcheck->vitri3==1): ?>checked="checked"<?php endif; ?> />
                    						<?php         
                    					 }
                    						else
                    						{
                    						?>
                    							<input class="news_checkbox form-check-input" type="checkbox" name="vitri3" value="1" />
                    						<?php             
                    					}
                    					?>
                    					<label class="form-check-label mb-0">Hiện thị</label>
            		                </div>
            		            </div>
                		    </div>
                		</div>
                		<div class="row">
                		    <div class="col-6">
                		        <div class="form-group row">
                		            <label for="link" class="col-sm-12 control-label col-form-label"><strong>Đường dẫn liên kết</strong></label>
                		            <div class="col-sm-12">
                		                <input class="form-control"type="text" name="link" value="<?php if(isset($id)) { echo $sqlcheck->link; }; ?>" />            
            		                </div>
        		                </div>
                		    </div>
                		    <div class="col-6">
                		        <div class="form-group row">
                		            <label for="thutu" class="col-sm-12 control-label col-form-label"><strong>Thứ tư</strong></label>
                		            <div class="col-sm-12">
                		                <input class="form-control"type="text" name="thutu" value="<?php if(isset($id)) { echo $sqlcheck->thutu; }; ?>" />            
            		                </div>
        		                </div>
                		    </div>
                		</div>
                		<div class="row">
                		    <div class="col-12">
                		        <div class="form-group row">
                		            <label for="status" class="col-sm-12 control-label col-form-label"><strong>Trạng thái</strong></label>
                		            <div class="col-sm-12">
                		               <?php 
                        					 if(isset($id))
                        					 {							 
                        					?>
                        						<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" <?php if($sqlcheck->status==1): ?>checked="checked"<?php endif; ?> />				
                        						<?php         
                        					 }
                        						else
                        						{
                        						?>
                    							<input class="news_checkbox form-check-input" type="checkbox" name="status" value="1" checked="checked" />
                        						<?php             
                        					}
                        					?>
                        					<label class="form-check-label mb-0">Xuất bản</label
            		                </div>
        		                </div>
                		    </div>
                		</div>
                    </div>
                    <div class="card-footer">
                        <center>
                    		<?php 
                    			if(isset($id)){
                    			?>			
                				<button class="btn btn-info btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Lưu thay đổi</button>						
                    			<?php    
                    			}
                    			else {
                    			?>			
                    			<button class="btn btn-info text-white btn-sm" type="submit" name="submit"/><i class="mdi mdi-content-save"></i> Nhập tin</button>
                    			<?php 
                    			}
                    		?>
                    		<a href="<?php echo site_url('admin/link'); ?>" class="btn btn-success text-white btn-sm"><i class="mdi mdi-subdirectory-arrow-left"></i> Quay lại</a>
                		</center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>